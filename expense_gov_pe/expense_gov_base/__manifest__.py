# -*- coding: utf-8 -*-


{
    "name": "Matriz de rendicion de gastos ",
    "version": "1.0",
    "category": "Localization",
    "description": u"""
    Matriz para generar la rendicion de gastos
    """,
    "depends": [
        "hr",
        "hr_expense",
        "l10n_toponyms_pe",
    ],
    "init_xml": [],
    "data": [
    ],
    "demo_xml": [],
    "update_xml": [
        'views/product_views.xml',
        'views/expense_gov_pe.xml',
        'security/ir.model.access.csv',
    ],
    'installable': True,
    'active': False,
}
