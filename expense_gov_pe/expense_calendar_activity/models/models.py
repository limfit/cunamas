# coding: utf-8
from odoo import models, fields, api


class Activity(models.Model):
    _name = "pncm.activity"

    name = fields.Char(string="Nombre")


class Calendarprogramming(models.Model):
    _inherit = 'calendar.event'

    is_activity = fields.Boolean(string=u'Es una actividad')
    activity = fields.Many2one(
        'pncm.activity',
        string=u'Tipo Activdad'
    )
    calendar_line = fields.One2many(
        'calendar.event.line',
        'calendar_id',
        string=u'Lugares a visitar'
    )

    def generated_expensive(self):

        return True


class CalendarEventLine(models.Model):
    _name = 'calendar.event.line'

    calendar_id = fields.Many2one(
        'calendar.event',
        string=u'Calendario'
    )
    # Destino
    dest_state_id = fields.Many2one(
        'res.country.state', 'Departamento Destino',
        required=True,
        domain=[('country_id.code', '=', 'PE'),
                ('state_id', '=', False),
                ('province_id', '=', False)
                ]
    )
    dest_province_id = fields.Many2one(
        'res.country.state', 'Provincia Destino',
        domain="[('state_id', '=', dest_state_id),"
               " ('province_id', '=', False)]")
    dest_district_id = fields.Many2one(
        'res.country.state', 'Distrito Destino',
        domain="[('state_id', '=', dest_state_id), "
               "('province_id', '=', dest_province_id),"
               "('district_id', '=', False)]")
    dest_poblation_id = fields.Many2one(
        'res.country.state', 'Centro Poblado Destino',
        domain="[('state_id', '=', dest_state_id), "
               "('province_id', '=', dest_province_id), "
               "('district_id', '=', dest_district_id)]")

