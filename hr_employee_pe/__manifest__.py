# -*- coding: utf-8 -*-
# flake8: noqa
{
    'name': "Gestión de RRHH Peruano - Estado",
    'summary': """
        gestion_rrhh""",
    'description': """
    Gestión de Recursos Humanos
    """,
    'author': "CUNAMAS",
    'website': "http://www.cunamas.gob.pe",
    'category': 'Locations',
    'version': '0.1',
    'depends': ['hr', 'hr_contract', 'l10n_toponyms_pe'],
    'data': [
        # 'security/ir.model.access.csv',
        'security/hr_security.xml',
        'views/hr_views.xml',
    ],
    'demo': [
    ],
    'application': True,
}
