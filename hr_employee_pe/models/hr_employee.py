# -*- coding: utf-8 -*-
import logging
from dateutil.relativedelta import relativedelta
from openerp import fields, models, api
from openerp.exceptions import ValidationError
from datetime import datetime


_logger = logging.getLogger(__name__)

# ESTADO_CIVIL = [
#     ('Soltero', 'Solter@'),
#     ('Casado', 'Casad@'),
#     ('Viudo', 'Viud@'),
#     ('Divorciado', 'Divorciad@'),
#     ('Separado', 'Separad@'),
#     ('Conviviente', 'Conviviente@'),
# ]

ESTADO_CIVIL_NEW = [
    ('single', 'Soltero (a)'),
    ('married', 'Casado (a)'),
    ('widower', 'Viudo (a)'),
    ('divorced', 'Divorciado (a)'),
    ('cohabiting', 'Conviviente')
]


_opttipodocumento = [
    ('dni', 'DNI'),
    ('ext', u'Carnet de Extranjería'),
    ('pas', 'Pasaporte')
]


class EmployeeEducationLevel(models.Model):

    _name = 'hr.employee.education.level'

    name = fields.Char(u'Nivel Educación', size=100, required=True)
    code = fields.Char(u'Código', size=5, required=True)


class HrEmployee(models.Model):
    _inherit = 'hr.employee'
    _order = 'name'

    _upper_list_attrs = [
        'mobile_phone', 'work_location', 'work_phone', 'vehicle',
        'name', 'ape_paterno', 'ape_materno', 'nombres',
        'legal_street', 'street', 'street_reference', 'cuspp'
    ]

    def _default_country_id(self):
        return self.env['res.country'].search(
            [('code', '=', 'PE')],
            limit=1
        ).id

    # ############################################################
    # Identificación e Información Adicional
    # ############################################################
    firstname = fields.Char(string='Nombres')
    lastname = fields.Char(string='Apellido paterno')
    secondname = fields.Char(string='Apellido materno')
    birthdate = fields.Date(string='Fecha de nacimiento')
    gender = fields.Selection(
        selection=[
            ('m', 'Masculino'),
            ('f', 'Femenino')],
        string='Sexo',
        default='m'
    )
    type_document = fields.Selection(
        selection=_opttipodocumento,
        string="Tipo de Documento"
    )
    document_number = fields.Char(
        string=u'Documento de identidad'
    )
    # marital_status = fields.Selection(
    #     ESTADO_CIVIL,
    #     string='Estado Civil'
    # )
    marital = fields.Selection(
        ESTADO_CIVIL_NEW,
        string='Estado Civil'
    )
    age = fields.Char(
        string='Años',
        compute='_compute_age',
    )

    ruc = fields.Char('RUC', size=11)
    country_id = fields.Many2one(
        'res.country',
        default=_default_country_id
    )
    country_code = fields.Char(
        'Código de País',
        related='country_id.code'
    )

    # ############################################################
    # Información de contacto
    # ############################################################
    telefono_fijo = fields.Char('Teléfono Fijo')
    telefono_celular = fields.Char('Celular')
    telefono_celular2 = fields.Char('Celular2')
    email_personal = fields.Char('Email Personal')
    email_personal2 = fields.Char('Email Personal2')

    # ############################################################
    # Nacimiento
    # ############################################################
    birth_country_id = fields.Many2one(
        'res.country',
        'Nacimiento País',
        default=_default_country_id
    )
    birth_country_code = fields.Char(
        'Nacimiento Código de País',
        related='birth_country_id.code'
    )

    birth_zip = fields.Char(
        'Ubigeo del Lugar de Nacimiento',
        oldname='ubigeo'
    )
    birth_departament_id = fields.Many2one(
        'res.country.state', 'Departamento',
        domain="[('country_id', '=', country_id), ('state_id', '=', False), "
               "('province_id', '=', False)]")
    birth_province_id = fields.Many2one(
        'res.country.state', 'Provincia',
        domain="[('state_id', '=', birth_departament_id), "
               "('province_id', '=', False)]")
    birth_district_id = fields.Many2one(
        'res.country.state', 'Distrito',
        domain="[('state_id', '=', birth_departament_id),"
               " ('province_id', '=', birth_province_id)]")
    name = fields.Char(
        'Apellidos y Nombres',
        compute='_compute_name',
        store=True
    )
    # Datos del Domiclio Legal(Reniec)
    legal_street = fields.Char('Domicilio(Reniec)')
    legal_street2 = fields.Char('Ubicación(Reniec)')
    legal_reference = fields.Char('Referencia del Domicilio(Reniec)')
    legal_zip = fields.Char('Ubigeo del Domicilio(Reniec)', oldname='ubigeo')
    legal_departament_id = fields.Many2one(
        'res.country.state', 'Departamento del Domicilio(Reniec)',
        domain="[('country_id.code', '=', 'PE'), "
               "('state_id', '=', False), "
               "('province_id', '=', False),"
               "]"
    )
    legal_province_id = fields.Many2one(
        'res.country.state',
        'Provincia del Domicilio(Reniec)',
        domain="[('state_id', '=', legal_departament_id), "
               "('province_id', '=', False)]"
    )
    legal_district_id = fields.Many2one(
        'res.country.state', 'Distrito del Domicilio(Reniec)',
        domain="[('state_id', '=', legal_departament_id), "
               "('province_id', '=', legal_province_id),"
               "('district_id', '=', False)]"
    )
    work_phone_extension = fields.Char('Anexo')
    # Datos del Domiclio
    street = fields.Char('Domicilio', oldname='domicilio')
    street2 = fields.Char('Ubicación')
    street_reference = fields.Char('Referencia del Domicilio')
    street_zip = fields.Char('Ubigeo del Domicilio', oldname='ubigeo')
    street_departament_id = fields.Many2one(
        'res.country.state', 'Departamento del Domicilio',
        domain="[('country_id.code', '=', 'PE'), "
               "('state_id', '=', False), ('province_id', '=', False)]")
    street_province_id = fields.Many2one(
        'res.country.state', 'Provincia del Domicilio',
        domain="[('state_id', '=', street_departament_id), "
               "('province_id', '=', False)]"
    )
    street_district_id = fields.Many2one(
        'res.country.state', 'Distrito del Domicilio',
        domain="[('state_id', '=', street_departament_id),"
               " ('province_id', '=', street_province_id),"
               "('district_id', '=', False)]"
    )
    stret_interior = fields.Char('Domic. Dpto/Piso/Int.')
    stret_num_interior = fields.Char('Domic. No. Dpto/Piso/Int.')
    stret_via_tipo = fields.Char('Domic. Tipo de Via')
    stret_via_descrip = fields.Char('Domic. Tipo Descrip')
    stret_detalle = fields.Char('Domic. Detalle')
    stret_detalle_descrip = fields.Char('Domic. Desc. Detalle')
    stret_lote = fields.Char('Domic. Lote')

    # Otros
    state = fields.Selection([
        ('sinenviar', 'Sin Enviar'),
        ('enviado', 'Enviado'),
        ('observado', 'Observado'),
        ('aprobado', 'Aprobado'),
    ],
        'Estado Verif. Información',
        default='sinenviar', track_visibility='onchange'
    )

    study_ids = fields.One2many(
        comodel_name='hr.employee.study',
        inverse_name='employee_id',
        string='Estudios'
    )
    training_ids = fields.One2many(
        comodel_name='hr.employee.training',
        inverse_name='employee_id',
        string='Capacitaciones'
    )
    work_experience_ids = fields.One2many(
        comodel_name='hr.employee.work.experience',
        inverse_name='employee_id',
        string='Experiencia laboral'
    )

    @api.multi
    def button_enviar(self):
        for record in self:
            if record.state not in ['sinenviar', 'observado']:
                raise ValidationError('Error, ya se envió esta información')
        self.write(dict(state='enviado'))

    @api.multi
    def button_observar(self):
        for record in self:
            if record.state != 'enviado':
                raise ValidationError(
                    'Error, la información no ha sido enviada'
                )
        self.write(dict(state='observado'))

    @api.multi
    def button_aprobar(self):
        for record in self:
            if record.state != 'enviado':
                raise ValidationError(
                    'Error, la información no ha sido enviada'
                )
        self.write(dict(state='aprobado'))

    _sql_constraints = [
        ('name_unique',
         'UNIQUE(name)',
         "Ya existe el nombre del Empleado"),
        ('identification_unique',
         'UNIQUE(type_document, document_number)',
         "Ya existe el Empleado"),
    ]

    @api.depends('lastname', 'firstname', 'secondname')
    def _compute_name(self):
        for record in self:
            if record.firstname and record.lastname and record.secondname:
                record.name = '%s %s %s' % (
                    record.lastname,
                    record.secondname,
                    record.firstname
                )

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            name = record.name or ('id. %d' % record.id) or 0
            if record.lastname and record.secondname and record.firstname:
                name = "[%s] %s %s, %s" % (
                    record.document_number,
                    record.lastname,
                    record.secondname,
                    record.firstname
                )
            elif record.lastname and record.firstname:
                name = "[%s] %s, %s" % (
                    record.document_number,
                    record.lastname,
                    record.firstname
                )
            result.append((record.id, name))
        return result

    @api.multi
    def _compute_age(self):
        """
        Age computed depending based on the birth date in the
        """
        now = datetime.now()
        for record in self:
            if record.birthdate:
                birthdate = fields.Datetime.from_string(
                    record.birthdate,
                )
                delta = relativedelta(now, birthdate)
                is_deceased = ''
                years_months_days = '%d%s %d%s %d%s%s' % (
                    delta.years, 'Año(s)', delta.months, 'mes(es)',
                    delta.days, 'dia(s)', is_deceased
                )
                years = delta.years
            else:
                years_months_days = 'No FdN'
                years = False
            record.age = years_months_days
            if years:
                record.age_years = years

    @api.onchange('country_id')
    def _onchange_country_id(self):
        return {
            'value': {
                'birth_departament_id': None, 'birth_zip': None,
            },
        }

    @api.onchange('birth_departament_id')
    def _onchange_birth_departament_id(self):
        if not self.birth_zip:
            return {'value': {'birth_province_id': None}}

    @api.onchange('birth_province_id')
    def _onchange_birth_province_id(self):
        if not self.birth_zip:
            return {'value': {'birth_district_id': None}}

    @api.onchange('legal_departament_id')
    def _onchange_legal_departament_id(self):
        if not self.legal_zip:
            return {'value': {'legal_province_id': None}}

    @api.onchange('legal_province_id')
    def _onchange_legal_province_id(self):
        if not self.legal_zip:
            return {'value': {'legal_district_id': None}}

    @api.onchange('street_departament_id')
    def _onchange_street_departament_id(self):
        if not self.street_zip:
            return {'value': {'street_province_id': None}}

    @api.onchange('street_province_id')
    def _onchange_street_province_id(self):
        if not self.street_zip:
            return {'value': {'street_district_id': None}}

    @api.model
    def create(self, values):
        context = self._context.copy()
        context.update({'default_name': ''})
        employee = super(HrEmployee, self.with_context(context)).create(values)
        return employee


class EmployeeStudy(models.Model):
    _name = 'hr.employee.study'
    _description = 'Estudios'

    career = fields.Char(string=u'Título o grado', required=True)
    specialty = fields.Char(string='Especialidad')
    date = fields.Date(
        string=u'Fecha de expedición del título',
        default=fields.Date().today()
    )
    university = fields.Char(string='Universidad')
    country_id = fields.Many2one(
        comodel_name='res.country',
        string=u'País'
    )
    state_id = fields.Many2one(
        comodel_name='res.country.state',
        domain="[('country_id', '=', country_id)]",
        string='Cuidad'
    )
    file = fields.Binary(
        string='Archivo',
        help=u'Extensión doc, docx, xls, xlsx, pdf, jpg, png'
    )
    filename = fields.Char(string='Nombre del archivo')
    is_career = fields.Boolean(string=u'No tiene título')
    employee_id = fields.Many2one(
        comodel_name='hr.employee',
        string='Emplado',
        required=True
    )
    level_study = fields.Many2one(
        'hr.employee.education.level',
        string='Nivel de Estudios'
    )

    @api.model
    def get_record(self, id):
        domain = [
            ('user_id', '=', self.env.uid)
        ] if not id else [('id', '=', id)]
        return self.search(domain).mapped(lambda x: {
            'id': x.id,
            'career': x.career or '',
            'specialty': x.specialty or '',
            'date':  datetime.strptime(
                x.date, "%Y-%m-%d"
            ).date().strftime("%d/%m/%Y") if x.date else '',
            'university': x.university or '',
            'country': x.country_id.name or '',
            'state': x.state_id.name or '',
            'is_career': x.is_career
        })

    @api.model
    def delete_record(self, id):
        self.browse(id).unlink()
        return True


class EmployeeTraining(models.Model):
    _name = 'hr.employee.training'
    _description = 'Capacitacion'

    name = fields.Char(
        string='Capacitación',
        required=True
    )
    date_start = fields.Date(
        string='Fecha de inicio',
        default=fields.Date().today()
    )
    date_end = fields.Date(
        string=u'Fecha de término'
    )
    institution = fields.Char(
        string=u'Institución'
    )
    country_id = fields.Many2one(
        comodel_name='res.country',
        string=u'País'
    )
    state_id = fields.Many2one(
        comodel_name='res.country.state',
        domain="[('country_id', '=', country_id)]",
        string='Ciudad'
    )
    time = fields.Float(
        string='Horas lectivas'
    )
    file = fields.Binary(
        string='Archivo',
        help=u'Extensión doc, docx, xls, xlsx, pdf, jpg, png'
    )
    filename = fields.Char(string='Nombre del archivo')
    employee_id = fields.Many2one(
        comodel_name='hr.employee',
        string='Emplado',
        required=True
    )

    @api.model
    def get_record(self, id):
        def date_format(date):
            return datetime.strptime(
                date, "%Y-%m-%d").\
                date().\
                strftime("%d/%m/%Y") if date else ''

        domain = [
            ('user_id', '=', self.env.uid)
        ] if not id else [('id', '=', id)]
        return self.search(domain).mapped(lambda x: {
            'id': x.id,
            'name': x.name or '',
            'date_start':  date_format(x.date_start),
            'date_end':  date_format(x.date_end),
            'institution': x.institution or '',
            'country': x.country_id.name or '',
            'state': x.state_id.name or '',
            'time': x.time or ''
        })

    @api.model
    def delete_record(self, id):
        self.browse(id).unlink()
        return True


class EmployeeExperience(models.Model):
    _name = 'hr.employee.work.experience'
    _description = 'Experiencia laboral'

    name = fields.Char(
        string='Nombre de la entidad o  empresa',
        required=True
    )
    date_start = fields.Date(
        string='Fecha de inicio',
        default=fields.Date().today()
    )
    date_end = fields.Date(
        string=u'Fecha de término',
        default=fields.Date().today()
    )
    charge = fields.Char(
        string=u'Cargo desempeñado'
    )
    description = fields.Text(
        string=u'Descripción del trabajo realizado'
    )
    filename = fields.Char(
        string='Nombre del archivo'
    )
    file = fields.Binary(
        string='Archivo',
        help=u'Extensión doc, docx, xls, xlsx, pdf, jpg, png'
    )
    employee_id = fields.Many2one(
        comodel_name='hr.employee',
        string='Emplado',
        required=True
    )

    @api.model
    def get_record(self, id):
        def date_format(date):
            return datetime.strptime(date, "%Y-%m-%d").\
                date().strftime("%d/%m/%Y") if date else ''

        domain = [
            ('user_id', '=', self.env.uid)
        ] if not id else [('id', '=', id)]
        return self.search(domain).mapped(lambda x: {
            'id': x.id,
            'name': x.name or '',
            'date_start':  date_format(x.date_start),
            'date_end':  date_format(x.date_end),
            'charge': x.charge or '',
            'description': x.description or '',
        })

    @api.model
    def delete_record(self, id):
        self.browse(id).unlink()
        return True
