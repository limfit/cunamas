# -*- coding: utf-8 -*-

from collections import defaultdict
from datetime import datetime, timedelta

from odoo import api, fields, models
from odoo.exceptions import UserError, ValidationError


class GenPurchaseOrder(models.TransientModel):

    _name = 'gen.purchase.order'

    tipo_alimento = fields.Selection(
        string='Tipo de alimento',
        selection=(
            ('P', 'Perecible'),
            ('NP', 'No Perecible'),
        ),
        default='P',
        required=True,
    )

    fecha_inicio = fields.Date(
        'Fecha de inicio',
        required=True,
        default=fields.Date.today,
    )

    fecha_fin = fields.Date(
        'Fecha de fin',
        required=True,
        default=lambda x: datetime.today() + timedelta(days=7),
    )

    servicio_alimentario_id = fields.Many2one(
        'pncm.infra.local',
        string='Servicio alimentario',
        domain=[
            ('state', '=', 'activacion'),
            ('tipo_local', 'in', ('sa', 'ambos'))
        ],
        required=True,
    )

    partner_id = fields.Many2one(
        'res.partner',
        related='servicio_alimentario_id.partner_id',
        required=True,
    )

    @api.one
    @api.constrains('fecha_inicio', 'fecha_fin')
    def _check_periodo(self):
        if self.fecha_inicio and self.fecha_fin:
            inicio = fields.Date.from_string(self.fecha_inicio)
            fin = fields.Date.from_string(self.fecha_fin)
            if inicio > fin:
                raise UserError('La fecha de inicio no puede ser mayor a la fecha de fin')  # NOQA

    @api.constrains
    def _check_servicio_alimentario_id(self):
        partner = self.servicio_alimentario_id.partner_id
        if not partner:
            message = 'El servicio alimentario no tiene un partner asociado'
            raise ValidationError(message)
        if not self.env['stock.warehouse'].search([
            ('partner_id', '=', self.partner_id.id),
        ]):
            message = 'El servicio alimentario no tiene un almacén asociado'
            raise ValidationError(message)

    @api.multi
    def action_gen_orders(self):
        self.ensure_one()
        filters = [
            ('order_id.date_order', '<=', self.fecha_fin),
            ('order_id.date_order', '>=', self.fecha_inicio),
            ('order_id.state', '=', 'draft'),
            ('order_id.tipo_generacion', '=', 'default'),
            ('order_id.picking_type_id.warehouse_id.partner_id', '=', self.partner_id.id),  # NOQA
            ('product_id.tipo_alimento', '=', self.tipo_alimento),
            ('cunamas_procesado', '=', False),
        ]
        suppliers = defaultdict(dict)

        purchase_order_ids = []
        ids = []

        for line in self.env['purchase.order.line'].search(filters):
            key = (line.order_id.partner_id.id, line.order_id.picking_type_id.id)  # NOQA
            ids.append(line.id)
            product_key = line.product_id.id
            if product_key not in suppliers[key]:
                date_planned = fields.Datetime.from_string(line.date_planned)
                # hack para evitar ver una fecha "erronea" en las vistas tree
                # y form de purchase.order
                date_planned = date_planned.replace(hour=12)
                suppliers[key][product_key] = {
                    'name': line.name,
                    'product_uom': line.product_uom.id,
                    'product_id': product_key,
                    'product_qty': line.product_qty,
                    'price_unit': line.price_unit,
                    'date_planned': fields.Datetime.to_string(date_planned),
                }
            else:
                suppliers[key][product_key]['product_qty'] += line.product_qty

        for (partner_id, picking_type_id), data in suppliers.items():
            if not data:
                continue
            purchase_order_data = {
                'partner_id': partner_id,
                'order_line': [(0, False, x) for x in data.values()],
                'tipo_generacion': 'grouped',
                'picking_type_id': picking_type_id,
            }
            order = self.env['purchase.order'].create(purchase_order_data)
            purchase_order_ids.append(order.id)

        # Marca las lienas ya procesadas para no ser consideradas otra vez
        self.env['purchase.order.line'].search([('id', 'in', ids)]).write({
            'cunamas_procesado': True,
        })

        # Redirect to a tree view with the generated purchase orders

        view = self.env.ref('purchase.purchase_order_tree')
        return {
            'name': 'Pedidos de compra generados',
            'type': 'ir.actions.act_window',
            'view_mode': 'tree',
            'res_model': 'purchase.order',
            'view_id': view.id,
            'domain': [('id', 'in', purchase_order_ids)],
        }
