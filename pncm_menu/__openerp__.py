# -*- coding: utf-8 -*-
##############################################################################
#
#   pcnm_menu, Módulo de menús, recetas y raciones con valor nutricional para el Programa Nacional Cuna Más
#   Copyright (C) 2018 PNCM Programa Nacional Cuna Más (<http://www.cunamas.gob.pe>)
# 	Unauthorized copying of this file, via any medium is strictly prohibited
#	Proprietary and confidential
#	Written by Miguel Angel Vásquez Jiménez <mavasquez@cunamas.gob.pe>, Junio 2018
#
##############################################################################
{
    "name": "Modulo de Menú - PNCM",
    "version": "1.0",
    "author": "Cuna Más",
    "website": "http://www.cunamas.gob.pe/",
    "category": "Core",
    "description": u"""Módulo de menús, recetas y 
	raciones con valor nutricional para el Programa Nacional Cuna Más
    """,
    "depends": [
        'hr_department_ut',
        'pncm_infra',
        'pncm_comite_gestion',
        'base_cuna',
        'base',
        'product',
        'stock',
        'purchase_mrp',
        'sale_expense',
    ],
    "init_xml": [],
    "data": [
        'security/security.xml',
        'security/ir.model.access.csv',
        'security/unidad_territorial_rule.xml',
        'data/data.xml',
        'views/admin/menus.xml',
        'views/admin/product_views.xml',
        'views/admin/mrp_bom_views.xml',
        'views/admin/stock_views.xml',
        'views/admin/mrp_views.xml',
        'views/admin/elaboracion_menu_views.xml',
        'views/admin/aporte_nutricional_views.xml',
        'views/admin/beneficiario_alimento_views.xml',
        'views/admin/res_partner_views.xml',
        'views/admin/purchase_order_views.xml',
        'views/admin/costo_menu_views.xml',
        'views/admin/regla_proveedor_views.xml',
        'views/admin/precio_alimento_views.xml',
        'views/admin/stock_inventory_views.xml',
        'views/admin/product_uom_views.xml',
        'views/admin/pncm_infra_local_views.xml',
        'wizard/gen_purchase_order_views.xml',
        'report/requerimiento_alimento_report_views.xml',
        'report/elaboracion_menu_report_views.xml',
        'report/alimento_requerido_report_views.xml',
    ],
    "demo_xml": [],
    'installable': True,
    'active': False
}
