# -*- coding: utf-8 -*-

from calendar import monthrange
from collections import defaultdict
from datetime import datetime

from odoo import api, fields, models

from .servicio_alimentario_base_report import ServicioAlimentarioBaseReport


class AlimentoRequeridoReport(ServicioAlimentarioBaseReport, models.TransientModel):  # NOQA

    _name = 'alimento.requerido.report'

    total = fields.Float(
        string='Total',
        compute='_compute_total',
    )

    line_ids = fields.One2many(
        'alimento.requerido.report.line',
        'parent_id',
    )

    perecible_line_ids = fields.One2many(
        'alimento.requerido.report.line',
        'parent_id',
        compute='_compute_perecible_line_ids',
    )

    no_perecible_line_ids = fields.One2many(
        'alimento.requerido.report.line',
        'parent_id',
        compute='_compute_no_perecible_line_ids',
    )

    @api.depends('line_ids')
    def _compute_total(self):
        self.total = sum(self.line_ids.mapped(lambda x: x.qty))

    @api.depends('line_ids')
    def _compute_perecible_line_ids(self):
        self.perecible_line_ids = self.line_ids.filtered(lambda x: x.product_id.tipo_alimento == 'P')  # NOQA

    @api.depends('line_ids')
    def _compute_no_perecible_line_ids(self):
        self.no_perecible_line_ids = self.line_ids.filtered(lambda x: x.product_id.tipo_alimento == 'NP')  # NOQA

    @api.multi
    def action_gen_report(self):
        self.ensure_one()

        _, last_day = monthrange(self.year, self.month)
        tmp = datetime(self.year, self.month, 1)
        fecha_inicio = fields.Date.to_string(tmp)
        fecha_fin = fields.Date.to_string(tmp.replace(day=last_day))

        data = defaultdict(float)

        beneficiario_alimento_lines = self.env['beneficiario.alimento.line'].search([  # NOQA
            ('beneficiario_alimento_id.servicio_alimentario_id', '=', self.servicio_alimentario_id.id),  # NOQA
            ('beneficiario_alimento_id.month', '=', self.month),
            ('beneficiario_alimento_id.year', '=', self.year),
            ('beneficiario_alimento_id.state', '=', 'requerimiento'),
        ])

        for line in beneficiario_alimento_lines:
            elaboracion_menu_lines = self.env['elaboracion.menu.line'].search([
                ('elaboracion_menu_id.unidad_territorial_id', '=', self.unidad_territorial_id.id),  # NOQA
                ('elaboracion_menu_id.state', '=', 'approved'),
                ('elaboracion_menu_id.fecha', '<=', fecha_fin),
                ('elaboracion_menu_id.fecha', '>=', fecha_inicio),
                ('elaboracion_menu_id.attribute_value_id', '=', line.attribute_value_id.id),  # NOQA
            ])
            for eline in elaboracion_menu_lines:
                bom_lines = self.env['mrp.bom.line'].search([
                    ('bom_id.product_id', '=', eline.product_id.id),
                    ('bom_id.unidad_territorial_id', '=', self.unidad_territorial_id.id),  # NOQA
                ])

                for bline in bom_lines:
                    key = bline.product_id.id
                    data[key] += bline.product_qty * line.cantidad

        to_create = []

        for key, value in data.items():
            to_create.append({
                'product_id': key,
                'qty': value,
            })

        self.line_ids.unlink()
        self.write({
            'line_ids': [(0, False, x) for x in to_create],
        })

        return {
            'name': 'Alimento requerido',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'alimento.requerido.report',
            'res_id': self.id,
            'target': 'new',
        }

    @api.multi
    def action_print(self):
        self.ensure_one()
        report_id = 'pncm_menu.alimento_requerido_report_template'
        return self.env['report'].get_action(self, report_id)


class AlimentoRequeridoReportLine(models.TransientModel):

    _name = 'alimento.requerido.report.line'

    parent_id = fields.Many2one(
        'alimento.requerido.report',
        ondelete='cascade',
    )

    qty = fields.Float('Cantidad')

    product_id = fields.Many2one(
        'product.product',
        string='Producto',
    )

    uom_id = fields.Many2one(
        'product.uom',
        related='product_id.uom_id',
        string='Unidad',
    )
