# -*- coding: utf-8 -*-

import base64
from calendar import monthrange
from datetime import datetime
from StringIO import StringIO

import xlsxwriter
from dateutil.rrule import DAILY, rrule
from odoo import api, fields, models

from .. import utils


class ExcelReport(object):

    def __init__(self, env, unidad_territorial, inicio, fin):
        self.env = env
        self.unidad_territorial = unidad_territorial
        self.inicio = inicio
        self.fin = fin
        self.output = None

    def _prepare_data(self):
        grupos = {}
        tipos_comida = {}

        for line in self.env['elaboracion.menu.line'].search([
            ('elaboracion_menu_id.unidad_territorial_id', '=', self.unidad_territorial.id),  # NOQA
            ('elaboracion_menu_id.fecha', '<=', self.fin),
            ('elaboracion_menu_id.fecha', '>=', self.inicio),
            ('elaboracion_menu_id.state', '=', 'approved'),
        ]):
            tipo_comida = line.product_id.product_tmpl_id.tipo_comida_id
            tipos_comida.setdefault(tipo_comida.id, {
                'id': tipo_comida.id,
                'name': tipo_comida.name,
                'horario': tipo_comida.horario,
                'order': tipo_comida.order,
            })

            grupo = line.elaboracion_menu_id.attribute_value_id
            grupos.setdefault(grupo.id, {
                'id': grupo.id,
                'name': grupo.name,
            })

        tipos_comida = sorted(tipos_comida.values(), key=lambda x: x['order'])
        grupos = sorted(grupos.values(), key=lambda x: x['id'])

        return list(tipos_comida), list(grupos)

    def _process(self):
        output = StringIO()
        wb = xlsxwriter.Workbook(output)
        sheet = wb.add_worksheet('sheet1')

        title_format = wb.add_format({
            'align': 'center',
            'valign': 'vcenter',
            'font_size': 28,
            'bold': True,
        })

        format_ = wb.add_format({
            'border': 1,
            'align': 'center',
            'valign': 'vcenter',
            'text_wrap': True,
        })

        days = list(rrule(DAILY, dtstart=self.inicio, until=self.fin))

        tipos_comida, grupos = self._prepare_data()

        title = self.unidad_territorial.name.upper()
        sheet.merge_range(4, 0, 4, 3 + (len(days) * len(grupos)), title, title_format)  # NOQA
        sheet.write_string('A7', 'HORA', format_)
        sheet.write_string('B7', 'TIPO DE COMIDA', format_)
        sheet.write_string('C7', 'GRUPO', format_)

        grupos_len = len(grupos)

        row, col = 7, 3

        for day in days:
            row = 7
            for tipo_comida in tipos_comida:
                for grupo in grupos:
                    rec = self.env['elaboracion.menu.line'].search([
                        ('elaboracion_menu_id.unidad_territorial_id', '=', self.unidad_territorial.id),  # NOQA
                        ('elaboracion_menu_id.state', '=', 'approved'),
                        ('elaboracion_menu_id.fecha', '=', day),
                        ('elaboracion_menu_id.attribute_value_id', '=', grupo['id']),  # NOQA
                        ('product_id.tipo_comida_id', '=', tipo_comida['id']),
                    ], limit=1)

                    value = '✓' if rec else ''
                    sheet.merge_range(row, col, row, col + 1, value, format_)
                    row += 1
            col += 2

        x = 3
        for day in days:
            text = day.strftime('%d/%m/%Y')
            sheet.merge_range(6, x, 6, x + grupos_len - 1, text, format_)

            x += grupos_len

        for k in range(3, (len(days) + 1) * grupos_len, grupos_len):
            for j, grupo in enumerate(grupos):
                sheet.write_string(row, k + j, grupo['name'], format_)

        i = 7

        for index, tipo_comida in enumerate(tipos_comida):
            tmp = i + grupos_len - 1
            sheet.merge_range(i, 0, tmp, 0, tipo_comida['horario'], format_)
            sheet.merge_range(i, 1, tmp, 1, tipo_comida['name'], format_)

            for j, grupo in enumerate(grupos):
                sheet.write_string(i + j, 2, grupo['name'], format_)

            i += grupos_len

        # se agrega 1 debido a que que la posición estaba basada en 0
        i += 1
        sheet.merge_range('A{}:A{}'.format(i, i + 7), 'Valores', format_)
        sheet.write_string('B{}'.format(i), 'Aporte', format_)
        sheet.write_string('C{}'.format(i), 'Edad (meses)', format_)

        values = (
            'Costo S/', 'Calorias (kcal)', 'Proteinas (g)', 'Hierro (mg)',
            'Calcio (mg)', 'Vitamina C (mg)', 'Vitamina A (µg)',
        )

        for index, value in enumerate(values):
            row = i + index + 1
            sheet.merge_range('B{0}:C{0}'.format(row), value, format_)

        row, col = i, 3

        attrs = (
            'costo_total', 'energia', 'proteinas', 'hierro',
            'calcio', 'vitamina_c', 'vitamina_a',
        )

        for day in days:
            for grupo in grupos:
                rec = self.env['elaboracion.menu'].search([
                    ('unidad_territorial_id', '=', self.unidad_territorial.id),
                    ('state', '=', 'approved'),
                    ('fecha', '=', day),
                    ('attribute_value_id', '=', grupo['id']),
                ], limit=1)

                row = i
                for attr in attrs:
                    value = getattr(rec, attr, '')
                    sheet.write(row, col, value, format_)
                    row += 1
                col += 1

        wb.close()
        output.seek(0)
        self.output = output

    def render(self):
        self._process()
        return base64.encodestring(self.output.read())


class ElaboracionMenuReport(models.TransientModel):

    _name = 'elaboracion.menu.report'

    unidad_territorial_id = fields.Many2one(
        'hr.department',
        string='Unidad territorial',
        required=True,
        default=lambda x: x.env.user.unidad_territorial_id.id,
    )

    month = fields.Selection(
        selection=utils.month_selection(),
        string='Mes',
        required=True,
        default=lambda x: datetime.today().month,
    )

    year = fields.Selection(
        selection=utils.year_selection(),
        string='Año',
        required=True,
        default=lambda x: datetime.today().year,
    )

    filename = fields.Char()
    binary = fields.Binary(
        string='Reporte',
    )

    generado = fields.Boolean(default=False)

    @api.multi
    def action_gen_report(self):
        self.ensure_one()

        _, last_day = monthrange(self.year, self.month)
        inicio = datetime(self.year, self.month, 1)
        fin = inicio.replace(day=last_day)

        report = ExcelReport(self.env, self.unidad_territorial_id, inicio, fin)

        filename = '{}_{}_{}.xlsx'.format(
            self.unidad_territorial_id.name,
            self.year,
            self.month,
        )

        self.write({
            'filename': filename,
            'binary': report.render(),
            'generado': True,
        })

        return {
            'name': 'Elaboración de menú',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'elaboracion.menu.report',
            'res_id': self.id,
            'target': 'new',
        }
