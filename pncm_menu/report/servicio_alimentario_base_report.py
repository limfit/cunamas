# -*- coding: utf-8 -*-

from datetime import datetime

from odoo import fields, models

from .. import utils


class ServicioAlimentarioBaseReport(models.AbstractModel):

    _name = 'servicio.alimentario.base.report'

    unidad_territorial_id = fields.Many2one(
        'hr.department',
        string='Unidad territorial',
        required=True,
        default=lambda x: x.env.user.unidad_territorial_id.id,
    )

    comite_gestion_id = fields.Many2one(
        'pncm.comitegestion',
        string='Comité de gestión',
        required=True,
    )

    servicio_alimentario_id = fields.Many2one(
        'pncm.infra.local',
        string='Servicio alimentario',
        required=True,
    )

    month = fields.Selection(
        selection=utils.month_selection(),
        string='Mes',
        required=True,
        default=lambda x: datetime.today().month,
    )

    year = fields.Selection(
        selection=utils.year_selection(),
        string='Año',
        required=True,
        default=lambda x: datetime.today().year,
    )
