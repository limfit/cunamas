# -*- coding: utf-8 -*-

from calendar import monthrange
from datetime import datetime

from odoo import _, api, fields, models

from .servicio_alimentario_base_report import ServicioAlimentarioBaseReport


class RequerimientoAlimentoReport(ServicioAlimentarioBaseReport, models.TransientModel):  # NOQA

    _name = 'requerimiento.alimento.report'

    tipo_alimento = fields.Selection(
        string='Tipo de alimento',
        selection=(
            ('P', 'Perecible'),
            ('NP', 'No Perecible'),
        ),
        default='P',
        required=True,
    )

    total = fields.Float(
        string='Total',
        compute='_compute_total',
    )

    line_ids = fields.One2many(
        'requerimiento.alimento.report.line',
        'parent_id',
    )

    @api.depends('line_ids')
    def _compute_total(self):
        self.total = sum(self.line_ids.mapped(lambda x: x.total))

    @api.multi
    def action_gen_report(self):
        self.ensure_one()

        warehouse = self.env['stock.warehouse'].search([
            ('partner_id', '=', self.servicio_alimentario_id.partner_id.id),
        ], limit=1)

        stock_location = self.env['stock.location'].search([
            ('location_id', '=', warehouse.view_location_id.id),
            ('name', '=', _('Stock')),
        ], limit=1)

        picking_type = self.env['stock.picking.type'].search([
            ('default_location_dest_id', '=', stock_location.id),
        ], limit=1)

        __, last_day = monthrange(self.year, self.month)
        tmp = datetime(self.year, self.month, 1)
        fecha_inicio = fields.Date.to_string(tmp)
        fecha_fin = fields.Date.to_string(tmp.replace(day=last_day))

        line_to_create = []
        item_counter = 1

        for line in self.env['purchase.order.line'].search([
            ('order_id.picking_type_id', '=', picking_type.id),
            ('product_id.tipo_alimento', '=', self.tipo_alimento),
            ('order_id.date_planned', '<=', fecha_fin),
            ('order_id.date_planned', '>=', fecha_inicio),
        ]):
            line_to_create.append({
                'item': item_counter,
                'product_id': line.product_id.id,
                'uom_id': line.product_uom.id,
                'qty': line.product_qty,
                'precio': line.price_unit,
            })
            item_counter += 1

        self.line_ids.unlink()
        self.write({
            'line_ids': [(0, False, x) for x in line_to_create],
        })

        return {
            'name': 'Requerimiento de alimentos',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'requerimiento.alimento.report',
            'res_id': self.id,
            'target': 'new',
        }

    @api.multi
    def action_print(self):
        self.ensure_one()
        report_id = 'pncm_menu.requerimiento_alimento_report_template'
        return self.env['report'].get_action(self, report_id)


class RequerimientoAlimentoReportLine(models.TransientModel):

    _name = 'requerimiento.alimento.report.line'

    parent_id = fields.Many2one(
        'requerimiento.alimento.report',
        ondelete='cascade',
    )

    item = fields.Integer('Item')

    qty = fields.Float('Cantidad')

    uom_id = fields.Many2one(
        'product.uom',
        string='Unidad de medida',
    )

    product_id = fields.Many2one(
        'product.template',
        string='Alimento',
    )

    precio = fields.Float('Precio')

    total = fields.Float(
        'Total',
        compute='_compute_total',
    )

    @api.one
    @api.depends('qty', 'precio')
    def _compute_total(self):
        self.total = self.qty * self.precio
