# -*- coding: utf-8 -*-

from calendar import monthrange
from collections import defaultdict
from datetime import datetime

from odoo import api, fields, models

from .. import utils


class RacionGrupoEtarioReport(models.TransientModel):

    _name = 'racion.grupo.etario.report'

    unidad_territorial_id = fields.Many2one(
        'hr.department',
        string='Unidad territorial',
        required=True,
        default=lambda x: x.env.user.unidad_territorial_id.id,
    )

    month = fields.Selection(
        selection=utils.month_selection(),
        string='Mes',
        required=True,
        default=lambda x: datetime.today().month,
    )

    year = fields.Selection(
        selection=utils.year_selection(),
        string='Año',
        required=True,
        default=lambda x: datetime.today().year,
    )

    total = fields.Float(
        string='Total',
        compute='_compute_total',
    )

    line_ids = fields.One2many(
        'racion.grupo.etario.report.line',
        'parent_id',
    )

    @api.depends('line_ids')
    def _compute_total(self):
        self.total = sum(self.line_ids.mapped(lambda x: x.qty))

    @api.multi
    def action_gen_report(self):
        self.ensure_one()

        _, last_day = monthrange(self.year, self.month)
        fecha_inicio = datetime(self.year, self.month, 1)
        fecha_fin = fecha_inicio.replace(day=last_day)

        data = defaultdict(float)

        beneficiario_alimento_lines = self.env['beneficiario.alimento.line'].search([  # NOQA
            ('beneficiario_alimento_id.unidad_territorial_id', '=', self.unidad_territorial_id.id),  # NOQA
            ('beneficiario_alimento_id.month', '=', self.month),
            ('beneficiario_alimento_id.year', '=', self.year),
            ('beneficiario_alimento_id.state', '=', 'requerimiento'),
        ])

        for line in beneficiario_alimento_lines:
            elaboracion_menu_lines = self.env['elaboracion.menu.line'].search([
                ('elaboracion_menu_id.unidad_territorial_id', '=', self.unidad_territorial_id.id),  # NOQA
                ('elaboracion_menu_id.state', '=', 'approved'),
                ('elaboracion_menu_id.fecha', '<=', fecha_fin),
                ('elaboracion_menu_id.fecha', '>=', fecha_inicio),
                ('elaboracion_menu_id.attribute_value_id', '=', line.attribute_value_id.id),  # NOQA
            ])
            for eline in elaboracion_menu_lines:
                key = line.attribute_value_id.id
                data[key] += line.cantidad

        to_create = []

        for key, value in data.items():
            to_create.append({
                'attribute_value_id': key,
                'qty': value,
            })

        self.line_ids.unlink()
        self.write({
            'line_ids': [(0, False, x) for x in to_create],
        })

        return {
            'name': 'Ración por grupo etario',
            'type': 'ir.actions.act_window',
            'view_mode': 'form',
            'res_model': 'racion.grupo.etario.report',
            'res_id': self.id,
            'target': 'new',
        }

    @api.multi
    def action_print(self):
        self.ensure_one()
        report_id = 'pncm_menu.racion_grupo_etario_report_template'
        return self.env['report'].get_action(self, report_id)


class RacionGrupoEtarioReportLine(models.TransientModel):

    _name = 'racion.grupo.etario.report.line'

    parent_id = fields.Many2one(
        'racion.grupo.etario.report',
        ondelete='cascade',
    )

    attribute_value_id = fields.Many2one(
        'product.attribute.value',
        string='Grupo etario',
    )

    qty = fields.Float('Cantidad')
