Módulo de menús, recetas y raciones con valor nutricional para el Programa Nacional Cuna Más

Estándares y organización de codificación:
- Nomenclaturas y comentarios en español
- Respetar los estándares pep8 y flake8
- Todo archivo almacenado debe tener codificación UTF-8

* Modelos
Todos los modelos deben ubicarse en la carpeta /models 
con el nombre de archivo en minúscula  ejemplo: receta.py
un archivo por modelo.
La codificación de clase debe seguir la siguiente directriz:

#Clases con nomenclatura UpperCase
class Receta(models.Model):
	_name = 'pncm.menu.receta'
	
	##
	# Atributos
	#
	# Los stributos debe seguir la nomenclatura underscore: 
	# ejemplo: valor_nutricional
	# solo se podran usar en ingles los atributos por defecto de odoo (id, code, name)
	##
	
	##
	# Metodos y funciones de acceso privado y público
	#
	# todos los metodos y funciones deben seguir nomenclatura underscore
	# privados: _hacer_algo_de_acceso_privado
	# público: hacer_algo_deacceso_publico
	# los metodos y funcions deben describir claramento que es lo que realizan (autodocumentados)
	# asi mismo los nombres de variables internas que se utilicen dentro de los metodos
	##
	
	##
	# Restricciones odoo
	#
	# ejemplo: _sql_constraint por código unico
	##
	
* Vistas
Todas las vistas deben ubicarse en la carpeta /views
se considera archivo de vista todo aquel que termine con la extensión .xml

	--> Vistas Administrativas (vistas que se usan por el lado administrativo de odoo)
		Estas vistas deben ir en /views/admin
		* Vistas de Modelo:
		Totas las vistas de modelo deben de tener el mismo nombre del archivo del modelo
		por ejemplo: receta.xml
		la estructura esperada del archivo de vista es el siguiente: 
		
			<record id="pncm_menu_receta_tree_view" model="ir.ui.view">
				<!--Contenido-->
			</record>
			<record id="pncm_menu_receta_form_view" model="ir.ui.view">
				<!--Contenido-->
			</record>
			
			Nota: 
			-Todas las vistas que contenga el archivo deben estar relacionadas al modelo
			(para este ejemplo receta).
			
			-Este ejemplo muestra dos record, pero pueden existir mas de dos de ser necesario
			
		* Vista de Menú:
		El archivo all_menus.xml debe contener toda la estructura y jerarquía del modulo
		menú.
		
		* Vista de Secuencia
		El archivo all_sequences.xml debe contener toda definición de secuencias
		de ser necearías.
		
		* Vista de Tarea Programada:
		El archivo all_tasks.xml debe contener toda definición de tareas programadas
		de ser necearías.
		
	--> Vistas Web (Vistas usadas para uso público)
	De ser requerido vistas de acceso público, estas  deben colocarles en la carpeta
	/views/web
	
	Nota: Si el desarrollador decide usar templates ya sea por el lado administrativo o web
	la nomenclatura debe terminar con la extensión "tpl.xml"
	por ejemplo: admin_menu.tpl.xml, web_menu.tpl.xml
	
* Imágenes, estilos y validaciones por el lado del cliente:
Si se requieren el uso de Javascript, css e imágenes personalizadas estas deben colocarse 
respectiavemente en:
	
	imagenes: /static/scr/img
	estilos: /static/src/css
	javascript: /static/src/js
	
Nota:
	- Para la codificación css, se espera orden, nombres claros y evitar redundancia 
	de estilos.
	- Para la condificacipon javascript se debe cumplir:
		- No usar variables globales
		- Uso de 'strict'
		- Orden, nomenclatura clara (variables y  funciones) (autodocumentada) 
		- Se puede usar jQuery (ya viene incluido por odoo)
		- No hacer uso de peticiones AJAX sincronas (async = false)
		
	- Las imágenes usadas no deben superar el peso de 2MB
	
* Precargado de data:
Los archivos de precarga de datos para tablas maestras deben colocarse en la carpeta /data
y  deben tener el mismo nombre del modelo relacionado, ejemplo: pncm.menu.insumo.csv

* Configuración de Accesos:
Debe existir un unico archivo de definición de accesos este ya viene por defecto  vacío 
en: /security/ir.model.access.csv

* Controladores:
El uso de controladores se presenta al desarrollar parte web en odoo.
de ser requerido el uso de controladores estos deben colocarse en la carpeta /controllers
y seguir la nomenclatura y directrices que una clase de Modelo

* Confirmaciones (Commit):
Cada confirmación de código debe tener un comentario que lo describa claramente:
Estructura sugerida:

#########################################
#Título Corto
# 
#Pequeña descripción
#########################################

Ejemplo:

Programé forms administrativos de recetas

se programo: vista tree, kanban y formulario
de acuerdo a la historia de usuario definida en la tarea T1



	
		
		
	
	
	


	
	
	




	
