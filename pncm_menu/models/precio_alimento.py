# -*- coding: utf-8 -*-

from calendar import monthrange
from datetime import datetime

from odoo import api, fields, models
from odoo.exceptions import UserError

from .. import utils


class PrecioAlimento(models.Model):

    _name = 'precio.alimento'
    _inherit = ['mail.thread', 'ir.needaction_mixin']
    _rec_name = 'code'

    code = fields.Char(
        string='Código',
        required=True,
        readonly=True,
    )

    state = fields.Selection(
        selection=(
            ('draft', 'Borrador'),
            ('approved', 'Aprobado'),
        ),
        string='Estado',
        default='draft',
        track_visibility='onchange',
    )

    unidad_territorial_id = fields.Many2one(
        'hr.department',
        string='Unidad territorial',
        required=True,
        default=lambda x: x.env.user.unidad_territorial_id.id,
    )

    tipo_comite = fields.Selection(
        string='Tipo de comité',
        selection=lambda x: utils.tipo_comite_selection(),
        required=True,
        states={
            'approved': [('readonly', True)],
        }
    )

    month = fields.Selection(
        selection=utils.month_selection(),
        string='Mes',
        required=True,
        default=lambda x: datetime.today().month,
        states={
            'approved': [('readonly', True)],
        }
    )

    year = fields.Selection(
        selection=utils.year_selection(),
        string='Año',
        required=True,
        default=lambda x: datetime.today().year,
        states={
            'approved': [('readonly', True)],
        }
    )

    fecha_inicio = fields.Date(
        'Fecha de inicio',
        compute='_compute_date_range',
        store=True,
    )

    fecha_fin = fields.Date(
        'Fecha de fin',
        compute='_compute_date_range',
        store=True,
    )

    line_ids = fields.One2many(
        'precio.alimento.line',
        'precio_alimento_id',
        states={
            'approved': [('readonly', True)],
        }
    )

    _sql_constraints = (
        ('unique_unidad_territorial_tipo_comite_date_range',
         'UNIQUE(unidad_territorial_id, tipo_comite, fecha_inicio, fecha_fin)',
         'Solo se permite un registro por unidad territorial, tipo de comité y rango de fecha'),  # NOQA
        ('unique_code', 'UNIQUE(code)', 'El código debe ser único'),
    )

    @api.model
    def create(self, values):
        values.update({
            'code': self.env['ir.sequence'].next_by_code('precio.alimento'),
        })
        return super(PrecioAlimento, self).create(values)

    @api.one
    @api.depends('year', 'month')
    def _compute_date_range(self):
        if not self.year or not self.month:
            return
        tmp = datetime(self.year, self.month, 1)
        self.fecha_inicio = tmp
        _, last_day = monthrange(self.year, self.month)
        self.fecha_fin = tmp.replace(day=last_day)

    @api.one
    def action_aprobar(self):
        """
        Crea o actualiza un registro en product.supplierinfo con la
        información de regla.proveedor y precio.alimento, el registro en
        product.supplierinfo será usado al momento de generar
        los pedidos de compra en el flujo regular de Odoo.
        """
        sql = """
        SELECT
            regla.partner_id
        FROM
            regla_proveedor_producto AS rel
        JOIN regla_proveedor AS regla
            ON rel.regla_proveedor_id = regla.id
        JOIN product_template AS template
            ON rel.product_template_id = template.id
        WHERE
            regla.unidad_territorial_id = %(unidad_territorial_id)s
            AND tipo_comite = %(tipo_comite)s
            AND template.id = %(template_id)s
        """

        for line in self.line_ids:
            params = {
                'unidad_territorial_id': self.unidad_territorial_id.id,
                'tipo_comite': self.tipo_comite,
                'template_id': line.product_id.id,
            }
            self.env.cr.execute(sql, params)
            supplier_ids = [rec[0] for rec in self.env.cr.fetchall()]

            if not supplier_ids:
                continue

            for supplier_id in supplier_ids:
                records = self.env['product.supplierinfo'].search([
                    ('product_tmpl_id', '=', line.product_id.id),
                    ('tipo_comite', '=', self.tipo_comite),
                    ('name', '=', supplier_id),
                    ('date_start', '=', self.fecha_inicio),
                    ('date_end', '=', self.fecha_fin),
                ])
                values = self._prepare_supplierinfo(line, supplier_id)
                if records:
                    records.write(values)
                else:
                    self.env['product.supplierinfo'].create(values)
        self.write({
            'state': 'approved',
        })

    def _prepare_supplierinfo(self, line, supplier_id):
        return {
            'product_tmpl_id': line.product_id.id,
            'name': supplier_id,
            'tipo_comite': self.tipo_comite,
            'price': line.precio,
            'min_qty': 1,
            'date_start': self.fecha_inicio,
            'date_end': self.fecha_fin,
        }


class PrecioAlimentoLine(models.Model):

    _name = 'precio.alimento.line'

    precio_alimento_id = fields.Many2one(
        'precio.alimento',
        ondelete='cascade',
        required=True,
    )

    product_id = fields.Many2one(
        'product.template',
        string='Producto',
        domain=[
            ('tipo_cunamas', '=', 'alimento'),
        ],
        required=True,
    )

    product_uom_id = fields.Many2one(
        'product.uom',
        string='Unidad del producto',
        related='product_id.uom_id',
    )

    product_uom_po_id = fields.Many2one(
        'product.uom',
        string='Unidad de medida de compra',
        related='product_id.uom_po_id',
    )

    precio = fields.Float(
        string='Precio',
        required=True,
    )

    _sql_constraints = (
        ('unique_product_per_precio_alimento', 'UNIQUE(product_id, precio_alimento_id)', 'Los productos no pueden repetirse en el mismo precio de alimentos'),  # NOQA
    )

    @api.one
    @api.constrains('precio')
    def _check_precio(self):
        if self.precio <= 0.0:
            raise UserError('El precio debe ser positivo')
