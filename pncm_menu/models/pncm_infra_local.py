# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.exceptions import UserError


class PNCMLocal(models.Model):

    _inherit = 'pncm.infra.local'

    partner_id = fields.Many2one(
        'res.partner',
    )

    @api.one
    def action_gen_warehouse(self):
        if self.partner_id:
            return
        else:
            partner = self.env['res.partner'].create({
                'name': self.name,
                'es_servicio_alimentario': True,
            })
            self.partner_id = partner.id
        stock_warehouse = self.env['stock.warehouse']
        if stock_warehouse.search([
            ('partner_id', '=', self.partner_id.id),
        ]):
            message = 'El servicio alimentario actual ya tiene un almacén'
            raise UserError(message)

        stock_warehouse.create({
            'name': self.name,
            'code': self.codigo_almacen,
            'partner_id': self.partner_id.id,
            'company_id': self.partner_id.company_id.id,
        })
