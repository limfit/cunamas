# -*- coding: utf-8 -*-

from odoo import fields, models


class PurchaseOrder(models.Model):

    _inherit = 'purchase.order'

    tipo_generacion = fields.Selection(
        string='Tipo',
        selection=(
            ('default', 'Default'),
            # Usado para diferenciar los pedidos generados de forma regular
            ('grouped', 'Agrupado'),
            # Usado para diferenciar los pedidos generados desde el wizard
        ),
        default='default',
    )


class PurchaseOrderLine(models.Model):

    _inherit = 'purchase.order.line'

    cunamas_procesado = fields.Boolean(default=False)
    # Campo usado para "marcar" una linea de pedido y que solo se pueda
    # usar una vez
