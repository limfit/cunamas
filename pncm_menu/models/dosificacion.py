# -*- coding: utf-8 -*-

from odoo import api, fields, models
from odoo.exceptions import ValidationError


class Dosificacion(models.Model):

    _name = 'dosificacion'

    product_id = fields.Many2one('product.template', string='Producto')
    # TODO: Find a way to enable product variant on module installation
    attribute_value_id = fields.Many2one(
        'product.attribute.value',
        string='Grupo Etario',
        required=True,
    )
    # el grupo etario será manejado como un product.attribute.value por
    # tema de integrar el proceso
    # de producción
    tipo_preparacion_id = fields.Many2one(
        'tipo.preparacion',
        string='Tipos de preparación',
        required=True,
    )
    cantidad = fields.Float('Cantidad', required=True)
    uom_id = fields.Many2one(
        'product.uom',
        related='product_id.uom_id',
        string='Unidad',
    )

    _sql_constraints = (
        ('unique_dosificacion_per_attribute', 'UNIQUE(product_id, attribute_value_id, tipo_preparacion_id)', 'Solo se puede guardar un registro por atributo y tipo de preparación'),  # NOQA
    )

    @api.one
    @api.constrains('cantidad')
    def _check_cantidad(self):
        if self.cantidad and self.cantidad <= 0.0:
            raise ValidationError('La cantidad debe ser positiva')
