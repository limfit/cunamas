# -*- coding: utf-8 -*-

from odoo import fields, models


class ResPartner(models.Model):

    _inherit = 'res.partner'

    es_servicio_alimentario = fields.Boolean(
        '¿Es servicio alimentario?',
        default=False,
    )
