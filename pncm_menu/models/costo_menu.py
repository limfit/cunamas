# -*- coding: utf-8 -*-

from datetime import datetime, timedelta

from odoo import api, fields, models
from odoo.exceptions import UserError

from .. import utils


class CostoMenu(models.Model):

    _name = 'costo.menu'

    unidad_territorial_id = fields.Many2one(
        'hr.department',
        string='Unidad territorial',
        default=lambda x: x.env.user.unidad_territorial_id.id,
        required=True,
    )
    attribute_value_id = fields.Many2one(
        'product.attribute.value',
        string='Grupo etario',
        required=True,
    )
    tipo_comite = fields.Selection(
        string='Tipo de comité',
        selection=lambda x: utils.tipo_comite_selection(),
        required=True,
    )
    costo_maximo = fields.Float(
        string='Costo máximo',
        required=True,
    )
    fecha_inicio = fields.Date(
        'Fecha de inicio',
        required=True,
        default=fields.Date.today,
    )
    fecha_fin = fields.Date(
        'Fecha de fin',
        required=True,
        default=lambda x: datetime.today() + timedelta(days=7),
    )

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            name = '{}-{}'.format(record.fecha_inicio, record.fecha_fin)
            res.append((record.id, name))
        return res

    @api.one
    @api.constrains('fecha_inicio', 'fecha_fin')
    def _check_periodo(self):
        if self.fecha_inicio and self.fecha_fin:
            inicio = fields.Date.from_string(self.fecha_inicio)
            fin = fields.Date.from_string(self.fecha_fin)
            if inicio > fin:
                raise UserError('La fecha de inicio no puede ser mayor a la fecha de fin')  # NOQA

    @api.one
    @api.constrains('costo_maximo')
    def _check_costo_maximo(self):
        if self.costo_maximo <= 0:
            raise UserError('El costo debe ser positivo')
