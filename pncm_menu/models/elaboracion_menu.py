# -*- coding: utf-8 -*-

from collections import defaultdict

from odoo import api, fields, models

from .. import utils


class ElaboracionMenu(models.Model):

    _name = 'elaboracion.menu'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    unidad_territorial_id = fields.Many2one(
        'hr.department',
        string='Unidad territorial',
        default=lambda x: x.env.user.unidad_territorial_id.id,
        required=True,
    )
    tipo_comite = fields.Selection(
        string='Tipo de comité',
        selection=lambda x: utils.tipo_comite_selection(),
        required=True,
    )
    fecha = fields.Date('Fecha', required=True)
    attribute_value_id = fields.Many2one(
        'product.attribute.value',
        string='Grupo integrante',
        required=True,
    )
    state = fields.Selection(
        selection=(
            ('draft', 'Borrador'),
            ('approved', 'Aprobado'),
        ),
        string='Estado',
        default='draft',
        track_visibility='onchange',
    )

    mensaje_aporte_nutricional = fields.Char(
        string='Alerta aporte nutricional',
        compute='_compute_mensaje_aporte_nutricional',
        store=True,
    )

    mensaje_costo_menu = fields.Char(
        string='Alerta costo',
        compute='_compute_mensaje_costo_menu',
        store=True,
    )

    costo_total = fields.Float(
        string='Costo total',
        compute='_compute_costo_total',
        store=True,
    )

    line_ids = fields.One2many('elaboracion.menu.line', 'elaboracion_menu_id')

    energia = fields.Float(
        'Energia (kcal)',
        compute='_compute_informacion_nutricional',
        store=True,
    )
    agua = fields.Float(
        'Agua (g)',
        compute='_compute_informacion_nutricional',
        store=True,
    )
    proteinas = fields.Float(
        'Proteinas (g)',
        compute='_compute_informacion_nutricional',
        store=True,
    )
    grasa_total = fields.Float(
        'Grasa total (g)',
        compute='_compute_informacion_nutricional',
        store=True,
    )
    carbohidratos_totales = fields.Float(
        'Carbohidratos totales (g)',
        compute='_compute_informacion_nutricional',
        store=True,
    )
    fibra_cruda = fields.Float(
        'Fibra cruda (g)',
        compute='_compute_informacion_nutricional',
        store=True,
    )
    fibra_dietaria = fields.Float(
        'Fibra dietaria (g)',
        compute='_compute_informacion_nutricional',
        store=True,
    )
    calcio = fields.Float(
        'Calcio (mg)',
        compute='_compute_informacion_nutricional',
        store=True,
    )
    fosforo = fields.Float(
        'Fósforo (mg)',
        compute='_compute_informacion_nutricional',
        store=True,
    )
    zinc = fields.Float(
        'Zinc (g)',
        compute='_compute_informacion_nutricional',
        store=True,
    )
    hierro = fields.Float(
        'Hierro (g)',
        compute='_compute_informacion_nutricional',
        store=True,
    )
    retinol = fields.Float(
        'Retinol (µg)',
        compute='_compute_informacion_nutricional',
        store=True,
    )
    vitamina_a = fields.Float(
        'Vitamina A (µg)',
        compute='_compute_informacion_nutricional',
        store=True,
    )
    tiamina = fields.Float(
        'Tiamina (mg)',
        compute='_compute_informacion_nutricional',
        store=True,
    )
    riboflavina = fields.Float(
        'Riboflavina (mg)',
        compute='_compute_informacion_nutricional',
        store=True,
    )
    niacina = fields.Float(
        'Niacina (mg)',
        compute='_compute_informacion_nutricional',
        store=True,
    )
    vitamina_c = fields.Float(
        'Vitamina C (mg)',
        compute='_compute_informacion_nutricional',
        store=True,
    )

    calendar_display = fields.Char(compute='_compute_calendar_display')

    _sql_constraints = (
        ('unique_atribute_value_tipo_fecha', 'UNIQUE(tipo_comite, attribute_value_id, fecha)', 'Solo se puede registrar una elaboración por tipo de comite, grupo integrante y fecha'),  # NOQA
    )

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append(
                (record.id, '{}-{}'.format(record.tipo_comite, record.fecha))
            )
        return res

    @api.one
    @api.depends('tipo_comite', 'attribute_value_id', 'fecha', 'line_ids')
    def _compute_calendar_display(self):
        template = '{grupo_etario}-{tipo_comite}'
        self.calendar_display = template.format(
            grupo_etario=self.attribute_value_id.name,
            tipo_comite=self.tipo_comite.title(),
        )

    @api.one
    @api.depends('line_ids')
    def _compute_informacion_nutricional(self):
        mrp_bom = self.env['mrp.bom']
        total = defaultdict(float)
        fields = (
            'energia', 'agua', 'proteinas', 'grasa_total',
            'carbohidratos_totales', 'fibra_cruda', 'fibra_dietaria',
            'calcio', 'fosforo', 'zinc', 'hierro',
            'retinol', 'vitamina_a', 'tiamina', 'riboflavina',
            'niacina', 'vitamina_c'
        )
        for line in self.line_ids:
            mrp = mrp_bom.search([
                ('product_id', '=', line.product_id.id),
            ], limit=1)
            if mrp:
                for component in mrp.bom_line_ids:
                    for field in fields:
                        value = getattr(component.product_id, field)
                        factor = component.product_qty / component.product_id.porcion  # NOQA
                        total[field] += value * factor

        for field in fields:
            setattr(self, field, total[field])

    aporte_nutricional_fields = (
        'energia', 'proteinas', 'hierro', 'zinc',
        'calcio', 'vitamina_a', 'vitamina_c',
    )

    @api.one
    @api.depends(*(('attribute_value_id',) + aporte_nutricional_fields))
    def _compute_mensaje_aporte_nutricional(self):
        aporte_nutricional = self.env['aporte.nutricional'].search([
            ('attribute_value_id', '=', self.attribute_value_id.id),
        ], limit=1)
        if not aporte_nutricional:
            return
        messages = []
        for field in self.aporte_nutricional_fields:
            value = getattr(self, field)
            ap_value = getattr(aporte_nutricional, field)
            if value < ap_value:
                field_display_name = field.replace('_', ' ').upper()
                messages.append('{} por debajo de la recomendación ({})'.format(  # NOQA
                    field_display_name, ap_value,
                ))
        self.mensaje_aporte_nutricional = '<br>'.join(messages)

    @api.one
    @api.depends('line_ids')
    def _compute_costo_total(self):
        self.costo_total = sum([line.costo for line in self.line_ids])

    _compute_mensaje_costo_menu_fields = (
        'costo_total', 'unidad_territorial_id', 'attribute_value_id',
        'tipo_comite', 'fecha',
    )

    @api.one
    @api.depends(*_compute_mensaje_costo_menu_fields)
    def _compute_mensaje_costo_menu(self):
        costo_menu = self.env['costo.menu'].search([
            ('unidad_territorial_id', '=', self.unidad_territorial_id.id),
            ('attribute_value_id', '=', self.attribute_value_id.id),
            ('tipo_comite', '=', self.tipo_comite),
            ('fecha_inicio', '<=', self.fecha),
            ('fecha_fin', '>=', self.fecha),
        ], limit=1)
        if not costo_menu:
            return
        if self.costo_total > costo_menu.costo_maximo:
            msg = 'Costo del menú por encima del costo sugerido ({})'.format(
                costo_menu.costo_maximo,
            )
            self.mensaje_costo_menu = msg

    @api.one
    def action_aprobar(self):
        values = map(lambda x: getattr(self, x), self.aporte_nutricional_fields)  # NOQA

        if not all(values):
            raise ValidationError('No se puede aprobar el menú porque no cumple con el aporte nutricional mínimo')  # NOQA

        self.write({
            'state': 'approved',
        })


class ElaboracionMenuLine(models.Model):

    _name = 'elaboracion.menu.line'

    elaboracion_menu_id = fields.Many2one(
        'elaboracion.menu',
        ondelete='cascade',
    )

    tipo_comida_id = fields.Many2one(
        'tipo.comida',
        string='Tipo de comida',
        required=True,
    )

    tipo_preparacion_id = fields.Many2one(
        'tipo.preparacion',
        string='Tipo de preparación',
        required=True,
    )

    product_id = fields.Many2one(
        'product.product',
        string='Preparación',
        required=True,
    )

    costo = fields.Float(
        'Costo',
        compute='_compute_costo',
    )

    _compute_costo_fields = (
        'product_id', 'elaboracion_menu_id.unidad_territorial_id',
        'elaboracion_menu_id.tipo_comite', 'elaboracion_menu_id.fecha',
    )

    @api.one
    @api.depends(*_compute_costo_fields)
    def _compute_costo(self):
        parent = self.elaboracion_menu_id

        bom = self.env['mrp.bom'].search([
            ('product_id', '=', self.product_id.id),
            ('unidad_territorial_id', '=', parent.unidad_territorial_id.id),
        ], limit=1)

        costo = 0.0

        if not bom:
            return

        for line in bom.bom_line_ids:
            product_template_id = line.product_id.product_tmpl_id.id
            precio_alimento = self.env['precio.alimento.line'].search([
                ('precio_alimento_id.unidad_territorial_id', '=', parent.unidad_territorial_id.id),  # NOQA
                ('precio_alimento_id.tipo_comite', '=', parent.tipo_comite),
                ('precio_alimento_id.fecha_inicio', '<=', parent.fecha),
                ('precio_alimento_id.fecha_fin', '>=', parent.fecha),
                ('product_id', '=', product_template_id),
            ], limit=1)

            if not precio_alimento:
                continue

            product_uom = precio_alimento.product_uom_id
            product_po_uom = precio_alimento.product_uom_po_id
            precio = precio_alimento.precio
            precio_uom = product_po_uom._compute_price(precio, product_uom)
            costo += line.product_qty * precio_uom

        self.costo = costo if costo else None
