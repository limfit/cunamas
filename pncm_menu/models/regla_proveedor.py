# -*- coding: utf-8 -*-

from odoo import api, fields, models


class ReglaProveedor(models.Model):

    _name = 'regla.proveedor'

    unidad_territorial_id = fields.Many2one(
        'hr.department',
        string='Unidad territorial',
        default=lambda x: x.env.user.unidad_territorial_id.id,
        required=True,
    )
    comite_gestion_id = fields.Many2one(
        'pncm.comitegestion',
        string='Comité de gestión',
        required=True,
    )
    tipo_comite = fields.Selection(
        string='Tipo de comité',
        related='comite_gestion_id.tipo_comite',
        store=True,
    )
    partner_id = fields.Many2one(
        'res.partner',
        string='Proveedor',
        domain=[
            ('supplier', '=', True),
        ],
        required=True,
    )
    product_ids = fields.Many2many(
        'product.template',
        relation='regla_proveedor_producto',
        string='Productos',
        domain=[
            ('tipo_cunamas', '=', 'alimento'),
        ]
    )

    _sql_constraints = (
        ('unique_unidad_territorial_comite_gestion_tipo_comite_partner',
         'UNIQUE(unidad_territorial_id, comite_gestion_id, tipo_comite, partner_id)',  # NOQA
         'Solo es permitido un registro por unidad territorial, comité de gestión, proveedor y tipo de comité'),  # NOQA
    )

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            name = '{}-{}'.format(
                record.unidad_territorial_id.name,
                record.comite_gestion_id.name,
            )
            res.append((record.id, name))
        return res
