# -*- coding: utf-8 -*-

from odoo import api, fields, models


class AporteNutricional(models.Model):

    _name = 'aporte.nutricional'
    _rec_name = 'code'

    code = fields.Char('Código', required=True)
    attribute_value_id = fields.Many2one(
        'product.attribute.value',
        string='Grupo integrante',
        required=True,
    )

    energia = fields.Float('Energia (kcal)')
    proteinas = fields.Float('Proteinas (g)')
    hierro = fields.Float('Hierro (g)')
    zinc = fields.Float('Zinc (g)')
    calcio = fields.Float('Calcio (mg)')
    vitamina_a = fields.Float('Vitamina A (µg)')
    vitamina_c = fields.Float('Vitamina C (mg)')

    @api.model
    def create(self, values):
        values.update({
            'code': self.env['ir.sequence'].next_by_code('aporte.nutricional'),
        })
        return super(AporteNutricional, self).create(values)
