# -*- coding: utf-8 -*-

from calendar import monthrange
from datetime import datetime, timedelta

from dateutil.rrule import DAILY, rrule
from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError

from .. import utils


class BeneficiarioAlimento(models.Model):

    _name = 'beneficiario.alimento'
    _inherit = ['mail.thread', 'ir.needaction_mixin']

    unidad_territorial_id = fields.Many2one(
        'hr.department',
        string='Unidad territorial',
        default=lambda x: x.env.user.unidad_territorial_id.id,
        required=True,
        states={
            'requerimiento': [('readonly', True)],
        }
    )
    servicio_alimentario_id = fields.Many2one(
        'pncm.infra.local',
        string='Servicio alimentario',
        required=True,
        domain=[
            ('state', '=', 'activacion'),
            ('tipo_local', 'in', ('sa', 'ambos'))
        ],
        states={
            'requerimiento': [('readonly', True)],
        },
    )
    partner_id = fields.Many2one(
        'res.partner',
        string='Partner',
        related='servicio_alimentario_id.partner_id',
        states={
            'requerimiento': [('readonly', True)],
        }
    )
    warehouse_id = fields.Many2one(
        'stock.warehouse',
        string='Almacén',
        required=True,
        states={
            'requerimiento': [('readonly', True)],
        }
    )
    user_id = fields.Many2one(
        'res.users',
        string='Especialista nutricional',
        required=True,
        default=lambda s: s.env.user.id,
        states={
            'requerimiento': [('readonly', True)],
        }
    )
    state = fields.Selection(
        selection=(
            ('draft', 'Borrador'),
            ('requerimiento', 'Requerimiento generado'),
        ),
        string='Estado',
        default='draft',
        track_visibility='onchange',
    )
    month = fields.Selection(
        selection=utils.month_selection(),
        string='Mes',
        required=True,
        default=lambda x: datetime.today().month,
        states={
            'requerimiento': [('readonly', True)],
        }
    )
    year = fields.Selection(
        selection=utils.year_selection(),
        string='Año',
        required=True,
        default=lambda x: datetime.today().year,
        states={
            'requerimiento': [('readonly', True)],
        }
    )

    fecha_inicio = fields.Date(
        'Fecha de inicio',
        compute='_compute_date_range',
        store=True,
    )
    fecha_fin = fields.Date(
        'Fecha de fin',
        compute='_compute_date_range',
        store=True,
    )
    line_ids = fields.One2many(
        'beneficiario.alimento.line',
        'beneficiario_alimento_id',
        states={
            'requerimiento': [('readonly', True)],
        }
    )
    total_raciones = fields.Integer(
        'Total raciones',
        compute='_compute_total_raciones',
    )

    _sql_constraints = (
        (
            'unique_unidad_territorial_servicio_alimentario_rango_fecha',
            'UNIQUE(unidad_territorial_id, servicio_alimentario_id, year, month)',  # NOQA
            'Beneficiario alimento debe ser único por UT, servicio alimentario y rango de fechas',  # NOQA
        ),
    )

    @api.multi
    def name_get(self):
        res = []
        for record in self:
            res.append(
                (record.id, record.warehouse_id.name)
            )
        return res

    @api.constrains('servicio_alimentario_id')
    def _check_servicio_alimentario_id(self):
        partner = self.servicio_alimentario_id.partner_id
        if not partner:
            message = 'El servicio alimentario no tiene un partner asociado'
            raise ValidationError(message)
        if not self.env['stock.warehouse'].search([
            ('partner_id', '=', self.partner_id.id),
        ]):
            message = 'El servicio alimentario no tiene un almacén asociado'
            raise ValidationError(message)

    @api.onchange('servicio_alimentario_id')
    def _onchange_servicio_alimentario_id(self):
        if not self.servicio_alimentario_id:
            return
        stock_warehouse = self.env['stock.warehouse']
        error_message = 'El servicio alimentario elegido no tiene un almacén'
        partner = self.servicio_alimentario_id.partner_id
        if partner:
            self.partner_id = partner.id
            warehouse = stock_warehouse.search([
                ('partner_id', '=', partner.id),
            ], limit=1)
            if warehouse:
                self.warehouse_id = warehouse.id
            else:
                raise UserError(error_message)
        else:
            message = 'El servicio alimentario no tiene un almacén asociado'
            raise ValidationError(message)

    @api.one
    @api.depends('year', 'month')
    def _compute_date_range(self):
        if not self.year or not self.month:
            return
        tmp = datetime(self.year, self.month, 1)
        self.fecha_inicio = tmp
        _, last_day = monthrange(self.year, self.month)
        self.fecha_fin = tmp.replace(day=last_day)

    @api.one
    @api.depends('line_ids')
    def _compute_total_raciones(self):
        cantidades = self.line_ids.mapped(lambda x: x.cantidad)
        self.total_raciones = sum(cantidades)

    @api.one
    def action_generar_requerimiento_alimentos(self):
        inicio = fields.Date.from_string(self.fecha_inicio)
        fin = fields.Date.from_string(self.fecha_fin)

        mes_anterior = (inicio - timedelta(days=1)).month

        sql = """
        SELECT count(*)
        FROM stock_inventory
        WHERE
            (EXTRACT(MONTH FROM date)) = %s
            AND state = 'done'
        """
        self.env.cr.execute(sql, (mes_anterior,))
        (result,) = self.env.cr.fetchone()

        if not result:
            message = 'No hay un cierre de inventario en el mes anterior'
            raise ValidationError(message)

        db_elaboracion_menu_line = self.env['elaboracion.menu.line']
        db_sale_order = self.env['sale.order']
        for date in rrule(DAILY, dtstart=inicio, until=fin):
            sale_order_data = {
                'partner_id': self.partner_id.id,
                'warehouse_id': self.warehouse_id.id,
                'company_id': self.partner_id.company_id.id,
                'date_order': fields.Date.to_string(date),
            }
            sale_lines = []
            for line in self.line_ids:
                for em_line in db_elaboracion_menu_line.search([
                    ('elaboracion_menu_id.fecha', '=', fields.Date.to_string(date)),  # NOQA
                    ('elaboracion_menu_id.tipo_comite', '=', self.servicio_alimentario_id.comite_id.tipo_comite),  # NOQA
                    ('elaboracion_menu_id.attribute_value_id', '=', line.attribute_value_id.id),  # NOQA
                    ('elaboracion_menu_id.state', '=', 'approved'),
                ]):
                    sale_lines.append({
                        'product_id': em_line.product_id.id,
                        'product_uom_qty': line.cantidad,
                    })
            if not sale_lines:
                continue
            sale_order_data.update({
                'order_line': [(0, False, x) for x in sale_lines],
            })
            sale_order = db_sale_order.create(sale_order_data)
            sale_order.action_confirm()
            # actualiza procurement.order con el almacen y ubicación currectos
            location = self.env['stock.location'].search([
                ('location_id', '=', self.warehouse_id.view_location_id.id),
                ('name', '=', _('Stock')),
            ], limit=1)
            if location is None:
                raise ValidationError('El almacén no tiene una ubicación de existencias')  # NOQA
            procurement_orders = self.env['procurement.order'].search([
                ('sale_line_id.order_id', '=', sale_order.id),
            ])
            for procurement_order in procurement_orders:
                # Usa la lista de materiales especificada para la unidad
                # territorial actual
                bom = self.env['mrp.bom'].search([
                    ('product_id', '=', procurement_order.product_id.id),
                    ('unidad_territorial_id', '=', self.unidad_territorial_id.id),  # NOQA
                ], limit=1)
                date_planned = fields.Datetime.from_string(procurement_order.date_planned)  # NOQA
                date_planned = date_planned.replace(hour=12)
                # hack para ajustar la hora y que no se vea la fecha con un dia
                # menos a pesar de la zona horaria del usuario
                procurement_order.write({
                    'bom_id': bom.id,
                    'date_planned': date_planned
                })
            procurement_orders.write({
                'warehouse_id': self.warehouse_id.id,
                'location_id': location.id,
            })
            # Ejecutar abastecimientos
            procurement_orders.run()
        self.write({
            'state': 'requerimiento',
        })


class BeneficiarioAlimentoLine(models.Model):

    _name = 'beneficiario.alimento.line'

    beneficiario_alimento_id = fields.Many2one(
        'beneficiario.alimento',
        required=True,
        ondelete='cascade',
    )
    attribute_value_id = fields.Many2one(
        'product.attribute.value',
        string='Grupo Etario',
        required=True,
    )
    cantidad = fields.Integer(
        'Cantidad de personas',
        required=True,
    )

    _sql_constraints = (
        ('unique_attribute_value_per_beneficiario_alimento', 'UNIQUE(beneficiario_alimento_id, attribute_value_id)', 'Los grupos etarios no pueden repetirse'),  # NOQA
    )

    @api.one
    @api.constrains('cantidad')
    def _check_cantidad(self):
        error_message = 'La cantidad debe ser positiva'
        if self.cantidad is not None and self.cantidad <= 0:
            raise UserError(error_message)
