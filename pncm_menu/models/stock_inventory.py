# -*- coding: utf-8 -*-

from odoo import _, api, fields, models
from odoo.exceptions import ValidationError


class StockInventory(models.Model):

    _inherit = 'stock.inventory'

    servicio_alimentario_id = fields.Many2one(
        'pncm.infra.local',
        string='Servicio alimentario',
        domain=[
            ('state', '=', 'activacion'),
            ('tipo_local', 'in', ('sa', 'ambos'))
        ],
        readonly=True,
        states={
            'draft': [('readonly', False)],
        },
    )

    @api.onchange('servicio_alimentario_id')
    def _onchange_servicio_alimentario_id(self):
        if not self.servicio_alimentario_id:
            return

        partner = self.servicio_alimentario_id.partner_id

        if not partner:
            raise ValidationError('El servicio alimentario no tiene un partner asignado')  # NOQA

        warehouse = self.env['stock.warehouse'].search([
            ('partner_id', '=', partner.id),
        ], limit=1)

        if not warehouse:
            raise ValidationError('El servicio alimentario no tiene un almacén asignado')  # NOQA

        location = self.env['stock.location'].search([
            ('location_id', '=', warehouse.view_location_id.id),
            ('name', '=', _('Stock')),
        ], limit=1)

        if not location:
            raise ValidationError('El servicio alimentario no tiene una ubicación asignada')  # NOQA

        self.location_id = location.id

    @api.multi
    def _get_inventory_lines_values(self):
        """
        Filtra solo los productos de tipo `alimento` para la generación
        del inventario
        """
        values = super(StockInventory, self)._get_inventory_lines_values()
        ids = self.env['product.product'].search([
            ('tipo_cunamas', '=', 'alimento'),
        ]).ids
        return filter(lambda x: x['product_id'] in ids, values)
