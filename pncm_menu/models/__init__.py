# -*- coding: utf-8 -*-

from .aporte_nutricional import *  # NOQA
from .beneficiario_alimento import *  # NOQA
from .costo_menu import *  # NOQA
from .dosificacion import *  # NOQA
from .elaboracion_menu import *  # NOQA
from .mrp_bom import *  # NOQA
from .precio_alimento import *  # NOQA
from .procurement_order import *  # NOQA
from .product import *  # NOQA
from .purchase_order import *  # NOQA
from .regla_proveedor import *  # NOQA
from .res_partner import *  # NOQA
from .stock_inventory import *  # NOQA
from .pncm_infra_local import *  # NOQA

__all__ = [
    'product.TipoPreparacion',
    'product.ProductTemplate',
    'product.ProductSupplierInfo',
    'dosificacion.Dosificacion',
    'mrp_bom.MRPBomLine',
    'product.TipoComida',
    'res_partner.ResPartner',
    'elaboracion_menu.ElaboracionMenu',
    'elaboracion_menu.ElaboracionMenuLine',
    'beneficiario_alimento.BeneficiarioAlimento',
    'aporte_nutricional.AporteNutricional',
    'regla_proveedor.ReglaProveedor',
    'purchase_order.PurchaseOrder',
    'purchase_order.PurchaseOrderLine',
    'procurement_order.ProcurementOrder',
    'costo_menu.CostoMenu',
    'precio_alimento.PrecioAlimento',
    'precio_alimento.PrecioAlimentoLine',
    'stock_inventory.StockInventory',
    'pncm_infra_local.PNCMLocal',
]
