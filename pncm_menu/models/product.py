# -*- coding: utf-8 -*-

from odoo import _, api, fields, models
from odoo.exceptions import UserError, ValidationError

from .. import utils


class TipoComida(models.Model):

    _name = 'tipo.comida'

    name = fields.Char('Nombre', required=True)
    horario = fields.Char('Horario', required=True)
    order = fields.Integer('Orden', required=True)


class GrupoAlimento(models.Model):

    _name = 'grupo.alimento'

    name = fields.Char('Nombre', required=True)
    code = fields.Char('Código', required=True)


class TipoPreparacion(models.Model):

    _name = 'tipo.preparacion'

    name = fields.Char('Nombre', required=True)
    code = fields.Char('Código', required=True)


class ProductTemplate(models.Model):

    _inherit = 'product.template'

    tipo_comida_id = fields.Many2one('tipo.comida', string='Tipo de comida')
    grupo_alimento_id = fields.Many2one(
        'grupo.alimento',
        string='Grupo de alimento',
    )

    unidad_territorial_id = fields.Many2one(
        'hr.department',
        string='Unidad territorial',
        default=lambda x: x.env.user.unidad_territorial_id.id,
    )

    tipo_preparacion_id = fields.Many2one(
        'tipo.preparacion',
        string='Tipo de preparación',
    )
    tipo_cunamas = fields.Selection(selection=(
        ('alimento', 'Alimento'),
        ('preparacion', 'Preparación'),
    ), required=True)
    # Campo creado para usarse en lugar de purchase_ok y sale_ok
    # internamente se siguen usando purchase_ok y sale_ok pero
    # para el usuario es transparente
    code = fields.Char(
        string='Código de producto',
        required=True,
    )
    # Este código es usado para evitar errores con default_code ya que este no
    # puede ser único

    porcion = fields.Float('Tamaño de porción')
    tipo_alimento = fields.Selection(string='Tipo de alimento', selection=(
        ('P', 'Perecible'),
        ('NP', 'No Perecible'),
    ))
    porcentaje_parte_comestible = fields.Float('% parte comestible')

    energia = fields.Float('Energia (kcal)')
    agua = fields.Float('Agua (g)')
    proteinas = fields.Float('Proteinas (g)')
    grasa_total = fields.Float('Grasa total (g)')
    carbohidratos_totales = fields.Float('Carbohidratos totales (g)')
    fibra_cruda = fields.Float('Fibra cruda (g)')
    fibra_dietaria = fields.Float('Fibra dietaria (g)')
    calcio = fields.Float('Calcio (mg)')
    fosforo = fields.Float('Fósforo (mg)')
    zinc = fields.Float('Zinc (g)')
    hierro = fields.Float('Hierro (g)')
    retinol = fields.Float('Retinol (µg)')
    vitamina_a = fields.Float('Vitamina A (µg)')
    tiamina = fields.Float('Tiamina (mg)')
    riboflavina = fields.Float('Riboflavina (mg)')
    niacina = fields.Float('Niacina (mg)')
    vitamina_c = fields.Float('Vitamina C (mg)')

    dosificacion_ids = fields.One2many('dosificacion', 'product_id')

    @api.model
    def create(self, values):
        tipo_cunamas = values.get('tipo_cunamas', '')
        if tipo_cunamas == 'preparacion':
            values.update({
                'code': self.env['ir.sequence'].next_by_code('preparacion'),
            })
        return super(ProductTemplate, self).create(values)

    @api.one
    @api.constrains('porcentaje_parte_comestible')
    def _check_porcentaje_parte_comestible(self):
        error_message = 'El % parte comestible debe estar entre 0 y 100%'
        if self.porcentaje_parte_comestible and not (0.0 <= self.porcentaje_parte_comestible <= 100.0):  # NOQA
            raise ValidationError(error_message)

    @api.one
    @api.constrains('porcion')
    def _check_porcion(self):
        if self.tipo_cunamas == 'alimento' and self.porcion is not None and self.porcion <= 0:  # NOQA
            raise UserError('El valor de la porción debe ser positivo')

    @api.multi
    @api.onchange('sale_ok')
    def _onchange_sale_ok(self):
        if self.sale_ok:
            ids = self.env['stock.location.route'].search([
                ('name', 'in', (_('Manufacture'), _('Make To Order')))
            ]).ids
            return {
                'value': {
                    'route_ids': [(6, False, ids)]
                }
            }

    @api.multi
    @api.onchange('purchase_ok')
    def _onchange_purchase_ok(self):
        if self.purchase_ok:
            ids = self.env['stock.location.route'].search([
                ('name', 'in', (_('Buy'), _('Make To Order')))
            ]).ids
            return {
                'value': {
                    'route_ids': [(6, False, ids)]
                }
            }

    @api.onchange('tipo_cunamas')
    def _onchange_tipo_cunamas(self):
        if self.tipo_cunamas == 'alimento':
            self.purchase_ok = True
            self.sale_ok = False
        elif self.tipo_cunamas == 'preparacion':
            self.sale_ok = True
            self.purchase_ok = False

    _sql_constraints = (
        ('unique_code', 'UNIQUE(code)', 'El código debe ser único'),  # NOQA
    )


class ProductSupplierInfo(models.Model):

    _inherit = 'product.supplierinfo'

    tipo_comite = fields.Selection(
        string='Tipo de comité',
        selection=lambda x: utils.tipo_comite_selection(),
    )

    product_uom_id = fields.Many2one(
        string='Unidad del producto',
        related='product_tmpl_id.uom_id',
    )
