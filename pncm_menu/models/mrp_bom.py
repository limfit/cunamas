# -*- coding: utf-8 -*-

from odoo import api, fields, models


class MRPBom(models.Model):

    _inherit = 'mrp.bom'

    unidad_territorial_id = fields.Many2one(
        'hr.department',
        string='Unidad territorial',
        default=lambda x: x.env.user.unidad_territorial_id.id,
    )

    attribute_value_name = fields.Char(
        string='Grupo etario',
        compute='_compute_attribute_value_name',
    )

    @api.onchange('unidad_territorial_id')
    def _onchange_unidad_territorial(self):
        if self.unidad_territorial_id:
            self.code = self.unidad_territorial_id.name

    @api.depends('product_id')
    def _compute_attribute_value_name(self):
        values = self.product_id.attribute_value_ids

        if values:
            # Se considera el primer valor porque el sistema SOLO trabaja
            # con un único atributo: grupo etario
            self.attribute_value_name = values[0].name


class MRPBomLine(models.Model):

    _inherit = 'mrp.bom.line'

    @api.onchange('product_id')
    def _onchange_product_id(self):
        if not self.product_id:
            return

        product = self.bom_id.product_id
        attribute_value = product.attribute_value_ids
        dosificacion = self.env['dosificacion'].search([
            ('attribute_value_id', '=', attribute_value.id),
            ('product_id', '=', self.product_id.product_tmpl_id.id),
        ], limit=1)

        if dosificacion:
            self.product_qty = dosificacion.cantidad
        else:
            self.product_qty = 0.0
            message = 'No se ha ingresado la dosificación de {} para el grupo etario {}'.format(  # NOQA
                self.product_id.product_tmpl_id.name,
                attribute_value.name,
            )
            return {
                'warning': {
                    'title': 'Validación de dosificación',
                    'message': message,
                }
            }
