# -*- coding: utf-8 -*-

from odoo import models
from odoo.exceptions import ValidationError


class ProcurementOrder(models.Model):

    _inherit = 'procurement.order'

    def _make_po_select_supplier(self, suppliers):
        # La ubicación relacionada directamente es la ubicación
        # de existencias, el padre de esta es la que esta relacionada
        # al almacén y a partir del almacén se obtiene el servicio
        # alimentario(res.partner)
        main_location_id = self.location_id.location_id.id
        warehouse = self.env['stock.warehouse'].search([
            ('view_location_id', '=', main_location_id),
        ], limit=1)
        servicio_alimentario = self.env['pncm.infra.local'].search([
            ('partner_id', '=', warehouse.partner_id.id),
        ], limit=1)

        if not servicio_alimentario:
            message = 'El servicio alimentario no tiene un partner asociado'
            raise ValidationError(message)

        # Se extrae la lista de proveedores que venden el producto
        # de este procurement.order y hacia el comité de gestión
        # del servicio alimentario destino
        sql = """
        SELECT regla.partner_id
        FROM
            regla_proveedor_producto AS rel
        JOIN regla_proveedor AS regla
            ON rel.regla_proveedor_id = regla.id
        WHERE
            regla.comite_gestion_id = %(comite_gestion_id)s
            AND rel.product_template_id = %(template_id)s
        """
        params = {
            'comite_gestion_id': servicio_alimentario.comite_id.id,
            'template_id': self.product_id.product_tmpl_id.id,
        }
        self.env.cr.execute(sql, params)
        supplier_ids = {rec[0] for rec in self.env.cr.fetchall()}

        filtered = suppliers.filtered(lambda x: x.name.id in supplier_ids)

        if not filtered:
            msg = 'No existe un proveedor que venda {} en el comité de gestión {}'.format(  # NOQA
                self.product_id.name,
                servicio_alimentario.comite_id.name,
            )
            raise ValidationError(msg)

        return filtered[0]

    def _make_po_get_domain(self, partner):
        """
        Permite separar los pedidos de compra creados usando la fecha prevista,
        esta separación es usada para la generación de los pedidos de comnpra
        agrupados desde el wizard de "Generación de pedidos de compra"
        """
        domain = super(ProcurementOrder, self)._make_po_get_domain(partner)
        filters = (
            ('date_planned', '=', self.date_planned),
        )
        return domain + filters
