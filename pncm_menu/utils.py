# -*- coding: utf-8 -*-

from datetime import datetime

from odoo import _


def tipo_comite_selection():
    """Copied from pncm_comitegestion module"""
    return (
        ('23', u'Comité Rural'),
        ('24', u'Urbano Disperso'),
        ('25', u'Comité Urbano'),
    )


def month_selection():
    res = []
    now = datetime.now()
    for i in range(1, 13):
        res.append(
            (i, _(now.replace(day=1, month=i).strftime('%B')))
        )
    return res


def year_selection():
    return [
        (x, x) for x in range(2015, 2030)
    ]
