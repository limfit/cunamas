# -*- coding: utf-8 -*-

from openerp import fields, models, api


TYPE_AFILIATION = [
        ('individual', 'Individual'),
        ('familiar', 'Familiar'),
    ]


class Afiliados(models.Model):
    _name = 'pncm.afiliados'
    _inherits = {
        'res.partner': 'partner_id',
    }

    partner_id = fields.Many2one(
        'res.partner',
        string='Related Partner',
        required=True,
        ondelete='cascade',
        help='Afiliado asociado a un Partner ')
    type_afiliation = fields.Selection(
        TYPE_AFILIATION,
        string=u'Tipo Afiliacion'
    )
    identification_code = fields.Char(
        string='Afiliado ID',
        size=256,
        help='Codigo de identificacion',
        readonly=True
    )

    @api.depends(
        'name',
        'secondname',
        'firstname',
        'document_number'
    )
    @api.onchange(
        'name',
        'secondname',
        'firstname',
        'document_number'
    )
    def onchange_person_information(self):
        if self.name:
            self.name = self.name.upper()
        if self.secondname:
            self.secondname = self.secondname.upper()
        if self.firstname:
            self.firstname = self.firstname.upper()


    @api.multi
    def name_get(self):
        result = []
        for record in self:
            str_name = record.name or ('id. %d' % record.id) or 0
            if record.name and record.secondname and record.firstname:
                str_name = "[%s] %s %s, %s" % (
                record.document_number, record.firstname, record.secondname, record.name)
            elif record.firstname and record.name:
                str_name = "[%s] %s, %s" % (record.document_number, record.firstname, record.name)
            elif record.company_type=='company':
                str_name = "[%s] %s" % (record.document_number, record.business_name)
            result.append((record.id, str_name))
        return result


class ParnertInherit(models.Model):
    _inherit = 'res.partner'

    es_afiliado = fields.Boolean(
        string=u'Es afiliado'
    )
    es_propietatio_local = fields.Boolean(
        string=u'Propietario de local'
    )
    es_madre_cuidadora = fields.Boolean(
        string='Es madre cuidadora'
    )
    es_comite_gestion = fields.Boolean(
        string='Es del Comite de Gestion'
    )
    business_name = fields.Char(string='Nombre Empresa')
