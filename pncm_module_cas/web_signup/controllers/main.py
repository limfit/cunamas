# coding: utf-8
import openerp
import logging
from openerp.http import request

_logger = logging.getLogger(__name__)


class AuthSignupHome(openerp.addons.web.controllers.main.Home):
    def do_signup(self, qcontext):
        values = dict((key, qcontext.get(key)) for key in
                      ('login', 'name', 'password', 'partner_document_number',
                       'usr_firstname', 'usr_secondname'))
        assert \
            any([k for k in values.values()]), \
            "The form was not properly filled in."
        assert values.get('password') == qcontext.get('confirm_password'), \
            "Passwords do not match; please retype them."
        values['lang'] = request.lang
        values['partner_email'] = values.get('login')
        values['document_number'] = values.get('partner_document_number')
        values['partner_name'] = values.get('partner_name')
        values['partner_name'] = values.get('name')
        values['partner_firstname'] = values.get('usr_firstname')
        values['partner_secondname'] = values.get('usr_secondname')
        values['firstname'] = values.get('name')
        self._signup_with_values(qcontext.get('token'), values)
        request.cr.commit()
