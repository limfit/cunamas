odoo.define('hr_user.user_frontend', function (require) {
    "use strict";

    var ajax = require('web.ajax');
    var Dialog = require("web.Dialog");
    var core = require('web.core');
    var Widget = require('web.Widget');
    var session = require('web.session');
    var base = require('web_editor.base');
    var utils = require('web.utils');
    var Model = require('web.Model');
    var qweb = core.qweb;
    var _t = core._t;
    base.url_translations = '/website/translations';
    const model_country = new Model('res.country');
    const model_country_state = new Model('res.country.state');
    const model_country_province = new Model('res.country.province');
    const model_country_district = new Model('res.country.district');
    const model_partner_study = new Model('res.partner.study');
    const model_partner_training = new Model('res.partner.training');
    const model_applicant = new Model('hr.applicant');
    const model_partner_work_experience = new Model('res.partner.work.experience');

    const image_accept_extension = ['png', 'jpeg', 'PNG', 'JPEG', 'jpg', 'JPG'];

    const pncm = {
        validityAccepExtension: function(fileInput, contentAlerts){
            var strExtensions = $(fileInput).attr('extensions');
            if(strExtensions != undefined && strExtensions != null &&
            strExtensions != ''){
                var accepExtensions = strExtensions.split(',');
                var filePath = fileInput.files[0].name;
                var indexLast = filePath.lastIndexOf('.');
                var extension = filePath.substring(indexLast + 1, filePath.length).trim();
                if(!_.contains(accepExtensions, extension)){
                    $(fileInput).val(null);
                    showAlert('Advertencia:', 'Sole puede elegir archivos con las extensiones: '
                        + accepExtensions.toString(), 'warning', 2500, contentAlerts);
                    return false;
                }
            }
            return true;
        }
    };

    const view_modal = function(width, title){
        return
            '<div class="modal fade modalForm" role="dialog">' +
            '<div class="modal-dialog" style="width:'+width+'px; max-height: 700px;" role="document">' +
            '<div class="modal-content" style="width: auto;">' +
            '<div class="modal-header">' +
            '<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>' +
            '<div class="modal-title">' +
            '<h4>'+title+'</h4>' +
            '</div></div><div class="modal-body app-modal" style="width: auto;"></div></div></div></div>';
    }

    const remove_modals = function(){
        var modals = $('.modalForm');
        $.each(modals, function (index, element) {
            $(element).remove();
        });
    }

    const upload_file = function(form, callback){
        $.ajax({
            url: form.attr('action'),
            type: 'POST',
            data: new FormData(form[0]),
            cache: false,
            contentType: false,
            processData: false,
            success: function(response){
                response = JSON.parse(response);
                if(response.success){
                    if(callback != undefined){
                        callback(response);
                    }
                }
                else{
                    alert('Ocurrio un error al subir el archivo, contacte con el administrador');
                }
            },
            error: function(request){
                alert('Ocurrio un error en la aplicación, contacte a la unidad de Gestión del Talento del PNCM');
            }
        });
    }



    var get_selected_model = function(){
        if(document.getElementById('btn_save_study') != null){
            return model_partner_study;
        }
        if(document.getElementById('btn_save_training') != null){
            return model_partner_training;
        }
        if(document.getElementById('btn_save_experience') != null){
            return model_partner_work_experience;
        }
        if($('#in_applicant_view').length > 0){
            return model_applicant;
        }
    }

    var handleFileSelect = function (evt) {
        var files = evt.target.files;
        for (var i = 0, f; f = files[i]; i++) {
            if (!f.type.match('image.*')) {
                continue;
            }
            var reader = new FileReader();
            reader.onload = (function(theFile) {
                return function(e) {
                    $('#avatar').attr("src", e.target.result);
                };
            })(f);
            reader.readAsDataURL(f);
        }
    }

    var append_option = function (element, row, name){
        $(element).append($('<option>', {
            id: row.id,
            value: row.id,
            text : row.name,
            name: name
        }));
    }

    var result_append_option = function (element, result, attr){
        result.map(function(row){
            if ($('option[id="' + row.id + '"]').length == 0){
                append_option(element, row, attr);
            }
        });
    }
    var showAlert = function (title,message, type, closeDelay, contentAlerts, contentAlertsId) {

        if(contentAlerts == undefined){
            if ($("#alerts-container").length == 0) {
                $("body")
                    .append( $('<div id="alerts-container" style="position: fixed; width: 20%; right: 5%; top: 10%; z-index: 999">'
                     ));
            }
        }

        type = type || "info";

        var alert = $('<div class="alert alert-' + type + ' fade in" style="z-index: 999">')
            .append(
                $('<button type="button" class="close" data-dismiss="alert" aria-label="Close">')
                .append("&times;")
            )
            .append('<h4 class="alert-heading">'+ title +'</h4>')
            .append(message);

        if(contentAlerts == undefined){
            $("#alerts-container").prepend(alert);
        }
        else{
            contentAlerts.html(alert);
        }

        if (closeDelay)
            window.setTimeout(function() { alert.alert("close") }, closeDelay);
    }

    var bootstrap_confirm_delete = function (element, options ){
        this.element = $(element);
        this.settings = $.extend({
            debug: false,
            heading: 'Eliminar',
            form: $('<p id="bootstrap-confirm-dialog-text">¿Está seguro que  desea eliminar el registro?</p>'),
            btn_ok_label: 'Confirmar',
            btn_cancel_label: 'Cancelar',
            data_type: null,
            callback: null,
            delete_callback: null,
            cancel_callback: null
            }, options || {});

        this.onDelete = function(event){
            //event.preventDefault();
            var plugin = $(this).data('bootstrap_confirm_delete');
            if (undefined !== $( this ).attr( 'data-type')){
                var name = $( this ).attr( 'data-type' );
                plugin.settings.heading = 'Eliminar ' + name[ 0 ].toUpperCase() + name.substr( 1 );
                plugin.settings.message = '¿Está seguro que desea eliminar esto  ' + name + '?';
            }

            $('#bootstrap-confirm-edit-container').remove();
            $('#bootstrap-confirm-delete-container').remove();
            var div='';
            div += '<div id="bootstrap-confirm-delete-container">';
            div +=  '<div id="bootstrap-confirm-dialog" class="modal fade">';
            div +=      '<div class="modal-dialog modal-sm">';
            div +=          '<div class="modal-content" style="width: 500px;">';
            div +=              '<div class="modal-header">';
            div +=                  '<button type="button" class="close" data-dismiss="modal">';
            div +=                      '<span aria-hidden="true">&times;</span>';
            div +=                      '<span class="sr-only">Close</span>';
            div +=                  '</button>';
            div +=                  '<h4 id="bootstrap-confirm-dialog-heading"></h4>';
            div +=               '</div>';
            div +=               '<div class="modal-body">';
            div +=	                  plugin.settings.form.html()
            div +=                '</div>';
            div +=                  '<div class="modal-footer">';
            div +=	                    '<button id="bootstrap-confirm-dialog-cancel-delete-btn" type="button" class="btn btn-default pull-left" data-dismiss="modal">Cancel</button>';
            div +=	                    '<a id="bootstrap-confirm-dialog-delete-btn" href="#" class="btn btn-danger pull-right">Delete</a>';
            div +=                  '</div>';
            div +=              '</div>';
            div +=          '</div>';
            div +=      '</div>';
            div += '</div>';
            $('body').append(div);
            $('#bootstrap-confirm-dialog-heading').html(plugin.settings.heading);
            $('#bootstrap-confirm-dialog-delete-btn').html(plugin.settings.btn_ok_label);
            $('#bootstrap-confirm-dialog-cancel-delete-btn').html(plugin.settings.btn_cancel_label);
            $('#bootstrap-confirm-dialog').modal('toggle');

            var deleteBtn = $('a#bootstrap-confirm-dialog-delete-btn');
            var cancelBtn = $('a#bootstrap-confirm-dialog-cancel-delete-btn');
            var hasCallback = false;
            if (null !== plugin.settings.callback){
                if($.isFunction( plugin.settings.callback)){
                    deleteBtn.attr( 'data-dismiss', 'modal' ).off('.bs-confirm-delete').on( 'click.bs-confirm-delete', { originalObject: $( this ) }, plugin.settings.callback );
                    hasCallback = true;
                }
                else{
                    console.log( plugin.settings.callback + ' is not a valid callback' );
                }
            }
            if(null !== plugin.settings.delete_callback){
                if ($.isFunction( plugin.settings.delete_callback)){
                    deleteBtn.attr('data-dismiss', 'modal').off('.bs-confirm-delete').on( 'click.bs-confirm-delete', { originalObject: $( this ) }, plugin.settings.delete_callback );
                    hasCallback = true;
                }
                else{
                    console.log( plugin.settings.delete_callback + ' is not a valid callback' );
                }
            }
            if(!hasCallback &&  '' !== event.currentTarget.href ){
                deleteBtn.attr('href', event.currentTarget.href);
            }
            if(null !== plugin.settings.cancel_callback){
                cancelBtn.off('.bs-confirm-delete').on( 'click.bs-confirm-delete', {originalObject: $(this)}, plugin.settings.cancel_callback );
            }
        };
    };

    $.fn.bootstrap_confirm_delete = function (options){
        return this.each(function (){
            var element = $(this);
            if(element.data('bootstrap_confirm_delete')){
                return element.data('bootstrap_confirm_delete');
            }
            var plugin = new bootstrap_confirm_delete(this, options);
            element.data('bootstrap_confirm_delete', plugin);
            element.off('.bs-confirm-delete').on('click.bs-confirm-delete', plugin.onDelete);
            return plugin;
        } );
    };

    var bootstrap_confirm_edit = function (element, options, form){
        this.element = $(element);
        this.settings = $.extend({
            debug: false,
            heading: 'Editar',
            message: '',
            btn_ok_label: 'Confirmar',
            btn_cancel_label: 'Cancelar',
            data_type: null,
            callback: null,
            delete_callback: null,
            cancel_callback: null,
            form: form,
            }, options || {});

        this.onEdit = function(event){
            event.preventDefault();
            var plugin = $(this).data('bootstrap_confirm_edit');
            if (undefined !== $( this ).attr( 'data-type')){
                var name = $( this ).attr( 'data-type' );
                plugin.settings.heading = 'Editar ' + name[ 0 ].toUpperCase() + name.substr( 1 );
                plugin.settings.message = '¿Está seguro desea editar esto ' + name + '?';
            }
            // console.log(qweb.render("dialog_delete"));
            var div ='';
            $('#bootstrap-confirm-delete-container').remove();
            $('#bootstrap-confirm-edit-container').remove();
            div += '<div id="bootstrap-confirm-edit-container">';
            div +=  '<div id="bootstrap-confirm-dialog" class="modal fade">';
            div +=      '<div class="modal-dialog modal-sm">';
            div +=          '<div class="modal-content" style="width: 500px;">';
            div +=              '<div class="modal-header">';
            div +=                  '<button type="button" class="close" data-dismiss="modal">';
            div +=                      '<span aria-hidden="true">&times;</span>';
            div +=                      '<span class="sr-only">Close</span>';
            div +=                  '</button>';
            div +=                  '<h4 id="bootstrap-confirm-dialog-heading"></h4>';
            div +=                 '</div>';
            div +=                 '<div class="modal-body">';
            div +=	                   form.html();
            div +=                 '</div>';
            div +=              '</div>';
            div +=          '</div>';
            div +=      '</div>';
            div += '</div>';
            $('body').append(div);
            $('#bootstrap-confirm-dialog-heading').html(plugin.settings.heading);
            $('#bootstrap-confirm-dialog').modal('toggle');
            $(".modal-body").find("#btn_save_study, #btn_save_training, #btn_save_experience").before("<input type='button' value='CANCELAR' style='margin-right:60px;' class='btn btn-default'>");

            is_career = form.find('#is_career');
            if (is_career.length){
                $(".modal-body").find('#is_career').prop("checked", is_career.prop('checked'));
            }
            partner_country = $(".modal-body").find("#partner_country");
            if (partner_country.length){
                partner_country.on('click', function(){
                    model_country.call("get_record").then(function(result) {
                        result_append_option(partner_country, result, 'country_id');
                    });
                });
                partner_state = $(".modal-body").find("#partner_state");
                if (partner_state.length){
                    var state_obj;
                    partner_state.on('click', function(){
                        if (partner_country.find(":selected").length != 0){
                            state_obj = parseInt(partner_country.find(":selected").val());
                        }
                        model_country_state.call("get_record", [state_obj]).then(function(result) {
                            result_append_option(partner_state, result, 'state_id');
                        });
                    });
                }
            }

            var deleteBtn = $('a#bootstrap-confirm-dialog-delete-btn');
            var cancelBtn = $('a#bootstrap-confirm-dialog-cancel-delete-btn');
            var hasCallback = false;

            if (null !== plugin.settings.callback){
                if($.isFunction( plugin.settings.callback)){
                    deleteBtn.attr( 'data-dismiss', 'modal' ).off('.bs-confirm-edit').on( 'click.bs-confirm-edit', { originalObject: $(this) }, plugin.settings.callback );
                    hasCallback = true;
                }
                else{
                    console.log( plugin.settings.callback + ' is not a valid callback' );
                }
            }
            if(null !== plugin.settings.delete_callback){
                if ($.isFunction( plugin.settings.delete_callback)){
                    deleteBtn.attr('data-dismiss', 'modal').off('.bs-confirm-edit').on( 'click.bs-confirm-edit', { originalObject: $(this) }, plugin.settings.delete_callback );
                    hasCallback = true;
                }
                else{
                    console.log( plugin.settings.delete_callback + ' is not a valid callback' );
                }
            }
            if(!hasCallback &&  '' !== event.currentTarget.href ){
                deleteBtn.attr('href', event.currentTarget.href);
            }
            if(null !== plugin.settings.cancel_callback){
                cancelBtn.off('.bs-confirm-edit').on( 'click.bs-confirm-edit', {originalObject: $(this)}, plugin.settings.cancel_callback );
            }
        };
    };

    $.fn.bootstrap_confirm_edit = function (options, form){
        return this.each(function (){
            var element = $(this);
            if(element.data('bootstrap_confirm_edit')){
                return element.data('bootstrap_confirm_edit');
            }
            var plugin = new bootstrap_confirm_edit(this, options, form);
            element.data('bootstrap_confirm_edit', plugin);
            element.off('.bs-confirm-edit').on('click.bs-confirm-edit', plugin.onEdit);
            return plugin;
        } );
    };

    $(document).ready( function(){
        var model_selected;
        var self = this;
        var country_obj =0, state_obj=0 , province_obj=0;

        var dtToday = new Date();
        var month = dtToday.getMonth() + 1;
        var day = dtToday.getDate();
        var year = dtToday.getFullYear();
        if(month < 10)
            month = '0' + month.toString();
        if(day < 10)
            day = '0' + day.toString();
        var maxDate = year + '-' + month + '-' + day;
        $('#partner_birthdate').attr('max', maxDate);

        if(document.getElementById('files') != null){
            document.getElementById('files').addEventListener('change', handleFileSelect, false);
        }

        if (document.getElementById('partner_country') != null){
            $('#partner_country').on('click', function(){
                model_country.call("get_record").then(function(result) {
                    result_append_option('#partner_country', result, 'country_id');
                });
            });
            $('#partner_country').on('change', function(){
                if ($("#partner_state").children().length != 0){
                        $("#partner_state").children().each(function(){
                            $(this).remove();
                        });
                    }
            });
        }
        $('#time').keypress(function(event){
            if (event.which >= 48 && event.which <= 57) {
                if(parseInt($('#time').val() +''+event.key )<=10000){
                    return true;
                }
                return false;
            }
            else if (event.which === 8) {
                var numbertmp =$('#time').val();
                $('#time').val(numbertmp.substring(0,numbertmp.length-1));
                return true;
            }
            return false;
        });

        if (document.getElementById('partner_state') != null){
            $('#partner_state').on('click', function(){
                if ($("#partner_country").find(":selected").length != 0){
                    state_obj = parseInt($("#partner_country").find(":selected").val());
                }
                model_country_state.call("get_state", [state_obj]).then(function(result) {
                    result_append_option('#partner_state', result, 'state_id');
                });
            });
            $('#partner_state').on('change', function(){
                if ($("#partner_province").children().length != 0){
                        $("#partner_province").children().each(function(){
                            $(this).remove();
                        });
                    }
                    if ($("#partner_district").children().length != 0){
                        $("#partner_district").children().each(function(){
                            $(this).remove();
                        });
                    }
             });
         }
        if(document.getElementById('partner_province') != null) {
            $('#partner_province').on('click', function(){
                if( $("#partner_state").val()!=null){
                    state_obj = parseInt($("#partner_state").find(":selected").val());
                    model_country_state.call("get_province", [state_obj]).then(function(result) {
                        result_append_option('#partner_province', result, 'province_id');
                    });
                    $('#partner_province').on('change', function(){
                        if ($("#partner_district").children().length != 0){
                            $("#partner_district").children().each(function(){
                                $(this).remove();
                            });
                        }
                    });
                }
            });
        }
        if(document.getElementById('partner_district') != null) {
            $('#partner_district').on('click', function(){
               if( $("#partner_state").val()!=null && $("#partner_province").val()!=null){
                    province_obj = parseInt($("#partner_province").find(":selected").val());
                    model_country_state.call("get_district", [province_obj]).then(function(result) {
                        result.map(function(row){
                           if ($('#partner_district').children().length == 0){
                             append_option('#partner_district', row, 'district_id');
                           }
                           else if ($('option[id="' + row.id + '"]').length == 0){
                                append_option('#partner_district', row, 'district_id');
                            }
                        });
                    });
                }
            });
        }

        if(document.getElementById('btn_save_study') != null){
             $('#user_form_study').submit(function(evt){
                if($('input[name=data_send]').length == 0){
                    $('#btn_save_study').after("<input type='hidden' value='data' name='data_send'>");
                    evt.preventDefault();
                }
                else{
                    return true;
                }
             });

             $('#btn_save_study').click(function(){
                $('#user_form_study').off('submit.blocker').trigger('submit');
                $('#user_form_study').submit();
             });
        }


        if(document.getElementById('btn_save_training') != null){
             $('#user_form_training').submit(function(evt){
                if($('input[name=data_send]').length == 0){
                    $('#btn_save_training').after("<input type='hidden' value='data' name='data_send'>");
                    evt.preventDefault();
                }
                else{
                    return true;
                }
             });

             $('#btn_save_training').click(function(){
                if(!($('#date_end').val()>= $('#date_start').val())){
                    showAlert('Advertencia:','La Fecha Fin de Capacitación debe ser mayor a su fecha de inicio.','warning',2500)
                    evt.preventDefault();
                }else{
                    $('#user_form_training').off('submit.blocker').trigger('submit');
                    $('#user_form_training').submit();
                }
             });
        }


        if(document.getElementById('btn_save_experience') != null){
             if($("#partner_country").find("option[value='" + $("#partner_country").val() + "']").text()=='Perú'){
                $('#partner_state').attr('required', true);

             }else{
                $('#partner_state').removeAttr('required');
             }
             $('#enlable_labor').change(function(evt){
                var value = $(this).prop('checked');
                if(value){
                    $('#date_end').val(null);
                    $('#date_end').prop('readonly', true);
                    $('#date_end').attr('required', true);
                }
                else{
                    $('#date_end').prop('readonly', false);
                    $('#date_end').removeAttr('required');
                }
             });
             $('#user_form_experience').submit(function(evt){
                if($('input[name=data_send]').length == 0){
                   $('#btn_save_experience').after("<input type='hidden' value='data' name='data_send'>");
                   evt.preventDefault();
                }
                else{
                    return true;
                }
             });

             $('#btn_save_experience').click(function(){
                if(!$('#enlable_labor').prop('checked') && !($('#date_end').val()>= $('#date_start').val())){
                    showAlert('Advertencia:','La Fecha de Termino Laboral debe ser mayor a su fecha de inicio.','warning',2500)
                    evt.preventDefault();
                }
                else{
                    var funcionesPrincipales = $('#description').val();
                    if(funcionesPrincipales == undefined  || funcionesPrincipales == null || funcionesPrincipales.trim() == ''){
                        $('#description').val('');
                        showAlert('Advertencia:','Ingrese principales funciones','warning',2000)
                    }
                    else{
                        $('#user_form_experience').off('submit.blocker').trigger('submit');
                        $('#user_form_experience').submit();
                    }
                }
             });
        }

        if($('#in_applicant_view').length > 0){
            $('.column_adjuntar_documento').click(function(evt){
                remove_modals();
                var convocatoria = $(this).attr('convocatoria');
                var cargo = $(this).attr('cargo');
                var completo = $(this).attr('completo');
                if(completo == undefined) completo = false;
                $.ajax({
                    url: '../my/applicants/documents',
                    type: 'POST',
                    data: {
                        id: $(this).attr('id'),
                        cargo: cargo,
                        job_id: $(this).attr('job_id'),
                        convocatoria: convocatoria,
                        completo: completo
                    },
                    success: function(response){
                        var new_modal = $(view_modal(1150, convocatoria +
                        ': ' + cargo));
                        new_modal.find('.modal-body').html(response);
                        new_modal.modal({
                            cache: false
                        });

                        new_modal.on('hide.bs.modal', function(){
                            var applicant_id = $('#applicant_id_hidden').val();
                            var completo = $('#applicant_completo_hidden').val();
                            if(!completo || completo == 'false' || completo == 'False'){
                                $.ajax({
                                    url: '../my/applicants/documents/complete',
                                    type: 'POST',
                                    data: {id: applicant_id},
                                    success: function(response){
                                        response = JSON.parse(response);
                                        if(response.success){
                                            if(response.completo){
                                                var link_applicant = $('#user_applicants_list_table').find('a[id='+applicant_id+']');
                                                link_applicant.attr('completo', 'true');
                                                link_applicant.html('Completado');
                                                link_applicant.removeClass('alert-danger');
                                                link_applicant.addClass('alert-success');
                                            }
                                        }
                                        else{
                                            alert('Ocurrio un error en la aplicación, contacte a la unidad de Gestión del Talento del PNCM');
                                        }
                                    },
                                    error: function(request){
                                        alert('Ocurrio un error en la aplicación, contacte a la unidad de Gestión del Talento del PNCM');
                                    }
                                });
                            }
                        });

                        var on_change_file_document = function(inputfile){
                            var form = $(inputfile).closest('form');
                            upload_file(form, function(response){
                                console.log('deberia entrar form');
                                var link = form.find('a');
                                link.attr('href', response.url);
                                link.css('display', 'inline');
                            });
                        }

                        new_modal.on('shown.bs.modal', function() {
                            $('.form-study').find('.imageFile2mB').change(function(evt) {
                                if(pncm.validityAccepExtension(this, new_modal.find('.modal-body').find('.alert-container-upload'))){
                                    if(this.files[0]!=undefined){
                                        if (this.files[0].size > 2000000){
                                            var message= 'El archivo supera el tamaño máximo permitido de 2MB';
                                            showAlert('Advertencia:',message,'warning',2500, new_modal.find('.modal-body').find('.alert-container-upload'));
                                            $(this).val(null)
                                            evt.preventDefault();
                                        }
                                        else{
                                            on_change_file_document(this);
                                        }
                                     }
                                }
                            });
                            $('a[data-toggle="tab"]').click(function(e) {
                                var target = $(e.target).attr("href");
                                if (target === '#capacitaciones') {
                                    $.ajax({
                                        url: '../my/applicants/documents/trainings',
                                        type: 'POST',
                                        data: {
                                            id: $('#applicant_id_hidden').val(),
                                            job_id: $('#job_id_hidden').val()
                                        },
                                        success: function(response){
                                            $('#capacitaciones').html(response);
                                            $('.form-training').find('.imageFile2mB').change(function(evt) {
                                                if(pncm.validityAccepExtension(this, new_modal.find('.modal-body').find('.alert-container-upload'))){
                                                    if(this.files[0]!=undefined){
                                                        if (this.files[0].size > 2000000){
                                                            var message= 'El archivo supera el tamaño máximo permitido de 2MB';
                                                            showAlert('Advertencia:',message,'warning',2500, new_modal.find('.modal-body').find('.alert-container-upload'));
                                                            $(this).val(null)
                                                            evt.preventDefault();
                                                        }
                                                        else{
                                                            on_change_file_document(this);
                                                        }
                                                     }
                                                }
                                            });
                                        },
                                        error: function(request){
                                            alert('Ocurrio un error en la aplicación, contacte a la unidad de Gestión del Talento del PNCM');
                                        }
                                    });
                                }
                                else if (target === '#experiencia') {
                                    $.ajax({
                                        url: '../my/applicants/documents/works',
                                        type: 'POST',
                                        data: {
                                            id: $('#applicant_id_hidden').val(),
                                            job_id: $('#job_id_hidden').val()
                                        },
                                        success: function(response){
                                            $('#experiencia').html(response);
                                            $('.form-work').find('.imageFile2mB').change(function(evt) {
                                                if(pncm.validityAccepExtension(this, new_modal.find('.modal-body').find('.alert-container-upload'))){
                                                    if(this.files[0]!=undefined){
                                                        if (this.files[0].size > 2000000){
                                                            var message= 'El archivo supera el tamaño máximo permitido de 2MB';
                                                            showAlert('Advertencia:',message,'warning',2500, new_modal.find('.modal-body').find('.alert-container-upload'));
                                                            $(this).val(null)
                                                            evt.preventDefault();
                                                        }
                                                        else{
                                                            on_change_file_document(this);
                                                        }
                                                     }
                                                }
                                            });
                                        },
                                        error: function(request){
                                            alert('Ocurrio un error en la aplicación, contacte a la unidad de Gestión del Talento del PNCM');
                                        }
                                    });
                                }
                                return;
                            });
                        });
                    },
                    error: function(request){
                        alert('Ocurrio un error en la aplicación, contacte a la unidad de Gestión del Talento del PNCM');
                    }
                });
            });
        }

        setTimeout(function() {
            $('#es_colegiado_habilitado_ck').click(function() {
                if($(this).is(':checked'))
                    $('.es_colegiado_habilitado_ck').show();
                else{
                    $('.es_colegiado_habilitado_ck').hide();
                    $(this).value=None;
                }
            });

            $('#es_discapacitado_ck').click(function() {
                if($(this).is(':checked'))
                    $('.es_discapacitado_ck').show();
                else{
                    $('.es_discapacitado_ck').hide();
                    $(this).value=None;
                }
            });

            $('#is_studing').click(function() {
                if($(this).is(':checked'))
                    $('#date').prop('readonly', true);
                else{
                    $('#date').prop('readonly', false);
                }
            });

            $('#es_de_fuerzas_armadas_ck').click(function() {
                if($(this).is(':checked'))
                    $('.es_de_fuerzas_armadas_ck').show();
                else{
                    $('.es_de_fuerzas_armadas_ck').hide();
                    $(this).value=None;
                }
            });

            $('.delete-row-default').on('click', function(){
                $(this).bootstrap_confirm_delete({
                    callback: function(event){
                        var link = event.data.originalObject;
                           link.closest('tr').remove();
                           model_selected = get_selected_model();
                           model_selected.call("delete_record", [parseInt(link.context.id)]);
                    }
                });
            });

            $('.edit-row-default').each(function(){
                $(this).on('click', function(){
                    const  id  =  parseInt($(this).context.id);
                    var form, res;
                    if (model_selected.name == 'res.partner.study'){
                        form = $('#user_form_study').clone();
                        model_selected.call("get_record", [id]).then(function(result){
                            res = result[0];
                            $(form).find('#career').attr("value", res.career);
                            $(form).find('#specialty').attr("value", res.specialty);
                            $(form).find('#is_career').prop("checked", res.is_career);
                            $(form).find('#date').attr("value", res.date);
                            $(form).find('#university').attr("value", res.university);
                            $(form).find('#partner_state').attr("value", res.state);
                            $(form).find('#partner_country').attr("value", res.country);
                            $(form).find('#btn_save_study').after("<input type='hidden' value='"+res.id+"' name='edit_id'>");
                        });
                    }
                    if (model_selected.name == 'res.partner.training'){
                        form = $('#user_form_training').clone();
                        model_selected.call("get_record", [id]).then(function(result){
                            res = result[0];
                            $(form).find('#name').attr("value", res.name);
                            $(form).find('#date_start').attr("value", res.date_start);
                            $(form).find('#date_end').attr("value", res.date_end);
                            $(form).find('#institution').attr("value", res.institution);
                            $(form).find('#partner_state').attr("value", res.state);
                            $(form).find('#partner_country').attr("value", res.country);
                            $(form).find('#btn_save_training').after("<input type='hidden' value='"+res.id+"' name='edit_id'>");
                        });
                    }
                    if(model_selected.name == 'res.partner.work.experience'){
                        form = $('#user_form_experience').clone();
                        model_selected.call("get_record", [id]).then(function(result){
                            res = result[0];
                            $(form).find('#name').attr("value", res.name);
                            $(form).find('#date_start').attr("value", res.date_start);
                            $(form).find('#date_end').attr("value", res.date_end);
                            $(form).find('#charge').attr("value", res.charge);
                            $(form).find('#description').attr("value", res.description);
                            $(form).find('#btn_save_experience').after("<input type='hidden' value='"+res.id+"' name='edit_id'>");
                        });
                    }
                    $(this).bootstrap_confirm_edit({
                        callback: function(event){
                            var link = event.data.originalObject;
                        }
                    }, form);
                });
            });

        }, 500);
        $('.numbersOnly').keypress(function (e) {
            var valueKey = parseInt(e.key);
            if(valueKey >= 0 && valueKey <= 9){
                return true;
            }
            return false;
        });

        $('.imageFile').change(function(evt) {
            if(pncm.validityAccepExtension(this)){
                if(this.files[0]!=undefined)
                    if (this.files[0].size > 1000000){
                        var message= 'El archivo supera el tamaño máximo permitido de 1MB';
                        showAlert('Advertencia:',message,'warning',2500);
                        $(this).val(null)
                        evt.preventDefault();
                    }
            }
        });

        $('.funcionesPrincipales').click(function(){
            console.log(this);
            console.log($(this).attr('description'));
            remove_modals();
            var new_modal = $(view_modal(600, 'Funciones Principales'));
            console.log($(this).attr('description'));
            new_modal.find('.modal-body').html($(this).attr('description'));
            new_modal.modal({
                cache: false
            });
        });

        $('.imageFile2mB').change(function(evt) {
            if(pncm.validityAccepExtension(this)){
                if(this.files[0]!=undefined)
                    if (this.files[0].size > 2000000){
                        var message= 'El archivo supera el tamaño máximo permitido de 2MB';
                        showAlert('Advertencia:',message,'warning',2500);
                        $(this).val(null)
                        evt.preventDefault();
                    }
            }
        });

        $('.imageDoc').change(function(evt) {
            if(this.files[0]!=undefined)
                if (this.files[0].size > 1000000){
                    var message= 'El archivo supera el tamaño máximo permitido de 1MB';
                    showAlert('Advertencia:',message,'warning',2500);
                    $(this).val(null)
                    evt.preventDefault();
                }
        });

        var max_chars = 500;
        $('#max').html(max_chars);
        $('#description').keyup(function() {
            var chars = $(this).val().length;
            var diff = max_chars - chars;
            $('#contador').html(diff);
        });


        $('#btn_save_profile').on('click', function (event){
            var message='';
            if($('#partner_document_number').val().length==0){
                message+='<br>- Debe ingresar un Número de documento de identidad.';
                event.preventDefault();
            }
            if($('#partner_document_number').val().length>0 ){
                if(! $.isNumeric($('#partner_document_number').val())){
                    message+='<br>- Sólo se permiten Números en el documento de identidad.';
                    event.preventDefault();
                }
            }
            var datebirth =$('#partner_birthdate').val().split('-');
            var fecNac =new Date(datebirth[0],datebirth[1]-1,datebirth[2]).setHours(0,0,0,0);
            var fecNow =new Date().setHours(0,0,0,0);
            if(fecNac>fecNow){
                message+='<br>- Fecha de Nacimiento Incorrecta.';
                event.preventDefault();
            }
            if ($('#partner_vat').val().length>0 )
            {
                if($('#partner_vat').val().trim().length!=11){
                    message+='<br>- El Ruc debe tener 11 dígitos.';
                    event.preventDefault();
                }
                if(!$.isNumeric($('#partner_vat').val())){
                    message+='<br>- Sólo se permiten Números en el campo Ruc.';
                    event.preventDefault();
                }
            }
            if($('[name=partner_phone]').val().length>0){
                if(! $.isNumeric($('#partner_phone').val())){
                message+='<br>- Sólo se permiten Números en el campo teléfono.';
                event.preventDefault();
                }
            }
            if($('#partner_sex').val()==""){
                message+='<br>- El campo Sexo es un dato obligatorio.';
                event.preventDefault();
            }
            if($('#partner_civil').val()==""){
                message+='<br>- El campo Estado Civil es un dato obligatorio.';
                event.preventDefault();
            }
            if(message.length>0){
               showAlert('Advertencia:',message,'warning',2500)
            }
        });
    });
});