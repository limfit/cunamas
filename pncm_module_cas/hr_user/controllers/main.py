# coding: utf-8

from odoo import http, fields
from odoo.http import request
import base64
import os
import errno
import json
from datetime import datetime


class ControllerHrUser(http.Controller):
    _path_applicant = 'applicant'

    @http.route(
        route='/my/home',
        type='http',
        auth='user',
        website=True,
        csrf=False
    )
    def render_profile(self, **post):
        if post:
            data = {key: post[key] for key in post if post[key]}
            path_os = http.request.env[
                'ir.config_parameter'].get_param(
                'path.filestatic')
            path_http = http.request.env[
                'ir.config_parameter'].get_param(
                'path.filestatic.http')
            usuario = http.request.env['res.users'].browse(
                http.request.uid
            )
            if data.get('date', False):
                if data.get('partner_birthdate', False) \
                        and data.get('date').find('/') is 2:
                    data.update(
                        {
                            'partner_birthdate':
                                '-'.join(
                                    data.get('date').split('/')[::-1]
                                )
                        }
                    )
            if data.get('state_id', False):
                data.update({'state_id': int(data.get('state_id'))})
            if data.get('province_id', False):
                data.update({'province_id': int(data.get('province_id'))})
            if data.get('district_id', False):
                data.update({'district_id': int(data.get('district_id'))})
            if data.get('partner_image', False):
                data.update(
                    {
                        'image':
                            base64.encodestring(post['partner_image'].read())
                    }
                )
            if data.get('dni_file', False):
                format_name_file = datetime.now().strftime(
                    "%Y%m%d%H%M%S%f"
                )
                name_file = ('dni_' + format_name_file
                             ) + os.path.splitext(
                    post.get('dni_file').filename
                )[1]
                path_url = '/' + str(request.env.user.id) + '/' + name_file
                path = path_os + path_url
                if not os.path.exists(os.path.dirname(path)):
                    try:
                        os.makedirs(os.path.dirname(path))
                    except OSError as exc:  # Guard against race condition
                        if exc.errno != errno.EEXIST:
                            raise
                with open(path, 'w') as f:
                    f.write(post['dni_file'].read())
                f.close()

                if usuario and usuario.file_name_dni:
                    file_name_dni_ant = path_os + '/' + str(
                        request.env.user.id
                    ) + '/' + usuario.file_name_dni

                    if file_name_dni_ant:
                        if os.path.isfile(file_name_dni_ant):
                            try:
                                os.remove(file_name_dni_ant)
                            except OSError:
                                raise

                data.update({'file_url_dni': path_http + path_url})
                data.update({'file_name_dni': name_file})
                data.update({'file_dni': False})

            if data.get('fuerzas_armadas_file', False):
                format_name_file = datetime.now().strftime(
                    "%Y%m%d%H%M%S%f"
                )
                name_file = (
                                    'fuerzas_armadas_'+format_name_file
                            )+os.path.splitext(
                    post.get('fuerzas_armadas_file').filename)[1]
                path_url = '/' + str(request.env.user.id) + '/' + name_file
                path = path_os + path_url
                if not os.path.exists(os.path.dirname(path)):
                    try:
                        os.makedirs(os.path.dirname(path))
                    except OSError as exc:  # Guard against race condition
                        if exc.errno != errno.EEXIST:
                            raise
                with open(path, 'w') as f:
                    f.write(post['fuerzas_armadas_file'].read())
                f.close()

                if usuario and usuario.file_name_fuerzas_armadas:
                    file_name_fuerzas_armadas_ant = path_os+'/'+str(
                        request.env.user.id
                    )+'/'+usuario.file_name_fuerzas_armadas

                    if file_name_fuerzas_armadas_ant:
                        if os.path.isfile(file_name_fuerzas_armadas_ant):
                            try:
                                os.remove(file_name_fuerzas_armadas_ant)
                            except OSError:
                                raise
                data.update(
                    {'file_url_fuerzas_armadas': path_http + path_url})
                data.update({'file_name_fuerzas_armadas_new': name_file})
            if data.get('file_colegiatura', False):
                format_name_file = datetime.now().strftime(
                    "%Y%m%d%H%M%S%f"
                )
                name_file = (
                                    'colegiatura_'+format_name_file
                            )+os.path.splitext(
                    post.get('file_colegiatura').filename)[1]
                path_url = '/' + str(request.env.user.id) + '/' + name_file
                path = path_os + path_url
                if not os.path.exists(os.path.dirname(path)):
                    try:
                        os.makedirs(os.path.dirname(path))
                    except OSError as exc:  # Guard against race condition
                        if exc.errno != errno.EEXIST:
                            raise
                with open(path, 'w') as f:
                    f.write(post['file_colegiatura'].read())
                f.close()

                if usuario and usuario.file_name_fuerzas_armadas:
                    file_name_fuerzas_armadas_ant = path_os+'/'+str(
                        request.env.user.id
                    )+'/'+usuario.file_name_fuerzas_armadas

                    if file_name_fuerzas_armadas_ant:
                        if os.path.isfile(file_name_fuerzas_armadas_ant):
                            try:
                                os.remove(file_name_fuerzas_armadas_ant)
                            except OSError:
                                raise
                data.update(
                    {'file_url_colegiatura': path_http + path_url})
                data.update({'file_name_colegiatura': name_file})

            if data.get('discapacitado_file', False):
                format_name_file = datetime.now().strftime(
                    "%Y%m%d%H%M%S%f"
                )
                name_file = (
                                    'discapacidad_'+format_name_file
                            )+os.path.splitext(
                    post.get('discapacitado_file').filename)[1]
                path_url = '/'+str(request.env.user.id)+'/'+name_file
                path = path_os + path_url
                if not os.path.exists(os.path.dirname(path)):
                    try:
                        os.makedirs(os.path.dirname(path))
                    except OSError as exc:  # Guard against race condition
                        if exc.errno != errno.EEXIST:
                            raise
                with open(path, 'w') as f:
                    f.write(post['discapacitado_file'].read())
                f.close()

                if usuario and usuario.file_name_discapacitado:
                    file_name_discapacitado_ant = path_os+'/'+str(
                        request.env.user.id
                    )+'/'+usuario.file_name_discapacitado

                    if file_name_discapacitado_ant:
                        if os.path.isfile(file_name_discapacitado_ant):
                            try:
                                os.remove(file_name_discapacitado_ant)
                            except OSError:
                                raise

                data.update({'file_url_discapacitado': path_http + path_url})
                data.update({'file_name_discapacitado_new': name_file})
            if data.get('partner_type_document', False):
                data.update(
                    {
                        'partner_type_document': post['partner_type_document']
                    }
                )
            data.update({
                'enlable_number':
                    True if data.get('enlable_number') == 'on' else False
            })

            http.request.env['res.users']. \
                browse(http.request.uid).sudo().write(data)

            if request:
                return request.redirect('/my/home')

        partner = request.env.user
        profile_complete = False
        if partner._compute_get_profle():
            profile_complete = partner._compute_get_profle()[0]
        state_id = partner.state_id.id
        province_id = partner.province_id.id
        district_id = partner.district_id.id
        values = {
            'error': {},
            'error_message': []
        }
        provinces = district = []
        default_countries = request.env['res.country'] \
            .sudo().search([('code', '=', 'PE')], limit=1)
        states = request.env['res.country.state'].sudo().search([
            ('country_id', '=', default_countries.id),
            ('state_id', '=', False),
            ('province_id', '=', False),
            ('district_id', '=', False)
        ])
        if province_id:
            provinces = request.env['res.country.state'].sudo().search([
                ('country_id', '=', default_countries.id),
                ('state_id', '=', state_id),
                ('province_id', '=', False),
                ('district_id', '=', False)
            ])
        if district_id:
            district = request.env['res.country.state'].sudo().search([
                ('country_id', '=', default_countries.id),
                ('state_id', '=', state_id),
                ('province_id', '=', province_id),
                ('district_id', '=', False)
            ])
        person = request.env.user.partner_id
        values.update({
            'person': person,
            'partner': partner,
            'states': states,
            'provinces': provinces,
            'profile_complete': profile_complete,
            'district': district,
            'has_check_vat': hasattr(
                request.env['res.partner'], 'check_vat'
            ),
        })
        return http.request.render('hr_user.res_users_form', values)

    @http.route(
        route='/my/studies',
        type='http',
        auth='public',
        website=True,
        csrf=False
    )
    def render_studies(self, **post):
        if post:
            data = {key: post[key] for key in post if post[key]}
            data.update({'user_id': http.request.uid})
            data.update({'specialty': data.get('specialty').upper()})
            data.update({'university': data.get('university').upper()})

            if data.get('is_career', False):
                data.update({
                    'is_career':
                        True if data.get('is_career') == 'on' else False
                })
            if data.get('date', False) and data.get('date').find('/') is 2:
                data.update(
                    {'date': '-'.join(data.get('date').split('/')[::-1])}
                )
            if data.get('country_id', False):
                data.update({'country_id': int(data.get('country_id'))})
            if data.get('state_id', False):
                data.update({'state_id': int(data.get('state_id'))})
            if data.get('level_study', False):
                data.update({'level_study': int(data.get('level_study'))})
            if data.get('edit_id', False):
                id = int(data.get('edit_id'))
                del (data['edit_id'])
                http.request.env['res.partner.study'].browse(id).write(data)
            elif data:
                send = data.get('data_send')
                if send:
                    del (data['data_send'])
                    http.request.env['res.partner.study'].create(data)
                    return request.redirect('/my/studies')

        partner = request.env.user
        values = {
            'error': {},
            'error_message': []
        }
        countries = request.env['res.country'].sudo().search([])
        level_study = request.env['hr.employee.education.level'] \
            .sudo().search([])
        default_countries = request.env['res.country'] \
            .sudo().search([('code', '=', 'PE')], limit=1)

        states = request.env['res.country.state'].sudo().search([
            ('country_id', '=', default_countries.id),
            ('state_id', '=', False),
            ('province_id', '=', False)
        ])
        date_now = fields.Date().today()
        person = request.env.user.partner_id
        profile_complete = False
        if partner._compute_get_profle():
            profile_complete = partner._compute_get_profle()[0]

        values.update({
            'partner': partner,
            'levels': level_study,
            'person': person,
            'countries': countries,
            'profile_complete': profile_complete,
            'default_countries': default_countries,
            'date_now': date_now,
            'states': states,
            'has_check_vat': hasattr(request.env['res.partner'], 'check_vat'),

        })
        return http.request.render('hr_user.res_partner_study_form', values)

    @http.route(
        route='/my/training',
        type='http',
        auth='public',
        website=True,
        csrf=False
    )
    def render_training(self, **post):
        if post:
            data = {key: post[key] for key in post if post[key]}
            data.update({'user_id': http.request.uid})
            data.update({'name': data.get('name').upper()})
            data.update({'institution': data.get('institution').upper()})

            if data.get('date_start', False) \
                    and data.get('date_start').find('/') is 2:
                data.update({'date_start': '-'.
                            join(data.get('date_start').split('/')[::-1])})
            if data.get('date_end', False) \
                    and data.get('date_end').find('/') is 2:
                data.update({'date_end': '-'.
                            join(data.get('date_end').split('/')[::-1])})
            if data.get('time', False):
                data.update({'time': float(data.get('time'))})
            if data.get('country_id', False):
                data.update({'country_id': int(data.get('country_id'))})
            if data.get('state_id', False):
                data.update({'state_id': int(data.get('state_id'))})
            if data.get('edit_id', False):
                id = int(data.get('edit_id'))
                del (data['edit_id'])
                http.request.env['res.partner.training'].browse(id).write(data)
            elif data:
                send = data.get('data_send')
                if send:
                    del (data['data_send'])
                    http.request.env['res.partner.training'].create(data)
                    return request.redirect('/my/training')
        partner = request.env.user
        values = {
            'error': {},
            'error_message': []
        }
        countries = request.env['res.country'].sudo().search([])
        default_countries = request.env['res.country'] \
            .sudo().search([('code', '=', 'PE')], limit=1)
        states = request.env['res.country.state'].sudo().search([
            ('country_id', '=', default_countries.id),
            ('state_id', '=', False),
            ('province_id', '=', False)
        ])
        default_countries = request.env['res.country'].sudo() \
            .search([('code', '=', 'PE')], limit=1)
        person = request.env.user.partner_id
        date_now = fields.Date().today()
        profile_complete = False
        if partner._compute_get_profle():
            profile_complete = partner._compute_get_profle()[0]
        values.update({
            'partner': partner,
            'person': person,
            'date_now': date_now,
            'profile_complete': profile_complete,
            'countries': countries,
            'states': states,
            'default_countries': default_countries,
            'has_check_vat': hasattr(request.env['res.partner'], 'check_vat'),

        })
        return http.request.render('hr_user.res_partner_training_form', values)

    @http.route(route='/my/work_experience',
                type='http',
                auth='public',
                website=True,
                csrf=False)
    def render_work_experience(self, **post):
        if post:
            data = {key: post[key] for key in post if post[key]}
            data.update({'user_id': http.request.uid})
            data.update({'name': data.get('name').upper()})
            data.update({'charge': data.get('charge').upper()})
            data.update({'description': data.get('description').upper()})

            if data.get('date_start', False) \
                    and data.get('date_start').find('/') is 2:
                data.update({'date_start': '-'.
                            join(data.get('date_start').split('/')[::-1])})
            if data.get('date_end', False) \
                    and data.get('date_end').find('/') is 2:
                data.update({'date_end': '-'.
                            join(data.get('date_end').split('/')[::-1])})
            if data.get('edit_id', False):
                id = int(data.get('edit_id'))
                del (data['edit_id'])
                http.request.env['res.partner.work.experience']. \
                    browse(id).write(data)
            elif data:
                send = data.get('data_send')
                if send:
                    del (data['data_send'])
                    http.request.env['res.partner.work.experience']. \
                        create(data)
                    return request.redirect('/my/work_experience')
        partner = request.env.user
        values = {
            'error': {},
            'error_message': []
        }
        countries = request.env['res.country'].sudo().search([])
        default_countries = request.env['res.country'] \
            .sudo().search([('code', '=', 'PE')], limit=1)
        states = request.env['res.country.state'].sudo().search([
            ('country_id', '=', default_countries.id),
            ('state_id', '=', False),
            ('province_id', '=', False)
        ])
        person = request.env.user.partner_id
        date_now = fields.Date().today()
        profile_complete = False
        if partner._compute_get_profle():
            profile_complete = partner._compute_get_profle()[0]
        values.update({
            'person': person,
            'partner': partner,
            'countries': countries,
            'date_now': date_now,
            'profile_complete': profile_complete,
            'default_countries': default_countries,
            'states': states,
            'has_check_vat': hasattr(request.env['res.partner'], 'check_vat'),

        })
        return http.request.render(
            'hr_user.res_partner_work_experience_form',
            values
        )

    @http.route(
        route='/my/applicants',
        type='http',
        auth='public',
        website=True,
        csrf=False
    )
    def render_my_applicants(self):
        bindings = {
            'error': {},
            'error_message': []
        }
        bindings.update({
            'partner': request.env.user,
            'applicants': request.env['hr.applicant'].sudo().search([
                ('profile_user', '=', http.request.uid),
            ], order='create_date desc'),
            'current_date': datetime.now().date()
        })
        return http.request.render(
            'hr_user.res_partner_applicants_form',
            bindings
        )

    @http.route(
        route='/my/applicants/documents',
        type='http',
        auth='public',
        website=True,
        csrf=False
    )
    def render_documentos_modal(self, **post):
        bindings = {
            'error': {},
            'error_message': []
        }
        if post:
            applicant_id = int(post.get('id'))
            job_id = int(post.get('job_id'))
            studies = request.env['applicant.study'].sudo().search(
                [('applicant_id', '=', applicant_id)], order='date desc')
            job = request.env['hr.job'].sudo().browse(job_id)
            bindings.update({
                'cargo': post.get('cargo'),
                'convocatoria': post.get('convocatoria'),
                'completo': post.get('completo'),
                'applicant_id': applicant_id,
                'studies': studies,
                'job': job,
                'current_date': datetime.now().date()
            })
        else:
            bindings.update({
                'error': {'code': 2},
                'error_message': ['No ha enviado el parametro id']
            })

        return http.request.render(
            'hr_user.res_partner_applicants_complete_document',
            bindings
        )

    @http.route(
        route='/my/applicants/documents/trainings',
        type='http',
        auth='public',
        website=True,
        csrf=False
    )
    def render_documentos_modal_training(self, **post):
        bindings = {
            'error': {},
            'error_message': []
        }
        if post:
            applicant_id = int(post.get('id'))
            job_id = int(post.get('job_id'))
            trainings = request.env['applicant.training'].sudo().search(
                [('applicant_id', '=', applicant_id)], order='date_end desc')
            job = request.env['hr.job'].sudo().browse(job_id)
            bindings.update({
                'applicant_id': applicant_id,
                'trainings': trainings,
                'job': job,
                'current_date': datetime.now().date()
            })
        else:
            bindings.update({
                'error': {'code': 2},
                'error_message': ['No ha enviado el parametro id']
            })

        return http.request.render(
            'hr_user.res_partner_applicants_complete_document_trainings',
            bindings
        )

    @http.route(
        route='/my/applicants/documents/works',
        type='http',
        auth='public',
        website=True,
        csrf=False
    )
    def render_documentos_modal_work(self, **post):
        bindings = {
            'error': {},
            'error_message': []
        }
        if post:
            applicant_id = int(post.get('id'))
            job_id = int(post.get('job_id'))
            works = request.env['applicant.work.experience'].sudo().search(
                [('applicant_id', '=', applicant_id)], order='date_start desc')
            job = request.env['hr.job'].sudo().browse(job_id)
            bindings.update({
                'applicant_id': applicant_id,
                'works': works,
                'job': job,
                'current_date': datetime.now().date()
            })
        else:
            bindings.update({
                'error': {'code': 2},
                'error_message': ['No ha enviado el parametro id']
            })

        return http.request.render(
            'hr_user.res_partner_applicants_complete_document_works',
            bindings
        )

    def _create_file_path(self, file_path):
        if not os.path.exists(os.path.dirname(file_path)):
            try:
                os.makedirs(os.path.dirname(file_path))
            except OSError as exc:
                if exc.errno != errno.EEXIST:
                    raise
        return True

    def _remove_applicant_file(self, path_os, name_file, applicant_id):
        path_url = '/' + self._path_applicant + '/' + \
                   str(applicant_id) + '/' + name_file
        path = path_os + path_url

        if os.path.isfile(path):
            try:
                os.remove(path)
            except OSError:
                raise

    @http.route(
        route='/my/applicants/documents/study/upload',
        type='http',
        auth='public',
        website=True,
        csrf=False,
        cors="*"
    )
    def upload_study(self, **post):
        if post:
            try:
                study_id = int(post.get('id'))
                obj_study = http.request.env['applicant.study'].sudo().\
                    browse(study_id)

                path_os = http.request.env['ir.config_parameter'].get_param(
                    'path.filestatic')
                path_http = http.request.env['ir.config_parameter'].get_param(
                    'path.filestatic.http')
                format_name_file = datetime.now().strftime("%Y%m%d%H%M%S%f")

                if obj_study.filename:
                    self._remove_applicant_file(path_os, obj_study.filename,
                                                obj_study.applicant_id.id)

                name_file = ('study_' + str(study_id) + '_'
                             + format_name_file) \
                    + os.path.splitext(post.get('file').filename)[1]

                path_url = '/' + self._path_applicant + '/' + \
                           str(obj_study.applicant_id.id) + '/' + name_file
                path = path_os + path_url

                self._create_file_path(path)

                with open(path, 'w') as f:
                    f.write(post['file'].read())
                    f.close()

                file_url = path_http + path_url

                data = {
                    'file_url': file_url,
                    'filename': name_file
                }

                obj_study.sudo().write(data)
                return json.dumps({
                    "success": True,
                    "url": file_url
                })
            except ValueError:
                return json.dumps({"success": False})

    @http.route(
        route='/my/applicants/documents/training/upload',
        type='http',
        auth='public',
        website=True,
        csrf=False,
        cors="*"
    )
    def upload_training(self, **post):
        if post:
            try:
                training_id = int(post.get('id'))
                obj_training = http.request.env['applicant.training'].sudo().\
                    browse(training_id)

                path_os = http.request.env['ir.config_parameter'].get_param(
                    'path.filestatic')
                path_http = http.request.env['ir.config_parameter'].get_param(
                    'path.filestatic.http')
                format_name_file = datetime.now().strftime("%Y%m%d%H%M%S%f")

                if obj_training.filename:
                    self._remove_applicant_file(path_os, obj_training.filename,
                                                obj_training.applicant_id.id)

                name_file = ('training_' + str(training_id)
                             + '_' + format_name_file) \
                    + os.path.splitext(post.get('file').filename)[1]

                path_url = '/' + self._path_applicant + '/' + \
                           str(obj_training.applicant_id.id) + '/' + name_file
                path = path_os + path_url

                self._create_file_path(path)

                with open(path, 'w') as f:
                    f.write(post['file'].read())
                    f.close()

                file_url = path_http + path_url

                data = {
                    'file_url': file_url,
                    'filename': name_file
                }

                obj_training.sudo().write(data)
                return json.dumps({
                    "success": True,
                    "url": file_url
                })
            except ValueError:
                return json.dumps({"success": False})

    @http.route(
        route='/my/applicants/documents/work/upload',
        type='http',
        auth='public',
        website=True,
        csrf=False,
        cors="*"
    )
    def upload_work(self, **post):
        if post:
            try:
                work_id = int(post.get('id'))
                obj_work = http.request.env['applicant.work.experience'].\
                    sudo().browse(work_id)

                path_os = http.request.env['ir.config_parameter'].get_param(
                    'path.filestatic')
                path_http = http.request.env['ir.config_parameter'].get_param(
                    'path.filestatic.http')
                format_name_file = datetime.now().strftime("%Y%m%d%H%M%S%f")

                if obj_work.filename:
                    self._remove_applicant_file(path_os, obj_work.filename,
                                                obj_work.applicant_id.id)

                name_file = ('work_' + str(work_id) + '_' + format_name_file) \
                    + os.path.splitext(post.get('file').filename)[1]

                path_url = '/' + self._path_applicant + '/' + \
                           str(obj_work.applicant_id.id) + '/' + name_file
                path = path_os + path_url

                self._create_file_path(path)

                with open(path, 'w') as f:
                    f.write(post['file'].read())
                    f.close()

                file_url = path_http + path_url

                data = {
                    'file_url': file_url,
                    'filename': name_file
                }

                obj_work.sudo().write(data)
                return json.dumps({
                    "success": True,
                    "url": file_url
                })
            except ValueError:
                return json.dumps({"success": False})

    @http.route(
        route='/my/applicants/documents/complete',
        type='http',
        auth='public',
        website=True,
        csrf=False,
        cors="*"
    )
    def verify_applicant_document_is_complete(self, **post):
        if post:
            try:
                completo = True
                data = {
                    'documentacion_completada': False
                }
                applicant_id = int(post.get('id'))
                applicant = http.request.env['hr.applicant'].sudo(). \
                    browse(applicant_id)

                if applicant.study_ids:
                    for study in applicant.study_ids:
                        if not study.filename:
                            completo = False
                            break

                if applicant.training_ids:
                    for training in applicant.training_ids:
                        if not training.filename:
                            completo = False
                            break

                if applicant.work_experience_ids:
                    for work in applicant.work_experience_ids:
                        if not work.filename:
                            completo = False
                            break

                if completo:
                    data.update({
                        'documentacion_completada': True
                    })
                    applicant.sudo().write(data)

                return json.dumps({"success": True, "completo": completo})
            except ValueError:
                return json.dumps({"success": False})
