# -*- coding: utf-8 -*-
import logging
from __builtin__ import staticmethod

_logger = logging.getLogger(__name__)

# Solo se debe colocar una tupla como directiva vigente (True)
_DIRECTIVAS_CAS = [
    ('rde_001_2017', 'RDE N° 001-2017-MIDIS/PNCM', False),
    ('rde_007_2018', 'RDE N° 007-2018-MIDIS/PNCM', True)
]

_PUNTAJES_NUEVA_DIRECTIVA = [
    ('0.0', '0 pts'),
    ('3.5', '3.5 pts'),
    ('5.0', '5 pts')
]

_PUNTAJES_ANTERIOR_DIRECTIVA = [
    ('0.0', '0 pts'),
    ('0.5', '0.5 pts'),
    ('1.0', '1 pts'),
    ('1.5', '1.5 pts'),
    ('2.0', '2 pts'),
    ('2.5', '2.5 pts')
]


class Bonificacion(object):

    def calcular_puntaje_bonificacion(self, application):
        return 0

    def calcular_puntaje_total(self, application):
        return application.puntuacion_curricular + \
               application.puntuacion_entrevista + \
               application.puntaje_bonificacion


class BonificacionFuerzasArmadas(Bonificacion):

    def calcular_puntaje_bonificacion(self, application):
        return application.puntuacion_entrevista * 0.10


class BonificacionDiscapacidad(Bonificacion):

    def calcular_puntaje_bonificacion(self, application):
        return (application.puntuacion_entrevista +
                application.puntuacion_curricular) * 0.15


class BonificacionDiscapacidadFFAA(Bonificacion):

    def calcular_puntaje_bonificacion(self, application):
        subtotal = (application.puntuacion_curricular +
                application.puntuacion_entrevista +
                (application.puntuacion_entrevista * 0.10)) * 0.15
        _logger.info(' subtotal function {0}'.format(subtotal))
        return subtotal

    def calcular_puntaje_total(self, application):
        subtotal = application.puntuacion_curricular + \
                   application.puntuacion_entrevista + \
                   (application.puntuacion_entrevista * 0.10)
        _logger.info(' subtotal total {0}'.format(subtotal))
        return subtotal + subtotal * 0.15


class BonificacionFactory(object):

    @staticmethod
    def get_instance(bonificacion_code):
        return {
            'fa001': BonificacionFuerzasArmadas(),
            'di002': BonificacionDiscapacidad(),
            'fadi003': BonificacionDiscapacidadFFAA()
        }[bonificacion_code]


class ApplicantUtils(object):

    @staticmethod
    def order_by_fullname(app):
        app.full_name = ' '.join([
            app.partner_firstname if app.partner_firstname else
            app.profile_user.partner_firstname,
            app.partner_secondname,
            app.partner_name
        ])
        app.full_name = app.full_name.strip().upper()
        return app.full_name


class JobUtils(object):

    @staticmethod
    def get_lista_directivas():
        directivas = []
        for dir in _DIRECTIVAS_CAS:
            directivas.append(
                (dir[0], dir[1])
            )
        return directivas

    @staticmethod
    def get_directiva_vigente():
        for dir in _DIRECTIVAS_CAS:
            if dir[2]:
                return dir[0]
        return False

    @staticmethod
    def get_lista_puntajes():
        return _PUNTAJES_NUEVA_DIRECTIVA

    @staticmethod
    def order_by_merito(app):
        ApplicantUtils.order_by_fullname(app)
        return app.puntuacion_total

    @staticmethod
    def attr_sort(objs):
        return lambda obj: [ JobUtils.operation_additional(obj,attr) for attr in objs ]

    @staticmethod
    def operation_additional(obj, attr):
        full_name = ' '.join([
            obj.partner_firstname if obj.partner_firstname else
            obj.profile_user.partner_firstname,
            obj.partner_secondname,
            obj.partner_name
        ])
        obj.full_name = full_name.strip().upper()
        return getattr(obj, attr)
