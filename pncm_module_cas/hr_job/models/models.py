# -*- coding: utf-8 -*-
from openerp import fields, models, api
from openerp.exceptions import ValidationError
from odoo.addons.pncm_comite_gestion.models import utils
import domain
import math


class CMNivelExperiencia(models.Model):
    _name = 'hr.pncm.nivel_experiencia'

    name = fields.Char(
        string=u'Descripción',
        required=True,
        size=250
    )
    activo = fields.Boolean(
        string=u'Activo',
        default=True,
        required=True
    )
    id = fields.Char(
        string=u'Código',
        required=True,
        size=10
    )

    _sql_constraint = [
        ('code_unique', 'unique(id)', u'El código ya existe')
    ]


class CMNivelCapacitacion(models.Model):
    _name = 'hr.pncm.nivel_capacitacion'

    name = fields.Char(
        string=u'Descripción',
        required=True,
        size=250
    )
    activo = fields.Boolean(
        string=u'Activo',
        default=True,
        required=True
    )
    id = fields.Char(
        string=u'Código',
        required=True,
        size=10
    )

    _sql_constraint = [
        ('code_unique', 'unique(id)', u'El código ya existe')
    ]


class CMPuntuacionNivelEducativo(models.Model):
    _name = 'hr.pncm.puntuacion.nivel_educativo'

    nivel_educativo_id = fields.Many2one(
        'hr.employee.education.level',
        string=u'Nivel de Estudios',
        required=True
    )
    puntaje = fields.Float(
        string=u'Puntaje',
        required=True
    )
    tabla_puntuacion_id = fields.Many2one(
        'hr.pncm.tabla_puntuacion',
        string=u'Tabla Puntuación',
        required=True,
        ondelete='cascade'
    )

    @api.multi
    def name_get(self):
        return [
            (obj.id, u'{} ({}pts)'.
             format(obj.nivel_educativo_id.name,
                    obj.puntaje)) for obj in self
        ]


class CMPuntuacionNivelExperiencia(models.Model):
    _name = 'hr.pncm.puntuacion.nivel_experiencia'

    nivel_experiencia_id = fields.Many2one(
        'hr.pncm.nivel_experiencia',
        required=True,
        string=u'Nivel Experiencia'
    )
    puntaje = fields.Float(
        string=u'Puntaje',
        required=True
    )
    tabla_puntuacion_id = fields.Many2one(
        'hr.pncm.tabla_puntuacion',
        string=u'Tabla Puntuación',
        required=True,
        ondelete='cascade'
    )

    @api.multi
    def name_get(self):
        return [
            (obj.id, u'{} ({}pts)'.
             format(obj.nivel_experiencia_id.name,
                    obj.puntaje)) for obj in self
        ]


class CMPuntuacionNivelCapacitacion(models.Model):
    _name = 'hr.pncm.puntuacion.nivel_capacitacion'

    nivel_capacitacion_id = fields.Many2one(
        'hr.pncm.nivel_capacitacion',
        required=True,
        string=u'Nivel Capacitacion'
    )
    puntaje = fields.Float(
        string=u'Puntaje',
        required=True
    )
    tabla_puntuacion_id = fields.Many2one(
        'hr.pncm.tabla_puntuacion',
        string=u'Tabla Puntuación',
        required=True,
        ondelete='cascade'
    )

    @api.multi
    def name_get(self):
        return [
            (obj.id, u'{} ({}pts)'.
             format(obj.nivel_capacitacion_id.name,
                    obj.puntaje)) for obj in self
        ]


class CMTipoBonificacion(models.Model):
    _name = 'hr.pncm.tipo_bonificacion'

    id = fields.Char(
        string=u'Código',
        required=True,
        size=10
    )

    acronimo = fields.Char(
        string=u'Acronimo',
        required=True,
        size=10
    )

    name = fields.Char(
        string=u'Nombre',
        required=True
    )

    formula = fields.Char(
        string=u'Fórmula',
        size=255
    )

    activo = fields.Boolean(
        string=u'Activo',
        default=True,
        required=True
    )


class CMTablaPuntuacion(models.Model):
    _name = 'hr.pncm.tabla_puntuacion'
    _puntaje_total = 100

    code = fields.Char(
        string=u'Código',
        size=10,
        required=True
    )
    name = fields.Char(
        string=u'Descripción',
        required=True
    )

    directiva = fields.Selection(
        selection=domain.JobUtils.get_lista_directivas(),
        string=u'Directiva',
        default=domain.JobUtils.get_directiva_vigente(),
        readonly=False
    )

    fecha_inicio = fields.Date(
        string=u'Fecha de Inicio',
    )
    fecha_fin = fields.Date(
        string=u'Fecha de Fin'
    )
    state = fields.Selection(
        selection=[
            ('borrador', u'Borrador'),
            ('activo', u'Activo'),
            ('baja', u'De Baja')
        ],
        default='borrador',
        required=True,
        string=u'Estado'
    )

    # Fase Curricular:
    puntuacion_max_curricular = fields.Float(
        string=u'Puntaje Máximo',
        store=True
    )
    peso_curricular = fields.Float(
        string=u'Peso %',
        store=False,
        compute='_get_peso_curricular'
    )
    puntuacion_educativa = fields.One2many(
        'hr.pncm.puntuacion.nivel_educativo',
        'tabla_puntuacion_id',
        string=u'Formación Académica',
        required=True
    )
    puntuacion_experiencia = fields.One2many(
        'hr.pncm.puntuacion.nivel_experiencia',
        'tabla_puntuacion_id',
        string=u'Experiencia Laboral Específica',
        required=True
    )
    puntuacion_capacitacion = fields.One2many(
        'hr.pncm.puntuacion.nivel_capacitacion',
        'tabla_puntuacion_id',
        string=u'Capacitación Relacionada al Servicio',
        required=True
    )

    # Fase Entrevista:
    puntuacion_max_entrevista = fields.Float(
        string=u'Puntaje Máximo',
        store=True
    )
    puntuacion_min_entrevista = fields.Float(
        string=u'Puntaje Mínimo'
    )
    peso_entrevista = fields.Float(
        string=u'Peso %',
        store=False,
        compute='_get_peso_entrevista'
    )

    # Fase Evaluacion Escrita:
    requiere_evaluacion_escrita = fields.Boolean(
        string=u'Evaluación Escrita',
        default=False
    )
    puntuacion_max_escrita = fields.Float(
        string=u'Puntaje Máximo',
        store=True
    )
    puntuacion_min_escrita = fields.Float(
        string=u'Puntaje Mínimo',
        store=True
    )
    peso_escrita = fields.Float(
        string=u'Peso %',
        store=False,
        compute='_get_peso_escrita'
    )

    _sql_constraint = [
        ('code_unique', 'unique(code)', u'El código ya existe')
    ]

    @api.model
    def create(self, values):
        values['code'] = values['code'].upper()
        values['name'] = values['name'].upper()
        return super(CMTablaPuntuacion, self).create(values)

    @api.multi
    def write(self, values):
        ctx = dict(self._context or {})
        if 'code' in values:
            values['code'] = values['code'].upper()
        if 'name' in values:
            values['name'] = values['name'].upper()
        return super(CMTablaPuntuacion, self.with_context(ctx)).write(values)

    @api.constrains(
        'puntuacion_max_curricular'
    )
    def _constrain_puntaje_maximo_curricular(self):
        if self.state != 'borrador':
            return self._validar_puntaje_maximo_curricular()

    def _validar_puntaje_maximo_curricular(self):
        if self.puntuacion_max_curricular \
                and self.puntuacion_max_curricular > 0:
            return True

        puntaje_curricular = self._calcular_puntaje_maximo_curricular()
        if puntaje_curricular > 0:
            return True

        raise ValidationError(u'Debe Ingresar un valor mayor que cero a la '
                              u'puntuación máxima curricular')
        return False

    @api.constrains(
        'puntuacion_max_entrevista'
    )
    def _constrain_puntaje_maximo_entrevista(self):
        if self.state != 'borrador':
            return self._validar_puntuacion_max_entrevista()

    @api.constrains(
        'puntuacion_min_entrevista'
    )
    def _constrain_puntaje_min_entrevista(self):
        if self.state != 'borrador':
            return self._validar_puntuacion_min_entrevista()

    @api.constrains(
        'puntuacion_min_escrita'
    )
    def _constrain_puntaje_min_escrita(self):
        if self.state != 'borrador':
            return self._validar_puntuacion_min_escrita()

    @api.constrains(
        'puntuacion_min_entrevista',
        'puntuacion_max_entrevista'
    )
    def _constrain_puntaje_min_max_entrevista(self):
        if self.state != 'borrador':
            return self._validar_puntuacion_min_max_entrevista()

    @api.constrains(
        'puntuacion_min_escrita',
        'puntuacion_max_escrita'
    )
    def _constrain_puntaje_min_max_escrita(self):
        if self.state != 'borrador':
            return self._validar_puntuacion_min_max_escrita()

    def _validar_puntuacion_max_entrevista(self):
        if self.puntuacion_max_entrevista \
                and self.puntuacion_max_entrevista > 0:
            return True
        raise ValidationError(u'Debe Ingresar un valor mayor que cero a la '
                              u'puntuación máxima de la entrevista personal')
        return False

    def _validar_puntuacion_min_entrevista(self):
        if self.puntuacion_min_entrevista \
                and self.puntuacion_min_entrevista > 0:
            return True
        raise ValidationError(u'Debe Ingresar un valor mayor que cero a la '
                              u'puntuación mínima de la entrevista personal')

    def _validar_puntuacion_min_escrita(self):
        if self.puntuacion_min_escrita \
                and self.puntuacion_min_escrita > 0:
            return True
        raise ValidationError(u'Debe Ingresar un valor mayor que cero a la '
                              u'puntuación mínima de la evaluación escrita')

    def _validar_puntuacion_min_max_entrevista(self):
        if self.puntuacion_min_entrevista and self.puntuacion_max_entrevista:
            if self.puntuacion_min_entrevista > self.puntuacion_max_entrevista:
                raise ValidationError(u'El puntaje mínimo de entrevista '
                                      u'no debe susperar al máximo')
                return False
        return True

    def _validar_puntuacion_min_max_escrita(self):
        if self.puntuacion_min_escrita and self.puntuacion_max_escrita:
            if self.puntuacion_min_escrita > self.puntuacion_max_escrita:
                raise ValidationError(u'El puntaje mínimo de la evaluación '
                                      u'escrita no debe susperar al máximo')
                return False
        return True

    @api.constrains(
        'requiere_evaluacion_escrita',
        'puntuacion_max_escrita'
    )
    def _constrain_puntaje_maximo_escrita(self):
        if self.state != 'borrador':
            return self._validar_puntuacion_max_escrita()

    def _validar_puntuacion_max_escrita(self):
        if self.requiere_evaluacion_escrita:
            if self.puntuacion_max_escrita and self.puntuacion_max_escrita > 0:
                return True

            raise ValidationError(u'Debe Ingresar un valor mayor que cero '
                                  u'a la puntuación máxima de la evaluación '
                                  u'escrita')
            return False
        return True

    @api.constrains(
        'fecha_inicio'
    )
    def _constrain_fecha_inicio(self):
        if self.state != 'borrador':
            return self._validar_fecha_inicio()

    def _validar_fecha_inicio(self):
        if self.fecha_inicio:
            dh = utils.DateHelper()
            return dh._validar_fechas_futuras(self.fecha_inicio,
                                              u'La fecha de inicio '
                                              u'no puede ser mayor a la '
                                              u'fecha actual')
        else:
            raise ValidationError(
                u'Ingrese fecha de inicio')
            return False

    @api.constrains(
        'fecha_inicio',
        'fecha_fin'
    )
    def _constrain_rango_fechas_tabla(self):
        if self.state != 'borrador':
            return self._validar_rango_fechas_tabla()

    def _validar_rango_fechas_tabla(self):
        if self.fecha_inicio and self.fecha_fin:
            dh = utils.DateHelper()
            return dh._validar_fecha_cierre(self.fecha_inicio, self.fecha_fin,
                                            u'La fecha de fin debe ser mayor '
                                            u'a la fecha de inicio')
        return True

    @api.constrains(
        'puntuacion_max_curricular',
        'puntuacion_max_entrevista',
        'puntuacion_max_escrita'
    )
    def _constrain_puntuacion_total(self):
        if self.state != 'borrador':
            return self._validar_puntuacion_total()

    def _validar_puntuacion_total(self):
        if self.requiere_evaluacion_escrita:
            suma_total = self.puntuacion_max_curricular + \
                         self.puntuacion_max_entrevista + \
                         self.puntuacion_max_escrita
        else:
            suma_total = self.puntuacion_max_curricular + \
                         self.puntuacion_max_entrevista

        if suma_total != self._puntaje_total:
            raise ValidationError(u'La suma de puntajes máximos debe '
                                  u'ser igual a 100')
            return False
        return True

    @api.constrains(
        'fecha_fin'
    )
    def _constrain_fecha_fin(self):
        if self.state == 'baja':
            return self._valida_fecha_fin()

    def _validar_fecha_fin(self):
        if not self.fecha_fin:
            raise ValidationError(
                u'Ingrese fecha de fin')

    @api.onchange('requiere_evaluacion_escrita')
    def _on_change_evaluacion_escrita(self):
        if not self.requiere_evaluacion_escrita:
            self.puntuacion_max_escrita = False

    @api.onchange('puntuacion_max_curricular')
    def _on_change_puntaje_maximo_curricular(self):
        self.puntuacion_max_curricular = \
            self._calcular_puntaje_maximo_curricular()
        self._get_peso_curricular()

    @api.onchange('puntuacion_max_entrevista')
    def _on_change_puntaje_maximo_entrevista(self):
        self._get_peso_entrevista()

    @api.onchange('puntuacion_max_escrita')
    def _on_change_puntaje_maximo_escrita(self):
        self._get_peso_escrita()

    @api.constrains(
        'puntuacion_educativa',
        'puntuacion_experiencia',
        'puntuacion_capacitacion'
    )
    def _constrain_validar_contenido_curricular(self):
        if self.state != 'borrador':
            return self._validar_contenido_curricular()

    def _validar_contenido_curricular(self):
        if len(self.puntuacion_educativa) == 0:
            raise ValidationError(u'Ingrese Formación Académica')
            return False
        if len(self.puntuacion_experiencia) == 0:
            raise ValidationError(u'Ingrese Experiencia Laboral '
                                  u'especifica')
        if len(self.puntuacion_capacitacion) == 0:
            raise ValidationError(u'Ingrese Capacitación Relacionada al '
                                  u'Servicio')
            return False
        return True

    @api.one
    def _get_peso_curricular(self):
        if self.puntuacion_max_curricular:
            porcentaje = math.ceil(
                (self.puntuacion_max_curricular/self._puntaje_total) * 100
            )
            self.peso_curricular = porcentaje

    @api.one
    def _get_peso_entrevista(self):
        if self.puntuacion_max_entrevista:
            porcentaje = math.ceil((self.puntuacion_max_entrevista /
                                    self._puntaje_total) * 100)
            self.peso_entrevista = porcentaje

    @api.one
    def _get_peso_escrita(self):
        if self.puntuacion_max_escrita:
            porcentaje = math.ceil((self.puntuacion_max_escrita /
                                    self._puntaje_total) * 100)
            self.peso_escrita = porcentaje

    @api.onchange('puntuacion_educativa')
    def _on_change_educativa(self):
        self._get_maxima_curricular()

    @api.onchange('puntuacion_experiencia')
    def _on_change_experiencia(self):
        self._get_maxima_curricular()

    @api.onchange('puntuacion_capacitacion')
    def _on_change_capacitacion(self):
        self._get_maxima_curricular()

    def _get_maxima_curricular(self):
        for r in self:
            r.puntuacion_max_curricular \
                = self._calcular_puntaje_maximo_curricular()

    def _calcular_puntaje_maximo_curricular(self):
        max_educativa = 0
        max_experiencia = 0
        max_capacitacion = 0
        if self.puntuacion_educativa:
            for pe in self.puntuacion_educativa:
                if pe.puntaje > max_educativa:
                    max_educativa = pe.puntaje

        if self.puntuacion_experiencia:
            for pe in self.puntuacion_experiencia:
                if pe.puntaje > max_experiencia:
                    max_experiencia = pe.puntaje

        if self.puntuacion_capacitacion:
            for pc in self.puntuacion_capacitacion:
                if pc.puntaje > max_capacitacion:
                    max_capacitacion = pc.puntaje

        return max_educativa + max_experiencia + max_capacitacion

    def _validar_tabla_activa(self):
        if self.requiere_evaluacion_escrita:
            tabla = self.env['hr.pncm.tabla_puntuacion']\
                .search([('state', '=', 'activo'),
                         ('requiere_evaluacion_escrita', '=', True)])
            if tabla:
                raise ValidationError(u'Ya existe una tabla activa, '
                                      u'con evaluación escrita. Recuerde que '
                                      u'solo puede existir una tabla activa a '
                                      u'la vez')
                return False
        else:
            tabla = self.env['hr.pncm.tabla_puntuacion'] \
                .search([('state', '=', 'activo'),
                         ('requiere_evaluacion_escrita', '=', False)])
            if tabla:
                raise ValidationError(u'Ya existe una tabla activa, '
                                      u'sin evaluación escrita. Recuerde que '
                                      u'solo puede existir una tabla activa a '
                                      u'la vez')
                return False
        return True

    @api.multi
    def activar(self):
        # success = self._validar_tabla_activa()
        success = self._validar_fecha_inicio()
        if success:
            success = self._validar_rango_fechas_tabla()
        if success:
            success = self._validar_puntaje_maximo_curricular()
        if success:
            success = self._validar_puntuacion_max_entrevista()
        if success:
            success = self._validar_puntuacion_max_escrita()
        if success:
            success = self._validar_puntuacion_total()
        if success:
            success = self._validar_contenido_curricular()
        if success:
            self.write(dict(state='activo'))

    @api.multi
    def dar_de_baja(self):
        success = self._validar_fecha_fin()
        if success:
            success = self._validar_rango_fechas_tabla()
        if success or not success:
            self.write(dict(state='baja'))
