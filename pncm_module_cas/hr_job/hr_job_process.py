# -*- coding: utf-8 -*-
from odoo import fields, models, api
from openerp.exceptions import UserError, RedirectWarning,ValidationError
from datetime import datetime
from dateutil.relativedelta import relativedelta
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
from models import domain
import sys
import pytz
import logging

reload(sys)
sys.setdefaultencoding('utf8')

_logger = logging.getLogger(__name__)

_SELECT_RESULT = [
    ('apto', 'Apto'),
    ('noapto', 'No apto'),
]


class HrJob(models.Model):
    _inherit = 'hr.job'
    _order = "date_begin desc"

    task_revision_curricular = fields.Char(string=u'Task Revision Curricular ')

    number = fields.Integer(
        string=u'Número convocatoria',
        track_visibility='always',
        required=True
    )
    name = fields.Char(
        string=u'Titulo de Convocatoria',
        track_visibility='always',
        required=True
    )
    date_begin = fields.Date(
        string=u'Fecha inicio de postulación',
        track_visibility='always',
        required=True
    )
    date_end = fields.Date(
        string=u'Fecha fin de postulación',
        track_visibility='always',
        required=True
    )
    date_end_hour = fields.Char(
        'Hora Cierre',
        default='23:59:00',
        required=True
    )
    date_review_start = fields.Date(
        string=u'Fecha de Revisión inicial',
        track_visibility='always',
    )
    date_start_adjunt_document = fields.Date(
        string=u'Fecha inicio de adjuntar documentos',
        track_visibility='always',
    )
    date_end_adjunt_document = fields.Date(
        string=u'Fecha fin de adjuntar documentos',
        track_visibility='always',
    )
    date_evaluation_resume = fields.Date(
        string=u'Fecha evaluación hoja de vida',
        track_visibility='always',
    )
    date_interview = fields.Date(
        string=u'Fecha de entrevista',
        track_visibility='always',
    )
    date_publish = fields.Date(
        string=u'Fecha de Publicación',
        track_visibility='always',
        default=fields.Date.today()
    )
    date_publish_result = fields.Date(
        string=u'Fecha de Publicación resultados',
        track_visibility='always',
        default=fields.Date.today()
    )
    date_publish_cv_evaluation = fields.Date(
        string=u'Fecha de Publicación de Evaluación Curricular',
        track_visibility='always',
    )
    date_contract_subscription_begin = fields.Date(
        string=u'Fecha Inicio'
    )
    date_contract_subscription_end = fields.Date(
        string=u'Fecha Fin'
    )
    code = fields.Char(
        string=u'Código',
        track_visibility='always',
        size=250,
        required=True
    )
    street = fields.Char(
        string=u'Lugar de Entrevista',
        track_visibility='always',
        required=True
    )
    observation = fields.Text(
        string=u'Observación Curricular',
        track_visibility='always',
    )
    attach_base = fields.Binary(
        string='Bases',
        help=u'Extensión doc, docx, xls, xlsx, pdf, jpg, png',
        required=True
    )
    attach_base_filename = fields.Char(
        string='Nombre del archivo',
    )
    exam = fields.Boolean(string=u'Evaluación de conocimientos')
    file_anexo = fields.Many2one(
        comodel_name='hr.job.filesanexos',
        string=u'Anexos'
    )
    anexos = fields.Binary(string="Anexos")
    anexos_filename = fields.Char(
        string="Nombre Anexo"
    )
    confirmo_hoja_vida = fields.Boolean(
        string=u'1. Confirmar Resultado de Evaluación Inicial',
        default=False
    )
    confirmo_curricular = fields.Boolean(
        string=u'2. Confirmar Resultado Curricular'
    )
    confirmo_resultado_final = fields.Boolean(
        string=u'4. Confirmar Resultado Final'
    )
    confirmo_publicacion_escrita = fields.Boolean(
        string=u'3. Confirmar Resultado Evaluación Escrita'
    )
    resultado_curricular = fields.Binary(string="Resultado Eva. Curricular")
    resultado_curricular_filename = fields.Char(
        string="Nombre Resultado Corricular"
    )
    result = fields.Binary(string="Resultado Final")
    result_filename = fields.Char(
        string="Nombre Anexo"
    )
    firm_area_user = fields.Many2one(
        comodel_name='hr.employee',
        string=u'Presidente de la Comisión',
        track_visibility='always',
    )
    firm_oga_user = fields.Many2one(
        comodel_name='hr.employee',
        track_visibility='always',
        string=u'Miembro de la Comisión 1'
    )
    firm_rrhh_user = fields.Many2one(
        comodel_name='hr.employee',
        track_visibility='always',
        string=u'Miembro de la Comisión 2'
    )
    date_evaluation = fields.Date(
        string=u'Fecha de Evaluación Escrita',
        track_visibility='always',
    )
    hora_escrita = fields.Char(
        string=u'Hora de Evaluación Escrita',
        size=5
    )
    fecha_publicacion_escrita = fields.Date(
        string=u'Fecha de Publicación Evaluación Escrita'
    )
    fecha_publicacion_revision_inicial = fields.Date(
        string=u'Fecha de Publicación de la Revisión Inicial'
    )
    puntaje_minimo_evaluacion_curricular = fields.Integer(
        string=u'Puntaje minimo evaluación curricular',
        track_visibility='always',
    )
    area_usuaria = fields.Many2one(
        comodel_name='hr.department',
        track_visibility='always',
        string=u'Área usuaria',
        domain="[('is_office','=',False)]"
    )
    puesto_trabajo = fields.Many2one(
        'hr.positions',
        track_visibility='always',
        string=u'Puesto de Trabajo',
        required=True
    )
    cronograma_postulacion = fields.Many2one(
        'hr.job.timeline',
        string=u'Cronograma de Postulación',
        required=True,
        ondelete='restrict'
    )

    perfil_puesto = fields.One2many(
        'perfil.puesto.trabajo',
        'puesto_id',
        u'Perfil Puesto'
    )
    comunicados = fields.One2many(
        'hr.job.notices',
        'puesto_id',
        u'Comunicados',
    )
    state = fields.Selection(
        selection=[
            ('draft', 'Borrador'),
            ('recruit', 'Abierta'),
            ('evaluation', 'En evaluación'),
            ('result', 'Con resultados'),
            ('open', 'Cerrado'),
        ],
        string='Estado',
        required=True,
        track_visibility='always',
        copy=False,
        default='draft',
    )
    lugar_examen = fields.Char(
        string=u'Lugar de Examen'
    )

    def _get_evaluacion_escrita_puntuacion_minima(self):
        puntuacion_minima_escrita=0
        tb_puntuacion = self.env['hr.pncm.tabla_puntuacion'].search(
            [
                ('directiva', '=', self.cronograma_postulacion.directiva),
                ('state', '!=','borrador'),
                ('requiere_evaluacion_escrita', '=', self.exam)
            ])
        if tb_puntuacion:
            puntuacion_minima_escrita=tb_puntuacion.puntuacion_min_escrita

        return puntuacion_minima_escrita

    def _format_date(self,date):
        return datetime.strptime(date,'%Y-%m-%d').strftime('%d-%m-%Y')

    def _get_date_actual(self):
        return datetime.now().strftime('%d de %B de %Y')

    @api.onchange('firm_oga_user')
    def validar_firm_oga_user(self):
        if self.firm_oga_user :
            return {'domain':{
                'firm_rrhh_user':[
                    ('id','!=',self.firm_oga_user.id),
                    ('id','!=',self.firm_area_user.id)],
                'firm_area_user': [
                    ('id','!=',self.firm_oga_user.id),
                    ('id','!=',self.firm_rrhh_user.id)]
                }
            }

    @api.onchange('firm_area_user')
    def validar_firm_area_user(self):
        if self.firm_area_user:
            return {'domain': {
                'firm_rrhh_user': [
                    ('id', '!=', self.firm_area_user.id),
                    ('id', '!=', self.firm_oga_user.id)],
                'firm_oga_user': [
                    ('id', '!=', self.firm_area_user.id),
                    ('id', '!=', self.firm_rrhh_user.id)]
            }
            }

    @api.onchange('firm_rrhh_user')
    def validar_firm_rrhh_user(self):
        if self.firm_rrhh_user:
            return {'domain': {
                'firm_oga_user': [
                    ('id', '!=', self.firm_rrhh_user.id),
                    ('id', '!=', self.firm_area_user.id)],
                'firm_area_user': [
                    ('id', '!=', self.firm_rrhh_user.id),
                    ('id', '!=', self.firm_oga_user.id)]
            }
            }


    @api.model
    def create(self, values):
        try:
            if 'number' in values:
                number=int(values['number'])
                if not number>0:
                    raise ValidationError(
                        'Ingrese un Número de Convocatoria Valido.')
        except ValueError:
            raise ValidationError(
                'Número de convocatoria solo acepta valores númericos.')
        values['task_revision_curricular'] = 'open'
        return super(HrJob, self).create(values)

    @api.multi
    def write(self, values):
        try:
            if 'number' in values:
                number=int(values['number'])
                if not number>0:
                    raise ValidationError(
                        'Ingrese un Número de Convocatoria Valido.')
        except ValueError:
            raise ValidationError(
                'Número de convocatoria solo acepta valores númericos.')

        if 'result' in values:
            values.update({'state': 'result'})
        return super(HrJob, self).write(values)


    @api.multi
    def copy(self,default=None):
        default = dict(default or {})
        copied_count = self.search_count(
            [('name', '=like', '{}%'.format(self.name))])+1
        new_name = u"{} ({})".format(self.name, copied_count)
        default['name']= new_name
        default['department_id']= False
        default['number']= self.number
        return super(HrJob, self).copy(default)


    @api.multi
    @api.constrains(
        'date_begin',
        'date_end',
        'date_review_start',
        'date_start_adjunt_document',
        'date_end_adjunt_document',
        'date_evaluation',
        'date_evaluation_resume',
        'date_interview',
        'date_publish',
        'date_contract_subscription_begin',
        'date_contract_subscription_end',
        'fecha_publicacion_escrita',
        'date_publish_cv_evaluation',
        'fecha_publicacion_revision_inicial'
    )
    def _constraint_fechas_postulacion(self):
        if not self.date_publish <= self.date_begin:
            raise Exception(
                'La fecha publicación debe de ser '
                'MENOR que la fecha inicial de postulación '
            )
        if not self.date_begin <= self.date_end:
            raise Exception(
                'La fecha inicial de postulación debe de ser '
                'MENOR que la final de postulación '
            )
        if not self.date_end <= self.date_review_start:
            raise Exception(
                'La fecha final de postulación debe '
                'de ser MENOR revisión inicial'
            )
        if not self.date_review_start <= self.date_start_adjunt_document:
            raise Exception(
                'La fecha de revision inicial debe de '
                'ser MENOR a la fecha inicio de adjuntar documentos'
            )

        if self.fecha_publicacion_revision_inicial:
            if not self.date_review_start <= \
                   self.fecha_publicacion_revision_inicial:
                raise Exception(
                    'La fecha de revision inicial debe de '
                    'ser MENOR a la fecha de publicación de revisión inicial '
                )

            if not self.fecha_publicacion_revision_inicial <= \
                   self.date_start_adjunt_document:
                raise Exception(
                    'La fecha de publicación de revision inicial debe de '
                    'ser MENOR a la fecha de inicio de adjuntar documentos '
                )

        if not self.date_start_adjunt_document <= self. \
                date_end_adjunt_document:
            raise Exception(
                'La fecha de inicio de adjuntar documentos debe de '
                'ser MENOR a la fecha final de adjuntar documentos'
            )
        if not self.date_end_adjunt_document <= self.date_evaluation_resume:
            raise Exception(
                'La fecha de final de adjuntar documentos debe de '
                'ser MENOR a la fecha evaluación de hoja de vida'
            )
        if not self.date_evaluation_resume <= self.date_publish_cv_evaluation:
            raise Exception(
                'La fecha de publicación de evaluación de hoja de vida de '
                'ser MENOR a la fecha de la entrevista'
            )
        if self.exam:
            if not self.date_publish_cv_evaluation <= self.date_evaluation:
                raise Exception(
                    'La fecha de publicación de evaluación de hoja de vida de '
                    'ser MENOR a la fecha de la evaluación'
                )

        if self.exam:
            if not self.date_evaluation <= self.fecha_publicacion_escrita:
                raise Exception(
                    'La fecha de evaluación escrita debe '
                    'ser MENOR a la fecha de publicación de la evaluación '
                    'escrita'
                )

        if self.exam:
            if not self.fecha_publicacion_escrita <= self.date_interview:
                raise Exception(
                    'La fecha de publicación de la evaluación escrita debe '
                    'ser MENOR a la fecha de la entrevista'
                )

        if not self.date_publish_cv_evaluation <= self.date_interview:
            raise Exception(
                'La fecha de publicación de evaluación de hoja de vida de '
                'ser MENOR a la fecha de la entrevista'
            )
        if not self.date_interview <= self.date_publish_result:
            raise Exception(
                'La fecha de la entrevista de '
                'ser MENOR a la fecha de la publicación resultados'
            )
        if not self.date_publish_result < \
           self.date_contract_subscription_begin:
            raise Exception(
                'La fecha de inicio de supscripción del contrato debe '
                'ser MAYOR a la fecha de publicación de resultados'
            )
        if not self.date_contract_subscription_begin < \
           self.date_contract_subscription_end:
            raise Exception(
                'La fecha de fin de supscripción del contrato debe '
                'ser MAYOR a la fecha de inicio de supscripción del contrato'
            )

    @api.model
    def _cron_update_state(self):
        jobs = self.env['hr.job'].search([
            '|', ('state', '=', 'recruit'), ('state', '=', 'evaluation'),
        ])
        date_now = datetime.now(tz=pytz.timezone('America/Lima')).replace(
                    tzinfo=None)
        for job in jobs:
            date_end = datetime.strptime(job.date_end + ' ' +
                                         job.date_end_hour,
                                         '%Y-%m-%d %H:%M:%S')
            date_publish = datetime.strptime(job.date_publish_result,
                                             '%Y-%m-%d')
            if date_end <= date_now and date_now < date_publish:
                job.write({
                    'state': 'evaluation'
                })
            if date_publish < date_now:
                job.write({
                    'state': 'open'
                })

    @api.multi
    def _update_profile_in_applicants(self):
        count_jobs_update=0
        count_applicants_update=0
        for job in self:
            if job.id:
                applicants = self.env['hr.applicant'].search(
                    [
                        ('job_id', '=', job.id)
                    ]
                )
                for applicant in applicants:
                    details_profile_applicant = []
                    profiles_job=[]
                    profiles_in_applicant = self.env['detail.evaluation.applicant'].search([ ('perfil_puesto', '!=', False), ('postulacion_id', '=', applicant.id)])
                    for profile_job in profiles_in_applicant:
                        details_profile_applicant.append(profile_job.perfil_puesto.id)

                    profile_job = self.env['perfil.puesto.trabajo'].search(
                        [
                            ('puesto_id', '=', job.id),
                            ('id', 'not in', details_profile_applicant)
                        ]
                    )
                    for profile in profile_job:
                        profiles_job.append((0, 0, {
                            'postulacion_id': applicant.id,
                            'perfil_puesto': profile.id,
                            'status_evaluation': '2_no_acumple',
                            'name_perfil_puesto': profile.name
                        }))
                    count_applicants_update+=1
                    applicant.write({
                        'evaluacion_curricular_postulante':
                            profiles_job,
                        'first_execute' : True
                        # 'task_revision_curricular':
                        #     'finish'
                    })
                count_jobs_update += 1

        body = '{} ({})'.format('Puesto de Trabajo Actualizados', count_jobs_update)+'\n' + '{} ({})'.format('Solicitudes Finalizadas para Evaluación Curricular', count_applicants_update)
        params = {'body': body, 'title': '', 'istitlesub' : False, 'isfooter' : False}
        view_id = self.env['hr.job.messages.wizard']
        new = view_id.create(params)
        return {
            'type': 'ir.actions.act_window',
            'name': 'Proceso Finalizado:',
            'res_model': 'hr.job.messages.wizard',
            'view_type': 'form',
            'view_mode': 'form',
            'res_id': new.id,
            'view_id': self.env.ref('hr_job.hr_job_messages_wizard', False).id,
            'target': 'new',
            'context': {'body': body}
        }

    @api.multi
    def open_report_evaluacion_hoja_vida(self, context=None):
        datas = {
            'job_id': self.id
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'hr_job.report_evaluacion_hoja_vida',
            'data': datas
        }

    @api.multi
    def open_report_evaluation_curricular(self, context=None):
        datas = {
            'job_id': self.id
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'hr_job.report_evaluation_curricular',
            'data': datas
        }

    @api.multi
    def open_report_curricular(self, context=None):
        datas = {
            'job_id': self.id
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'hr_job.resultado_curricular',
            'data': datas
        }

    @api.multi
    def open_report_final(self, context=None):
        datas = {
            'job_id': self.id
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'hr_job.resultado_final',
            'data': datas
        }

    @api.multi
    def open_report_entrevista(self, context=None):
        datas = {
            'job_id': self.id
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'hr_job.entrevista',
            'data': datas
        }

    @api.multi
    def open_report_result_curricular(self, context=None):
        datas = {
            'job_id': self.id
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'hr_job.report_curriculum_evaluation',
            'data': datas
        }

    @api.multi
    def open_report_escrita(self, context=None):
        datas = {
            'job_id': self.id
        }
        return {
            'type': 'ir.actions.report.xml',
            'report_name': 'hr_job.resultado_escrita',
            'data': datas
        }


class HrApplicant(models.Model):
    _inherit = 'hr.applicant'
    _default_puntuacion = '0.0'
    full_name = None
    task_revision_curricular = fields.Char(string=u'Task Revision Curricular ')
    profile_user = fields.Many2one(
        comodel_name='res.users',
        string=u'Postulante'
    )
    sequence_stage_id = fields.Integer(
        compute='_get_sequence_stage_id'
    )
    mostrar_evaluacion_jurada = fields.Boolean(
        compute='_verificar_boton_evaluacion_jurada'
    )
    mostrar_evaluacion_curricular = fields.Boolean(
        compute='_verificar_boton_evaluacion_curricular'
    )
    mostrar_pestana_curricular = fields.Boolean(
        compute='_verificar_pestana_evaluacion_curricular'
    )
    mostrar_pestana_escrita = fields.Boolean(
        compute='_verificar_pestana_evaluacion_escrita'
    )
    # mostrar_campos_entrevista_en_curricular = fields.Boolean(
    #     compute='_mostrar_campos_entrevista_en_curricular'
    # )
    # mostrar_campos_entrevista_en_escrita = fields.Boolean(
    #     compute='_mostrar_campos_entrevista_en_escrita'
    # )
    es_examen = fields.Boolean(
        compute='_get_es_examen'
    )
    resultado_curricular = fields.Selection(
        selection=[('2_no_apto', 'NO APTO'), ('1_apto', 'APTO')],
        string=u'Resultado de Evaluación Curricular',
        store=True,
        default='2_no_apto'
    )
    result_exam = fields.Selection(
        selection=[('2_no_apto', 'NO APTO'), ('1_apto', 'APTO')],
        string=u"Resultado Examen de Conocimientos"
    )
    result_person = fields.Selection(
        selection=[('2_no_apto', 'NO APTO'), ('1_apto', 'APTO')],
        string=u"Resultado Entrevista"
    )
    result = fields.Selection(
        selection=[('2_no_apto', 'NO APTO'), ('1_apto', 'APTO')],
        string=u"Resultado Final"
    )
    partner_name = fields.Char(
        string=u'Nombres',
        related='profile_user.partner_name',
        store="True",
    )
    partner_firstname = fields.Char(
        string=u'Apellido paterno',
        related='profile_user.partner_firstname',
        store="True",
    )
    partner_secondname = fields.Char(
        string=u'Apellido materno',
        related='profile_user.partner_secondname',
        store="True",
    )
    partner_birthdate = fields.Date(
        string=u'Fecha nacimiento',
        # related='profile_user.partner_birthdate',
        store="True",
    )

    partner_document_number = fields.Char(
        string=u'Número Documento',
        # related='profile_user.partner_document_number',
        store=True,
        size=10
    )
    partner_vat = fields.Char(
        string=u'RUC',
        # related='profile_user.partner_vat',
        store=True,
        size=12
    )
    state_id = fields.Many2one(
        comodel_name='res.country.state',
        string=u'Departamento',
        domain="[('country_id', '=', country_id),"
               " ('state_id', '=', false),"
               " ('province_id', '=', false)]"
    )
    province_id = fields.Many2one(
        comodel_name='res.country.state',
        string=u'Provincia',
        domain="[('state_id', '=', state_id), "
               "('province_id', '=', false)]"
    )
    district_id = fields.Many2one(
        comodel_name='res.country.state',
        string=u'Distrito',
        domain="[('state_id', '=', state_id), "
               "('province_id', '=', province_id)]"
    )
    partner_professional = fields.Char(
        string=u'Colegio profesional'
    )
    enlable_number = fields.Boolean(
        string=u'Colegiatura habilitada'
    )
    partner_phone = fields.Char(
        string=u'Teléfono'
    )
    partner_sex = fields.Selection(
        string='Sexo',
        selection=[
            ('m', 'Masculino'),
            ('f', 'Femenino')
        ]
    )
    partner_civil = fields.Selection(
        selection=[
            ('s', 'Soltero'),
            ('c', 'Casado'),
            ('v', 'Viudo'),
            ('d', 'Divorciado')
        ],
        string=u'Estado civil'
    )
    partner_street = fields.Char(
        string=u'Dirección'
    )
    study_ids = fields.One2many(
        comodel_name='applicant.study',
        inverse_name='applicant_id',
        string=u'Estudios'
    )
    training_ids = fields.One2many(
        comodel_name='applicant.training',
        inverse_name='applicant_id',
        string=u'Capacitaciones'
    )
    work_experience_ids = fields.One2many(
        comodel_name='applicant.work.experience',
        inverse_name='applicant_id',
        string=u'Experiencia laboral'
    )
    documentacion_completada = fields.Boolean(
        string=u'Documentación sutentatoria completada'
    )
    partner_image = fields.Binary(
        string=u'Foto de Perfil',
        related='profile_user.image'
    )
    file_url_colegiatura = fields.Char(
        string=u'Certificado de Habilidad',
        related='profile_user.file_url_colegiatura'
    )
    file_name_colegiatura = fields.Char(
        string=u'Certificado de Habilidad',
        related='profile_user.file_name_colegiatura',
        track_visibility='allways'
    )
    partner_image_dni = fields.Binary(
        string=u'DNI',
        related='profile_user.partner_image_dni',
        track_visibility='allways'
    )
    file_partner_image_dni = fields.Char(
        string=u'',
        related='profile_user.file_partner_image_dni',
        track_visibility='allways'
    )
    file_url_dni = fields.Char(
        string=u'DNI',
        related='profile_user.file_url_dni'
    )
    es_de_fuerzas_armadas = fields.Boolean(
        string=u'Es de Fuerzas armadas',
        related='profile_user.es_de_fuerzas_armadas',
        track_visibility='allways'
    )
    file_fuerzas_armadas = fields.Binary(
         string=u'Licencia de Fuerzas Armadas',
         related='profile_user.file_fuerzas_armadas',
         track_visibility='allways'
     )
    file_url_fuerzas_armadas = fields.Char(
        string=u'Licencia de Fuerzas Armadas',
        related='profile_user.file_url_fuerzas_armadas'
    )
    file_name_fuerzas_armadas = fields.Char(
        string=u'Licencia de Fuerzas Armadas',
        related='profile_user.file_name_fuerzas_armadas',
        track_visibility='allways'
    )
    file_name_fuerzas_armadas_new = fields.Char(
        string=u'Licencia de Fuerzas Armadas',
        related='profile_user.file_name_fuerzas_armadas_new',
        track_visibility='allways'
    )
    es_discapacitado = fields.Boolean(
        string=u'Es Discapacitado',
        related='profile_user.es_discapacitado',
        track_visibility='allways'
    )
    file_discapacitado = fields.Binary(
        string=u'Certificado Discapacidad',
        track_visibility='allways',
        related='profile_user.file_discapacitado',
    )
    file_url_discapacitado = fields.Char(
        string=u'Certificado Discapacidad',
        related='profile_user.file_url_discapacitado'
    )
    file_name_discapacitado = fields.Char(
        string=u'Certificado de Discapacidad',
        track_visibility='allways',
        related='profile_user.file_name_discapacitado'
    )
    file_name_discapacitado_new = fields.Char(
        string=u'Certificado de Discapacidad',
        track_visibility='allways',
        related='profile_user.file_name_discapacitado_new'
    )

    register_date_application = fields.Datetime(u'Fecha y Hora de Envio')
    report_rml = fields.Char(u'')
    register_filename_application = fields.Char(u'')
    register_file_application = fields.Binary(u'Archivo Generado')

    autogenerated = fields.Char(
        compute='_compute_autogenerated'
    )
    tabla_puntuacion_id = fields.Many2one(
        'hr.pncm.tabla_puntuacion',
        string=u'Tabla de puntuación',
        compute="_get_default_puntuacion"
    )
    puntuacion_nivel_educativo = fields.Many2one(
        'hr.pncm.puntuacion.nivel_educativo',
        domain="[('tabla_puntuacion_id', '=', tabla_puntuacion_id)]"
    )
    puntuacion_nivel_experiencia = fields.Many2one(
        'hr.pncm.puntuacion.nivel_experiencia',
        domain="[('tabla_puntuacion_id', '=', tabla_puntuacion_id)]"
    )
    puntuacion_nivel_capacitacion = fields.Many2one(
        'hr.pncm.puntuacion.nivel_capacitacion',
        domain="[('tabla_puntuacion_id', '=', tabla_puntuacion_id)]"
    )
    puntuacion_academica = fields.Float(
        string=u'Puntuación Formación Académica',
        default=0
    )
    puntuacion_experiencia = fields.Float(
        string=u'Puntuación Experiencia Laboral Expecifica',
        default=0
    )
    puntuacion_capacitacion = fields.Float(
        string=u'Puntuación Capacitación Relacionada al Servicio',
        default=0
    )
    puntuacion_curricular = fields.Float(
        string=u'Puntuación Total',
        default=0
    )
    fecha_entrevista = fields.Date(
        string=u'Fecha de entrevista'
    )
    hora_entrevista = fields.Char(
        string=u'Hora'
    )
    fecha_entrevista_escrita = fields.Date(
        string=u'Fecha de entrevista'
    )
    hora_entrevista_escrita = fields.Char(
        string=u'Hora'
    )
    asistio_entrevista = fields.Boolean(
        string=u'Asistió',
        default=True
    )
    asistio_escrita = fields.Boolean(
        string=u'Asistió',
        default=True
    )
    nota_escrita = fields.Float(
        string=U'Nota de Evaluación'
    )
    resultado_escrita = fields.Selection(
        selection=[('2_no_apto', 'NO APTO'), ('1_apto', 'APTO')],
        string=u'Resultado de Evaluación Escrita',
        store=True
    )
    tipo_bonificacion = fields.Many2one(
        'hr.pncm.tipo_bonificacion',
        string=u'Bonificación'
    )
    puntaje_analisis = fields.Selection(
        selection=domain.JobUtils.get_lista_puntajes(),
        string=u'Análisis',
        default=_default_puntuacion
    )
    puntaje_oral = fields.Selection(
        selection=domain.JobUtils.get_lista_puntajes(),
        string=u'Comunicación Oral',
        default=_default_puntuacion
    )
    puntaje_creatividad = fields.Selection(
        selection=domain.JobUtils.get_lista_puntajes(),
        string=u'Creatividad/Innovación',
        default=_default_puntuacion
    )
    puntaje_control = fields.Selection(
        selection=domain.JobUtils.get_lista_puntajes(),
        string=u'Control',
        default=_default_puntuacion
    )
    puntaje_empatia = fields.Selection(
        selection=domain.JobUtils.get_lista_puntajes(),
        string=u'Empatia',
        default=_default_puntuacion
    )
    puntaje_iniciativa = fields.Selection(
        selection=domain.JobUtils.get_lista_puntajes(),
        string=u'Iniciativa',
        default=_default_puntuacion
    )
    puntaje_negociacion = fields.Selection(
        selection=domain.JobUtils.get_lista_puntajes(),
        string=u'Negociación',
        default=_default_puntuacion
    )
    puntaje_planificacion = fields.Selection(
        selection=domain.JobUtils.get_lista_puntajes(),
        string=u'Planificación',
        default=_default_puntuacion
    )
    puntaje_tecnico = fields.Float(
        string=u'Puntaje Conocimiento Técnico'
    )
    puntuacion_entrevista = fields.Float(
        string=u'Puntuación Total'
    )
    puntaje_bonificacion = fields.Float(
        string=u'Puntaje Bonificación (PB)'
    )
    puntaje_curricular_read = fields.Float(
        string=u'Puntuación Curricular (PC)',
        compute='_get_puntaje_curricular'
    )
    puntaje_entrevista_read = fields.Float(
        string=u'Puntuación Entrevista (PE)',
        compute='_get_puntaje_entrevista'
    )
    puntuacion_total = fields.Float(
        string=u'Puntuación Total (PT)'
    )
    puntuacion_minima_entrevista_read = fields.Float(
        string=u'Puntuación mínima requerida',
        compute='_get_puntuacion_minima_entrevista',
        store=True
    )
    puntuacion_maximo_entrevista_read = fields.Float(
        string=u'Puntuación máximo requerida',
        compute='_get_puntuacion_minima_entrevista'
    )
    formula_read = fields.Char(
        string=u'Formula Bonificación',
        compute='_get_formula_bonificacion',
        store=False
    )
    evaluacion_curricular_postulante = fields.One2many(
        'detail.evaluation.applicant',
        'postulacion_id',
        u'Detalle de Evaluación Curricular',
        domain=[('perfil_puesto', '!=', False)]
    )

    rnsdd = fields.Boolean(
        string = u'RNSDD',
        default=False
    )
    redam = fields.Boolean(
        string=u'REDAM',
        default=False
    )
    sunedu = fields.Boolean(
        string=u'SUNEDU',
        default=False
    )
    observacion = fields.Text(
        string=u'Observación'
    )

    @api.model
    def create(self, values):
        values['task_revision_curricular'] = 'open'
        return super(HrApplicant, self).create(values)

    @api.multi
    def write(self, values):
        _logger.info('p1')
        ctx = dict(self._context or {})
        if not 'first_execute' in values :
            if 'evaluacion_curricular_postulante' in values:
                if len(values['evaluacion_curricular_postulante'])>0 :
                    apto=True
                    details_profile_applicant= self.env['detail.evaluation.applicant']\
                        .search(
                        [
                            ('perfil_puesto', '!=', False),
                            ('postulacion_id', '=', self.id)
                        ]
                    )
                    index=0
                    for obj_ev_cv in values['evaluacion_curricular_postulante']:
                        if obj_ev_cv[2]:
                            if obj_ev_cv[2]['status_evaluation'] == '2_no_acumple':
                                apto=False
                                break
                        elif details_profile_applicant[index].status_evaluation == '2_no_acumple':
                            apto=False
                            break
                        index+=1
                    values['resultado_curricular'] = '1_apto' if apto else '2_no_apto'

        _logger.info('p2')

        if 'resultado_curricular' in values \
                and values['resultado_curricular'] == '2_no_apto':
            values['fecha_entrevista'] = False
            values['hora_entrevista'] = False
        if 'resultado_curricular' in values \
                and not values['resultado_curricular']:
            values['fecha_entrevista'] = False
            values['hora_entrevista'] = False

        if 'resultado_escrita' in values \
                and values['resultado_escrita'] == '2_no_apto':
            values['fecha_entrevista_escrita'] = False
            values['hora_entrevista_escrita'] = False
        if 'resultado_escrita' in values \
                and not values['resultado_escrita']:
            values['fecha_entrevista_escrita'] = False
            values['hora_entrevista_escrita'] = False

        _logger.info('p3')

        if not ('puntuacion_minima_entrevista_read' in values):
            tabla_puntuation = self._get_tabla_puntuacion_for_job()

            if tabla_puntuation:
                values['puntuacion_minima_entrevista_read'] = \
                    tabla_puntuation.puntuacion_min_entrevista

        _logger.info('p4')

        return super(HrApplicant, self.with_context(ctx)).write(values)

    @api.onchange('evaluacion_curricular_postulante')
    def _on_change_evaluation_profile_applicant(self):
        validation = False
        if self.evaluacion_curricular_postulante and len(
                self.evaluacion_curricular_postulante) > 0:
            validation = True
            for profile_applicant in self.evaluacion_curricular_postulante:
                if profile_applicant.status_evaluation != '1_cumple':
                    validation = False
                    break

        if validation:
            adjunto_documentos = self._verificar_documentacion_adjunta()
            if adjunto_documentos:
                self.resultado_curricular = '1_apto'
            else:
                self.resultado_curricular = '2_no_apto'
                raise ValidationError(u'El postulante no adjunto documento '
                                      u'alguno, no puede ser declarado '
                                      u'como apto')
        else:
            self.resultado_curricular = '2_no_apto'

    @api.onchange('resultado_curricular')
    def _on_change_resultado_curricular(self):
        validation = False
        if self.evaluacion_curricular_postulante and len(
                self.evaluacion_curricular_postulante) > 0:
            validation = True
            for profile_applicant in self.evaluacion_curricular_postulante:
                if profile_applicant.status_evaluation != '1_cumple':
                    validation = False
                    break

        self.resultado_curricular = '1_apto' if validation else '2_no_apto'

    @api.multi
    def paso_declaracion_jurada(self):
        sequence = 2
        stage = self.env['hr.recruitment.stage']. \
            search([('sequence', '=', sequence)])
        if stage:
            self.write(dict(stage_id=stage.id))

    @api.multi
    def no_paso_declaracion_jurada(self):
        if self.observacion:
            if self.observacion.strip() == '':
                raise ValidationError(
                    u'Si declara el postulante como no apto en '
                    u'esta etapa, debe ingresar una observación')
            else:
                self.write(dict(observacion=self.observacion))
                return {'type': 'ir.actions.client', 'tag': 'reload'}
        else:
            raise ValidationError(u'Si declara el postulante como no apto en '
                                  u'esta etapa, debe ingresar una observación')

    @api.multi
    def paso_evaluacion_curricular(self):
        sequence = 4
        mensaje = False
        result = not (not self.puntuacion_nivel_educativo)

        if result:
            result = not (not self.puntuacion_nivel_experiencia)

        if result:
            result = not (not self.puntuacion_nivel_capacitacion)

        if result:
            result = not (not self.resultado_curricular)

        if result:
            if not self.puntuacion_curricular \
                    or self.puntuacion_curricular == 0:
                result = False

        if result:
            result = self._verificar_documentacion_adjunta()
            if not result:
                mensaje = u'El postulante no adjunto documento alguno, ' \
                          u'no puede pasar a la siguiente etapa'

            if result:
                if self.resultado_curricular == '2_no_apto':
                    result = False
                    mensaje = u'Un postulante declarado como No apto, ' \
                              u'no puede pasar a la siguiente etapa'
                else:
                    if not self.job_id.exam:
                        result = not (not self.fecha_entrevista)
                        if result:
                            result = not (not self.hora_entrevista)
                            if result:
                                result = (self.hora_entrevista.strip() != '')

                        if not result:
                            mensaje = u'Si declaro al postulante como apto ' \
                                      u'debe ingresar ' \
                                      u'fecha y hora de la entrevista'
        else:
            mensaje = u'Debe ingresar todos los puntajes y resultado de la ' \
                      u'evaluación curricular'

        if result:
            if self.job_id and self.job_id.exam:
                sequence = 3

            stage = self.env['hr.recruitment.stage']. \
                search([('sequence', '=', sequence)])
            if stage:
                self.write(dict(stage_id=stage.id))
        else:
            raise ValidationError(mensaje)

    def _verificar_documentacion_adjunta(self):
        if not self.study_ids and not self.training_ids \
                and not self.work_experience_ids:
            return True

        if len(self.study_ids) == 0 and len(self.training_ids) == 0 \
                and len(self.work_experience_ids):
            return True

        if self.study_ids and len(self.study_ids) > 0:
            for r in self.study_ids:
                if r.file_url:
                    return True

        if self.training_ids and len(self.training_ids) > 0:
            for r in self.training_ids:
                if r.file_url:
                    return True

        if self.work_experience_ids and len(self.work_experience_ids) > 0:
            for r in self.work_experience_ids:
                if r.file_url:
                    return True

        return False

    @api.multi
    def paso_evaluacion_escrita(self):
        if self.resultado_escrita == '2_no_apto':
            raise ValidationError(u'El postulante no puede pasar a '
                                  u'entrevista ya que figura como '
                                  u'NO APTO en la evaluación escrita')
        else:
            result = not (not self.fecha_entrevista_escrita)
            mensaje = False
            if result:
                result = not (not self.hora_entrevista_escrita)
                if result:
                    result = (self.hora_entrevista_escrita.strip() != '')

            if not result:
                mensaje = u'Si declaro al postulante como apto ' \
                          u'debe ingresar ' \
                          u'fecha y hora de la entrevista'

            if not mensaje:
                stage = self.env['hr.recruitment.stage']. \
                    search([('sequence', '=', 4)])
                self.write(dict(stage_id=stage.id))
            else:
                raise ValidationError(mensaje)

    @api.multi
    def paso_entrevista(self):
        sequence = 5
        mensaje = False
        result = True
        if self.asistio_entrevista:
            result = not (not self.puntaje_analisis)

            if result:
                result = not (not self.puntaje_oral)

            if result:
                result = not (not self.puntaje_creatividad)

            if result:
                result = not (not self.puntaje_control)

            if result:
                result = not (not self.puntaje_empatia)

            if result:
                result = not (not self.puntaje_iniciativa)

            if result:
                result = not (not self.puntaje_negociacion)

            if result:
                result = not (not self.puntaje_planificacion)

            if result:
                result = not (not self.puntaje_tecnico)

            if result:
                result = not (not self.result_person)

            if result:
                if not self.puntuacion_entrevista \
                        or self.puntuacion_entrevista == 0:
                    result = False

        if result:
            if not self.asistio_entrevista:
                result = False
                mensaje = u'Un postulante que no asistion a la entrevista, ' \
                          u'no puede pasar a la siguiente etapa'

            if result:
                if self.result_person == '2_no_apto':
                    result = False
                    mensaje = u'Un postulante declarado como No apto, ' \
                              u'no puede pasar a la siguiente etapa'

            if result:
                self._get_puntuacion_minima_entrevista()
                if self.puntuacion_entrevista < \
                        self.puntuacion_minima_entrevista_read:
                    result = False
                    mensaje = u'Un postulante que no tiene el puntaje ' \
                              u'mínimo en entrevista, ' \
                              u'no puede pasar a la siguiente etapa'

                if result:
                    if self.puntuacion_entrevista > \
                            self.puntuacion_maximo_entrevista_read:
                        result = False
                        mensaje = u'El puntaje de la entrevista no puede ' \
                                  u'superar el valor máximo establecido'
        else:
            mensaje = u'Debe ingresar todos los puntajes y resultado de la ' \
                      u'evaluación de entrevista'

        if result:
            stage = self.env['hr.recruitment.stage']. \
                search([('sequence', '=', sequence)])
            if stage:
                apto_final = '2_no_apto'
                if self.puntuacion_entrevista >= \
                        self.puntuacion_minima_entrevista_read:
                    apto_final = '1_apto'
                self.write(
                    dict(stage_id=stage.id,
                         puntuacion_total=self._calcular_puntaje_total(),
                         result=apto_final))
        else:
            raise ValidationError(mensaje)

    @api.one
    def _get_puntaje_curricular(self):
        self.puntaje_curricular_read = self.puntuacion_curricular

    @api.one
    def _get_formula_bonificacion(self):
        if self.tipo_bonificacion:
            self.formula_read = self.tipo_bonificacion.formula

    @api.one
    def _get_puntaje_entrevista(self):
        self.puntaje_entrevista_read = self.puntuacion_entrevista

    @api.onchange('tipo_bonificacion')
    def _on_change_bonificacion(self):
        self._calcular_bonificacion()
        self.puntuacion_total = self._calcular_puntaje_total()
        self._get_formula_bonificacion()

    def _calcular_bonificacion(self):
        if self.tipo_bonificacion:
            bonificacion = domain.BonificacionFactory.get_instance(
                self.tipo_bonificacion.acronimo)
            if bonificacion:
                self.puntaje_bonificacion = \
                    bonificacion.calcular_puntaje_bonificacion(self)
        else:
            self.puntaje_bonificacion = 0

    def _calcular_puntaje_total(self):
        if self.tipo_bonificacion:
            bonificacion = domain.BonificacionFactory.get_instance(
                self.tipo_bonificacion.acronimo)
            if bonificacion:
                return bonificacion.calcular_puntaje_total(self)
        else:
            return self.puntuacion_curricular + \
                   self.puntuacion_entrevista

    @api.onchange(
        'puntaje_analisis',
        'puntaje_oral',
        'puntaje_creatividad',
        'puntaje_control',
        'puntaje_empatia',
        'puntaje_iniciativa',
        'puntaje_negociacion',
        'puntaje_planificacion',
        'puntaje_tecnico',
    )
    def _sumar_puntajes_entrevista(self):
        suma = 0.0
        suma += float(self.puntaje_analisis) \
            if self.puntaje_analisis else 0.0
        suma += float(self.puntaje_oral) if self.puntaje_oral else 0.0
        suma += float(self.puntaje_creatividad) \
            if self.puntaje_creatividad else 0.0
        suma += float(self.puntaje_control) if self.puntaje_control else 0.0
        suma += float(self.puntaje_empatia) if self.puntaje_empatia else 0.0
        suma += float(self.puntaje_iniciativa) \
            if self.puntaje_iniciativa else 0.0
        suma += float(self.puntaje_negociacion) \
            if self.puntaje_negociacion else 0.0
        suma += float(self.puntaje_planificacion) \
            if self.puntaje_planificacion else 0.0
        suma += self.puntaje_tecnico if self.puntaje_tecnico else 0.0
        self.puntuacion_entrevista = suma

    @api.onchange('puntuacion_entrevista')
    def _on_change_puntuacion_entrevista(self):
        self._sumar_puntajes_entrevista()
        self._get_puntuacion_minima_entrevista()
        if self.puntuacion_entrevista:
            if self.puntuacion_entrevista \
                    >= self.puntuacion_minima_entrevista_read:
                self.result_person = '1_apto'
                self.result = '1_apto'
            else:
                self.result_person = '2_no_apto'
                self.result = '2_no_apto'

            self.puntuacion_total = self._calcular_puntaje_total()

    @api.onchange('puntuacion_total')
    def _on_change_puntaje_total(self):
        self.puntuacion_total = self._calcular_puntaje_total()

    @api.onchange('puntaje_bonificacion')
    def _on_change_puntaje_bonificacion(self):
        self._calcular_bonificacion()

    @api.one
    def _get_sequence_stage_id(self):
        self.sequence_stage_id = self.stage_id.sequence

    @api.one
    def _verificar_boton_evaluacion_jurada(self):
        self.mostrar_evaluacion_jurada = False
        directiva_convocatoria = self.job_id.cronograma_postulacion.directiva
        if directiva_convocatoria:
            directiva_vigente = domain.JobUtils.get_directiva_vigente()
            if directiva_vigente == directiva_convocatoria:
                self.mostrar_evaluacion_jurada = True

    @api.one
    def _verificar_boton_evaluacion_curricular(self):
        directiva_convocatoria = self.job_id.cronograma_postulacion.directiva
        if directiva_convocatoria:
            directiva_vigente = domain.JobUtils.get_directiva_vigente()
            if directiva_convocatoria == directiva_vigente:
                self.mostrar_evaluacion_curricular = \
                    (self.stage_id.sequence == 2)
            else:
                self.mostrar_evaluacion_curricular = \
                    (self.stage_id.sequence in [1, 2])
        else:
            self.mostrar_evaluacion_curricular = \
                (self.stage_id.sequence in [1, 2])

    @api.one
    def _verificar_pestana_evaluacion_curricular(self):
        directiva_convocatoria = self.job_id.cronograma_postulacion.directiva
        if directiva_convocatoria:
            directiva_vigente = domain.JobUtils.get_directiva_vigente()
            if directiva_convocatoria == directiva_vigente:
                self.mostrar_pestana_curricular = \
                    (self.stage_id.sequence >= 2)
            else:
                self.mostrar_pestana_curricular = \
                    (self.stage_id.sequence >= 1)
        else:
            self.mostrar_pestana_curricular = \
                (self.stage_id.sequence >= 1)

    @api.one
    def _verificar_pestana_evaluacion_escrita(self):
        if self.job_id.exam:
            self.mostrar_pestana_escrita = (self.stage_id.sequence >= 3)
        else:
            self.mostrar_pestana_escrita = False

    @api.one
    def _mostrar_campos_entrevista_en_curricular(self):
        if self.job_id.exam:
            self.mostrar_campos_entrevista_en_curricular = False
        else:
            self.mostrar_campos_entrevista_en_curricular = \
                self.resultado_curricular and self.resultado_curricular \
                == '2_apto'

    @api.one
    def _mostrar_campos_entrevista_en_escrita(self):
        if self.job_id.exam:
            self.mostrar_campos_entrevista_en_escrita = \
                self.resultado_escrita and self.resultado_escrita == '2_apto'
        else:
            self.mostrar_campos_entrevista_en_escrita = False

    @api.one
    def _get_es_examen(self):
        self.es_examen = self.job_id.exam

    @api.one
    def _get_puntuacion_minima_entrevista(self):
        tabla_puntuation = self._get_tabla_puntuacion_for_job()
        if tabla_puntuation:
            self.puntuacion_minima_entrevista_read = \
                tabla_puntuation.puntuacion_min_entrevista
            self.puntuacion_maximo_entrevista_read = \
                tabla_puntuation.puntuacion_max_entrevista

    def _get_tabla_puntuacion_for_job(self):
        tabla_puntuation = False
        if self.job_id:
            if self.job_id.cronograma_postulacion:
                tabla_puntuation = self.env['hr.pncm.tabla_puntuacion'].search(
                    [('state', '!=', 'borrador'),
                     ('requiere_evaluacion_escrita', '=', self.job_id.exam),
                     ('directiva', '=',
                      self.job_id.cronograma_postulacion.directiva)]
                )
            else:
                tabla_puntuation = self.env['hr.pncm.tabla_puntuacion'].search(
                    [('state', '=', 'activo'),
                     ('requiere_evaluacion_escrita', '=', self.job_id.exam)]
                )
        return tabla_puntuation



    @api.one
    def _get_default_puntuacion(self):
        tabla_puntuation = self._get_tabla_puntuacion_for_job()
        if tabla_puntuation:
            self.tabla_puntuacion_id = tabla_puntuation

    @api.onchange('puntuacion_nivel_educativo')
    def _onchange_nivel_educativo(self):
        if self.puntuacion_nivel_educativo:
            self.puntuacion_academica = self.puntuacion_nivel_educativo.puntaje
        else:
            self.puntuacion_academica = 0

        self._calcular_puntaje_curricular_total()

    @api.onchange('puntuacion_academica')
    def _onchange_texto_puntuacion_academica(self):
        if self.puntuacion_nivel_educativo:
            self.puntuacion_academica = self.puntuacion_nivel_educativo.puntaje
        else:
            self.puntuacion_academica = 0

    @api.onchange('puntuacion_nivel_experiencia')
    def _onchange_nivel_experiencia(self):
        if self.puntuacion_nivel_experiencia:
            self.puntuacion_experiencia = \
                self.puntuacion_nivel_experiencia.puntaje
        else:
            self.puntuacion_experiencia = 0

        self._calcular_puntaje_curricular_total()

    @api.onchange('puntuacion_experiencia')
    def _onchange_texto_puntuacion_experiencia(self):
        if self.puntuacion_nivel_experiencia:
            self.puntuacion_experiencia = \
                self.puntuacion_nivel_experiencia.puntaje
        else:
            self.puntuacion_experiencia = 0

    @api.onchange('puntuacion_nivel_capacitacion')
    def _onchange_nivel_capacitacion(self):
        if self.puntuacion_nivel_capacitacion:
            self.puntuacion_capacitacion = \
                self.puntuacion_nivel_capacitacion.puntaje
        else:
            self.puntuacion_capacitacion = 0

        self._calcular_puntaje_curricular_total()

    @api.onchange('puntuacion_capacitacion')
    def _onchange_texto_puntuacion_capacitacion(self):
        if self.puntuacion_nivel_capacitacion:
            self.puntuacion_capacitacion = \
                self.puntuacion_nivel_capacitacion.puntaje
        else:
            self.puntuacion_capacitacion = 0

    @api.onchange('puntuacion_curricular')
    def _onchange_texto_puntuacion_curricular(self):
        self._calcular_puntaje_curricular_total()

    def _calcular_puntaje_curricular_total(self):
        total = self.puntuacion_academica + self.puntuacion_experiencia \
                + self.puntuacion_capacitacion
        self.puntuacion_curricular = total

    @api.onchange('resultado_curricular')
    def _onchange_resultado_curricular(self):
        if self.resultado_curricular and \
                self.resultado_curricular == '1_apto':
            self.fecha_entrevista = \
                datetime.strptime(self.job_id.date_interview,
                                  DEFAULT_SERVER_DATE_FORMAT).date()
        else:
            self.fecha_entrevista = False
            self.hora_entrevista = False

    @api.one
    def _compute_autogenerated(self):
        self.autogenerated = 'CNV000' + str(self.id)

    @api.onchange('asistio_escrita')
    def _on_change_asistio_escrita(self):
        if not self.asistio_escrita:
            self.nota_escrita = 0
            self.resultado_escrita = '2_no_apto'
        else:
            self.resultado_escrita = False

    @api.onchange('nota_escrita')
    def _on_change_nota_escrita(self):
        if self.nota_escrita:
            tabla_puntuation = self._get_tabla_puntuacion_for_job()
            if tabla_puntuation:
                if self.nota_escrita \
                        >= tabla_puntuation.puntuacion_min_escrita \
                        and self.nota_escrita \
                        <= tabla_puntuation.puntuacion_max_escrita:
                    self.resultado_escrita = '1_apto'
                else:
                    self.resultado_escrita = '2_no_apto'
                    if self.nota_escrita \
                            > tabla_puntuation.puntuacion_max_escrita:
                        pm = tabla_puntuation.puntuacion_max_escrita
                        raise ValidationError(u'La nota no '
                                              u'puede superar el puntaje '
                                              u'máximo de ' + str(pm))
        else:
            self.resultado_escrita = '2_no_apto'

    @api.onchange('resultado_escrita')
    def _on_change_resultado_escrita(self):
        if self.nota_escrita:
            tabla_puntuation = self._get_tabla_puntuacion_for_job()
            if tabla_puntuation:
                if self.nota_escrita \
                        >= tabla_puntuation.puntuacion_min_escrita \
                        and self.nota_escrita \
                        <= tabla_puntuation.puntuacion_max_escrita:
                    self.resultado_escrita = '1_apto'
                    self.fecha_entrevista_escrita = \
                        datetime.strptime(self.job_id.date_interview,
                                          DEFAULT_SERVER_DATE_FORMAT).date()
                else:
                    self.resultado_escrita = '2_no_apto'
                    self.fecha_entrevista_escrita = False
                    self.hora_entrevista_escrita = False
                    if self.nota_escrita \
                            > tabla_puntuation.puntuacion_max_escrita:
                        pm = tabla_puntuation.puntuacion_max_escrita
                        raise ValidationError(u'La nota no '
                                              u'puede superar el puntaje '
                                              u'máximo de ' + str(pm))


        else:
            self.resultado_escrita = '2_no_apto'

    @api.constrains(
        'nota_escrita'
    )
    def _contrain_nota_escrita(self):
        if self.sequence_stage_id == 3:
            tabla_puntuation = self._get_tabla_puntuacion_for_job()
            if tabla_puntuation:
                if self.nota_escrita < 0:
                    raise ValidationError(u'La nota debe ser '
                                          u'mayor o  igual que cero')
                elif self.nota_escrita \
                        > tabla_puntuation.puntuacion_max_escrita:
                    pm = tabla_puntuation.puntuacion_max_escrita
                    raise ValidationError(u'La nota no '
                                          u'puede superar el puntaje '
                                          u'máximo de ' + str(pm))

    @api.multi
    def update_url(self):
        for obj in self:
            if obj.profile_user:
                for study_app in obj.study_ids:
                    for study_profile in obj.profile_user.study_ids:
                        if study_profile.id == study_app.user_study_id.id:
                            study_app.write(
                                {'file_url': study_profile.file_url})

                for training_app in obj.training_ids:
                    for training_profile in obj.profile_user.training_ids:
                        if training_profile.id \
                                == training_app.user_training_id.id:
                            training_app.write(
                                {'file_url': training_profile.file_url})

                for work_app in obj.work_experience_ids:
                    for work_profile in obj.profile_user.work_experience_ids:
                        if work_profile.id == work_app.user_work_id.id:
                            work_app.write({'file_url': work_profile.file_url})

                name = "{} {} {} ".format(obj.partner_name,
                                          obj.partner_firstname,
                                          obj.partner_secondname)
                obj.write({'name': name})

    def generated_pdf(self):
        datas = {
            'ids': [self.id],
            'model': 'hr.applicant',
        }

        return {
            'type': 'ir.actions.report.xml',
            'report_name': "hr_job.report_application",
            'datas': datas,
        }


class ApplicantStudy(models.Model):
    _name = 'applicant.study'
    _description = 'Estudios'

    applicant_id = fields.Many2one(
        comodel_name='hr.applicant',
        string=u'Postulante',
        required=True
    )
    user_study_id = fields.Many2one(
        comodel_name='res.partner.study',
        string=u'Estudio Relacionado',
    )
    level_study = fields.Many2one(
        'hr.employee.education.level',
        string=u'Nivel de Estudios'
    )
    career = fields.Char(
        string=u'Título o grado',
        store=True,
        required=False
    )
    specialty = fields.Char(
        string=u'Especialidad',
        store=True,
    )
    date = fields.Date(
        string=u'Fecha de expedición del título',
        store=True,
        default=fields.Date().today()
    )
    university = fields.Char(
        string=u'Universidad',
        store=True,
    )
    country_id = fields.Many2one(
        comodel_name='res.country',
        store=True,
        string=u'País'
    )
    state_id = fields.Many2one(
        comodel_name='res.country.state',
        domain="[('country_id', '=', country_id)]",
        store=True,
        string=u'Cuidad'
    )
    file_url = fields.Char(u'URL')
    file = fields.Binary(
        string=u'Archivo',
        store=True,
        help=u'Extensión doc, docx, xls, xlsx, pdf, jpg, png'
    )
    filename = fields.Char(
        string=u'Nombre del archivo',
        store=True,
    )
    is_career = fields.Boolean(
        string=u'No tiene título',
        store=True
    )


class ApplicantTraining(models.Model):
    _name = 'applicant.training'
    _description = 'Capacitacion'

    applicant_id = fields.Many2one(
        comodel_name='hr.applicant',
        string=u'Postulante',
        required=True
    )
    user_training_id = fields.Many2one(
        comodel_name='res.partner.training',
        string=u'Estudio Relacionado'
    )
    name = fields.Char(
        string=u'Capacitación',
        store=True,
        required=True
    )
    date_start = fields.Date(
        string=u'Fecha de inicio',
        store=True,
        default=fields.Date().today()
    )
    date_end = fields.Date(
        string=u'Fecha de término',
        store=True,
    )
    institution = fields.Char(
        string=u'Institución',
        store=True,
    )
    country_id = fields.Many2one(
        comodel_name='res.country',
        store=True,
        string=u'País'
    )
    state_id = fields.Many2one(
        comodel_name='res.country.state',
        store=True,
        domain="[('country_id', '=', country_id)]",
        string=u'Ciudad'
    )
    time = fields.Float(
        string=u'Horas lectivas',
        store=True
    )
    file = fields.Binary(
        string=u'Archivo',
        store=True,
        help=u'Extensión doc, docx, xls, xlsx, pdf, jpg, png'
    )
    filename = fields.Char(
        string=u'Nombre del archivo',
        store=True,
    )
    file_url = fields.Char(u'URL')


class ApplicantWorkExperience(models.Model):
    _name = 'applicant.work.experience'
    _description = 'Experiencia laboral'

    applicant_id = fields.Many2one(
        comodel_name='hr.applicant',
        string=u'Postulante',
        required=True
    )
    user_work_id = fields.Many2one(
        comodel_name='res.partner.work.experience',
        string=u'Experiencia Relacionada',
    )
    name = fields.Char(
        string=u'Nombre de la entidad o  empresa',
        required=True
    )
    date_start = fields.Date(
        string=u'Fecha de inicio',
        default=fields.Date().today()
    )
    date_end = fields.Date(
        string=u'Fecha de término',
        default=fields.Date().today()
    )
    type_experience = fields.Selection(
        selection=[
            ('general', u'LABORAL GENERAL'),
            ('especifica', u'LABORAL ESPECÍFICA')
        ],
        default='general',
        string=u'Tipo de Experiencia'
    )
    charge = fields.Char(
        string=u'Cargo desempeñado'
    )
    description = fields.Text(
        string=u'Descripción del trabajo realizado'
    )
    filename = fields.Char(
        string=u'Nombre del archivo',
        # related='user_work_id.filename'
    )
    file = fields.Binary(
        string=u'Archivo',
        help=u'Extensión doc, docx, xls, xlsx, pdf, jpg, png',
        # related='user_work_id.file'
    )
    file_url = fields.Char(u'URL')
    tiempo_cargo = fields.Char(
        string=u'Tiempo en el cargo',
        compute='_get_tiempo_en_el_cargo'
    )

    @api.one
    def _get_tiempo_en_el_cargo(self):
        texto = ''
        if self.date_end and self.date_start:
            dia_inicio = datetime. \
                strptime(self.date_start, DEFAULT_SERVER_DATE_FORMAT). \
                date().day
            mes_inicio = datetime. \
                strptime(self.date_start, DEFAULT_SERVER_DATE_FORMAT). \
                date().month
            anio_inicio = datetime. \
                strptime(self.date_start, DEFAULT_SERVER_DATE_FORMAT). \
                date().year

            dia_fin = datetime. \
                strptime(self.date_end, DEFAULT_SERVER_DATE_FORMAT).date().day
            mes_fin = datetime. \
                strptime(self.date_end, DEFAULT_SERVER_DATE_FORMAT). \
                date().month
            anio_fin = datetime. \
                strptime(self.date_end, DEFAULT_SERVER_DATE_FORMAT).date().year

            if dia_fin - dia_inicio >= 0:
                dias = dia_fin - dia_inicio
            else:
                str_fecha_aux = '{}-{}-01'.format(anio_inicio, mes_inicio)
                fecha_aux = datetime. \
                    strptime(str_fecha_aux, DEFAULT_SERVER_DATE_FORMAT)
                fecha_minus = \
                    fecha_aux + relativedelta(months=1)-relativedelta(days=1)
                dias = fecha_minus.day - dia_inicio + dia_fin
                mes_fin = mes_fin - 1

            if mes_fin - mes_inicio >= 0:
                meses = mes_fin - mes_inicio
            else:
                meses = (mes_fin - mes_inicio) + 12
                anio_fin = anio_fin - 1

            anios = anio_fin - anio_inicio

            if dias == 0:
                cada_dia = ''
            elif dias == 1:
                cada_dia = '{} dia'.format(dias)
            else:
                cada_dia = '{} dias'.format(dias)

            if meses == 0:
                cada_mes = ''
            elif meses == 1:
                cada_mes = '{} mes'.format(meses)
            else:
                cada_mes = '{} meses'.format(meses)

            if anios == 0:
                cada_anio = ''
                if meses > 1:
                    texto = texto + 'n'
                if meses == 0 and dias > 1:
                    texto = texto + 'n'

            elif anios == 1:
                cada_anio = '{} año'.format(anios)
            else:
                cada_anio = '{} años'.format(anios)
                texto = texto + 'n'

            if meses != 0:
                if dias != 0:
                    cada_mes = cada_mes + ' y '
                    if anios != 0:
                        cada_anio = cada_anio + ', '
                else:
                    if anios != 0:
                        cada_anio = cada_anio + ' y '

            else:
                if anios != 0 and dias != 0:
                    cada_mes = cada_mes + ' y '

            texto = cada_anio + cada_mes + cada_dia
            self.tiempo_cargo = texto
        else:
            self.tiempo_cargo = ''


class PerfilPuesto(models.Model):
    _name = 'perfil.puesto.trabajo'

    puesto_id = fields.Many2one(
        'hr.job',
        u'Puesto de Trabajo',
        required=True
    )
    name = fields.Text(
        string=u'Descripción',
        required=True
    )


class Notices(models.Model):
    _name = 'hr.job.notices'

    puesto_id = fields.Many2one(
        'hr.job',
        u'Convocatoria'
    )
    name = fields.Char(
        string=u'Descripción'
    )
    filename = fields.Char(
        string=u'Nombre del archivo'
    )
    file = fields.Binary(
        string=u'Archivo',
        help=u'Extensión doc, docx, xls, xlsx, pdf, jpg, png'
    )


class ApplicationTimeLine(models.Model):
    _name = 'hr.job.timeline'

    name = fields.Char(u'Cronograma')
    year = fields.Integer(
        string=u'Año',
        required=True,
        default=lambda self: self._get_year_now()
    )
    number = fields.Integer(
        string=u'N°',
        required=True,
        default=lambda self: self._get_correlative_timeline()
    )
    directiva = fields.Selection(
        selection=domain.JobUtils.get_lista_directivas(),
        string=u'Directiva',
        default=lambda self: self._compute_directiva_vigente(),
        readonly=True
    )

    def _get_year_now(self):
        now = datetime.now()
        return now.year

    def _get_correlative_timeline(self):
        return self.search_count([('year', '=', self._get_year_now())]) + 1

    def _compute_directiva_vigente(self):
        return domain.JobUtils.get_directiva_vigente()


    @api.model
    def create(self, values):
        values['year'] = self._get_year_now()
        values['number'] = self._get_correlative_timeline()
        values['name'] = str(values['year'])+'-'+str(values['number']).zfill(2)
        return super(ApplicationTimeLine, self).create(values)

    _sql_constraints = [
        ('year_number_unique',
         'unique(year,number)',
         'Clave Duplicada!')
    ]


class DetailEvaluationCurricularApplicant(models.Model):
    _name = 'detail.evaluation.applicant'

    postulacion_id = fields.Many2one(
        'hr.applicant',
        u'Postulación',
        required=True
    )
    perfil_puesto = fields.Many2one(
        comodel_name='perfil.puesto.trabajo',
        string=u'Perfil Puesto',
        ondelete='set null'
    )
    name_perfil_puesto = fields.Char(
        string=u'Nombre del Puesto de Trabajo'
    )
    status_evaluation = fields.Selection(
        selection=[
            ('2_no_acumple', 'NO'),
            ('1_cumple', 'SI')
        ],
        string=u'Cumple el Perfil',
        required=True
    )
    _sql_constraints = [
        ('code_unique',
         'unique(postulacion_id, perfil_puesto)',
         u'Estimado usuario, no se pueden elegir dos valores la misma  '
         u'evaluación del perfil minimo.')
    ]


class FilesAnexos(models.Model):
    _name ='hr.job.filesanexos'

    file_anexos = fields.Binary(
        string=u'File Anexo',
        required=True
    )
    filename_anexos = fields.Char(
        string=u'Nombre de Archivo',
        required=True
    )
    name = fields.Char(
        string=u'Descripción',
        required=True
    )
