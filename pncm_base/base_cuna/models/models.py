# -*- coding: utf-8 -*-

from .base import months
from dateutil.relativedelta import relativedelta
from odoo import fields, models, api
from odoo.exceptions import ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
from datetime import datetime
import errno
import pytz
import os


def _drop_records(self, model, domain):
    self.env[model].search(domain).sudo().unlink()


def _obtener_tiempo(self, time):
    date_now = datetime.now(tz=pytz.timezone('America/Lima')).replace(tzinfo=None)
    if time == 'mes':
        return months[date_now.month - 1]
    else:
        rec = self.env['tabla.anios'].search([
            ('name', '=', date_now.year)
        ])
        return rec.id


def _set_fields_lines(self, lines, obj_id, type_name=None):
    """
        Completa la informacion de las afiliaciones dependiendo el tipo de cargo
    """
    for i in lines:
        if obj_id._name == 'afiliacion.organizacion':
            i.afiliacion_id = obj_id
            i.person_id = obj_id.person_id
            i.unidad_territorial_id = obj_id.unidad_territorial_id
        """Servicio alimentario"""
        if obj_id._name == 'pncm.infra.local':
            i.comite_gestion_id = obj_id.comite_id
            i.unidad_territorial_id = obj_id.ut_id
        if i.tipo_guia:
            type_name = i.tipo_guia.name
        tipo = self.env['tipo.persona'].search([('name', '=', type_name)])
        i.cargo = tipo.name
        i.tipo_guia = tipo


def accion_agregar(nombre, form_id, model, context):
    return {
        'type': 'ir.actions.act_window',
        'name': nombre,
        'view_type': 'form',
        'view_mode': 'form',
        'res_model': model,
        'views': [(form_id, 'form')],
        'view_id': form_id,
        'target': 'new',
        'context': context
    }


def filtrar_en_vistas(self, name, model, modo):
    """
        modo:
        # filtrar
        1 = unidad_territorial
        2 = servicios
        0 = ambos

    """
    action = {
        'name': name,
        'type': "ir.actions.act_window",
        'res_model': model,
        'view_type': "form",
        'view_mode': "tree,form,pivot,graph",
    }
    user = self.env['res.users'].browse(self._uid)
    if modo == 0:
        if user.unidad_territorial_id and user.servicio and user.servicio != 'ambas':
            action['domain'] = "[('unidad_territorial_id', 'in', %s), ('services', '=', '%s')]" % \
                               (user.unidad_territorial_id.ids, user.servicio)
        elif user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', 'in', %s)]" % user.unidad_territorial_id.ids
        elif user.servicio and user.servicio != 'ambas':
            action['domain'] = "[('services', '=', '%s')]" % user.servicio

    elif modo == 1:
        if user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', 'in', %s)]" % user.unidad_territorial_id.ids

    elif modo == 2:
        if user.servicio and user.servicio != 'ambas':
            action['domain'] = "[('services', '=', '%s')]" % user.servicio
    return action


def validar_fechas(fecha_inicio, fecha_fin):
    if fecha_fin and fecha_inicio:
        if fecha_inicio >= fecha_fin:
            raise ValidationError('La fecha de fin no puede ser menor o igual a la fecha de inicio')


def _controlador_ruta(self, nombre, archvio_binario, filename):
    path_os = self.env['ir.config_parameter'].get_param('path.filestatic')
    path_http = self.env['ir.config_parameter'].get_param('path.filestatic.http')
    usuario = self.env.user.id

    format_name_file = datetime.now().strftime("%Y%m%d%H%M%S%f")
    name_file = (nombre + format_name_file) + os.path.splitext(filename)[1]
    path_url = '/' + str(usuario) + '/' + name_file
    path = path_os + path_url
    if not os.path.exists(os.path.dirname(path)):
        try:
            os.makedirs(os.path.dirname(path))
        except OSError as exc:
            if exc.errno != errno.EEXIST:
                raise
    with open(path, 'w') as f:
        f.write(archvio_binario)
    f.close()
    url = path_http + path_url
    return url, name_file


estado_servicio = [
    ('deteriorado', 'Deteriorado'),
    ('conservado', 'Conservado')
]

estados = [
    ('activo', 'Activo'),
    ('cerrado', 'Cerrado'),
]


class ActorComunal(models.Model):
    _inherit = 'pncm.comitegestion.actorcomunal'

    partner_id = fields.Many2one(
        comodel_name='res.partner',
        string='Integrante',
        required=True
    )
    fecha_creacion = fields.Date(
        string='Fecha Creacion',
        default=fields.Date.today()
    )
    fecha_mod = fields.Date(
        string=u'Fecha Modificación'
    )
    usuario_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Creador',
        default=lambda self: self.env.user
    )
    usuario_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Modifico'
    )
    usuario_bd_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Creador'
    )
    usuario_bd_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Modifico'
    )
    cunanet_id = fields.Integer()
    sisaf_id = fields.Integer()


class UnidadFamiliar(models.Model):
    _name = 'unidad.familiar'
    _inherit = 'tabla.base'

    facilitador_id = fields.Many2one(
        comodel_name='afiliacion.organizacion.lines',
        inverse_name='unidad_familiar_fac_id',
        string='Facilitador/a',
        domain="[('comite_gestion_id', '=', comite_gestion_id),"
               "('cargo','=', 'FACILITADOR'),"
               "('fecha_retiro', '=', False)]"
    )
    departamento_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Departamento',
        compute='_compute_provincia_vivienda_dpt',
        store=True
    )
    provincia_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Provincia',
        compute='_compute_provincia_vivienda_dpt',
        store=True
    )
    distrito_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Distrito',
        domain="[('state_id', '=', departamento_id), "
               "('province_id', '=', provincia_id)]",
        compute='_compute_provincia_vivienda_dpt',
        store=True
    )
    centro_poblado_id = fields.Many2one(
        comodel_name='res.country.state',
        string=u'Centro Poblado',
        required=True
    )
    vivienda_ids = fields.One2many(
        comodel_name='vivienda.familia',
        inverse_name='familia_id',
        string='Vivienda'
    )
    services = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento a Familias')
    ],
        string=u'Servicio',
        default=lambda self: False if self.env.user.servicio == 'ambos' else self.env.user.servicio,
        required=True
    )
    servicio_log = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento a Familias'),
        ('ambos', u'Ambos'),
    ],
        string=u'Servicio usuario logueado',
        default=lambda self: self.env.user.servicio
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        default=lambda self: self.env.user.unidad_territorial_id[0].id if self.env.user.unidad_territorial_id else False
    )
    unidad_territorial_id_log = fields.Many2many(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True), ('estado','=', 'activo')]",
        default=lambda self: self.env.user.unidad_territorial_id if self.env.user.unidad_territorial_id else False
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        inverse_name='familias_ids',
        domain="[('unidad_territorial_id','=',unidad_territorial_id),"
               "('state', '=', 'activo'),"
               "('servicio', '=', services)]",
        string=u'Comité de Gestión',
        required=True
    )
    migraciones_ids = fields.Many2many(
        comodel_name='migracion.familia',
        string='Familia'
    )
    integrantes_ids = fields.One2many(
        comodel_name='integrante.unidad.familiar',
        inverse_name='familia_id',
        string='Integrantes',
        store=True
    )
    state = fields.Selection(
        selection=[
            ('borrador', 'Borrador'),
            ('activo', 'Activo'),
            ('cerrado', 'Cerrado'),
        ],
        string='Estado',
        track_visibility='always',
        default='borrador'
    )
    fecha_inicio = fields.Date(
        string='Fecha Inicio',
        default=fields.Date.today(),
        readonly=True
    )
    fecha_fin = fields.Date(
        string='Fecha Cierre',
        readonly=True
    )
    hogar_activo = fields.Boolean(
        string='tiene algun hogar activo?',
        compute='compute_hogar_activo',
        store=True
    )
    fecha_activo = fields.Date(
        string='Fecha Activacion'
    )
    fecha_cierre = fields.Date(
        string='Fecha Activacion'
    )

    @api.onchange('unidad_territorial_id_log')
    def _onchange_domain(self):
        if self.unidad_territorial_id_log:
            return {'domain': {'unidad_territorial_id': [('id', 'in', self.unidad_territorial_id_log.ids)]}}
        else:
            return {'domain': {'unidad_territorial_id': [('is_office', '=', True), ('estado', '=', 'activo')]}}

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.state != 'borrador':
                raise ValidationError('Solo se puede eliminar si se encuentra en estado "Borrador".')
            else:
                rec.integrantes_ids.unlink()
                rec.vivienda_ids.unlink()
        res = super(UnidadFamiliar, self).unlink()
        return res

    @api.onchange('comite_gestion_id')
    def _al_cambiar_distrito(self):
        self.centro_poblado_id = False
        if self.comite_gestion_id:
            intervenciones = self.comite_gestion_id.intervenciones
            centro_poblado_ids = [obj.centro_poblado_id.id for obj in intervenciones]
            return {'domain': {'centro_poblado_id': [('id', 'in', centro_poblado_ids)]}}

    @api.multi
    @api.depends('centro_poblado_id')
    def _compute_provincia_vivienda_dpt(self):
        for rec in self:
            if rec.comite_gestion_id:
                rec.departamento_id = rec.centro_poblado_id.state_id
                rec.provincia_id = rec.centro_poblado_id.province_id
                rec.distrito_id = rec.centro_poblado_id.district_id

    @api.multi
    def name_get(self):
        return [(obj.id, u'{}'.format(obj.id)) for obj in self]

    @api.multi
    @api.depends('vivienda_ids')
    def compute_hogar_activo(self):
        for rec in self:
            line = rec.vivienda_ids.filtered(lambda x: x.state == 'activo')
            if line:
                rec.hogar_activo = True
            else:
                rec.hogar_activo = False

    @api.constrains('fecha_inicio', 'fecha_fin')
    def constrains_fechas(self):
        validar_fechas(self.fecha_inicio, self.fecha_fin)

    @api.multi
    def accion_agregar_integrante(self):
        form_id = self.env.ref('base_cuna.form_view_integrante_familia2').id
        context = {
            'default_familia_id': self.id,
            'default_unidad_territorial_id': self.unidad_territorial_id.id,
            'default_services': self.services,
        }
        return accion_agregar('Integrante Familia', form_id, 'integrante.unidad.familiar', context)

    @api.multi
    def accion_vivienda(self):
        if self.vivienda_ids:
            activos = self.vivienda_ids.filtered(lambda x: x.state == 'activo')
            if activos:
                raise ValidationError('No puede agregar otra vivienda mientras otra este activa.')

        form_id = self.env.ref('base_cuna.form_view_vivienda_familia_crear').id
        context ={
            'default_familia_id': self.id,
            'default_unidad_territorial_id': self.unidad_territorial_id.id,
            'default_comite_gestion_id': self.comite_gestion_id.id,
            'default_services': self.services,
            'default_departamento_id': self.departamento_id.id,
            'default_provincia_id': self.provincia_id.id,
            'default_distrito_id': self.distrito_id.id,
            'default_centro_poblado_id': self.centro_poblado_id.id,

        }
        return accion_agregar('Vivienda', form_id, 'vivienda.familia', context)

    @api.multi
    def estado_activo(self):
        self.validar_integrantes_de_familia()
        self.fecha_activo = fields.Date.today()
        self.state = 'activo'
        for line in self.integrantes_ids:
            line.activo = True

    @api.multi
    def estado_cierre(self):
        for line in self.integrantes_ids:
            line.fecha_fin = fields.Date.today()
            line.activo = False
            if line.parentesco_id.name == u'Niño(a)':
                if line.integrante_id.modulos_ninios_ids:
                    rec = line.integrante_id.modulos_ninios_ids.filtered(lambda x: not x.fecha_fin)
                    rec.fecha_fin = fields.Date.today()
                    rec.state = 'cerrado'
                    line.permitir_modulo = False
                    line.permitir_familia = False
        vivienda = self.vivienda_ids.filtered(lambda x: x.state == 'activo')
        if vivienda:
            vivienda.state = 'cerrado'
            vivienda.fecha_fin = fields.Date.today()
        self.fecha_cierre = fields.Date.today()
        self.state = 'cerrado'
        self.hogar_activo = False

    """
        Verifica que al crear o modificar el registro de vivienda:
        - Debe tener integrantes,
        - No debe haber unico miembro un niño
        - Debe tener solo un cuidador principal
    """
    def validar_integrantes_de_familia(self):
        if not self.integrantes_ids or not self.vivienda_ids:
            raise ValidationError('Debe agregar integrantes y vivienda al registro.')
        else:
            if len(self.integrantes_ids) == 1:
                if self.integrantes_ids[0].es_ninio:
                    raise ValidationError(u'No se puede crear registro si el único miembro es un niño.')
            else:
                cuidador = len(self.integrantes_ids.filtered(lambda x: x.es_cuidador_principal))
                if cuidador != 1:
                    raise ValidationError(u'Debe haber un cuidador principal en la familia.')

    @api.multi
    def filtrar_vistas(self):
        return filtrar_en_vistas(self, 'Unidad Familiar', 'unidad.familiar', 0)


class IntegranteUnidadFamiliar(models.Model):
    _name = 'integrante.unidad.familiar'
    _inherit = 'tabla.base'

    modulo_ninios_ids = fields.One2many(
        comodel_name='modulo.ninios',
        inverse_name='ninios_id'
    )
    integrante_id = fields.Many2one(
        comodel_name='res.partner',
        inverse_name='integrantes_lines_ids',
        string='Integrante',
        domain="[('familia_id', '=', False)])"
    )
    codigo_integrante = fields.Char(
        string=u'Código'
    )
    fecha_inicio = fields.Date(
        string='Fecha Inicio',
        default=fields.Date.today(),
        required=True
    )
    fecha_fin = fields.Date(
        string='Fecha Fin'
    )
    es_jefe_hogar = fields.Boolean(
        string='¿Es jefe de hogar?',
    )
    es_cuidador_principal = fields.Boolean(
        string='¿Es cuidador principal?'
    )
    es_cuidador_sec = fields.Boolean(
        string='¿Es cuidador secundario?'
    )
    parentesco_id = fields.Many2one(
        comodel_name='tipo.parentesco',
        string='Parentesco',
        required=True
    )
    tipo_parentesco = fields.Char(
        string='Tipo Parentesco',
        compute='compute_parentesco_id',
        store=True
    )
    services = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento a Familias'),
    ],
        string=u'Servicio'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True), ('estado','=', 'activo')]",
        default=lambda self: self.env.user.unidad_territorial_id[0].id if self.env.user.unidad_territorial_id else False
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        string=u'Comité de Gestión',
        related='familia_id.comite_gestion_id',
        store=True
    )
    vivienda_id = fields.Many2one(
        comodel_name='vivienda.familia',
        string='Vivienda'
    )
    familia_id = fields.Many2one(
        comodel_name='unidad.familiar',
        inverse_name='integrantes_ids',
        string='Familia'
    )
    permitir_modulo = fields.Boolean(
        string='Esta activo en algun modulo?',
        related='integrante_id.permitir_modulo',
        store=True
    )
    es_ninio = fields.Boolean(
        string='es ninio?',
        compute='compute_parentesco_id',
        store=True
    )
    madre_gestante = fields.Boolean(
        string='Madre Gestante',
        compute='compute_madre_gestante',
        store=True
    )
    name = fields.Char(
        string='Nombres y Apellidos',
        related='integrante_id.display_name'
    )
    activo = fields.Boolean(
        string='Persona activa',
        default=False
    )

    @api.multi
    @api.depends('integrante_id')
    def compute_madre_gestante(self):
        for rec in self:
            if rec.integrante_id:
                rec.madre_gestante = rec.integrante_id.madre_gestante

    @api.multi
    def afiliar_ninio(self):
        if self.services == 'saf':
            raise ValidationError('No se puede afiliar a miembros del servicio SAF')
        if not self.fecha_fin:
            form_id = self.env.ref('base_cuna.wizard_mantenimiento_ninios_view').id
            context = {
                'default_unidad_territorial_id': self.unidad_territorial_id.id,
                'default_comite_gestion_id': self.comite_gestion_id.id,
                'default_ninio_ids': [self.id]
            }
            if self.integrante_id.modulos_ninios_ids:
                rec = self.integrante_id.modulos_ninios_ids.filtered(lambda r: not r.fecha_fin)
                if rec:
                    cerrar_context = {
                        'default_modulo_ninios_ids': [rec.id],
                        'default_editar': True
                    }
                    context.update(cerrar_context)
        else:
            raise ValidationError('No se puede afiliar cuando el integrante tiene fecha fin.')

        return accion_agregar(u'Afiliar Niños', form_id, 'mantenimiento.ninios.wizard', context)

    @api.constrains('fecha_inicio', 'fecha_fin')
    def constrains_fechas(self):
        validar_fechas(self.fecha_inicio, self.fecha_fin)

    @api.onchange('es_ninio')
    def _onchange_parentesco_id(self):
        self.es_jefe_hogar = self.es_cuidador_principal = self.es_cuidador_sec = self.integrante_id = False

    @api.multi
    @api.depends('parentesco_id')
    def compute_parentesco_id(self):
        for rec in self:
            rec.tipo_parentesco = rec.parentesco_id.name
            if rec.parentesco_id:
                if rec.parentesco_id.name == u'Niño(a)':
                    rec.es_ninio = True
                else:
                    rec.es_ninio = False

    @api.onchange('integrante_id')
    def _onchange_codigo(self):
        if self.integrante_id:
            self.codigo_integrante = self.integrante_id.id

    @api.multi
    def create_record(self):
        self.ensure_one()


class Vivienda(models.Model):
    _name = 'vivienda.familia'
    _inherit = 'tabla.base'

    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True), ('estado','=', 'activo')]",
        readonly=True,
    )
    services = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento a Familias'),
        ('ambos', u'Ambos'),
    ],
        string=u'Servicio',
        readonly=True
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        string=u'Comite Gestión',
        readonly=True
    )
    departamento_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Departamento',
        readonly=True,
        domain="[('country_id', '=', pais_id), "
               "('state_id', '=', False), "
               "('province_id', '=', False)]"
    )
    provincia_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Provincia',
        readonly=True
    )
    distrito_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Distrito',
        readonly=True,
        domain="[('state_id', '=', departamento_id), "
               "('province_id', '=', provincia_id)]"
    )
    centro_poblado_id = fields.Many2one(
        comodel_name='res.country.state',
        string=u'Centro Poblado',
        readonly=True
    )
    empadronador_id = fields.Many2one(
        comodel_name='res.partner',
        string='Empadronador'
    )
    tipo_via = fields.Selection(
        selection=[
            ('avenida', 'Avenida'),
            ('jiron', 'Jiron'),
            ('calle', 'Calle'),
            ('pasaje', 'Pasaje'),
            ('carretera', 'Carretera'),
        ],
        string=u'Tipo de Vía:',
        required=True,
        default='avenida'
    )
    direccion = fields.Char(
        string=u'Dirección'
    )
    referencia = fields.Char(
        string='Referencia'
    )
    state = fields.Selection(
        selection=estados,
        string='Estado',
        track_visibility='always',
        default='activo'
    )
    familia_id = fields.Many2one(
        comodel_name='unidad.familiar',
        inverse_name='vivienda_ids',
        string='Familia',
    )
    cod_familia = fields.Integer(
        string='Codigo familia',
        compute='_compute_cod_familia',
        store=True
    )
    fecha_inicio = fields.Date(
        string='Fecha Inicio',
        default=fields.Date.today(),
        readonly=True
    )
    fecha_fin = fields.Date(
        string='Fecha Cierre',
        readonly=True
    )

    @api.multi
    @api.depends('familia_id')
    def _compute_cod_familia(self):
        for rec in self:
            if rec.familia_id:
                rec.cod_familia = rec.familia_id.id

    @api.multi
    def create_record(self):
        self.ensure_one()

    @api.constrains('fecha_inicio', 'fecha_fin')
    def constrains_fechas(self):
        validar_fechas(self.fecha_inicio, self.fecha_fin)

    @api.multi
    def filtrar_vistas(self):
        vista = filtrar_en_vistas(self, 'Viviendas', 'vivienda.familia', 0)
        form_id = self.env.ref('base_cuna.form_view_vivienda_familia').id
        tree_id = self.env.ref('base_cuna.tree_view_vivienda').id
        graph_id = self.env.ref('base_cuna.graph_view_vivienda_familia').id
        pivot_id = self.env.ref('base_cuna.pivot_view_vivienda_familia').id
        vista['views'] = [(tree_id, 'tree'),(form_id, 'form'),(pivot_id, 'pivot'),(graph_id, 'graph')]
        return vista


class AfiliacionOrganizacion(models.Model):

    _name = 'afiliacion.organizacion'
    _inherit = 'tabla.base'

    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        default=lambda self: self.env.user.unidad_territorial_id[0].id if self.env.user.unidad_territorial_id else False
    )
    unidad_territorial_id_log = fields.Many2many(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True), ('estado','=', 'activo')]",
        default=lambda self: self.env.user.unidad_territorial_id if self.env.user.unidad_territorial_id else False
    )
    person_code = fields.Char(
        string=u'Código:',
        compute='_compute_datos_persona',
        store=True
    )
    person_id = fields.Many2one(
        comodel_name='res.partner',
        string='Persona'
    )
    fecha_nacimiento = fields.Date(
        string='Fecha Nacimiento:',
        compute='_compute_datos_persona',
        store=True
    )
    mc_afiliacion_lines_ids = fields.Many2many(
        comodel_name='afiliacion.organizacion.lines',
        relation='mc_afiliacion_lines'
    )
    guia_afiliacion_lines_ids = fields.Many2many(
        comodel_name='afiliacion.organizacion.lines',
        relation='guia_afiliacion_lines'
    )
    educ_afiliacion_lines_ids = fields.Many2many(
        comodel_name='afiliacion.organizacion.lines',
        relation='educ_afiliacion_lines'
    )
    state = fields.Selection(selection=[
        ('borrador', 'Borrador'),
        ('activo', u'Activo'),
        ('Retiro', u'Retiro')
    ],
        string='Estado',
        default='borrador'
    )

    @api.onchange('unidad_territorial_id_log')
    def _onchange_domain(self):
        if self.unidad_territorial_id_log:
            return {'domain': {'unidad_territorial_id': [('id', 'in', self.unidad_territorial_id_log.ids)]}}
        else:
            return {'domain': {'unidad_territorial_id': [('is_office', '=', True), ('estado', '=', 'activo')]}}

    @api.multi
    @api.depends('person_id')
    def _compute_datos_persona(self):
        for rec in self:
            rec.fecha_nacimiento = rec.person_id.birthdate
            rec.person_code = str(rec.person_id.id)

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.mc_afiliacion_lines_ids or rec.guia_afiliacion_lines_ids or rec.educ_afiliacion_lines_ids:
                raise ValidationError('Solo se puede eliminar si el registro no tiene alguno relacionado.')
        res = super(AfiliacionOrganizacion, self).unlink()
        return res

    @api.model
    def create(self, values):
        obj = super(AfiliacionOrganizacion, self).create(values)
        self._search_in_afiliacion_lines(obj.id)
        return obj

    @api.multi
    def write(self, values):
        r = super(AfiliacionOrganizacion, self).write(values)
        id = self.read(['id'])
        self._search_in_afiliacion_lines(id[0]['id'])
        return r

    def _search_in_afiliacion_lines(self, obj_id):
        afiliacion = self.search([('id', '=', obj_id)])
        if afiliacion.mc_afiliacion_lines_ids:
            _set_fields_lines(self, afiliacion.mc_afiliacion_lines_ids, afiliacion, 'MADRE CUIDADORA')
        if afiliacion.guia_afiliacion_lines_ids:
            _set_fields_lines(self, afiliacion.guia_afiliacion_lines_ids, afiliacion)


class AfiliacionOrganizacionLineas(models.Model):

    _name = 'afiliacion.organizacion.lines'
    _inherit = 'tabla.base'

    unidad_familiar_fac_id = fields.One2many(
        comodel_name='unidad.familiar',
        inverse_name='facilitador_id',
        string='Relacion facilitador - familia'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True), ('estado','=', 'activo')]",
        ondelete='restrict'
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        inverse_name='facilitador_ids',
        string=u'Comite Gestión',
        ondelete='restrict'
    )
    local_id = fields.Many2one(
        comodel_name='pncm.infra.local',
        inverse_name='afiliaciones_ids',
        string='Local',
        domain="[('comite_id', '=', comite_gestion_id)]",
        ondelete='restrict'
    )
    ambiente_id = fields.Many2one(
        comodel_name='ambientes.local',
        string='Ambiente',
        domain="[('local_id', '=', local_id)]",
        ondelete='restrict'
    )
    salas_id = fields.Many2one(
        comodel_name='salas.local',
        string='Salas',
        domain="[('ambientes_id', '=', ambiente_id)]",
        ondelete='restrict'
    )
    modulo_id = fields.Many2one(
        comodel_name='modulo.local',
        string=u'Módulo',
        domain="[('sala_id', '=', salas_id)]",
        ondelete='restrict'
    )
    state = fields.Selection(
        selection=[
            ('active', 'Activo'),
            ('retire', 'Retiro'),
        ],
        string='Estado',
        default='active',
        compute='compute_state',
        store=True
    )
    tipo_guia = fields.Many2one(
        comodel_name='tipo.persona',
        string=u'Cargo',
        ondelete='restrict'
    )
    fecha_ingreso = fields.Date(
        string='Fecha Ingreso'
    )
    fecha_retiro = fields.Date(
        string='Fecha Retiro'
    )
    person_id = fields.Many2one(
        comodel_name='res.partner',
        inverse_name='afiliacion_ids',
        string='Persona',
        ondelete='restrict'
    )
    afiliacion_id = fields.Many2one(
        comodel_name='afiliacion.organizacion',
        string=u'Afiliación',
        ondelete='restrict'
    )
    cargo = fields.Char(
        string='Cargo de la Persona',
        readonly=True,
        compute='_compute_cargo',
        store=True
    )
    cargo_activo = fields.Boolean(
        sring='el mismo cargo esta activo?'
    )
    mensaje = fields.Text(
        string='error',
        compute='_compute_mensaje'
    )
    fecha_inicio_cert = fields.Date(
        string=u'Fecha Inicio Certificación'
    )
    fecha_fin_cert = fields.Date(
        string=u'Fecha Fin Certificación'
    )
    junta_directiva = fields.Boolean(
        string='Miembro junta directiva',
        compute='_compute_junta_directiva',
        store=True
    )
    resolucion_id = fields.Many2one(
        comodel_name='resoluciones.comite',
        string=u'Resolución',
        domain="[('comite_gestion_id', '=', comite_gestion_id),"
               " ('estado', '!=', 'vencido')]",
        ondelete='restrict'
    )
    resol_fecha_inicio = fields.Date(
        string='Res. Fecha Inicio',
        related='resolucion_id.fecha_inicio',
        store=True
    )
    resol_fecha_fin = fields.Date(
        string='Res. Fecha fin',
        related='resolucion_id.fecha_fin',
        store=True
    )
    servicio = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento a Familias'),
        ('ambos', u'Ambos'),
    ])

    @api.onchange('servicio')
    def _onchange_servicio(self):
        form_persona_id = self.env['ir.ui.view'].search([
            ('name', '=', 'afiliacion.organizacion.lines.persona.form'),
            ('mode', '=', 'primary')
        ])
        if self.env.context.get('form_id') and self.env.context.get('form_id') == form_persona_id.id:
            if self.servicio:
                if self.servicio == 'saf':
                    return {'domain': {'tipo_guia': "[('servicio', 'in', ['saf', False])]"}}
                elif self.servicio == 'scd':
                    return {'domain': {'tipo_guia': "[('servicio', 'in', ['scd', False])]"}}

    @api.multi
    @api.depends('cargo')
    def _compute_junta_directiva(self):
        cargo = [
            'PRESIDENTE DE COMITE DE GESTION',
            'SECRETARIO DE COMITE DE GESTION',
            'TESORERO DE COMITE DE GESTION',
            'PRIMER VOCAL',
            'SEGUNDO VOCAL'
        ]
        for rec in self:
            if rec.cargo in cargo:
                rec.junta_directiva = True

    @api.constrains('fecha_inicio_cert', 'fecha_fin_cert')
    def constrains_fechas(self):
        validar_fechas(self.fecha_inicio_cert, self.fecha_fin_cert)

    @api.multi
    @api.depends('tipo_guia')
    def _compute_cargo(self):
        for rec in self:
            if rec.tipo_guia:
                rec.cargo = rec.tipo_guia.name

    @api.onchange('cargo')
    def _onchange_tipo_guia(self):
        if self.cargo:
            if self.cargo != 'MADRE CUIDADORA':
                self.local_id = self.ambiente_id = self.salas_id = self.modulo_id = False
            cargo = [
                'PRESIDENTE DE COMITE DE GESTION',
                'PRIMER VOCAL',
                'SEGUNDO VOCAL',
                'SECRETARIO DE COMITE DE GESTION',
                'TESORERO DE COMITE DE GESTION',
                'APOYO ADMINISTRATIVO DEL COMITE DE GESTION'
            ]
            if self.cargo in cargo:
                rec = self.comite_gestion_id.afiliaciones_ids.filtered(
                    lambda x: x.cargo == self.cargo and not x.fecha_retiro and self.id != x.id
                )
                if rec:
                    self.cargo_activo = True
                else:
                    self.cargo_activo = False
            else:
                self.cargo_activo = False
        else:
            self.cargo_activo = False

    @api.depends('cargo_activo')
    def _compute_mensaje(self):
        if self.cargo_activo:
            self.mensaje = 'Existe otra persona con el mismo cargo %s activo en el comite.' % self.cargo
        else:
            self.mensaje = False

    @api.multi
    def create_record(self):
        self.ensure_one()
        self.person_id.tipo_persona = self.tipo_guia

    @api.multi
    def create_record_socias_cocina(self):
        self.ensure_one()
        self.person_id.tipo_persona = self.tipo_guia
        self.local_id.socias_cocina_ids = [(4, self.id)]

    @api.multi
    @api.depends('fecha_retiro')
    def compute_state(self):
        for rec in self:
            if rec.fecha_retiro:
                rec.state = 'retire'
            else:
                rec.state = 'active'

    @api.model
    def create(self, values):
        if values.get('nivel_mc') and values.get('fecha_ingreso'):
            x = self.validar_linea_requerimiento(values.get('comite_gestion_id'), values.get('fecha_ingreso'))
            if x:
                raise ValidationError('No se puede afiliar en el mes seleccionado, porque ya se generó el '
                                      'requerimiento.')
        obj = super(AfiliacionOrganizacionLineas, self).create(values)

        return obj

    def validar_linea_requerimiento(self, comite_id, fecha):
        comite = self.env['pncm.comitegestion'].search([('id', '=', comite_id)])
        fecha = datetime.strptime(fecha, '%Y-%m-%d')
        mes = str(fecha.month)
        if len(mes) == 1:
            mes = '0' + mes
        anio = str(fecha.year)
        req = comite.requerimientos_ids.filtered(lambda x: x.anio.name == anio and x.mes == mes and x.servicio == 'scd'
                                                 and x.estado != 'borrador')
        if req:
            return True
        else:
            return False

    @api.multi
    def write(self, values):
        r = super(AfiliacionOrganizacionLineas, self).write(values)
        return r

    # verifica que no crear otro afiliacion mientras otra se encuentre activa
    @api.constrains('person_id', 'state')
    def verified_active_afiliacion_lines_in_partner(self):
        recs = self.search([
            ('person_id', '=', self.person_id.id),
            ('state', '=', 'active'),
        ])
        if len(recs) == 2:
            raise ValidationError(u'No se puede activar otro afiliación mientras exista otra activa.')

    @api.multi
    def button_unlink(self):
        for rec in self:
            rec.unlink()


class ResolucionesComite(models.Model):

    _name = 'resoluciones.comite'
    _inherit = 'tabla.base'

    nro_resolucion = fields.Char(
        string=u'N° Resolución',
        required=True
    )
    fecha_publicacion = fields.Date(
        string=u'Fecha Publicación',
        required=True
    )
    fecha_inicio = fields.Date(
        string=u'Fecha Inicio',
        required=True
    )
    tiempo_rde = fields.Integer(
        string='Tiempo de RDE',
        default=24,
        readonly=True
    )
    fecha_fin = fields.Date(
        string=u'Fecha Fin',
        compute='_compute_fecha_fin',
        store=True
    )
    motivo = fields.Selection(
        selection=[
            ('reconocimiento', 'Reconocimiento inicial del CG y junta directiva'),
            ('cambio_parcial_jd', 'Cambio de JD parcial'),
            ('cambio_total_jd', 'Cambio de JD total'),
            ('fe_erratas', u'Fe de Erratas(Rectificación)')
        ],
        string=u'Motivo resolución',
        required=True,
        default='reconocimiento'
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        inverse_name='resoluciones_ids',
        string='Comité de Gestión',
        ondelete='restrict'
    )
    codigo_comite = fields.Char(
        string=u'Código Comité',
        related='comite_gestion_id.codigo'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string='Unidad Territorial',
        related='comite_gestion_id.unidad_territorial_id',
        store=True,
        ondelete='restrict'
    )
    adjuntar_archivo = fields.Binary(
        string="Archivo Adjunto"
    )
    file_name = fields.Char(
        string="Adjunto"
    )
    convenio_ids = fields.One2many(
        comodel_name='convenios.comite',
        inverse_name='resolucion_id',
        string='Convenios',
        readonly=True
    )
    estado = fields.Selection(
        selection=[
            ('vigente', 'Vigente'),
            ('por_vencer', 'Por vencer'),
            ('vencido', 'Vencido')
        ],
        string='Estado',
        default='vigente'
    )
    name = fields.Char(
        string='Nombre',
        compute='_compute_name',
        store=True
    )
    obs = fields.Text(
        string=u'Observación'
    )
    file_url = fields.Char('Archivo Adjunto')

    @api.model
    def create(self, values):
        if values.get('adjuntar_archivo'):
            values['file_url'], values['file_name'] = _controlador_ruta(
                self, 'resolucion_', values['adjuntar_archivo'], values['file_name'])
            values['adjuntar_archivo'] = False
        return super(ResolucionesComite, self).create(values)

    @api.multi
    def write(self, values):
        if values.get('adjuntar_archivo'):
            values['file_url'], values['file_name'] = _controlador_ruta(
                self, 'resolucion_', values['adjuntar_archivo'], values['file_name'])
            values['adjuntar_archivo'] = False
        return super(ResolucionesComite, self).write(values)

    @api.multi
    @api.depends('nro_resolucion')
    def _compute_name(self):
        for rec in self:
            if rec.nro_resolucion:
                anio = datetime.strptime(rec.fecha_inicio, "%Y-%m-%d").year
                rec.name = u"RDE Nº {}-{}-MIDIS/PNCM“ ".format(rec.nro_resolucion, anio)

    @api.depends('fecha_inicio', 'tiempo_rde')
    def _compute_fecha_fin(self):
        if self.fecha_inicio:
            fecha_aux = datetime.strptime(self.fecha_inicio, DEFAULT_SERVER_DATE_FORMAT)
            self.fecha_fin = fecha_aux + relativedelta(months=self.tiempo_rde)

    @api.multi
    def create_record(self):
        self.ensure_one()

    @api.multi
    def filtrar_vistas(self):
        action = {
            'name': u'Resoluciones',
            'type': "ir.actions.act_window",
            'res_model': 'resoluciones.comite',
                'view_type': "form",
            'view_mode': "tree,form",
        }
        user = self.env['res.users'].browse(self._uid)

        if user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', 'in', %s)]" % user.unidad_territorial_id.ids
        form_id = self.env.ref('base_cuna.form_view_resolucion_comite2').id
        tree_id = self.env.ref('base_cuna.tree_view_resoluciones_comite').id
        graph_id = self.env.ref('base_cuna.graph_view_resoluciones_comite').id
        pivot_id = self.env.ref('base_cuna.pivot_view_resoluciones_comite').id
        action['views'] = [(tree_id, 'tree'), (form_id, 'form'), (pivot_id, 'pivot'), (graph_id, 'graph')]
        return action


class ConvenioComite(models.Model):

    _name = 'convenios.comite'
    _inherit = 'tabla.base'

    name = fields.Char(
        string='Nombre',
        compute='_compute_name',
        store=True
    )
    resolucion_id = fields.Many2one(
        comodel_name='resoluciones.comite',
        inverse_name='convenio_ids',
        string='Resoluciones',
        domain="[('comite_gestion_id','=', comite_gestion_id)]",
        required=True,
        ondelete='restrict'
    )
    nro_convenio = fields.Char(
        string='N° Convenio',
        required=True
    )
    fecha_publicacion = fields.Date(
        string=u'Fecha Publicación',
        compute='_compute_fecha_publicacion',
        store=True
    )
    fecha_inicio = fields.Date(
        string=u'Fecha Inicio',
        required=True
    )
    tiempo_convenio = fields.Integer(
        string='Tiempo de convenio',
        default=24,
        required=True
    )
    fecha_fin = fields.Date(
        string=u'Fecha Fin',
        compute='_compute_fecha_fin',
        store=True
    )
    motivo = fields.Selection(
        selection=[
            ('convenio_inicial', 'Convenio Inicial'),
            ('ampliacion_vigencia', u'Ampliación de Vigencia de convenio')
        ],
        string=u'Motivo resolución',
        required=True,
        default='convenio_inicial'
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        inverse_name='convenios_ids',
        string='Comité de Gestión',
        ondelete='restrict'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string='Unidad Territorial',
        related='comite_gestion_id.unidad_territorial_id',
        store=True,
        ondelete='restrict'
    )
    adenda_ids = fields.One2many(
        comodel_name='adendas.comite',
        inverse_name='convenio_id',
        string='Adendas',
        readonly=True
    )
    adjuntar_archivo = fields.Binary(
        string="Archivo Adjunto"
    )
    file_name = fields.Char(
        string="Adjunto"
    )
    obs = fields.Text(
        string=u'Observación'
    )
    estado = fields.Selection(
        selection=[
            ('vigente', 'Vigente'),
            ('por_vencer', 'Por vencer'),
            ('vencido', 'Vencido')
        ],
        string='Estado',
        default='vigente'
    )
    codigo_comite = fields.Char(
        string=u'Código Comité',
        related='comite_gestion_id.codigo'
    )
    file_url = fields.Char('Archivo Adjunto')

    @api.model
    def create(self, values):
        if values.get('adjuntar_archivo'):
            values['file_url'], values['file_name'] = _controlador_ruta(
                self, 'resolucion_', values['adjuntar_archivo'], values['file_name'])
            values['adjuntar_archivo'] = False
        return super(ConvenioComite, self).create(values)

    @api.multi
    def write(self, values):
        if values.get('adjuntar_archivo'):
            values['file_url'], values['file_name'] = _controlador_ruta(
                self, 'resolucion_', values['adjuntar_archivo'], values['file_name'])
            values['adjuntar_archivo'] = False
        return super(ConvenioComite, self).write(values)

    @api.multi
    def filtrar_vistas(self):
        action = {
            'name': u'Convenios',
            'type': "ir.actions.act_window",
            'res_model': 'convenios.comite',
            'view_type': "form",
            'view_mode': "tree,form",
        }
        user = self.env['res.users'].browse(self._uid)

        if user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', 'in', %s)]" % user.unidad_territorial_id.ids
        form_id = self.env.ref('base_cuna.form_view_convenios_comite_edit').id
        tree_id = self.env.ref('base_cuna.tree_view_convenios_comite').id
        graph_id = self.env.ref('base_cuna.graph_view_convenios_comite').id
        pivot_id = self.env.ref('base_cuna.pivot_view_convenios_comite').id
        action['views'] = [(tree_id, 'tree'), (form_id, 'form'), (pivot_id, 'pivot'), (graph_id, 'graph')]
        return action

    @api.depends('resolucion_id')
    def _compute_fecha_publicacion(self):
        if self.resolucion_id:
            self.fecha_publicacion = self.resolucion_id.fecha_publicacion

    @api.onchange('resolucion_id')
    def _onchange_resolucion_id(self):
        self.fecha_inicio = False

    @api.constrains('fecha_inicio')
    def _constraints_fecha_inicio(self):
        if self.fecha_inicio:
            if self.resolucion_id.fecha_inicio > self.fecha_inicio:
                raise ValidationError(
                    u'Fecha Inicio del convenio no puede ser menor a la Fecha de Inicio de la resolución %s'
                    % self.resolucion_id.fecha_inicio
                )

    @api.multi
    @api.depends('fecha_inicio', 'nro_convenio')
    def _compute_name(self):
        for rec in self:
            if rec.fecha_inicio and rec.nro_convenio:
                anio = datetime.strptime(rec.fecha_inicio, "%Y-%m-%d").year
                servicio = rec.comite_gestion_id.servicio.upper()
                rec.name = "CONVENIO N° {}-{}-MIDIS/PNCM/UTAI/{}/{} ".format(
                    rec.nro_convenio, anio, servicio, rec.comite_gestion_id.unidad_territorial_id.code
                )

    @api.depends('fecha_inicio', 'tiempo_convenio')
    def _compute_fecha_fin(self):
        if self.fecha_inicio and self.tiempo_convenio:
            fecha_aux = datetime.strptime(self.fecha_inicio, DEFAULT_SERVER_DATE_FORMAT)
            self.fecha_fin = fecha_aux + relativedelta(months=self.tiempo_convenio)

    @api.multi
    def create_record(self):
        self.ensure_one()


class Adendas(models.Model):
    _name = 'adendas.comite'
    _inherit = 'tabla.base'

    name = fields.Char(
        string='Nombre',
        compute='_compute_name',
        store=True
    )
    nro_adenda = fields.Char(
        string='N° Adenda',
        required=True
    )
    convenio_id = fields.Many2one(
        comodel_name='convenios.comite',
        inverse_name='adenda_ids',
        string='Convenios',
        domain="[('comite_gestion_id','=', comite_gestion_id)]",
        required=True,
        ondelete='restrict'
    )
    fecha_publicacion = fields.Date(
        string=u'Fecha Publicación',
        required=True
    )
    fecha_inicio = fields.Date(
        string=u'Fecha Inicio',
        compute='_compute_fecha_inicio',
        store=True
    )
    tiempo_adenda = fields.Integer(
        string='Tiempo de adendas',
        default=24,
        required=True
    )
    fecha_fin = fields.Date(
        string=u'Fecha Fin',
        compute='_compute_fecha_fin',
        store=True
    )
    motivo = fields.Selection(
        selection=[
            ('ampliacion_vigencia', u'Ampliación de Vigencia de convenio'),
            ('fe_erratas', u'Fe de Erratas(Rectificación)')
        ],
        string=u'Motivo',
        required=True,
        default='ampliacion_vigencia'
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        inverse_name='adendas_ids',
        string='Comité de Gestión',
        ondelete='restrict'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string='Unidad Territorial',
        related='comite_gestion_id.unidad_territorial_id',
        store=True,
        ondelete='restrict'
    )
    adjuntar_archivo = fields.Binary(
        string="Archivo Adjunto"
    )
    file_name = fields.Char(
        string="Adjunto"
    )
    obs = fields.Text(
        string=u'Observación'
    )
    estado = fields.Selection(
        selection=[
            ('vigente', 'Vigente'),
            ('por_vencer', 'Por vencer'),
            ('vencido', 'Vencido')
        ],
        string='Estado',
        default='vigente'
    )
    codigo_comite = fields.Char(
        string=u'Código Comité',
        related='comite_gestion_id.codigo',
    )
    file_url = fields.Char('Archivo Adjunto')

    @api.model
    def create(self, values):
        if values.get('adjuntar_archivo'):
            values['file_url'], values['file_name'] = _controlador_ruta(
                self, 'resolucion_', values['adjuntar_archivo'], values['file_name'])
            values['adjuntar_archivo'] = False
        return super(Adendas, self).create(values)

    @api.multi
    def write(self, values):
        if values.get('adjuntar_archivo'):
            values['file_url'], values['file_name'] = _controlador_ruta(
                self, 'resolucion_', values['adjuntar_archivo'], values['file_name'])
            values['adjuntar_archivo'] = False
        return super(Adendas, self).write(values)

    @api.multi
    def filtrar_vistas(self):
        action = {
            'name': u'Adendas',
            'type': "ir.actions.act_window",
            'res_model': 'adendas.comite',
            'view_type': "form",
            'view_mode': "tree,form",
        }
        user = self.env['res.users'].browse(self._uid)

        if user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', 'in', %s)]" % user.unidad_territorial_id.ids
        form_id = self.env.ref('base_cuna.form_view_adendas_comite_edit').id
        tree_id = self.env.ref('base_cuna.tree_view_adendas_comite').id
        graph_id = self.env.ref('base_cuna.graph_view_adendas_comite').id
        pivot_id = self.env.ref('base_cuna.pivot_view_adendas_comite').id
        action['views'] = [(tree_id, 'tree'), (form_id, 'form'), (pivot_id, 'pivot'), (graph_id, 'graph')]
        return action

    @api.onchange('tiempo_adenda')
    def _onchange_tiempo_adenda(self):
        if self.tiempo_adenda > 48:
            self.tiempo_adenda = 48

    @api.depends('fecha_inicio', 'nro_adenda')
    def _compute_name(self):
        if self.fecha_inicio and self.nro_adenda and self.convenio_id:
            anio = datetime.strptime(self.fecha_inicio, "%Y-%m-%d").year
            servicio = self.comite_gestion_id.servicio.upper()
            self.name = "ADENDA N° {} AL CONVENIO N° {}-{}-MIDIS/PNCM/UTAI/{}/{} ".format(
                self.nro_adenda, self.convenio_id.nro_convenio,
                anio, servicio, self.comite_gestion_id.unidad_territorial_id.code
            )

    @api.depends('convenio_id')
    def _compute_fecha_inicio(self):
        if self.convenio_id:
            if self.convenio_id.fecha_fin:
                fecha_aux = datetime.strptime(self.convenio_id.fecha_fin, DEFAULT_SERVER_DATE_FORMAT)
                self.fecha_inicio = fecha_aux + relativedelta(days=1)

    @api.depends('fecha_inicio', 'tiempo_adenda')
    def _compute_fecha_fin(self):
        if self.fecha_inicio and self.tiempo_adenda:
            fecha_aux = datetime.strptime(self.fecha_inicio, DEFAULT_SERVER_DATE_FORMAT)
            self.fecha_fin = fecha_aux + relativedelta(months=self.tiempo_adenda)

    @api.multi
    def create_record(self):
        self.ensure_one()


class ComiteGestion(models.Model):
    _inherit = 'pncm.comitegestion'

    servicio = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento a Familias')
    ],
        string=u'Servicio'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        ondelete='restrict',
        default=lambda self: self.env.user.unidad_territorial_id[0].id if self.env.user.unidad_territorial_id else False
    )
    unidad_territorial_id_log = fields.Many2many(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        default=lambda self: self.env.user.unidad_territorial_id if self.env.user.unidad_territorial_id else False
    )
    entidad_bancaria_id = fields.Many2one(
        comodel_name='entidad.bancaria',
        string='Entidad Bancaria',
        ondelete='restrict'
    )
    nro_cuenta = fields.Char(
        string='N° de Cuenta Bancaria'
    )
    centro_poblado_ids = fields.Many2many(
        comodel_name='res.country.state',
        string='Centro Poblado',
        compute='_compute_centro_poblado_activos',
        store=True
    )
    servicio_alimentario_ids = fields.Many2many(
        comodel_name='pncm.infra.local',
        string='Servicios Alimentarios',
        compute='_compute_servicio_alimentario_ids',
    )
    convenios_ids = fields.One2many(
        comodel_name='convenios.comite',
        inverse_name='comite_gestion_id',
        string='Convenios'
    )
    resoluciones_ids = fields.One2many(
        comodel_name='resoluciones.comite',
        inverse_name='comite_gestion_id',
        string='Resoluciones'
    )
    adendas_ids = fields.One2many(
        comodel_name='adendas.comite',
        inverse_name='comite_gestion_id',
        string='Convenios'
    )
    afiliaciones_ids = fields.One2many(
        comodel_name='afiliacion.organizacion.lines',
        inverse_name='comite_gestion_id',
        string='Afiliaciones'
    )
    familias_ids = fields.One2many(
        comodel_name='unidad.familiar',
        inverse_name='comite_gestion_id',
        string='Familias'
    )
    state = fields.Selection(selection=[
        ('constitucion', u'En Formacion'),
        ('activo', 'Activo'),
        ('cerrado', 'Cerrado'),
    ],
        string='Estados',
        default='constitucion'
    )
    presidente_actual = fields.Char(
        string=u'Presidente',
        compute='_compute_presidente',
        store=True
    )
    tesorero_actual = fields.Char(
        string=u'Tesorero',
        compute='_compute_tesorero_actual',
        store=True
    )
    secretario_actual = fields.Char(
        string=u'Secretario',
        compute='_compute_secretario',
        store=True
    )
    primer_vocal_actual = fields.Char(
        string=u'Primer Vocal',
        compute='_compute_primer_vocal',
        store=True
    )
    segundo_vocal_actual = fields.Char(
        string=u'Segundo Vocal',
        compute='_compute_segundo_vocal',
        store=True
    )
    meta_actual = fields.Char(
        string='Meta',
        compute='_compute_meta_actual',
        store=True
    )
    fecha_inicio = fields.Date(
        string=u'Fecha de Inicio',
        compute='_compute_fecha_ini_cier_resolucion',
        store=True
    )
    fecha_cierre = fields.Date(
        string=u'Fecha de Cierre',
        compute='_compute_fecha_ini_cier_resolucion',
        store=True
    )
    resolucion = fields.Char(
        string=u'Resolución Activa',
        compute='_compute_fecha_ini_cier_resolucion',
        store=True
    )
    telefono = fields.Char(
        string='Telefono'
    )
    fecha_creacion = fields.Date(
        string='Fecha Creacion',
        default=fields.Date.today()
    )
    fecha_mod = fields.Date(
        string=u'Fecha Modificación'
    )
    usuario_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Creador',
        default=lambda self: self.env.user,
        ondelete='restrict'
    )
    usuario_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Modifico',
        ondelete='restrict'
    )
    usuario_bd_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Creador',
        ondelete='restrict'
    )
    usuario_bd_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Modifico',
        ondelete='restrict'
    )
    cunanet_id = fields.Integer()
    sisaf_id = fields.Integer()

    @api.onchange('unidad_territorial_id_log')
    def _onchange_domain(self):
        if self.unidad_territorial_id_log:
            return {'domain': {'unidad_territorial_id': [('id', 'in', self.unidad_territorial_id_log.ids)]}}
        else:
            return {'domain': {'unidad_territorial_id': [('is_office', '=', True), ('estado', '=', 'activo')]}}

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.state != 'constitucion':
                raise ValidationError('Solo se puede eliminar si se encuentra en estado "En constitucion".')
            else:
                rec.resoluciones_ids.unlink()
                rec.convenios_ids.unlink()
                rec.adendas_ids.unlink()
                rec.intervenciones.unlink()
                rec.afiliaciones_ids.unlink()
                rec.miembros_equipo.unlink()
        res = super(ComiteGestion, self).unlink()
        return res

    @api.onchange('unidad_territorial_id', 'servicio')
    def _al_cambiar_ut(self):
        if self.unidad_territorial_id and self.servicio:
            self._mostrar_codigo_generado()
            self.departamento_id = False
            return self._obtener_ambito_departamento_ut()

    @api.onchange('servicio')
    def _obtener_ut_por_servicio(self):
        return

    @api.multi
    def estado_activo(self):
        if not self.presidente_actual or not self.tesorero_actual or not self.secretario_actual:
            raise ValidationError("La junta directiva debe tener Presidente, Tesorero y Secretario para activar.")
        if not self.resoluciones_ids.filtered(lambda x: x.estado != 'vencido'):
            raise ValidationError(u"Se debe ingresar una resolución vigente.")
        if not self.convenios_ids.filtered(lambda x: x.estado != 'vencido') and not self.adendas_ids.filtered(lambda x: x.estado != 'vencido'):
            raise ValidationError(u"Debe tener un Convenio o adenda activa para activar.")
        self.state = 'activo'

    @api.multi
    def estado_cerrado(self):
        if self.afiliaciones_ids.filtered(lambda x: not x.fecha_retiro):
            raise ValidationError('Todos los Actores Comunitarios deben tener fecha de retiro')
        if self.miembros_equipo.filtered(lambda x: not x.fecha_fin):
            raise ValidationError('Todos los integrantes del equipo tecnico deben tener fecha fin')
        if self.resoluciones_ids.filtered(lambda x: x.estado != 'vencido'):
            raise ValidationError(u"Las resoluciones deben estar vencidas.")
        if self.convenios_ids.filtered(lambda x: x.estado != 'vencido'):
            raise ValidationError(u"Los convenios deben estar vencidas.")
        if self.adendas_ids.filtered(lambda x: x.estado != 'vencido'):
            raise ValidationError(u"Las adendas deben estar vencidas.")
        self.state = 'cerrado'

    @api.multi
    @api.depends('resoluciones_ids', 'resoluciones_ids.estado',
                 'resoluciones_ids.fecha_inicio', 'resoluciones_ids.fecha_fin')
    def _compute_fecha_ini_cier_resolucion(self):
        for rec in self:
            resolucion = rec.resoluciones_ids.filtered(lambda x: x.estado == 'vigente')
            if resolucion:
                rec.resolucion = resolucion.name
                rec.fecha_inicio = resolucion.fecha_inicio
                rec.fecha_cierre = resolucion.fecha_fin

    @api.multi
    @api.depends('metas', 'metas.fecha_fin')
    def _compute_meta_actual(self):
        fecha_actual = datetime.now().date()
        for rec in self:
            meta = rec.metas.filtered(
                lambda x: datetime.strptime(x.fecha_fin, DEFAULT_SERVER_DATE_FORMAT).date() > fecha_actual
            )
            if meta:
                for obj in meta:
                    rec.meta_actual = obj.meta_fisica

    @api.one
    @api.depends('afiliaciones_ids', 'afiliaciones_ids.fecha_retiro')
    def _compute_primer_vocal(self):
        self.primer_vocal_actual = self.obtener_actores_comunales_activos('PRIMER VOCAL')

    @api.one
    @api.depends('afiliaciones_ids', 'afiliaciones_ids.fecha_retiro')
    def _compute_segundo_vocal(self):
        self.segundo_vocal_actual = self.obtener_actores_comunales_activos('SEGUNDO VOCAL')

    @api.one
    @api.depends('afiliaciones_ids', 'afiliaciones_ids.fecha_retiro')
    def _compute_secretario(self):
        self.secretario_actual = self.obtener_actores_comunales_activos('SECRETARIO DE COMITE DE GESTION')

    @api.one
    @api.depends('afiliaciones_ids', 'afiliaciones_ids.fecha_retiro')
    def _compute_presidente(self):
        self.presidente_actual = self.obtener_actores_comunales_activos('PRESIDENTE DE COMITE DE GESTION')

    @api.one
    @api.depends('afiliaciones_ids', 'afiliaciones_ids.fecha_retiro')
    def _compute_tesorero_actual(self):
        self.tesorero_actual = self.obtener_actores_comunales_activos('TESORERO DE COMITE DE GESTION')

    def obtener_actores_comunales_activos(self, cargo):
        registro = self.afiliaciones_ids.filtered(
            lambda x: x.cargo == cargo and not x.fecha_retiro)
        if registro:
            return registro.person_id.display_name

    @api.multi
    @api.depends('intervenciones', 'intervenciones.fecha_fin')
    def _compute_centro_poblado_activos(self):
        for rec in self:
            list_ccpp = []

            for line in rec.intervenciones:
                if line.fecha_fin:
                    list_ccpp.append(line.centro_poblado_id.id)
            rec.centro_poblado_ids = list_ccpp

    @api.multi
    def accion_agregar_ac(self):
        form_id = self.env.ref('base_cuna.form_view_afiliacion_organizacion_lines_persona').id
        context = {
            'default_unidad_territorial_id': self.unidad_territorial_id.id,
            'default_comite_gestion_id': self.id,
            'default_state': 'active',
            'default_servicio': self.servicio,
            'form_id': form_id
        }
        return accion_agregar('Actor Comunal', form_id, 'afiliacion.organizacion.lines', context)

    @api.multi
    def agregar_apoyo_adm_activo(self):
        if self.servicio == 'ambos':
            form_id = self.env.ref('base_cuna.form_view_seleccion_servicio_wizard').id
            context = {
                'default_unidad_territorial_id': self.unidad_territorial_id.id,
                'default_comite_gestion_id': self.id
            }
            return accion_agregar(
                'Seleccion de servicio para Apoyo Administrativo', form_id, 'seleccion.servicio.wizard', context
            )
        else:
            form_id = self.env.ref('base_cuna.form_view_afiliacion_organizacion_lines_apoyo_adm').id
            tipo = self.env['tipo.persona'].search([
                ('name', '=', 'APOYO ADMINISTRATIVO DEL COMITE DE GESTION'),
                ('servicio', '=', self.servicio),
            ])
            context = {
                'default_unidad_territorial_id': self.unidad_territorial_id.id,
                'default_comite_gestion_id': self.id,
                'default_state': 'active',
                'default_tipo_guia': tipo.id,
            }
            return accion_agregar('Apoyo Administrativo', form_id, 'afiliacion.organizacion.lines', context)

    def eliminar_lineas(self, tipo_persona):
        afiliaciones = self.afiliaciones_ids.filtered(lambda x: x.cargo == tipo_persona)
        if not afiliaciones:
            raise ValidationError('No hay registro de %s' % tipo_persona)
        form_id = self.env.ref('base_cuna.form_view_eliminar_afiliaciones').id
        context = {
            'default_afiliacion_ids': afiliaciones.ids,
            'default_comite_gestion_id': self.id,
            'default_nombre_tipo': tipo_persona
        }
        return accion_agregar('Afiliaciones', form_id, 'eliminar.afiliaciones', context)

    @api.multi
    def accion_agregar_convenio(self):
        form_id = self.env.ref('base_cuna.form_view_convenios_comite').id
        return self.crear_convenio_resoluciones_adendas(form_id, 'Convenios', 'convenios.comite')

    @api.multi
    def accion_agregar_resolucion(self):
        form_id = self.env.ref('base_cuna.form_view_resolucion_comite').id
        return self.crear_convenio_resoluciones_adendas(form_id, 'Resolución', 'resoluciones.comite')

    @api.multi
    def accion_agregar_adenda(self):
        form_id = self.env.ref('base_cuna.form_view_adendas_comite').id
        return self.crear_convenio_resoluciones_adendas(form_id, 'Adenda', 'adendas.comite')

    def crear_convenio_resoluciones_adendas(self, form_id, nombre, modelo):
        context = {'default_comite_gestion_id': self.id}
        return accion_agregar(nombre, form_id, modelo, context)

    @api.multi
    def _compute_servicio_alimentario_ids(self):
        for r in self:
            if r.id:
                recs = r.env['pncm.infra.local'].search([
                    ('tipo_local', '!=', 'ciai'),
                    ('comite_id', '=', r.id),
                ])
                r.servicio_alimentario_ids = recs

    @api.model
    def enviar_correos_convenios_vencer(self):
        fecha_actual = datetime.now().date()
        template = self.env['ir.model.data'].sudo().get_object('base_cuna', 'convenio_a_vencer_template')

        list_to = self.integrantes_correo()
        if list_to:
            for rec in self:
                for convenio in rec.convenios_ids:
                    fecha_vencer = datetime.strptime(convenio.fecha_fin, DEFAULT_SERVER_DATE_FORMAT) - \
                                   relativedelta(days=90)

                    if fecha_vencer.date() == fecha_actual:
                        body = '<p>Estimado,</p>' \
                                '<p>'\
                                'El siguiente convenio "${object.name}" esta próximo a vercer:'\
                                '- Fecha fin: ${object.fecha_fin}'\
                                'Darle el siguimiento correspondiente.'\
                                '</p>'\
                                '<p>Saludos</p>'
                        mail_values = {
                            'subject': template.subject,
                            'body_html': body,
                            'email_to': ';'.join(map(lambda x: x, list_to)),
                            #'email_from': template_obj.email_from,
                        }
                        create_and_send_email = self.env['mail.mail'].create(mail_values).send()
                    #self.env['mail.template'].browse(template.id).sudo().send_mail(mail_values, force_send=True)

    def integrantes_correo(self):
        juts = self.env['res.groups'].search('name', '=', 'Jefe de Unidad Territorial')
        coords = self.env['res.groups'].search('name', '=', 'Coordinador')
        esps = self.env['res.groups'].search('name', '=', 'Especialista Integrales')
        list_to = []
        for j in juts.users:
            list_to.append(j.login)
        for c in coords.users:
            list_to.append(c.login)
        for e in esps.users:
            list_to.append(e.login)
        return list_to

    @api.model
    def resoluciones_vencer(self):
        fecha_actual = datetime.now().date()
        for rec in self:
            for resolucion in rec.resoluciones_ids:
                self.cambiar_estado_res_conv_adenda(resolucion, fecha_actual)
            for convenio in rec.convenios_ids:
                self.cambiar_estado_res_conv_adenda(convenio, fecha_actual)
            for adenda in rec.adendas_ids:
                self.cambiar_estado_res_conv_adenda(adenda, fecha_actual)

    def cambiar_estado_res_conv_adenda(self, doc, fecha_actual):
        fecha_fin = datetime.strptime(doc.fecha_fin, DEFAULT_SERVER_DATE_FORMAT)
        if fecha_actual > fecha_fin.date():
            doc.estado = 'vencido'
        else:
            fecha_vencer = fecha_fin - relativedelta(days=90)

            if fecha_vencer.date() == fecha_actual:
                doc.estado = 'por_vencer'
            else:
                doc.estado = 'vigente'

    @api.multi
    def filtrar_vistas(self):
        action = {
            'name': u'Comite Gestión',
            'type': "ir.actions.act_window",
            'res_model': 'pncm.comitegestion',
            'view_type': "form",
            'view_mode': "tree,form,pivot,graph",
        }
        user = self.env['res.users'].browse(self._uid)

        if user.unidad_territorial_id and user.servicio and user.servicio != 'ambas':
            action['domain'] = "[('unidad_territorial_id', 'in', %s), ('servicio', '=', '%s')]" % \
                               (user.unidad_territorial_id.ids, user.servicio)
        elif user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', 'in', %s)]" % user.unidad_territorial_id.ids
        elif user.servicio and user.servicio != 'ambas':
            action['domain'] = "[('servicio', '=', '%s')]" % user.servicio
        return action

    @api.model
    def create(self, vals):
        obj = self.env['res.country.state'] \
            .search([('id', '=', vals['distrito_id'])])
        vals['ubigeo'] = obj.code
        if vals.get('unidad_territorial_id'):
            vals['codigo'] = \
                self._generar_codigo_comite(
                    vals['unidad_territorial_id'], vals['servicio'])
        else:
            vals['codigo'] = \
                self._generar_codigo_comite(
                    vals['unidad_territorial_id_log'], vals['servicio'])
        return super(ComiteGestion, self).create(vals)

    @api.multi
    def write(self, values):
        r = super(ComiteGestion, self).write(values)
        self.resoluciones_vencer()
        return r


class EntidadBancaria(models.Model):
    _name = 'entidad.bancaria'
    _inherit = 'tabla.base'

    name = fields.Char(
        string='Nombre Entidad Bancaria'
    )


class MetasHistoricas(models.Model):
    _inherit = 'pncm.comite.metas.historicas'

    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string='Unidad Territorial',
        related='comite_gestion_id.unidad_territorial_id',
        store=True
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        inverse_name='metas',
        string='Comité de Gestión',
        required=True,
        ondelete='restrict'
    )
    codigo_comite = fields.Char(
        string=u'Código Comité',
        related='comite_gestion_id.codigo'
    )
    fecha_creacion = fields.Date(
        string='Fecha Creacion',
        default=fields.Date.today()
    )
    fecha_mod = fields.Date(
        string=u'Fecha Modificación'
    )
    usuario_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Creador',
        default=lambda self: self.env.user
    )
    usuario_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Modifico'
    )
    usuario_bd_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Creador'
    )
    usuario_bd_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Modifico'
    )
    cunanet_id = fields.Integer()
    sisaf_id = fields.Integer()

    @api.model
    def create(self, vals):
        metas = []
        for meta_periodo in self._obtener_periodos(
                vals['fecha_inicio'],
                vals['fecha_fin']):
            metas.append((0, 0, {
                'meta_id': self.id,
                'meta_presupuestal_planeada': vals['meta_presupuestal'],
                'meta_presupuestal_ejecutada': 0,
                'comite_gestion_id': vals['comite_gestion_id'],
                'meta_fisica_planeada': vals['meta_fisica'],
                'meta_fisica_ejecutada': 0,
                'periodo': meta_periodo,
                'meta': vals['codigo'],

            }))
        vals['metas_programadas'] = metas
        return super(MetasHistoricas, self).create(vals)

    @api.multi
    def filtrar_vistas(self):
        action = {
            'name': u'Metas',
            'type': "ir.actions.act_window",
            'res_model': 'pncm.comite.metas.historicas',
            'view_type': "form",
            'view_mode': "tree,form,pivot,graph",
        }
        user = self.env['res.users'].browse(self._uid)
        if user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', 'in', %s)]" % user.unidad_territorial_id.ids
        return action


class CMIntervencion(models.Model):
    _inherit = 'pncm.comitegestion.intervencion'

    ut_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office', '=', True)]",
        required=True,
        ondelete='restrict'
    )
    centro_poblado_ids = fields.Many2many(
        comodel_name='res.country.state',
        string='Centro Poblado',
        compute='_compute_centro_poblado_ids',
        store=True
    )
    fecha_creacion = fields.Date(
        string='Fecha Creacion',
        default=fields.Date.today()
    )
    fecha_mod = fields.Date(
        string=u'Fecha Modificación'
    )
    usuario_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Creador',
        default=lambda self: self.env.user,
        ondelete='restrict'
    )
    usuario_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Modifico',
        ondelete='restrict'
    )
    usuario_bd_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Creador',
        ondelete='restrict'
    )
    usuario_bd_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Modifico',
        ondelete='restrict'
    )
    cunanet_id = fields.Integer()
    sisaf_id = fields.Integer()

    @api.multi
    @api.depends('comite_gestion_id', 'comite_gestion_id.intervenciones')
    def _compute_centro_poblado_ids(self):
        for rec in self:
            rec.centro_poblado_ids = rec.comite_gestion_id.centro_poblado_ids

    @api.onchange('centro_poblado_ids', 'intervencion_ut_id')
    def onchange_domain_centro_poblado_ids(self):
        res = {}
        ids = []
        for centro in self.centro_poblado_ids:
            ids.append(centro.id)
        res['domain'] = {
            'centro_poblado_id': [
                ('id', 'not in', ids),
                ('state_id', '=', self.intervencion_ut_id.departamento_id.id),
                ('province_id', '=', self.intervencion_ut_id.provincia_id.id),
                ('district_id', '=', self.intervencion_ut_id.distrito_id.id)
            ],
        }
        return res


class CMAmbitoIntervencion(models.Model):
    _inherit = 'pncm.ambito.intervencion'

    name = fields.Char(
        string='Nombre',
        compute='_compute_name',
        store=True
    )
    fecha_creacion = fields.Date(
        string='Fecha Creacion',
        default=fields.Date.today()
    )
    fecha_mod = fields.Date(
        string=u'Fecha Modificación'
    )
    usuario_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Creador',
        default=lambda self: self.env.user
    )
    usuario_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Modifico'
    )
    usuario_bd_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Creador'
    )
    usuario_bd_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Modifico'
    )
    cunanet_id = fields.Integer()
    sisaf_id = fields.Integer()

    @api.multi
    @api.depends('departamento_id', 'provincia_id', 'distrito_id')
    def _compute_name(self):
        for obj in self:
            obj.name = u'{}/{}/{}'.format(obj.departamento_id.name, obj.provincia_id.name, obj.distrito_id.name)


class AmbientesLocal(models.Model):
    _name = 'ambientes.local'
    _inherit = 'tabla.base'

    name = fields.Char(
        string=u'Descripción',
        required=True
    )
    area = fields.Float(
        string=u'Área(m2)',
        required=True
    )
    largo = fields.Float(
        string='Largo(ml)',
        required=True
    )
    ancho = fields.Float(
        string='Ancho(ml)',
        required=True
    )
    cap_max = fields.Integer(
        string=u'Cap. Max. Niñas y Niños',
        required=True
    )
    salas_ids = fields.One2many(
        comodel_name='salas.local',
        inverse_name='ambientes_id',
        string='Salas'
    )
    local_id = fields.Many2one(
        comodel_name='pncm.infra.local',
        inverse_name='ambientes_ids',
        string='Local',
        readonly=True,
        ondelete='restrict'
    )

    @api.multi
    def create_record(self):
        self.ensure_one()


class SalasLocal(models.Model):
    _name = 'salas.local'
    _inherit = 'tabla.base'

    name = fields.Char(
        string=u'Descripción',
        required=True
    )
    fecha_inicio = fields.Date(
        string='Fecha Inicio',
        required=True
    )
    fecha_fin = fields.Date(
        string='Fecha Fin'
    )
    local_id = fields.Many2one(
        comodel_name='pncm.infra.local',
        inverse_name='salas_ids',
        string='Local',
        readonly=True,
        ondelete='restrict'
    )
    ambientes_id = fields.Many2one(
        comodel_name='ambientes.local',
        inverse_name='salas_ids',
        string='Ambiente',
        domain="[('local_id', '=', local_id)]",
        required=True,
        ondelete='restrict'
    )
    modulos_ids = fields.One2many(
        comodel_name='modulo.local',
        inverse_name='sala_id',
        string=u'Módulos'
    )

    @api.multi
    def create_record(self):
        self.ensure_one()

    @api.constrains('fecha_inicio', 'fecha_fin')
    def constrains_fechas(self):
        validar_fechas(self.fecha_inicio, self.fecha_fin)


class ModuloLocal(models.Model):
    _name = 'modulo.local'
    _inherit = 'tabla.base'

    name = fields.Char(
        string=u'Nombre',
        required=True
    )
    nro_ninios = fields.Integer(
        string=u'N° Niños',
        required=True
    )
    ninios_ids = fields.One2many(
        comodel_name='modulo.ninios',
        inverse_name='modulo_id',
        string=u'Niños',
    )
    fecha_inicio = fields.Date(
        string='Fecha Inicio',
        required=True
    )
    fecha_fin = fields.Date(
        string='Fecha Fin'
    )
    local_id = fields.Many2one(
        comodel_name='pncm.infra.local',
        inverse_name='modulos_ids',
        string='Local',
        ondelete='restrict'
    )
    sala_id = fields.Many2one(
        comodel_name='salas.local',
        inverse_name='modulos_ids',
        domain="[('local_id', '=', local_id)]",
        string='Sala',
        required=True,
        ondelete='restrict'
    )
    afiliacion_ids = fields.One2many(
        comodel_name='afiliacion.organizacion.lines',
        inverse_name='modulo_id',
        string='Afiliaciones'
    )

    @api.multi
    def name_get(self):
        return [(obj.id, u'{} - {}'.format(obj.local_id.name, obj.name)) for obj in self]

    @api.constrains('fecha_inicio', 'fecha_fin')
    def constrains_fechas(self):
        validar_fechas(self.fecha_inicio, self.fecha_fin)

    @api.multi
    def create_record(self):
        self.ensure_one()

    @api.constrains('ninios_ids')
    def _check_ninios_ids(self):
        if len(self.ninios_ids) > self.nro_ninios:
            raise ValidationError(u'El números de niños supera el limete del módulo')

    @api.model
    def create(self, values):
        obj = super(ModuloLocal, self).create(values)
        self._set_fields_ninios_afiliacion(obj.id)
        return obj

    @api.multi
    def write(self, values):
        r = super(ModuloLocal, self).write(values)
        id = self.read(['id'])
        self._set_fields_ninios_afiliacion(id[0]['id'])
        return r

    def _set_fields_ninios_afiliacion(self, obj_id):
        obj = self.search([('id', '=', obj_id)])
        for ninio in obj.ninios_ids:
            ninio.local_id = self.local_id
            ninio.comite_gestion_id = self.local_id.comite_id
            ninio.unidad_territorial_id = self.local_id.ut_id
            ninio.modulo_id = self.id
            ninio.sala_id = self.sala_id
            ninio.ambiente_id = ninio.sala_id.ambientes_id


class NiniosModulo(models.Model):

    _name = 'modulo.ninios'
    _inherit = 'tabla.base'

    ninios_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        inverse_name='modulo_ninios_ids',
        string=u'Niños',
        required=True,
        domain="[('tipo_parentesco','=', u'Niño(a)')]",
        ondelete='restrict'
    )
    fecha_inicio = fields.Date(
        string='Fecha Inicio',
        required=True
    )
    fecha_fin = fields.Date(
        string='Fecha Fin'
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        string=u'Comite Gestión',
        readoonly=True,
        ondelete='restrict'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        readoonly=True,
        ondelete='restrict'
    )
    cod_ninio = fields.Integer(
        string=u'Código Niño',
        readoonly=True,
        related='partner_id.id',
        store=True
    )
    partner_id = fields.Many2one(
        comodel_name='res.partner',
        inverse_name='modulos_ninios_ids',
        string='Persona',
        related='ninios_id.integrante_id',
        store=True,
        ondelete='restrict'
    )
    local_id = fields.Many2one(
        comodel_name='pncm.infra.local',
        string='Local',
        readoonly=True,
        ondelete='restrict'
    )
    modulo_id = fields.Many2one(
        comodel_name='modulo.local',
        inverse_name='ninios_ids',
        string='Modulo',
        ondelete='restrict'
    )
    sala_id = fields.Many2one(
        comodel_name='salas.local',
        string='Sala',
        ondelete='restrict'
    )
    ambiente_id = fields.Many2one(
        comodel_name='ambientes.local',
        string='Ambiente',
        domain="[('local_id', '=', local_id)]",
        ondelete='restrict'
    )
    state = fields.Selection(
        selection=estados,
        string='Estado',
        default='activo'
    )
    historial_modulo_ids = fields.Many2many(
        comodel_name='modulo.ninios',
        relation='ninios_modulo_rel',
        column1='col_name',
        column2='col_name_2',
        compute='_compute_historial_modulo_ids',
        string='Historial'
    )

    @api.depends('partner_id')
    def _compute_historial_modulo_ids(self):
        historial = self.search([('partner_id', '=', self.partner_id.id)])
        self.historial_modulo_ids = historial

    @api.onchange('fecha_fin')
    def _onchange_fecha_fin(self):
        if self.fecha_fin:
            self.state = 'cerrado'
        else:
            self.state = 'activo'

    @api.model
    def create(self, values):
        obj = super(NiniosModulo, self).create(values)
        obj.ninios_id.integrante_id.permitir_modulo = True
        return obj

    @api.multi
    def write(self, values):
        r = super(NiniosModulo, self).write(values)
        if self.fecha_fin:
            self.ninios_id.integrante_id.permitir_modulo = False
        else:
            self.ninios_id.integrante_id.permitir_modulo = True
        return r

    @api.multi
    def name_get(self):
        return [(obj.id, u'{} - {}'.format(obj.modulo_id.name, obj.ninios_id.integrante_id.name)) for obj in self]

    @api.constrains('fecha_inicio', 'fecha_fin')
    def constrains_fechas(self):
        validar_fechas(self.fecha_inicio, self.fecha_fin)


class InfraLocal(models.Model):

    _inherit = 'pncm.infra.local'

    tipo_local = fields.Selection(
        selection=[
            ('hcd', 'HCD'),
            ('ccd', 'CCD'),
            ('ciai', u'CIAI'),
            ('sa', u'SA'),
            ('ambos', u'Ambos (CIAI y SA)'),
        ],
        string='Tipo de local'
    )
    ambientes_ids = fields.One2many(
        comodel_name='ambientes.local',
        inverse_name='local_id',
        string='Ambientes'
    )
    salas_ids = fields.One2many(
        comodel_name='salas.local',
        inverse_name='local_id',
        string='Salas'
    )
    modulos_ids = fields.One2many(
        comodel_name='modulo.local',
        inverse_name='local_id',
        string=u'Módulos'
    )
    afiliaciones_ids = fields.One2many(
        comodel_name='afiliacion.organizacion.lines',
        inverse_name='local_id',
        string='Todas la afiliaciones'
    )
    madres_cuidadoras_ids = fields.Many2many(
        comodel_name='afiliacion.organizacion.lines',
        relation='mdcuid_lines',
        compute='_compute_afiliacion_lines',
        store=True
    )
    socias_cocina_ids = fields.Many2many(
        comodel_name='afiliacion.organizacion.lines',
        string='Socias de Cocina'
    )
    sa_ids = fields.Many2many(
        comodel_name='pncm.infra.local',
        relation='sa_rel_local',
        column1='sa',
        column2='local',
        string='Servicio Alimentario',
        domain="[('id', '!=', id), "
               "('state', '=', 'activacion')]"
    )
    nro_telefono = fields.Char(
        string='N° Telefónico'
    )
    proapepat = fields.Char(
        string='Apellido paterno propietario',
        related='propietario_id.firstname',
        store=True
    )
    proapemat = fields.Char(
        string='Apellido paterno propietario',
        related='propietario_id.secondname',
        store=True
    )
    pronom = fields.Char(
        string='Nombre propietario',
        related='propietario_id.name',
        store=True
    )
    prodni = fields.Char(
        string='DNI propietario',
        related='propietario_id.document_number',
        store=True
    )
    tieserali = fields.Selection(
        selection=[
            ('1', 'SI'),
            ('0', 'NO'),
        ],
        string='Tiene SA',
        compute='_compute_tieserali',
        store=True
    )
    fecha_creacion = fields.Date(
        string='Fecha Creacion',
        default=fields.Date.today()
    )
    fecha_mod = fields.Date(
        string=u'Fecha Modificación'
    )
    usuario_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Creador',
        default=lambda self: self.env.user
    )
    usuario_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Modifico'
    )
    usuario_bd_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Creador'
    )
    usuario_bd_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Modifico'
    )
    cunanet_id = fields.Integer()
    sisaf_id = fields.Integer()

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.state != 'identificacion':
                raise ValidationError('El registro debe encontrarse en estado identificacion para ser eliminado.')
            else:
                rec.fichas_preevalucion.unlink()
                rec.fichas_informe.unlink()
                rec.fotos.unlink()
                rec.socias_cocina_ids.unlink()
        res = super(InfraLocal, self).unlink()
        return res

    @api.multi
    @api.depends('sa_ids')
    def _compute_tieserali(self):
        for rec in self:
            if rec.sa_ids:
                rec.tieserali = '1'
            else:
                rec.tieserali = '0'

    @api.onchange('comite_id')
    def _al_cambiar_comite(self):
        if self.comite_id:
            intervenciones_comite = \
                self.env['pncm.comitegestion.intervencion'].search(
                    [('comite_gestion_id', '=', self.comite_id.id)])
            departamentos_ids = [obj.departamento_id.id for
                                 obj in intervenciones_comite]
            return {'domain': {'departamento_id': ['&',
                                                   ('country_id.code',
                                                    '=', 'PE'),
                                                   ('id', 'in',
                                                    departamentos_ids)]}}

    @api.multi
    def accion_agregar_socias_cocinas(self):
        form_id = self.env.ref('base_cuna.form_view_afiliacion_organizacion_lines_socias_cocina').id
        tipo = self.env['tipo.persona'].search([('name', '=', 'SOCIO DE COCINA')])
        context = {
            'default_unidad_territorial_id': self.ut_id.id,
            'default_comite_gestion_id': self.comite_id.id,
            'default_local_id': self.id,
            'default_state': 'active',
            'default_tipo_guia': tipo.id
        }
        return accion_agregar('Agrega Socias de Cocina', form_id, 'afiliacion.organizacion.lines', context)

    @api.multi
    def dar_de_baja(self):
        if self.es_activado:
            if self.socias_cocina_ids.filtered(lambda x: x.state == 'activo'):
                raise ValidationError('No se pueda dar de baja mientras tenga socias de cocina en estado "Activo".')
            if self.state == 'suspension':
                if len(self.suspensiones) > 0:
                    self.write(dict(state='baja',
                                    no_validar_suspension=True))
                else:
                    raise ValidationError(u'No ha registrado ninguna '
                                          u'suspensión')
            elif self.state == 'activacion':
                self.write(dict(state='baja'))
        else:
            raise ValidationError(u'El local debe estar activo para que '
                                  u'se pueda dar de baja')

    @api.multi
    @api.depends('afiliaciones_ids')
    def _compute_afiliacion_lines(self):
        for rec in self:
            if rec.afiliaciones_ids:
                rec.madres_cuidadoras_ids = rec.afiliaciones_ids.filtered(lambda x: x.cargo == 'MADRE CUIDADORA')

    @api.model
    def create(self, values):
        groups = self.env.user.groups_id
        cat_cunamas = self.env['ir.module.category'].sudo().search([('name', '=', 'Usuario Cunamas')])
        especialista_nutricion = self.env['res.groups'].sudo().search(
            [('name', '=', u'Especialista en Nutrición'), ('category_id', '=', cat_cunamas.id)]
        )
        if especialista_nutricion in groups:
            values['tipo_local'] = 'sa'
        _drop_records(self, 'salas.local', [('local_id', '=', False)])
        _drop_records(self, 'modulo.local', [('sala_id', '=', False)])
        obj = super(InfraLocal, self).create(values)
        self._search_in_afiliacion_lines(obj.id)
        return obj

    @api.multi
    def write(self, values):
        _drop_records(self, 'salas.local', [('local_id', '=', False)])
        _drop_records(self, 'modulo.local', [('sala_id', '=', False)])
        r = super(InfraLocal, self).write(values)
        id = self.read(['id'])
        self._search_in_afiliacion_lines(id[0]['id'])
        return r

    def _search_in_afiliacion_lines(self, obj_id):
        servicio_obj = self.search([('id', '=', obj_id)])
        if servicio_obj.socias_cocina_ids:
            _set_fields_lines(self, servicio_obj.socias_cocina_ids, servicio_obj, 'SOCIO DE COCINA')

    @api.multi
    def action_crear_ambientes(self):
        form_id = self.env.ref('base_cuna.form_view_ambientes_local_crear').id
        context = {'default_local_id': self.id}
        return accion_agregar('Ambientes', form_id, 'ambientes.local', context)

    @api.multi
    def action_crear_salas(self):
        form_id = self.env.ref('base_cuna.form_view_salas_local_crear').id
        context = {'default_local_id': self.id}
        return accion_agregar('Salas', form_id, 'salas.local', context)

    @api.multi
    def action_crear_modulos(self):
        form_id = self.env.ref('base_cuna.form_view_modulo_local_crear').id
        context = {'default_local_id': self.id}
        return accion_agregar(u'Módulos', form_id, 'modulo.local', context)

    @api.multi
    def filtrar_vistas(self):
        action = {
            'name': 'Locales',
            'type': "ir.actions.act_window",
            'res_model': 'pncm.infra.local',
            'view_type': "form",
            'view_mode': "tree,form,pivot,graph",
        }
        user = self.env['res.users'].browse(self._uid)

        groups = self.env.user.groups_id

        cat_cunamas = self.env['ir.module.category'].sudo().search([('name', '=', 'Usuario Cunamas')])
        jefe_ut = self.env['res.groups'].sudo().search(
            [('name', '=', 'Jefe de Unidad Territorial'), ('category_id', '=', cat_cunamas.id)]
        )
        coordinador = self.env['res.groups'].sudo().search(
            [('name', '=', 'Coordinador'), ('category_id', '=', cat_cunamas.id)]
        )
        asistente_adm = self.env['res.groups'].sudo().search(
            [('name', '=', 'Administrador/ Asistente Administrativo'), ('category_id', '=', cat_cunamas.id)]
        )
        tec_informatico = self.env['res.groups'].sudo().search(
            [('name', '=', u'Técnico Informático'), ('category_id', '=', cat_cunamas.id)]
        )
        especialista_integrales = self.env['res.groups'].sudo().search(
            [('name', '=', 'Especialista Integrales'), ('category_id', '=', cat_cunamas.id)]
        )
        acompaniante_tecnico = self.env['res.groups'].sudo().search(
            [('name', '=', u'Acompañante Técnico'), ('category_id', '=', cat_cunamas.id)]
        )
        servicios_terceros = self.env['res.groups'].sudo().search(
            [('name', '=', u'Servicios por Terceros'), ('category_id', '=', cat_cunamas.id)]
        )
        especialista_nutricion = self.env['res.groups'].sudo().search(
            [('name', '=', u'Especialista en Nutrición'), ('category_id', '=', cat_cunamas.id)]
        )
        gestion_comunal = self.env['res.groups'].sudo().search(
            [('name', '=', u'Gestión Comunal'), ('category_id', '=', cat_cunamas.id)]
        )
        cunamas_groups = [
            jefe_ut.id, coordinador.id, asistente_adm.id, tec_informatico.id, especialista_integrales.id,
            acompaniante_tecnico.id, servicios_terceros.id, especialista_nutricion.id, gestion_comunal.id
        ]

        new = [c for c in groups.ids if c in cunamas_groups]
        if not len(new) == 9:
            if not especialista_nutricion in groups:
                if user.unidad_territorial_id:
                    action['domain'] = "[('ut_id', 'in', %s)]" % user.unidad_territorial_id.ids
            else:
                if user.unidad_territorial_id:
                    action['domain'] = "[('ut_id', 'in', %s),('tipo_local', 'in', ['sa', 'ambos'])]" % \
                                       user.unidad_territorial_id.ids
                else:
                    action['domain'] = "[('tipo_local', 'in', ['sa', 'ambos'])]"
        else: # es administrador
            if user.unidad_territorial_id:
                action['domain'] = "[('ut_id', 'in', %s)]" % user.unidad_territorial_id.ids
        return action


class MigracionFamilias(models.Model):

    _name = 'migracion.familia'
    _inherit = 'tabla.base'

    cambiar_ut = fields.Boolean(
        string=u'¿Cambiar de unidad territorial?',
        default=True
    )
    unidad_territorial_id_log = fields.Many2many(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True), ('estado','=', 'activo')]",
        default=lambda self: self.env.user.unidad_territorial_id if self.env.user.unidad_territorial_id else False
    )
    unidad_territorial_origen_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial Origen',
        readonly=True
    )
    unidad_territorial_destino_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial Destino',
        domain="[('is_office','=', True),"
               "('id','!=', unidad_territorial_origen_id)]"
    )
    comite_gestion_origen_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=',unidad_territorial_origen_id)]",
        string=u'Comite Gestión Origen',
        required=True
    )
    comite_gestion_destino_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=',unidad_territorial_destino_id),"
               "('id', '!=', comite_gestion_origen_id)]",
        string=u'Comite Gestión Destino',
        required=True
    )
    familias_ids = fields.Many2many(
        comodel_name='unidad.familiar',
        domain="[('comite_gestion_id','=', comite_gestion_origen_id),"
               "('state', '=', 'activo'),"
               "('hogar_activo', '=', True)]",
        string='Familias',
        required=True
    )
    fecha_migracion = fields.Date(
        string=u'Fecha Migración',
        default=fields.Date.today(),
        readonly=True
    )

    @api.onchange('unidad_territorial_id_log')
    def _onchange_domain(self):
        if self.unidad_territorial_id_log:
            return {'domain': {'unidad_territorial_origen_id': [('id', 'in', self.unidad_territorial_id_log.ids)]}}
        else:
            return {'domain': {'unidad_territorial_origen_id': [('is_office', '=', True), ('estado', '=', 'activo')]}}

    @api.onchange('comite_gestion_origen_id')
    def _onchange_comite_gestion_origen_id(self):
        self.comite_gestion_destino_id = False
        self.familias_ids = False

    @api.onchange('cambiar_ut')
    def _onchange_cambiar_ut(self):
        self.unidad_territorial_destino_id = False
        self.comite_gestion_origen_id = False
        self.comite_gestion_destino_id = False
        self.familias_ids = False
        if not self.cambiar_ut:
            self.unidad_territorial_destino_id = self.unidad_territorial_origen_id

    @api.constrains('familias_ids')
    def _constraints_familias_ids(self):
        for rec in self.familias_ids:
            ninios = rec.integrantes_ids.filtered(lambda x: x.parentesco_id.name == u'Niño(a)')
            for ninio in ninios:
                if ninio.integrante_id.modulos_ninios_ids.filtered(lambda x: not x.fecha_fin):
                    raise ValidationError('Existen niños con afiliaciones activas. Familia:  %d' % rec.id)

    @api.multi
    def create_record(self):
        self.ensure_one()
        for rec in self.familias_ids:
            rec.comite_gestion_id = self.comite_gestion_destino_id
            rec.unidad_territorial_id = self.unidad_territorial_destino_id
            rec.migraciones_ids = [(4, self.id)]
            for line in rec.vivienda_ids:
                line.state = 'cerrado'
                line.fecha_fin = self.fecha_migracion


class UT(models.Model):

    _inherit = 'hr.department'

    services = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento a Familias'),
        ('ambos', u'Ambos'),
    ],
        string=u'Servicio'
    )
    fecha_creacion = fields.Date(
        string='Fecha Creacion',
        default=fields.Date.today()
    )
    fecha_mod = fields.Date(
        string=u'Fecha Modificación'
    )
    usuario_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Creador',
        default=lambda self: self.env.user
    )
    usuario_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Modifico'
    )
    usuario_bd_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Creador'
    )
    usuario_bd_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Modifico'
    )
    cunanet_id = fields.Integer()
    sisaf_id = fields.Integer()
    country_id = fields.Many2one(
        comodel_name='res.country',
        string=u'País',
        default=lambda self: self._obtener_pais_peru(),
        readonly=True
    )
    estado = fields.Selection(
        selection=[
            ('constitucion', u'En Formacion'),
            ('activo', 'Activo'),
            ('cerrado', 'Cerrado'),
        ],
        string='Estado',
        default='constitucion'
    )
    adjuntar_archivo = fields.Binary(
        string="Archivo Adjunto"
    )
    file_name = fields.Char(
        string="Adjunto"
    )
    file_url = fields.Char('Archivo Adjunto')

    @api.model
    def create(self, values):
        if values.get('adjuntar_archivo'):
            values['file_url'], values['file_name'] = _controlador_ruta(
                self, 'resolucion_', values['adjuntar_archivo'], values['file_name'])
            values['adjuntar_archivo'] = False
        return super(UT, self).create(values)

    @api.multi
    def write(self, values):
        if values.get('adjuntar_archivo'):
            values['file_url'], values['file_name'] = _controlador_ruta(
                self, 'resolucion_', values['adjuntar_archivo'], values['file_name'])
            values['adjuntar_archivo'] = False
        return super(UT, self).write(values)

    @api.constrains('fecha_inicio', 'fecha_fin')
    def _constrains_fechas_resolucion(self):
        if self.fecha_inicio and self.fecha_fin:
            if self.fecha_inicio > self.fecha_fin:
                raise ValidationError('La fecha de inicio no puede ser mayor a fecha fin.')

    def _obtener_pais_peru(self):
        pais = self.env['res.country'].search([('name', '=', 'Perú')], limit=1)
        return pais.id

    @api.multi
    def estado_activo(self):
        self.estado = 'activo'

    @api.multi
    def estado_cerrado(self):
        if self.ut_comites_gestion.filtered(lambda x: x.state != 'cerrado'):
            raise ValidationError('No se puede cerrar Unidad Territorial mientras tenga Comités de Gestión activos.')
        self.estado = 'cerrado'

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.ut_comites_gestion:
                raise ValidationError('El registro esta haciendo referenciado por uno/muchos Comite de Gestion.')
        res = super(UT, self).unlink()
        return res

    @api.multi
    def filtrar_vistas(self):
        search_id = self.env.ref('hr.view_department_ut_filter').id
        context ={"search_default_is_office": 1, 'default_is_office':1}
        action = {
            'name': 'Unidades Territoriales',
            'type': "ir.actions.act_window",
            'res_model': 'hr.department',
            'view_type': "form",
            'view_mode': "tree,form,pivot,graph",
            'search_view_id': search_id,
            'context': context
        }
        user = self.env['res.users'].browse(self._uid)
        if user.unidad_territorial_id and user.servicio and user.servicio != 'ambas':
            action['domain'] = "[('id', 'in', %s), ('services', '=', '%s')]" % \
                               (user.unidad_territorial_id.ids, user.servicio)
        elif user.unidad_territorial_id:
            action['domain'] = "[('id', 'in', %s)]" % user.unidad_territorial_id.ids
        elif user.servicio and user.servicio != 'ambas':
            action['domain'] = "[('services', '=', '%s')]" % user.servicio
        return action


class CMEquipoTecnico(models.Model):

    _inherit = 'pncm.comite.equipo.tecnico'

    cargo = fields.Char(
        string=u'Cargo',
        compute='_compute_cargo',
        store=True
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string='Unidad Territorial',
        domain="[('is_office','=',True), ('estado','=', 'activo')]",
        ondelete='restrict',
        default=lambda self: self.env.user.unidad_territorial_id[0].id if self.env.user.unidad_territorial_id else False
    )
    fecha_creacion = fields.Date(
        string='Fecha Creacion',
        default=fields.Date.today()
    )
    fecha_mod = fields.Date(
        string=u'Fecha Modificación'
    )
    usuario_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Creador',
        default=lambda self: self.env.user,
        ondelete='restrict'
    )
    usuario_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Modifico',
        ondelete='restrict'
    )
    usuario_bd_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Creador',
        ondelete='restrict'
    )
    usuario_bd_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Modifico',
        ondelete='restrict'
    )
    cunanet_id = fields.Integer()
    sisaf_id = fields.Integer()

    @api.onchange('empleado_cunamas_id')
    def _complete_fields(self):
        if self.empleado_cunamas_id:
            nombres = self.empleado_cunamas_id.name
            cargo = ''
            if self.empleado_cunamas_id.position_id:
                cargo = self.empleado_cunamas_id.position_id.name
            return {'value': {'nombres': nombres, 'cargo': cargo}}

    @api.multi
    @api.depends('empleado_cunamas_id')
    def _compute_cargo(self):
        for rec in self:
            rec.cargo = rec.empleado_cunamas_id.position_id.name

    @api.multi
    def name_get(self):
        return [(obj.id, u'{}'.format(obj.empleado_cunamas_id.display_name)) for obj in self]


class TablaCierre(models.Model):

    _name = 'tabla.cierre.periodo'
    _inherit = 'tabla.base'

    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string='Unidad Territorial',
        domain="[('is_office','=',True), ('estado','=', 'activo')]",
        ondelete='restrict'
    )
    mes = fields.Selection(
        selection=months,
        string='Periodo',
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        string=u'Año'
    )
    tipo_cierre = fields.Selection(
        selection=[
            ('requerimientos', 'Requerimientos'),
            ('justificaciones', 'Justificaciones'),
            ('visitas_r', 'Visitas Reconocimiento'),
            ('visitas_f', 'Visitas Fortalecimiento'),
            ('visitas_g', 'Visitas Gestante'),
            ('asistencias', 'Asistencia'),
        ],
        string='Registro'
    )
    fecha_inicio = fields.Date(
        string='Fecha Inicio'
    )
    fecha_fin = fields.Date(
        string='Fecha fin'
    )
    servicio = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento a Familias')
    ])

    @api.model
    def validar_periodo_cerrado(self):
        mes = _obtener_tiempo(self, 'mes')
        anio = _obtener_tiempo(self, 'anio')

        recs = self.search([
            ('anio', '=', mes),
            ('mes', '=', anio.id)
        ])
        hoy = fields.Date.today()

        for rec in recs:
            if not (rec.fecha_inicio <= hoy <= rec.fecha_fin):
                if rec.tipo_cierre == 'requerimientos':
                    reqs = self.env['requerimiento.lines'].search([
                        ('anio', '=', self.anio.id),
                        ('mes', '=', mes),
                        ('unidad_territorial_id', '=', rec.unidad_territorial_id.id)
                    ])
                    for req in reqs:
                        req.fecha_cierre = fields.Date().today()
                elif rec.tipo_cierre == 'visitas_r':
                    fichas = self.env['ficha.reconocimiento'].search([
                        ('anio', '=', self.anio.id),
                        ('mes', '=', mes),
                        ('unidad_territorial_id', '=', rec.unidad_territorial_id.id)
                    ])
                    for ficha in fichas:
                        ficha.estado = 'cerrado'
                        ficha.fecha_cierre = fields.Date().today()
                elif rec.tipo_cierre == 'visitas_f':
                    fichas = self.env['ficha.fortalecimiento'].search([
                        ('anio', '=', self.anio.id),
                        ('mes', '=', mes),
                        ('unidad_territorial_id', '=', rec.unidad_territorial_id.id)
                    ])
                    for ficha in fichas:
                        ficha.estado = 'cerrado'
                        ficha.fecha_cierre = fields.Date().today()
                elif rec.tipo_cierre == 'visitas_g':
                    fichas = self.env['ficha.gestante'].search([
                        ('anio', '=', self.anio.id),
                        ('mes', '=', mes),
                        ('unidad_territorial_id', '=', rec.unidad_territorial_id.id)
                    ])
                    for ficha in fichas:
                        ficha.estado = 'cerrado'
                        ficha.fecha_cierre = fields.Date().today()
                elif rec.tipo_cierre == 'asistencias':
                    asists = self.env['asistencia.control'].search([
                        ('anio', '=', self.anio.id),
                        ('mes', '=', mes),
                        ('unidad_territorial_id', '=', rec.unidad_territorial_id.id)
                    ])
                    for asis in asists:
                        asis.state = 'closed'
                        asis.fecha_cierre = fields.Date().today()
                else:
                    j1_ids = self.env['justificaciones.lines'].search([
                        ('anio', '=', self.anio.id),
                        ('mes', '=', mes),
                        ('unidad_territorial_id', '=', rec.unidad_territorial_id.id)
                    ])
                    j2_ids = self.env['justificacion.actor'].search([
                        ('anio', '=', self.anio.id),
                        ('mes', '=', mes),
                        ('unidad_territorial_id', '=', rec.unidad_territorial_id.id)
                    ])
                    j3_ids = self.env['justificacion.gastos'].search([
                        ('anio', '=', self.anio.id),
                        ('mes', '=', mes),
                        ('unidad_territorial_id', '=', rec.unidad_territorial_id.id)
                    ])
                    for j1 in j1_ids:
                        j1.estado = 'cerrado'
                        j1.fecha_cierre = fields.Date().today()
                    for j2 in j2_ids:
                        j2.estado = 'cerrado'
                        j2.fecha_cierre = fields.Date().today()
                    for j3 in j3_ids:
                        j3.estado = 'cerrado'
                        j3.fecha_cierre = fields.Date().today()


class CMEditarIdentificacion(models.TransientModel):
    _inherit = 'pncm.infra.wizard.local.editar_identificacion'

    tipo_local = fields.Selection(
        selection=[
            ('hcd', 'HCD'),
            ('ccd', 'CCD'),
            ('ciai', u'CIAI'),
            ('sa', u'SA'),
            ('ambos', u'Ambos (CIAI y SA)'),
        ],
        string='Tipo de local'
    )
