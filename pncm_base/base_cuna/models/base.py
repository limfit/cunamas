# -*- coding: utf-8 -*-

from datetime import datetime
from dateutil.relativedelta import relativedelta
from lxml import etree
from lxml.builder import E
from odoo import api, fields, models, tools, _
from odoo.exceptions import ValidationError
from odoo.http import request
from suds.client import Client
from suds.sudsobject import asdict
from suds.transport.https import WindowsHttpAuthenticated
from pysimplesoap.client import SoapClient, SoapFault
from collections import Counter



months = [
    ('01', 'Enero'),
    ('02', 'Febrero'),
    ('03', 'Marzo'),
    ('04', 'Abril'),
    ('05', 'Mayo'),
    ('06', 'Junio'),
    ('07', 'Julio'),
    ('08', 'Agosto'),
    ('09', 'Setiembre'),
    ('10', 'Octubre'),
    ('11', 'Noviembre'),
    ('12', 'Diciembre')
]

_opttipodocumento = [
    ('dni', 'DNI'),
    ('ruc', 'RUC'),
    ('ext', u'Carnet de Extranjería'),
    ('pas', 'Pasaporte'),
    ('partida_nac', 'Partida Nacimiento')
]


def name_boolean_group(id):
    return 'in_group_' + str(id)


def name_selection_groups(ids):
    return 'sel_groups_' + '_'.join(map(str, ids))


def _validar_wsdl(url):
    wsdl1 = url
    try:
        SoapClient(wsdl=wsdl1, cache=None, ns="ser", soap_ns="soapenv", trace=True)
    except Exception:
        raise ValidationError('Error en consultar DNI, intente nuevamente.', )


def suds2dict(d):
    out = {'__class__': d.__class__.__name__}
    for k, v in asdict(d).iteritems():
        if hasattr(v, '__keylist__'):
            out[k] = suds2dict(v)
        elif isinstance(v, list):
            out[k] = []
            for item in v:
                if hasattr(item, '__keylist__'):
                    out[k].append(suds2dict(item))
                else:
                    out[k].append(item)
        else:
            out[k] = v
    return out


def verificar_lineas_activas(rec):
    """
        Verifica si el usuario tiene alguna linea de afiliacion a familia, modulo activa.
    """
    if rec:
        total = len(rec)
        lineas = rec.filtered(lambda x: x.fecha_fin)
        if total == len(lineas):
            return False
        else:
            return True
    else:
        return False


class TablasBase(models.Model):
    _name = 'tabla.base'

    name = fields.Char(
        string='Nombre'
    )
    fecha_creacion = fields.Date(
        string='Fecha Creacion',
        default=fields.Date.today()
    )
    fecha_mod = fields.Date(
        string=u'Fecha Modificación'
    )
    usuario_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Creador',
        default=lambda self: self.env.user
    )
    usuario_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Modifico'
    )
    usuario_bd_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Creador'
    )
    usuario_bd_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Modifico'
    )
    cunanet_id = fields.Integer()
    sisaf_id = fields.Integer()
    activo = fields.Boolean(
        string='Activo',
        default=True
    )

    @api.multi
    def write(self, vals):
        vals['usuario_mod_id'] = self.env.user.id
        vals['fecha_mod'] = fields.Date.today()
        return super(TablasBase, self).write(vals)


class CountryState(models.Model):
    _inherit = 'res.country.state'

    codcat = fields.Selection(
        selection=[
            ('ND', 'No Definido'),
            ('01', 'CIUDAD'),
            ('02', 'PP.JJ.AA.HH.'),
            ('03', 'URBANIZACION'),
            ('04', 'PUEBLO'),
            ('05', 'CASERIO'),
            ('06', 'ANEXO'),
            ('07', 'VILLA'),
            ('08', 'UNIDAD AGROPECUARIA'),
            ('09', 'COOPERATIVA AGRARIA'),
            ('10', 'CAMPAMENTO MINERO'),
            ('11', 'CONJUNTO HABITACIONAL'),
            ('12', 'ASOCIACION DE VIVIENDA'),
            ('13', 'COOPERATIVA DE VIVIENDA'),
            ('14', 'BARRIO O CUARTEL'),
            ('15', 'OTROS'),
            ('99', 'SIN DATOS'),
        ],
        string='Codigo Categoria',
    )
    nomcat = fields.Char(
        string='Nombre de categoria',
        compute='_compute_nomcat',
        store=True
    )
    fecha_creacion = fields.Date(
        string='Fecha Creacion',
        default=fields.Date.today()
    )
    fecha_mod = fields.Date(
        string=u'Fecha Modificación'
    )
    usuario_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Creador',
        default=lambda self: self.env.user
    )
    usuario_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Modifico'
    )
    usuario_bd_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Creador'
    )
    usuario_bd_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Modifico'
    )
    cunanet_id = fields.Integer()
    sisaf_id = fields.Integer()

    @api.multi
    @api.depends('codcat')
    def _compute_nomcat(self):
        for rec in self:
            if rec.codcat:
                rec.nomcat = str(dict(rec._fields['codcat'].selection).get(rec.codcat))


class TipoDocumento(models.Model):
    _name = 'tipo.documento'

    name = fields.Char(
        string='Tipo de Documento',
        requiered=True
    )
    ficha_persona = fields.Boolean(
        string='Filtrar en ficha de persona?'
    )
    abrev = fields.Char(
        string='abreviacion para busqueda'
    )

    @api.onchange('name')
    def _onchange_name(self):
        if self.name:
            self.name = self.name.upper()

    @api.constrains('name')
    def _check_default_code(self):
        code = self.search([('name', '=', self.name)])
        if len(code) > 1:
            raise ValidationError('¡Nombre duplicado!')


class ResPartner(models.Model):
    _inherit = 'res.partner'

    type_document2 = fields.Many2one(
        comodel_name='tipo.documento',
        string='Tipo de Documento',
        domain="[('ficha_persona', '=', True)]",
    )
    type_document = fields.Selection(
        selection=_opttipodocumento,
        string="Tipo de Documento"
    )
    cat_ninio = fields.Selection(
        selection=[
            ('0_5', u'Niños 0-5 meses'),
            ('6_12', u'Niños 6-12 meses'),
            ('13_18', u'Niños 13-18 meses'),
            ('19_24', u'Niños 19-24 meses'),
            ('25_32', u'Niños 25-32 meses'),
        ],
        string='Categoria ninio',
        compute='_compute_cat_ninio',
        store=True
    )
    marital_status = fields.Selection(
        selection=[
            ('Soltero', 'Solter@'),
            ('Casado', 'Casad@'),
            ('Viudo', 'Viud@'),
            ('Divorciado', 'Divorciad@'),
            ('Separado', 'Separad@'),
            ('Conviviente', 'Conviviente')
        ],
        string='Estado Civil'
    )
    fecha_ingreso_pncm = fields.Date(
        string='Fecha de Ingreso al PNCM'
    )
    nivel_educativo_id = fields.Many2one(
        comodel_name='nivel.educativo',
        string='Nivel Educativo',
    )
    ultimo_grado_apr_id = fields.Many2one(
        comodel_name='ultimo.grado.aprobado',
        string=u'Último Grado Aprobado'
    )
    seguro_id = fields.Many2one(
        comodel_name='tipo.seguro',
        string='Tipo de Seguro'
    )
    ocupacion_id = fields.Many2one(
        comodel_name='tipo.ocupacion',
        string=u'Tipo de Ocupación'
    )
    discapacidad_id = fields.Many2many(
        comodel_name='tipo.discapacidad',
        string='Tipo de Discapacidad'
    )
    lengua_materna_id = fields.Many2one(
        comodel_name='lengua.materna',
        string='Lengua Materna'
    )
    nro_hijos = fields.Integer(
        string=u'Número de Hijos'
    )
    programa_social_ids = fields.Many2many(
        comodel_name='programa.social',
        string='Programa Social'
    )
    religion_id = fields.Many2one(
        comodel_name='tipo.religion',
        string=u'Religión'
    )
    person_code = fields.Char(
        string='Código Persona'
    )
    sabe_leer_y_escribir = fields.Boolean(
        string='¿Sabe leer y escribir?'
    )
    codigo_seguro = fields.Char(
        string=u'Código Seguro'
    )
    familia_id = fields.Many2one(
        comodel_name='unidad.familiar',
        string='Familia'
    )
    tipo_persona = fields.Many2one(
        comodel_name='tipo.persona',
        string='Tipo de Cargo',
        compute='compute_afiliacion',
        store=True
    )
    nombre_tipo_persona = fields.Char(
        string='Tipo de Cargo-Nombre',
        compute='compute_afiliacion',
        store=True
    )
    es_ninio = fields.Boolean(
        string='Es ninio?',
        compute='_compute_months',
        store=True
    )
    months = fields.Integer(
        string='Meses',
        compute='_compute_months',
        store=True
    )
    dias = fields.Integer(
        string='Dias',
        compute='_compute_months',
        store=True
    )
    anios = fields.Integer(
        string=u'Años',
        compute='_compute_months',
        store=True
    )
    permitir_familia = fields.Boolean(
        string='Esta activo en otra familia?',
        compute='compute_permitir_familia',
        store=True
    )
    integrantes_lines_ids = fields.One2many(
        comodel_name='integrante.unidad.familiar',
        inverse_name='integrante_id',
        string='Familia',
        store=True
    )
    modulos_ninios_ids = fields.One2many(
        comodel_name='modulo.ninios',
        inverse_name='partner_id',
        string=u'Afiliación a módulos',
        readonly=True
    )
    permitir_modulo = fields.Boolean(
        string=u'Esta activo en algún módulo?',
        readonly=True
    )
    afiliacion_ids = fields.One2many(
        comodel_name='afiliacion.organizacion.lines',
        inverse_name='person_id',
        string='Afiliaciones',
        readonly=True
    )
    permitir_afiliacion = fields.Boolean(
        string='Tiene activo alguna afiliacion?',
        compute='compute_afiliacion',
        store=True
    )
    madre_gestante = fields.Boolean(
        string='Madre Gestante'
    )
    validado_reniec = fields.Boolean(
        string='Validado por reniec'
    )
    mes_gestacion = fields.Selection(
        selection=[
            ('1_mes', '1 mes'),
            ('2_meses', '2 meses'),
            ('3_meses', '3 meses'),
            ('4_meses', '4 meses'),
            ('5_meses', '5 meses'),
            ('6_meses', '6 meses'),
            ('7_meses', '7 meses'),
            ('8_meses', '8 meses'),
            ('9_meses', '9 meses'),
        ],
        string=u'Mes Gestación'
    )
    sustento_gestante = fields.Text(
        string='Sustento'
    )
    semana_gestacion = fields.Integer(
        string=u'Semana de Gestación',
    )
    reniec_activo = fields.Boolean(
        default=lambda self: self._validar_reniec_activo()
    )
    state_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Departamento',
    )
    province_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Provincia',
    )
    district_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Distrito',
    )
    poblation_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Centro Poblado',
    )
    fecha_creacion = fields.Date(
        string='Fecha Creacion',
        default=fields.Date.today()
    )
    fecha_mod = fields.Date(
        string=u'Fecha Modificación'
    )
    usuario_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Creador',
        default=lambda self: self.env.user
    )
    usuario_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Modifico'
    )
    usuario_bd_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Creador'
    )
    usuario_bd_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Modifico'
    )
    cunanet_id = fields.Integer()
    sisaf_id = fields.Integer()
    my_button = fields.Boolean('Validar Reniec')
    validar_actor = fields.Boolean('Validar actor')
    invisible_names = fields.Text(help="Guarda datos del onchange - reniec para guardarlo al darle crear")
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        string=u'Comite Gestión'
    )
    validar_nombre = fields.Char()
    _sql_constraints = [('check_name', "CHECK(1>10)", 'Contacts require a name.')]

    @api.onchange('name', 'firstname', 'secondname')
    def _onchange_complete_name(self):
        if self.name:
            if not self.name.isalpha():
                self.name = False
                self.validar_nombre = u'El campo Nombre no debe contener números'
            else:
                self.validar_nombre = False
        if self.firstname:
            if not self.firstname.isalpha():
                self.firstname = False
                self.validar_nombre = u'El campo Apellido Paterno no debe contener números'
            else:
                self.validar_nombre = False
        if self.secondname:
            if not self.secondname.isalpha():
                self.secondname = False
                self.validar_nombre = u'El campo Apellido Materno no debe contener números'
            else:
                self.validar_nombre = False

    @api.constrains('validar_actor', 'months')
    def _constraints_validar_actor(self):
        if self.validar_actor and self.birthdate:
            if self.months < 216:
                raise ValidationError('El actor comunal debe ser mayor de edad.')

    @api.onchange('validar_actor', 'poblation_id')
    def _onchange_centro_poblado_id_actor_comunal(self):
        if self.validar_actor and self.poblation_id:
            self.country_id = self.poblation_id.country_id
            self.state_id = self.poblation_id.state_id
            self.province_id = self.poblation_id.province_id
            self.district_id = self.poblation_id.district_id

    @api.onchange('comite_gestion_id')#, 'document_number')
    def _al_cambiar_distrito(self):
        self.poblation_id = False
        if self.comite_gestion_id:
            intervenciones = self.comite_gestion_id.intervenciones
            centro_poblado_ids = [obj.centro_poblado_id.id for obj in intervenciones]
            return {'domain': {'poblation_id': [('id', 'in', centro_poblado_ids)]}}

    @api.onchange('type_document2')
    def _onchange_type_document2(self):
        if self.type_document2:
            self.type_document = self.type_document2.abrev

    @api.onchange('type_document')
    def _onchange_tipo_documento(self):
        if self.type_document != 'dni':
            self.reniec_activo = False
        else:
            self.reniec_activo = self._validar_reniec_activo()
        if self.type_document in ['partida_nac', 'dni', 'ruc']:
            self.country_id = self.env.user.company_id.country_id
        else:
            self.country_id = False

    @api.onchange('gender', 'birthdate')
    def _onchange_madre_gestante(self):
        if self.gender != 'f' or self.gender != 'f' and not self.birthdate:
            self.madre_gestante = self.mes_gestacion = self.semana_gestacion = False

    @api.onchange('seguro_id')
    def _onchange_seguro_id(self):
        self.codigo_seguro = False

    # Se esta utilzando un booleano como boton para cuando la validacion reniec falle,
    # no se cree ningun registro vacio.
    @api.onchange('my_button')
    def onchange_my_button(self):
        if self.document_number:
            if self.reniec_activo:
                if self.type_document == 'dni' and self.document_number and len(self.document_number) == 8:
                    values = self.consulta_reniec()
                    if values:
                        self.firstname = values['firstname']
                        self.name = values['name']
                        self.secondname = values['secondname']
                        self.birthdate = values['birthdate']
                        self.gender = values['gender']
                        self.street = values['street']
                        self.validado_reniec = values['validado_reniec']
                        self.invisible_names = values
                else:
                    raise ValidationError('Verificar los campos:\n  -Tipo de Documento: "DNI"\n'
                                          '  -Documento de Identidad: 8 digitos.')
            else:
                raise ValidationError('El sistema de reniec no esta activo')

    def consulta_reniec(self):
        if self.type_document == 'dni':
            url = 'http://192.168.252.7/ServiceShared/ConsultasReniec.svc?wsdl'
            client = Client(url, timeout=20)
            response = client.service.ObtenerDatosPersonaDefault(dniConsulta=self.document_number)
            data = suds2dict(response)
            if data.get('Nombre') is not None:
                values = ({
                    'birthdate': data.get('FechaNacimiento'),
                    'gender': 'f' if int(data.get('Genero')) == 50 else 'm',
                    'name': data.get('Nombre'),
                    'firstname': data.get('ApellidoPaterno'),
                    'secondname': data.get('ApellidoMaterno'),
                    'street': data.get('Direccion'),
                    'validado_reniec': True
                })
                return values
            else:
                raise ValidationError('N° de DNI inválido')
        else:
            raise ValidationError('El tipo de documento debe ser DNI')

    @api.onchange('reniec_activo')
    def onchange_validado_reniec(self):
        if not self.reniec_activo and self.type_document == 'dni':
            raise ValidationError('Debe ingresar los datos del usuario manualmente.')

    @api.onchange('type_document')
    def onchange_type_document(self):
        if self.type_document != 'dni':
            self.validado_reniec = True

    @api.multi
    def action_validar_reniec(self):
        cat_cunamas = self.env['ir.module.category'].sudo().search([('name', '=', 'Usuario Cunamas')])
        especialista_nutricion = self.env['res.groups'].sudo().search(
            [('name', '=', u'Especialista en Nutrición'), ('category_id', '=', cat_cunamas.id)]
        )
        tec_informatico = self.env['res.groups'].sudo().search(
            [('name', '=', u'Técnico Informático'), ('category_id', '=', cat_cunamas.id)]
        )
        especialista_integrales = self.env['res.groups'].sudo().search(
            [('name', '=', 'Especialista Integrales'), ('category_id', '=', cat_cunamas.id)]
        )
        groups = self.env.user.groups_id
        for rec in self:
            if especialista_nutricion in groups or tec_informatico in groups or especialista_integrales in groups:
                if len(rec.document_number) != 8:
                    raise ValidationError('Las cantidad de digitos del documento de identidad debe ser 8.')
                if rec.type_document == 'dni' and rec.document_number:
                    if self._validar_reniec_activo():
                        raise ValidationError('Servicio no disponible.')
                    else:
                        url = 'http://192.168.252.7/ServiceShared/ConsultasReniec.svc?wsdl'
                        client = Client(url, timeout=20)
                        response = client.service.ObtenerDatosPersonaDefault(dniConsulta=rec.document_number)
                        data = suds2dict(response)
                        if data.get('Nombre') is not None:
                            values = ({
                                'name': data.get('Nombre'),
                                'firstname': data.get('ApellidoPaterno'),
                                'secondname': data.get('ApellidoMaterno'),
                                'birthdate': data.get('FechaNacimiento'),
                                'gender': 'f' if data.get('Genero') == 49 else 'm',
                                'street': data.get('Direccion'),
                                'validado_reniec': True
                            })
                            rec.write(values)
                else:
                    raise ValidationError('El tipo de documento debe ser DNI.')
            else:
                raise ValidationError('Necesitas rol de Especialista Integral, Tecnico Informatico o Especialista'
                                      u' Nutrición para realizar esta acción.')

    def _validar_reniec_activo(self):
        url = 'http://192.168.252.7/ServiceShared/ConsultasReniec.svc?wsdl'
        try:
            client = Client(url, timeout=10)
            response = client.service.ObtenerDatosPersonaDefault(dniConsulta='76882425')
            data = suds2dict(response)

            if data.get('Nombre') is not None:
                return True
            else:
                return False
        except Exception:
            return False

    def _get_months(self, birthdate):
        now = datetime.now()
        birthdate = fields.Datetime.from_string(str(birthdate))
        delta = relativedelta(now, birthdate)
        years = int(delta.years)
        month = int(delta.months)
        months = years * 12 + month
        return months

    @api.onchange(
        'name',
        'secondname',
        'firstname',
        'business_name',
        'company_type',
        'document_number'
    )
    def onchange_person_information(self):
        if self.name:
            self.name = self.name.upper()
        if self.secondname:
            self.secondname = self.secondname.upper()
        if self.firstname:
            self.firstname = self.firstname.upper()
        if self.business_name:
            self.business_name = self.business_name.upper()

        if self.company_type == 'company':
            self.name = str(self.business_name).strip().upper()
            self.secondname = ''
            self.firstname = ''
            self.type_document = 'ext'

    @api.model
    def create(self, values):
        if values.get('reniec_activo') and values.get('type_document') and values.get('type_document') == 'dni':
            url = 'http://192.168.252.7/ServiceShared/ConsultasReniec.svc?wsdl'
            client = Client(url, timeout=20)
            response = client.service.ObtenerDatosPersonaDefault(dniConsulta=values['document_number'])
            data = suds2dict(response)
            if data.get('Nombre') is not None:
                values['name'] = data.get('Nombre')
                values['firstname'] = data.get('ApellidoPaterno')
                values['secondname'] = data.get('ApellidoMaterno')
                values['birthdate'] = data.get('FechaNacimiento')
                values['gender'] ='f' if data.get('Genero') == 49 else 'm'
                values['street'] = data.get('Direccion')
                values['validado_reniec'] = True

        if values.get('birthdate'):
            months = self._get_months(values.get('birthdate'))
            if months < 216:
                values['marital_status'] = 'Soltero'
        obj = super(ResPartner, self).create(values)
        return obj

    @api.multi
    def write(self, values):
        if values.get('birthdate'):
            months = self._get_months(values.get('birthdate'))
            if months < 216:
                values['marital_status'] = 'Soltero'
        r = super(ResPartner, self).write(values)
        return r

    @api.constrains('fecha_ingreso_pncm', 'birthdate')
    def constraints_fecha_ingreso_pncm(self):
        if self.fecha_ingreso_pncm and self.birthdate:
            if self.fecha_ingreso_pncm < self.birthdate:
                raise ValidationError('La fecha de ingreso al PNCM no puede ser menor o igual a la fecha de nacimiento.')


    @api.onchange('country_id')
    def validar_firm_oga_user(self):
        if self.country_id:
            return {'domain': {
                'state_id': [
                    ('country_id', '=', self.country_id.id),
                    ('state_id', '=', False),
                    ('province_id', '=', False)
                ],
            }}

    @api.multi
    @api.depends('es_ninio', 'months')
    def _compute_cat_ninio(self):
        for rec in self:
            if 0 <= rec.months <= 5:
                rec.cat_ninio = '0_5'
            elif 6 <= rec.months <= 12:
                rec.cat_ninio = '6_12'
            elif 13 <= rec.months <= 18:
                rec.cat_ninio = '13_18'
            elif 19 <= rec.months <= 24:
                rec.cat_ninio = '19_24'
            elif 25 <= rec.months <= 32:
                rec.cat_ninio = '25_32'

    @api.depends('is_company', 'name', 'parent_id.name', 'type',
                 'company_name', 'document_number', 'firstname', 'secondname')
    def _compute_display_name(self):
        diff = dict(show_address=None, show_address_only=None, show_email=None)
        names = dict(self.with_context(**diff).name_get())
        for partner in self:
            partner.display_name = names.get(partner.id)

    @api.multi
    def name_get(self):
        result = []
        for record in self:
            if record.name and not record.secondname and not record.firstname:
                str_name = "%s" % record.name
                result.append((record.id, str_name))
            elif record.name and record.secondname and record.firstname:
                str_name = "[%s] %s %s, %s" % (record.document_number, record.firstname, record.secondname, record.name)
                result.append((record.id, str_name))
            elif record.firstname and record.name:
                str_name = "[%s] %s, %s" % (record.document_number, record.firstname, record.name)
                result.append((record.id, str_name))
            elif record.company_type == 'company':
                str_name = "[%s] %s" % (record.document_number, record.business_name)
                result.append((record.id, str_name))
        return result

    @api.onchange('semana_gestacion')
    def _onchange_semana_gestacion(self):
        if self.semana_gestacion:
            if self.semana_gestacion > 40:
                self.semana_gestacion = 40

    """
        Dependiendo a la afiliación activa se filtra el tipo de cargo de la persona y
        ademas cuando el campo permitir_afiliacion este activo no te permitira filtrar en otra afiliación
    """
    @api.multi
    @api.depends('afiliacion_ids', 'afiliacion_ids.state', 'afiliacion_ids.fecha_retiro')
    def compute_afiliacion(self):
        for rec in self:
            afiliacion = rec.afiliacion_ids.filtered(lambda x: x.state == 'active')
            if afiliacion:
                rec.permitir_afiliacion = True
                rec.tipo_persona = afiliacion[0].tipo_guia
                rec.nombre_tipo_persona = afiliacion[0].tipo_guia.name

    @api.multi
    @api.depends('integrantes_lines_ids', 'integrantes_lines_ids.fecha_fin')
    def compute_permitir_familia(self):
        for rec in self:
            rec.permitir_familia = verificar_lineas_activas(rec.integrantes_lines_ids)

    """
        Dependiendo a la fecha de nacimiento calcula cuantos meses tiene, ademas valida si es niño
        que este en el rango de 3 años como max. para afiliarlo a algun modulo o servicio.
    """
    @api.multi
    @api.depends('birthdate')
    def _compute_months(self):
        now = datetime.now()
        years = days = month = 0
        for rec in self:
            if rec.birthdate:
                birthdate = fields.Datetime.from_string(
                    rec.birthdate,
                )
                delta = relativedelta(now, birthdate)
                years = int(delta.years)
                month = int(delta.months)
                days = int(delta.days)
            if years < 3:
                rec.es_ninio = True
            else:
                rec.es_ninio = False
            rec.months = years * 12 + month
            rec.anios = years
            rec.dias = days + rec.months * 30


class IrUiMenu(models.Model):
    _inherit = 'ir.ui.menu'

    @api.model
    @tools.ormcache('frozenset(self.env.user.groups_id.ids)', 'debug')
    def _visible_menu_ids(self, debug=False):
        """ Return the ids of the menu items visible to the user. """
        # retrieve all menus, and determine which ones are visible
        context = {'ir.ui.menu.full_list': True}
        menus = self.with_context(context).search([])

        groups = self.env.user.groups_id
        servicio = self.env.user.servicio
        if not debug:
            groups = groups - self.env.ref('base.group_no_one')
        # first discard all menus with groups the user does not have
        menus = menus.filtered(
            lambda menu: not menu.groups_id or menu.groups_id & groups)

        # take apart menus that have an action
        action_menus = menus.filtered(lambda m: m.action and m.action.exists())
        folder_menus = menus - action_menus
        visible = self.browse()

        # process action menus, check whether their action is allowed
        access = self.env['ir.model.access']
        model_fname = {
            'ir.actions.act_window': 'res_model',
            'ir.actions.report.xml': 'model',
            'ir.actions.server': 'model_id',
        }
        for menu in action_menus:
            fname = model_fname.get(menu.action._name)
            if not fname or not menu.action[fname] or \
                    access.check(menu.action[fname], 'read', False):
                # make menu visible, and its folder ancestors, too
                visible += menu
                menu = menu.parent_id
                while menu and menu in folder_menus and menu not in visible:
                    visible += menu
                    menu = menu.parent_id
        visible = self.checks_menu(visible, groups)
        return set(visible.ids)

    def checks_menu(self, menus, groups):

        # GRUPO CUNAMAS
        cat_cunamas = self.env['ir.module.category'].sudo().search([('name', '=', 'Usuario Cunamas')])

        jefe_ut = self.env['res.groups'].sudo().search(
            [('name', '=', 'Jefe de Unidad Territorial'), ('category_id', '=', cat_cunamas.id)]
        )
        coordinador = self.env['res.groups'].sudo().search(
            [('name', '=', 'Coordinador'), ('category_id', '=', cat_cunamas.id)]
        )
        asistente_adm = self.env['res.groups'].sudo().search(
            [('name', '=', 'Administrador/ Asistente Administrativo'), ('category_id', '=', cat_cunamas.id)]
        )
        tec_informatico = self.env['res.groups'].sudo().search(
            [('name', '=', u'Técnico Informático'), ('category_id', '=', cat_cunamas.id)]
        )
        especialista_integrales = self.env['res.groups'].sudo().search(
            [('name', '=', 'Especialista Integrales'), ('category_id', '=', cat_cunamas.id)]
        )
        acompaniante_tecnico = self.env['res.groups'].sudo().search(
            [('name', '=', u'Acompañante Técnico'), ('category_id', '=', cat_cunamas.id)]
        )
        servicios_terceros = self.env['res.groups'].sudo().search(
            [('name', '=', u'Servicios por Terceros'), ('category_id', '=', cat_cunamas.id)]
        )
        especialista_nutricion = self.env['res.groups'].sudo().search(
            [('name', '=', u'Especialista en Nutrición'), ('category_id', '=', cat_cunamas.id)]
        )
        gestion_comunal = self.env['res.groups'].sudo().search(
            [('name', '=', u'Gestión Comunal'), ('category_id', '=', cat_cunamas.id)]
        )
        servicio_saf = self.env['res.groups'].sudo().search([('name', '=', 'SAF')])
        servicio_scd = self.env['res.groups'].sudo().search([('name', '=', 'SCD')])

        context = {'ir.ui.menu.full_list': True}
        cunamas_groups = [
            jefe_ut.id, coordinador.id, asistente_adm.id, tec_informatico.id, especialista_integrales.id,
            acompaniante_tecnico.id, servicios_terceros.id, especialista_nutricion.id, gestion_comunal.id
        ]

        # ACCESO A LOS 9 GRUPOS CUNAMAS == ADMIN
        if not groups:
            groups = self.env.user.groups_id
        new = [c for c in groups.ids if c in cunamas_groups]

        # VERIFICAR POR GRUPO LOS MENUS A MOSTRAR DEPENDIENDO DEL SERVICIO
        if not len(new) == 9:
            # Para Jefe de Unidad Territorial
            if jefe_ut in groups:
                if servicio_saf in groups and not servicio_scd in groups:
                    infra = self.with_context(context).sudo().search([('name', '=', 'INFRAESTRUCTURA')])
                    r_afi = self.with_context(context).sudo().search(
                        [('name', '=', u'Registros de Afiliación a la organización')])
                    ate_integral_scd = self.with_context(context).sudo().search([('name', '=', u'SCD')])
                    menus = menus - infra - r_afi - ate_integral_scd

                elif not servicio_saf in groups and servicio_scd in groups:
                    ate_integral_saf = self.with_context(context).sudo().search([('name', '=', u'SAF')])
                    menus -= ate_integral_saf

            # Para Coordinador
            elif coordinador in groups:
                reg = self.with_context(context).sudo().search([('name', '=', 'REGISTROS')])
                conf = self.with_context(context).sudo().search(
                    [('name', '=', 'CONFIGURACIÓN'), ('parent_id', '=', reg.id)])
                ut_menu = self.with_context(context).sudo().search([('name', '=', 'Unidad Territorial')])
                if conf in menus:
                    menus -= conf
                if ut_menu in menus:
                    menus -= ut_menu

                if servicio_saf in groups and not servicio_scd in groups:
                    infra = self.with_context(context).sudo().search([('name', '=', 'INFRAESTRUCTURA')])
                    r_afi = self.with_context(context).sudo().search(
                        [('name', '=', u'Registros de Afiliación a la organización')])
                    ate_integral_scd = self.with_context(context).sudo().search([('name', '=', u'SCD')])
                    menus = menus - infra - r_afi - ate_integral_scd
                elif not servicio_saf in groups and servicio_scd in groups:
                    ate_integral_saf = self.with_context(context).sudo().search([('name', '=', u'SAF')])
                    menus = menus - ate_integral_saf

            # Para Administrador/ Asistente Administrativo
            elif asistente_adm in groups:
                reg = self.with_context(context).sudo().search([('name', '=', 'REGISTROS')])
                conf = self.with_context(context).sudo().search(
                    [('name', '=', 'CONFIGURACIÓN'), ('parent_id', '=', reg.id)])

                ut_menu = self.with_context(context).sudo().search([('name', '=', 'Unidad Territorial')])
                reportes_menu = self.with_context(context).sudo().search([('name', '=', 'REPORTES')])

                esc_subv = self.with_context(context).sudo().search([('name', '=', u'Escala Subvención')])
                nive_esc = self.with_context(context).sudo().search([('name', '=', u'Nivel Escala Subvención')])
                trans_menu = self.with_context(context).sudo().search([('name', '=', 'TRANSFERENCIAS')])
                trans_conf = self.with_context(context).sudo().search(
                    [('name', '=', 'CONFIGURACIÓN'), ('parent_id', '=', trans_menu.id)]
                )
                if esc_subv in menus:
                    menus -= esc_subv
                if nive_esc in menus:
                    menus -= nive_esc
                if trans_conf in menus:
                    menus -= trans_conf
                if conf in menus:
                    menus -= conf
                if ut_menu in menus:
                    menus -= ut_menu
                if reportes_menu in menus:
                    menus -= reportes_menu

            # Para Técnico Informático
            elif tec_informatico in groups:
                ut_menu = self.with_context(context).sudo().search([('name', '=', 'Unidad Territorial')])
                reg = self.with_context(context).sudo().search([('name', '=', 'REGISTROS')])
                conf = self.with_context(context).sudo().search(
                    [('name', '=', 'CONFIGURACIÓN'), ('parent_id', '=', reg.id)])
                afi_menu = self.with_context(context).sudo().search([('name', '=', u'AFILIACIÓN')])
                conf_afi = self.with_context(context).sudo().search(
                    [('name', '=', 'CONFIGURACIÓN'), ('parent_id', '=', afi_menu.id)]
                )
                reportes_menu = self.with_context(context).sudo().search([('name', '=', 'REPORTES')])
                migracion = self.with_context(context).sudo().search([('name', '=', 'MIGRACIONES')])
                transfer_menu = self.with_context(context).sudo().search([('name', '=', u'TRANSFERENCIAS')])

                if ut_menu in menus:
                    menus -= ut_menu
                if conf in menus:
                    menus -= conf
                if conf_afi in menus:
                    menus -= conf_afi
                if reportes_menu in menus:
                    menus -= reportes_menu
                if migracion in menus:
                    menus -= migracion
                if transfer_menu in menus:
                    menus -= transfer_menu

                if servicio_saf in groups and not servicio_scd in groups:
                    infra = self.with_context(context).sudo().search([('name', '=', 'INFRAESTRUCTURA')])
                    afi_ninio = self.with_context(context).sudo().search([('name', '=', 'Afiliaciones del Niño SCD')])
                    r_afi = self.with_context(context).sudo().search(
                        [('name', '=', u'Registros de Afiliación a la organización')])
                    afi_scd = self.with_context(context).sudo().search(
                        [('name', '=', u'Afiliacion a la organizacion SCD')])
                    ate_integral_scd = self.with_context(context).sudo().search([('name', '=', u'SCD')])
                    menus = menus - infra - ate_integral_scd - r_afi - afi_scd - afi_ninio

                elif not servicio_saf in groups and servicio_scd in groups:
                    ate_integral_saf = self.with_context(context).sudo().search([('name', '=', u'SAF')])
                    menus -= ate_integral_saf

            # Para Especialista Integrales
            elif especialista_integrales in groups:
                ut_menu = self.with_context(context).sudo().search([('name', '=', 'Unidad Territorial')])
                reg = self.with_context(context).sudo().search([('name', '=', 'REGISTROS')])
                conf = self.with_context(context).sudo().search(
                    [('name', '=', 'CONFIGURACIÓN'), ('parent_id', '=', reg.id)])
                afi_menu = self.with_context(context).sudo().search([('name', '=', u'AFILIACIÓN')])
                conf_afi = self.with_context(context).sudo().search(
                    [('name', '=', 'CONFIGURACIÓN'), ('parent_id', '=', afi_menu.id)]
                )
                reportes_menu = self.with_context(context).sudo().search([('name', '=', 'REPORTES')])
                migracion = self.with_context(context).sudo().search([('name', '=', 'MIGRACIONES')])
                ate_ = self.with_context(context).sudo().search([('name', '=', 'ATENCIÓN INTEGRAL')])
                ate_conf = self.with_context(context).sudo().search(
                    [('name', '=', 'CONFIGURACIÓN'), ('parent_id', '=', ate_.id)])
                oms = self.with_context(context).sudo().search([('name', '=', 'TABLA OMS')])

                if ut_menu in menus:
                    menus -= ut_menu
                if conf in menus:
                    menus -= conf
                if conf_afi in menus:
                    menus -= conf_afi
                if reportes_menu in menus:
                    menus -= reportes_menu
                if migracion in menus:
                    menus -= migracion
                if ate_conf in menus:
                    menus -= ate_conf
                if oms in menus:
                    menus -= oms

                if servicio_saf in groups and not servicio_scd in groups:
                    infra = self.with_context(context).sudo().search([('name', '=', 'INFRAESTRUCTURA')])
                    afi_ninio = self.with_context(context).sudo().search([('name', '=', 'Afiliaciones del Niño SCD')])
                    r_afi = self.with_context(context).sudo().search(
                        [('name', '=', u'Registros de Afiliación a la organización')])
                    afi_scd = self.with_context(context).sudo().search(
                        [('name', '=', u'Afiliacion a la organizacion SCD')])
                    ate_integral_scd = self.with_context(context).sudo().search([('name', '=', u'SCD')])
                    menus = menus - infra - ate_integral_scd - r_afi - afi_scd - afi_ninio

                elif not servicio_saf in groups and servicio_scd in groups:
                    ate_integral_saf = self.with_context(context).sudo().search([('name', '=', u'SAF')])
                    menus -= ate_integral_saf

            # Para Acompañante Tecnico
            elif acompaniante_tecnico in groups:
                ut_menu = self.with_context(context).sudo().search([('name', '=', 'Unidad Territorial')])
                reg = self.with_context(context).sudo().search([('name', '=', 'REGISTROS')])
                conf = self.with_context(context).sudo().search(
                    [('name', '=', 'CONFIGURACIÓN'), ('parent_id', '=', reg.id)])

                afi_menu = self.with_context(context).sudo().search([('name', '=', u'AFILIACIÓN')])
                conf_afi = self.with_context(context).sudo().search(
                    [('name', '=', 'CONFIGURACIÓN'), ('parent_id', '=', afi_menu.id)]
                )
                reportes_menu = self.with_context(context).sudo().search([('name', '=', 'REPORTES')])
                migracion = self.with_context(context).sudo().search([('name', '=', 'MIGRACIONES')])
                ate_ = self.with_context(context).sudo().search([('name', '=', 'ATENCIÓN INTEGRAL')])
                ate_conf = self.with_context(context).sudo().search(
                    [('name', '=', 'CONFIGURACIÓN'), ('parent_id', '=', ate_.id)])
                oms = self.with_context(context).sudo().search([('name', '=', 'TABLA OMS')])

                if ut_menu in menus:
                    menus -= ut_menu
                if conf in menus:
                    menus -= conf
                if conf_afi in menus:
                    menus -= conf_afi
                if reportes_menu in menus:
                    menus -= reportes_menu
                if migracion in menus:
                    menus -= migracion
                if ate_conf in menus:
                    menus -= ate_conf
                if oms in menus:
                    menus -= oms

                if servicio_saf in groups and not servicio_scd in groups:
                    infra = self.with_context(context).sudo().search([('name', '=', 'INFRAESTRUCTURA')])
                    afi_ninio = self.with_context(context).sudo().search([('name', '=', 'Afiliaciones del Niño SCD')])
                    r_afi = self.with_context(context).sudo().search(
                        [('name', '=', u'Registros de Afiliación a la organización')])
                    afi_scd = self.with_context(context).sudo().search(
                        [('name', '=', u'Afiliacion a la organizacion SCD')])
                    ate_integral_scd = self.with_context(context).sudo().search([('name', '=', u'SCD')])
                    menus = menus - infra - ate_integral_scd - r_afi - afi_scd - afi_ninio
                elif not servicio_saf in groups and servicio_scd in groups:
                    ate_integral_saf = self.with_context(context).sudo().search([('name', '=', u'SAF')])
                    menus -= ate_integral_saf - reg

            # Para Especialista Nutricion
            elif especialista_nutricion in groups:
                reg = self.with_context(context).sudo().search([('name', '=', 'REGISTROS')])
                if reg in menus:
                    menus -= reg

            # Para Servicio de Terceros
            elif servicios_terceros in groups:
                reg = self.with_context(context).sudo().search([('name', '=', 'REGISTROS')])
                ut_menu = self.with_context(context).sudo().search([('name', '=', 'Unidad Territorial')])
                conf = self.with_context(context).sudo().search(
                    [('name', '=', 'CONFIGURACIÓN'), ('parent_id', '=', reg.id)])
                reportes_menu = self.with_context(context).sudo().search([('name', '=', 'REPORTES')])
                vivienda = self.with_context(context).sudo().search([('name', '=', 'Viviendas')])
                familia = self.with_context(context).sudo().search([('name', '=', 'Unidad Familiar')])
                if ut_menu in menus:
                    menus -= ut_menu
                if conf in menus:
                    menus -= conf
                if reportes_menu in menus:
                    menus -= reportes_menu
                if not vivienda in menus:
                    menus += vivienda
                if not familia in menus:
                    menus += familia
                if servicio_saf in groups and not servicio_scd in groups:
                    infra = self.with_context(context).sudo().search([('name', '=', 'INFRAESTRUCTURA')])
                    menus = menus - infra
        return menus


class ResUsers(models.Model):

    _inherit = 'res.users'

    unidad_territorial_id = fields.Many2many(
        comodel_name='hr.department',
        string='Unidad Territorial',
        domain="[('is_office','=',True), ('estado','=', 'activo')]",
    )
    servicio = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento a Familias'),
    ],
        string='Servicio'
    )
    rol_admin = fields.Boolean(
        string='Vista general'
    )
    cunanet_id = fields.Integer()
    sisaf_id = fields.Integer()

    # SE CREARON 2 GRUPOS ADICIONALES INVISIBLES PARA C/U DE LOS SERVICIOS
    # ESTOS AYUDARA A OCULTAR LOS MENUS QUE EN CIERTOS SERVICIOS NO SE DEBERIAN OBSERVAR
    def verificar_permisos_servicios(self, rec):
        self.eliminar_permisos_servicios(rec)
        if rec.servicio and rec.servicio == 'saf':
            group_id = self.env['ir.model.data'].get_object('base_cuna', 'serv_saf')
            group_id.sudo().write({'users': [(4, rec.id)]})
        elif rec.servicio and rec.servicio == 'scd':
            group_id = self.env['ir.model.data'].get_object('base_cuna', 'serv_scd')
            group_id.sudo().write({'users': [(4, rec.id)]})
        else:
            group_id = self.env['ir.model.data'].get_object('base_cuna', 'serv_saf')
            group_id.sudo().write({'users': [(4, rec.id)]})
            group_id = self.env['ir.model.data'].get_object('base_cuna', 'serv_scd')
            group_id.sudo().write({'users': [(4, rec.id)]})

    def eliminar_permisos_servicios(self, rec):
        group_id = self.env['ir.model.data'].get_object('base_cuna', 'serv_saf')
        group_id.sudo().write({'users': [(3, rec.id)]})
        group_id = self.env['ir.model.data'].get_object('base_cuna', 'serv_scd')
        group_id.sudo().write({'users': [(3, rec.id)]})

    @api.model
    def create(self, values):
        r = super(ResUsers, self).create(values)
        self.verificar_permisos_servicios(r)
        self.env['ir.ui.menu'].sudo()._filter_visible_menus()
        return r

    @api.multi
    def write(self, values):
        r = super(ResUsers, self).write(values)
        self.verificar_permisos_servicios(self)
        self.env['ir.ui.menu'].sudo()._filter_visible_menus()
        return r


class ResGroup(models.Model):

    _inherit = 'res.groups'

    # Hace invisible los grupos SCD Y SAF los cuales te permiten restringir acceso a las vistas
    @api.model
    def _update_user_groups_view(self):
        """ Modify the view with xmlid ``base.user_groups_view``, which inherits
            the user form view, and introduces the reified group fields.
        """
        if self._context.get('install_mode'):
            # use installation/admin language for translatable names in the view
            user_context = self.env['res.users'].context_get()
            self = self.with_context(**user_context)

        # We have to try-catch this, because at first init the view does not
        # exist but we are already creating some basic groups.
        view = self.env.ref('base.user_groups_view', raise_if_not_found=False)
        if view and view.exists() and view._name == 'ir.ui.view':
            group_no_one = view.env.ref('base.group_no_one')
            xml1, xml2 = [], []
            xml1.append(E.separator(string=_('Application'), colspan="2"))
            for app, kind, gs in self.get_groups_by_application():
                # hide groups in categories 'Hidden' and 'Extra' (except for group_no_one)
                attrs = {}
                if app.xml_id in (
                'base.module_category_hidden', 'base.module_category_extra', 'base.module_category_usability'):
                    attrs['groups'] = 'base.group_no_one'

                saf_group = view.env.ref('base_cuna.serv_saf')
                scd_group = view.env.ref('base_cuna.serv_scd')
                if kind == 'selection':
                    # application name with a selection field
                    field_name = name_selection_groups(gs.ids)
                    if gs == saf_group or gs == scd_group:
                        xml1.append(E.field(name=field_name, invisible="1", **attrs))
                    else:
                        xml1.append(E.field(name=field_name, **attrs))

                    xml1.append(E.newline())
                else:
                    # application separator with boolean fields
                    app_name = app.name or _('Other')
                    xml2.append(E.separator(string=app_name, colspan="4", **attrs))
                    for g in gs:
                        field_name = name_boolean_group(g.id)

                        if g == group_no_one or g == scd_group or g == saf_group:
                            # make the group_no_one invisible in the form view
                            xml2.append(E.field(name=field_name, invisible="1", **attrs))
                        else:
                            xml2.append(E.field(name=field_name, **attrs))

            xml2.append({'class': "o_label_nowrap"})
            xml = E.field(E.group(*(xml1), col="2"), E.group(*(xml2), col="4"), name="groups_id", position="replace")
            xml.addprevious(etree.Comment("GENERATED AUTOMATICALLY BY GROUPS"))
            xml_content = etree.tostring(xml, pretty_print=True, xml_declaration=True, encoding="utf-8")
            view.with_context(lang=None).write({'arch': xml_content, 'arch_fs': False})


class DiasComite(models.Model):

    _name = 'dias.comite'

    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        string=u'Comite Gestión',
        required=True
    )
    dias_utiles_especial = fields.Integer(
        string=u'Días Utiles Especial',
        default=30,
        required=True
    )
    meses_dias_id = fields.Many2one(
        comodel_name='meses.dias',
        inverse_name='comite_gestion_ids'
    )


class MesesDias(models.Model):

    _name = 'meses.dias'
    _inherit = 'tabla.base'

    month = fields.Selection(
        selection=months,
        required=True,
        string='Mes'
    )
    dias_utiles = fields.Integer(
        string=u'Días Utiles',
        required=True,
        default=30
    )
    comite_gestion_ids = fields.One2many(
        comodel_name='dias.comite',
        inverse_name='meses_dias_id',
        string=u'Comite Gestión',
    )

    @api.multi
    def create_record(self):
        self.ensure_one()

    @api.multi
    def agregar_cg(self):
        form_id = self.env.ref('base_cuna.form_view_meses_dias_create').id
        return {
            'type': 'ir.actions.act_window',
            'name': 'Dias Especiales',
            'view_type': 'form',
            'view_mode': 'form',
            'res_model': 'meses.dias',
            'views': [(form_id, 'form')],
            'view_id': form_id,
            'target': 'new',
            'res_id': self.id
        }


class Anios(models.Model):
    _name = 'tabla.anios'
    _inherit = 'tabla.base'

    name = fields.Char(
        string='Nombre'
    )
    month_days_ids = fields.Many2many(
        comodel_name='meses.dias',
        default=lambda self: self._default_deposit_lines_ids(),
        string='Meses'
    )

    @api.model
    def _default_deposit_lines_ids(self):
        result = [
            dict(month=value) for value in
            ['01', '02', '03', '04', '05', '06', '07', '08', '09', '10', '11', '12']
        ]
        return result

    @api.model
    def get_actual_year(self):
        obj_year = self.search([('name', '=', str(datetime.now().year))])
        if not obj_year:
            self.create({'name': str(datetime.now().year)})

    @api.constrains('month_days_ids')
    def _constraints_month_days_ids(self):
        if len(self.month_days_ids) != 12:
            raise ValidationError('Solo debe existir 12 registros, uno por cada mes')


class TipoSeguro(models.Model):
    _name = 'tipo.seguro'
    _inherit = 'tabla.base'


class Discapacidad(models.Model):
    _name = 'tipo.discapacidad'
    _inherit = 'tabla.base'

    abrev = fields.Char()


class Parentesco(models.Model):
    _name = 'tipo.parentesco'
    _inherit = 'tabla.base'


class ProgramaSocial(models.Model):
    _name = 'programa.social'
    _inherit = 'tabla.base'


class Religion(models.Model):
    _name = 'tipo.religion'
    _inherit = 'tabla.base'


class NivelEducativo(models.Model):
    _name = 'nivel.educativo'
    _inherit = 'tabla.base'


class UltimoGradoAprobado(models.Model):
    _name = 'ultimo.grado.aprobado'
    _inherit = 'tabla.base'

    secuencia = fields.Char()
    # para migracion


class TipoOcupacion(models.Model):
    _name = 'tipo.ocupacion'
    _inherit = 'tabla.base'


class LenguaMaterna(models.Model):
    _name = 'lengua.materna'
    _inherit = 'tabla.base'


class TipoPersona(models.Model):
    _name = 'tipo.persona'
    _inherit = 'tabla.base'

    afiliacion = fields.Boolean(
        string='Filtrar en afiliacion?'
    )
    escala_subvencion = fields.Boolean(
        string='Filtrar en escala de subvencion?'
    )
    justificacion = fields.Boolean(
        string='Filtrar en el proceso de justificacion?'
    )
    abrev = fields.Char(
        string='Abreviatura'
    )
    servicio = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento a Familias'),
        ('ambos', u'Ambos'),
    ])

    @api.multi
    def name_get(self):
        names = []
        for record in self:
            if record.name == 'APOYO ADMINISTRATIVO DEL COMITE DE GESTION':
                name = u'{} [{}]'.format(record.name, record.servicio.upper())
            else:
                name = u'{}'.format(record.name)
            names.append((record.id, name))
        return names
