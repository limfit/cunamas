##############################################################################
# For copyright and license notices, see __manifest__.py file in module root
# directory
##############################################################################

import logging
from odoo import http
from odoo.addons.survey.controllers.main import WebsiteSurvey
from odoo.http import request


_logger = logging.getLogger(__name__)


class SurveyConditional(WebsiteSurvey):

    # TODO deberiamos heredar esto correctamente
    @http.route()
    def fill_survey(self, survey, token, prev=None, **post):
        '''Display and validates a survey'''
        cr, uid, context = request.cr, request.uid, request.context
        survey_obj = request.env['survey.survey']
        user_input_obj = request.env['survey.user_input']


        # Controls if the survey can be displayed
        errpage = self._check_bad_cases(survey)
        if errpage:
            return errpage

        if not token:
            vals = {'survey_id': survey.id}
            if request.website.user_id != request.env.user:
                vals['partner_id'] = request.env.user.partner_id.id
            user_input = user_input_obj.create(vals)
        else:
            user_input = user_input_obj.sudo().search([('token', '=', token)], limit=1)
            if not user_input:
                return request.render("website.403")

        # Do not open expired survey
        errpage = self._check_deadline(user_input)
        if errpage:
            return errpage

        # Select the right page
        if user_input.state == 'new':  # First page
            page, page_nr, last = survey_obj.next_page(user_input, 0, go_back=False)
            data = {'survey': survey, 'page': page,
                    'page_nr': page_nr, 'token':  token}
            data['hide_question_ids'] = user_input_obj.get_list_questions(
                cr, uid, survey, user_input)
            if last:
                data.update({'last': True})
            return request.render('survey.survey', data)
        elif user_input.state == 'done':  # Display success message
            return request.render('survey.sfinished',
                                          {'survey': survey,
                                           'token': token,
                                           'user_input': user_input})
        elif user_input.state == 'skip':
            flag = (True if prev and prev == 'prev' else False)
            page, page_nr, last = survey_obj.next_page(
                user_input,
                user_input.last_displayed_page_id.id,
                go_back=flag
            )

            # special case if you click "previous" from the last page,
            # then  leave the survey, then reopen it from the URL,
            #  avoid crash
            if not page:
                page, page_nr, last = survey_obj.next_page(
                    user_input,
                    user_input.last_displayed_page_id.id,
                    go_back=True
                )

            data = {'survey': survey, 'page': page,
                    'page_nr': page_nr, 'token': user_input.token}
            if last:
                data.update({'last': True})
            data['hide_question_ids'] = user_input_obj.get_list_questions(
                cr, uid, survey, user_input)
            return request.render('survey.survey', data)
        else:
            return request.render("website.403")
