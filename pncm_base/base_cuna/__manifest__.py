# -*- coding: utf-8 -*-

{
    "name": "Modulo Base para Cunamas ",
    "version": "1.0",
    "author": "Ganemo",
    "website": "http://www.ganemocorp.com/",
    "description": """
        - Tablas base(años, tipo persona,parentesco, lengua materna, ocupacion, nivel educativo, etc )
        - Agrega campo de ut y tipo de servicio al usuario logeado
        - Agrega relacion a local:
	        . Muestra Servicios Alimentarios.
	        . Ambientes -> Sala -> Modulos.
        - Agrega relacion a comite gestion :
	        . Convenios -> resoluciones y adendas.
	        . Muestra facilitadores.
	        . Muestra acompañante tecnico.
	        . Muestra guias. 
        - Agrega relacion de Familia con vivienda.
        - Agrega migracion de familia.
        - Agrega afiliacion de niños y a la organización.
    """,
    "depends": [
        'report_xlsx',
        'res_partner_base_pe',
        'hr_department_ut',
        'hr_positions',
        'mail',
        'survey',
        'website',
        'pncm_comite_gestion'
    ],
    "data": [
        'data/programa.social.csv',
        'data/tabla.anios.csv',
        'data/tipo.discapacidad.csv',
        'data/tipo.documento.csv',
        'data/tipo.parentesco.csv',
        'data/tipo.persona.csv',
        'security/security.xml',
        'security/ir.model.access.csv',
        'views/email_template.xml',
        'views/crons.xml',
        'views/forms.xml',
        'views/graphs.xml',
        'views/trees.xml',
        'views/actions.xml',
        'views/menus.xml',
        'views/reports.xml',
        'views/pivots.xml',
        'views/survey_qweb.xml'
    ],
    'qweb': [
        'static/src/xml/*.xml',
    ],
    'installable': True,
    'active': False
}
