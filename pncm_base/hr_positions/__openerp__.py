# -*- coding: utf-8 -*-
{
    "name": "Repositorio para la Plataforma Base Cunamas",
    "version": "1.0",
    "author": "CUNAMAS",
    "website": "http://www.cunamas.gob.pe/",
    "category": "Localization",
    "description": """
    Puestos de Trabajo
    """,
    "depends": [
        "base",
        "web",
        'hr'
    ],
    "init_xml": [],
    "data": [
        'views/hr_positions_view.xml'
    ],
    "demo_xml": [],
    'installable': True,
    'active': False
}
