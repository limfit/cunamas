# -*- coding: utf-8 -*-

{
    "name": "Repositorio para la Plataforma Base Cunmas",
    "version": "1.0",
    "author": "CUNAMAS",
    "website": "http://www.cunamas.gob.pe/",
    "category": "Localization",
    "description": """
    Crea todos el catagolo principal para la plataforma base cunamas
    """,
    "depends": [
        "base",
        "web",
        'hr'
    ],
    "init_xml": [],
    "data": [
        'views/comitegestion.xml',
        'views/empleado_cunamas.xml'
    ],
    "demo_xml": [],
    'installable': True,
    'active': False
}
