# -*- coding: utf-8 -*-
from openerp import fields, models, api
from openerp.exceptions import ValidationError
import utils
from datetime import datetime
from openerp.osv import osv

_select_cargo = [
    (12, 'CUIDADORA'),
    (13, 'GUIA'),
    (14, 'SOCIO DE COCINA'),
    (15, 'PRESIDENTE DE COMITE DE GESTION'),
    (16, 'CONSEJO DE VIGILANCIA'),
    (17, 'APOYO ADMINISTRATIVO DEL COMITE DE GESTION'),
    (18, 'PADRES DE FAMILIA'),
    (19, 'REPARTIDOR'),
    (20, 'TESORERO DE COMITE DE GESTION'),
    (21, 'SECRETARIO DE COMITE DE GESTION'),
    (22, 'PRIMER VOCAL'),
    (23, 'PRIMER VOCAL'),
    (24, 'TERCER VOCAL'),
    (25, 'FACILITADOR'),
    (26, 'GUIA FAMILIAR')
]


class CMComiteGestion(models.Model):
    _name = 'pncm.comitegestion'
    _prefijo_codigo = 'CG'
    _completar_con_zeros = '04'

    codigo = fields.Char(
        string=u'Código',
        required=True,
        readonly=True
    )
    name = fields.Char(
        string=u'Nombre CG',
        required=True,
        size=250
    )
    servicio = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento Familiar'),
        ('ambos', u'Ambos'),
    ])
    unidad_territorial_id = fields.Many2one(
        'hr.department',
        u'Unidad Territorial',
        domain="[('is_office','=',True)]",
        required=True
    )
    fecha_inicio = fields.Date(string=u'Fecha de Inicio')
    fecha_cierre = fields.Date(string=u'Fecha de Cierre')
    tipo_comite = fields.Selection([
        ('23', u'Comité Rural'),
        ('24', u'Urbano Disperso'),
        ('25', u'Comité Urbano')
        ],
        string=u'Tipo de Comité',
        required=True
    )
    meta_actual = fields.Char(
        string=u'Meta Actual',
    )
    pais_id = fields.Many2one(
        'res.country',
        'País',
        default=lambda self: self.env.user.company_id.country_id
    )
    departamento_id = fields.Many2one(
        'res.country.state', 'Departamento',
        domain="[('country_id', '=', pais_id), "
               "('state_id', '=', False), "
               "('province_id', '=', False)]"
    )
    provincia_id = fields.Many2one(
        'res.country.state', 'Provincia',
        domain="[('state_id', '=', departamento_id), "
               "('province_id', '=', False)]"
    )
    distrito_id = fields.Many2one(
        'res.country.state', 'Distrito',
        domain="[('state_id', '=', departamento_id), "
               "('province_id', '=', provincia_id)]"
    )
    centro_poblado_id = fields.Many2one(
        'res.country.state',
        u'Centro Poblado',

        domain="[('state_id', '=', departamento_id), "
               "('province_id', '=', provincia_id), "
               "('district_id', '=', distrito_id)]"
    )
    active = fields.Boolean(string=u'Activo', default=True)
    direccion = fields.Char(
        string=u'Dirección',
        required=True, size=255
    )
    referencia = fields.Text(
        string=u'Referencia'
    )
    presidente_actual = fields.Char(
        string=u'Presidente',
        default=lambda self: self._get_presidente_actual(),
        store=False
    )
    tesorero_actual = fields.Char(
        string=u'Tesorero',
        default=lambda self: self._get_tesorero_actual(),
        store=False
    )
    secretario_actual = fields.Char(
        string=u'Secretario',
        default=lambda self: self._get_secretario_actual(),
        store=False
    )
    primer_vocal_actual = fields.Char(
        string=u'Primer Vocal',
        default=lambda self: self._get_primer_vocal_actual(),
        store=False
    )
    segundo_vocal_actual = fields.Char(
        string=u'Segundo Vocal',
        default=lambda self: self._get_segundo_vocal_actual(),
        store=False
    )
    intervenciones = fields.One2many(
        'pncm.comitegestion.intervencion',
        'comite_gestion_id',
        u'Intervención',
        required=True
    )
    actores_comunales = fields.One2many(
        'pncm.comitegestion.actorcomunal',
        'comite_gestion_id',
        u'Actores Comunales',
        required=True
    )
    state = fields.Selection(selection=[
        ('constitucion', u'En Formacion'),
        ('activo', 'Activo'),
        ('cerrado', 'Cerrado'),
    ])
    resolucion = fields.Char(string=u'Resolución Activa')
    miembros_equipo = fields.One2many('pncm.comite.equipo.tecnico',
                                      'comite_gestion_id', u'Equipo Tecnico')
    resoluciones = fields.One2many('pncm.comite.resoluciones.convenios',
                                   'comite_gestion_id', u'Resoluciones')
    metas = fields.One2many('pncm.comite.metas.historicas',
                            'comite_gestion_id', u'Metas')
    _sql_constraints = [
        ('code_unique', 'unique(codigo)', u'El código generado ya existe')
    ]

    @api.model
    def create(self, vals):
        obj = self.env['res.country.state'] \
            .search([('id', '=', vals['distrito_id'])])
        vals['ubigeo'] = obj.code
        if vals.get('unidad_territorial_id'):
            vals['codigo'] = \
                self._generar_codigo_comite(
                    vals['unidad_territorial_id'], vals['servicio'])
        else:
            vals['codigo'] = \
                self._generar_codigo_comite(
                    vals['unidad_territorial_id_log'], vals['servicio'])
        return super(CMComiteGestion, self).create(vals)

    @api.onchange('fecha_inicio')
    def _validar_fecha_inicio(self):
        dh = utils.DateHelper()
        dh._validar_fechas_futuras(self.fecha_inicio)
        dh._validar_rango_fechas(self.fecha_inicio, self.fecha_cierre)

    @api.onchange('fecha_cierre')
    def _validar_fecha_cierre(self):
        dh = utils.DateHelper()
        dh._validar_fechas_futuras(self.fecha_cierre)
        dh._validar_rango_fechas(self.fecha_inicio, self.fecha_cierre)

    @api.onchange('servicio')
    def _obtener_ut_por_servicio(self):
        if self.servicio:
            self.unidad_territorial_id = False
            return {'domain': {
                'unidad_territorial_id':
                    ['|', ('services', '=', self.servicio),
                     ('services', '=', 'ambos')]}}

    @api.onchange('unidad_territorial_id')
    def _al_cambiar_ut(self):
        self._mostrar_codigo_generado()
        self.departamento_id = False
        return self._obtener_ambito_departamento_ut()

    def _obtener_ambito_departamento_ut(self):
        if self.unidad_territorial_id:
            intervenciones_ut = self.env['pncm.ambito.intervencion']\
                .search([('ut_id', '=', self.unidad_territorial_id.id)])
            departamentos_ids = \
                [obj.departamento_id.id for obj in intervenciones_ut]
            return {'domain': {'departamento_id': ['&', ('country_id.code',
                                                   '=', 'PE'),
                                                   ('id', 'in',
                                                    departamentos_ids)]}}

    @api.onchange('departamento_id')
    def _al_cambiar_departamento(self):
        if self.departamento_id:
            intervenciones_ut = self.env['pncm.ambito.intervencion']\
                .search([('ut_id', '=', self.unidad_territorial_id.id)])
            provincia_ids = [obj.provincia_id.id for obj in intervenciones_ut]
            return {'domain': {'provincia_id': [('id', 'in', provincia_ids)]}}

    @api.onchange('provincia_id')
    def _al_cambiar_provincia(self):
        if self.provincia_id:
            intervenciones_ut = self.env['pncm.ambito.intervencion']\
                .search([('ut_id', '=', self.unidad_territorial_id.id)])
            desitritos_ids = [obj.distrito_id.id for obj in intervenciones_ut]
            return {'domain': {'distrito_id': [('id', 'in', desitritos_ids)]}}

    @api.onchange('intervenciones')
    def _validar_intervenciones(self):
        dh = utils.DateHelper()
        if self.intervenciones:
            for obj in self.intervenciones:
                dh._validar_fechas_futuras(obj.fecha_inicio)
                dh._validar_fechas_futuras(obj.fecha_fin)
                dh._validar_rango_fechas(obj.fecha_inicio, obj.fecha_fin)

    @api.onchange('actores_comunales')
    def _validar_actores_comunales(self):
        dh = utils.DateHelper()
        dnih = utils.DNIHelper()
        if self.actores_comunales:
            for obj in self.actores_comunales:
                dnih._validar_peruvian_dni(obj.dni)
                dh._validar_fechas_futuras(obj.fecha_inicio)
                dh._validar_fechas_futuras(obj.fecha_fin)
                dh._validar_rango_fechas(obj.fecha_inicio, obj.fecha_fin)

    @api.onchange('resoluciones')
    def _validar_resoluciones(self):
        dh = utils.DateHelper()
        sh = utils.StringHelper()
        if self.resoluciones:
            for obj in self.resoluciones:
                dh._validar_fechas_futuras(obj.fecha_inicio_resolucion)
                if sh._is_white_space(obj.resolucion):
                    raise ValidationError('Ingrese número de resolución')
                if sh._is_white_space(obj.motivo_resolucion):
                    raise ValidationError('Ingrese motivo')

    def _mostrar_codigo_generado(self):
        if self.unidad_territorial_id:
            self.codigo = self._generar_codigo_comite(
                self.unidad_territorial_id.id, self.servicio)
        else:
            self.codigo = False

    def _generar_codigo_comite(self, ut_id, servicio):
        unidad_territorial = self.env['hr.department']\
            .search([('id', '=', ut_id)])
        total_comites = self\
            .search_count([('unidad_territorial_id', '=', ut_id),
                           ('servicio', '=', servicio)])+1
        prefijo_servicio = servicio.upper() if servicio != 'ambos' else 'AMB'
        state = self.env['res.country.state'] \
            .search([('id', '=', unidad_territorial.departamento_id.id)],
                    limit=1)

        if not state.code_pe:
            raise osv.except_osv('Advertencia',
                                 'El departamento seleccionado no dispone de'
                                 ' un Acronimo, contacte con soporte.')
            codigo_generado = ''
        else:
            codigo_generado = '-'.join(
                [self._prefijo_codigo, state.code_pe, prefijo_servicio,
                 format(total_comites, self._completar_con_zeros)])
        return codigo_generado

    def _get_presidente_actual(self):
        if self.id:
            presidente = self.env['pncm.comitegestion.actorcomunal'].search(
                [('comite_gestion_id', '=', self.id),
                 ('cargo_actor_comunal_id', '=', 15),
                 ('estado', '=', True)], limit=1)
            return ' '.join(presidente.apellido_paterno,
                            presidente.apellido_materno,
                            presidente.nombres)
        return 'No Hay'

    @api.depends('actores_comunales')
    def _get_tesorero_actual(self):
        if self.id:
            tesorero = self.env['pncm.comitegestion.actorcomunal'].search(
                [('comite_gestion_id', '=', self.id),
                 ('cargo_actor_comunal_id', '=', 20),
                 ('estado', '=', True)], limit=1)
            return ' '.join(tesorero.apellido_paterno,
                            tesorero.apellido_materno,
                            tesorero.nombres)
        return ""

    def _get_secretario_actual(self):
        if self.id:
            secretario = self.env['pncm.comitegestion.actorcomunal'].search(
                [('comite_gestion_id', '=', self.id),
                 ('cargo_actor_comunal_id', '=', 21),
                 ('estado', '=', True)], limit=1)
            return ' '.join(secretario.apellido_paterno,
                            secretario.apellido_materno,
                            secretario.nombres)
        return ""

    def _get_primer_vocal_actual(self):
        if self.id:
            vocal1 = self.env['pncm.comitegestion.actorcomunal'].search(
                [('comite_gestion_id', '=', self.id),
                 ('cargo_actor_comunal_id', '=', 22),
                 ('estado', '=', True)], limit=1)
            return ' '.join(vocal1.apellido_paterno,
                            vocal1.apellido_materno,
                            vocal1.nombres)
        return ""

    def _get_segundo_vocal_actual(self):
        if self.id:
            vocal2 = self.env['pncm.comitegestion.actorcomunal'].search(
                [('comite_gestion_id', '=', self.id),
                 ('cargo_actor_comunal_id', '=', 23),
                 ('estado', '=', True)], limit=1)
            return ' '.join(vocal2.apellido_paterno,
                            vocal2.apellido_materno,
                            vocal2.nombres)
        return ""

    def _get_meta_actual(self):
        return "100"


class CMIntervencion(models.Model):
    _name = 'pncm.comitegestion.intervencion'

    comite_gestion_id = fields.Many2one(
        'pncm.comitegestion', u'Comite Gestión', required=True)
    ubigeo = fields.Char('Ubigeo')
    intervencion_ut_id = fields.Many2one(
        'pncm.ambito.intervencion', string=u'Intervencion Unidad Territorial',
        required=True
    )
    departamento_id = fields.Many2one(
        'res.country.state', u'Departamento',
        domain="[('country_id', '=', country_id), "
               "('state_id', '=', False), ('province_id', '=', False)]")
    provincia_id = fields.Many2one(
        'res.country.state', u'Provincia',
        domain="[('state_id', '=', departamento_id), "
               "('province_id', '=', False)]")
    distrito_id = fields.Many2one(
        'res.country.state', u'Distrito',
        domain="[('state_id', '=', departamento_id), "
               "('province_id', '=', provincia_id)]")
    centro_poblado_id = fields.Many2one(
        'res.country.state',
        u'Centro Poblado',
        domain="[('state_id', '=', departamento_id), "
               "('province_id', '=', provincia_id), "
               "('district_id', '=', distrito_id)]",
    )

    fecha_inicio = fields.Date(string=u'Fecha de Inicio', required=True)
    fecha_fin = fields.Date(string=u'Fecha de Fin', required=False)

    @api.onchange('intervencion_ut_id')
    def _update_list_centros_poblados(self):
        if self.intervencion_ut_id:
            departamento = self.intervencion_ut_id.departamento_id
            provincia = self.intervencion_ut_id.provincia_id
            distrito = self.intervencion_ut_id.distrito_id
            return {'value': {'departamento_id': departamento,
                              'provincia_id': provincia,
                              'distrito_id': distrito,
                              'centro_poblado_id': ''}}


class CMActorComunal(models.Model):
    _name = 'pncm.comitegestion.actorcomunal'

    comite_gestion_id = fields.Many2one('pncm.comitegestion',
                                        u'Comite Gestión', required=True)
    dni = fields.Char(string=u'DNI', required=True, size=8)
    apellido_paterno = fields.Char(string=u'Apellido Paterno',
                                   required=True, size=150)
    apellido_materno = fields.Char(string=u'Apellido Materno',
                                   required=True, size=150)
    nombres = fields.Char(string=u'Nombres', required=True, size=150)
    cargo_actor_comunal_id = fields.Selection(
        selection=_select_cargo,
        string=u'Cargo',
        required=True
    )
    fecha_inicio = fields.Date(string=u'Fecha de Inicio', required=True)
    fecha_fin = fields.Date(string=u'Fecha de Fin', required=False)
    estado = fields.Boolean(u'Estado')


class CMEquipoTecnico(models.Model):
    _name = 'pncm.comite.equipo.tecnico'

    comite_gestion_id = fields.Many2one('pncm.comitegestion',
                                        u'Comité de Gestión', required=True)

    empleado_cunamas_id = fields.Many2one(
        'hr.employee', u'Equipo Tecnico',
        required=True)

    nombres = fields.Char(u'Integrante', readonly=True)
    cargo = fields.Char(u'Cargo', readonly=True)
    fecha_inicio = fields.Date(u'Fecha Inicio', required=True)
    fecha_fin = fields.Date(u'Fecha Fin')
    status = fields.Char(u'Estado')
    estado = fields.Boolean(u'Estado')

    _sql_constraints = [
        ('code_unique', 'unique(empleado_cunamas_id,comite_gestion_id)',
         u'El miembro del equipo tecnico ya está asignado al comité')
    ]

    @api.onchange('empleado_cunamas_id')
    def _complete_fields(self):
        if self.empleado_cunamas_id:
            nombres = self.empleado_cunamas_id.name
            cargo = self.empleado_cunamas_id.job_id
            return {'value': {'nombres': nombres, 'cargo': cargo}}

    @api.model
    def create(self, vals):
        vals['nombres'] = self.nombres
        vals['cargo'] = self.cargo
        return super(CMEquipoTecnico, self).create(vals)


class CMResolucionesConvenios(models.Model):
    _name = 'pncm.comite.resoluciones.convenios'

    comite_gestion_id = fields.Many2one('pncm.comitegestion',
                                        u'Comité de Gestión',  required=True)
    resolucion = fields.Char(u'Resolución', required=True)
    convenio = fields.Char(u'Convenio', required=True)
    motivo_resolucion = fields.Selection(selection=[
        ('apertura', u'APERTURA'),
        ('cambio', u'CAMBIO DE JUNTA DIRECTIVA'),
        ('cierre', u'CIERRE'),
    ], required=True)
    fecha_inicio_resolucion = fields.Date(u'Fecha Inicio RDE', required=True)
    fecha_fin_resolucion = fields.Date(u'Fecha Fin RDE', required=True)


class CMMetasHistoricas(models.Model):
    _name = 'pncm.comite.metas.historicas'

    comite_gestion_id = fields.Many2one('pncm.comitegestion',
                                        u'Comité de Gestión', required=True)
    codigo = fields.Char(u'Código', required=True)
    meta_presupuestal = fields.Float(u'Meta Presupuestal', required=True)
    meta_fisica = fields.Integer(u'Meta Fisica', required=True)
    justificacion = fields.Char(u'Justificación')
    fecha_inicio = fields.Date(u'Fecha Inicio', required=True)
    fecha_fin = fields.Date(u'Fecha Fin', required=True)
    actualizar_meta = fields.Boolean(u'Actualizar Meta', default=True)
    estado = fields.Boolean(u'Estado', default=True)

    metas_programadas = fields.One2many('metas.programadas', 'meta_id')

    def _obtener_periodos(self, fecha_inicial, fecha_final):

        fecha_fin = datetime.strptime(fecha_final, '%Y-%m-%d')
        fecha_aux = datetime.strptime(fecha_inicial, '%Y-%m-%d')
        periodos = []
        while fecha_aux <= fecha_fin:
            periodo = str(fecha_aux.year) + '-' + str(fecha_aux.month)
            periodos.append(periodo)
            targetmonth = fecha_aux.month + 1
            targetyear = fecha_aux.year
            if targetmonth > 12:
                targetyear = targetyear + 1
                targetmonth = 1
            fecha_aux = fecha_aux.replace(targetyear, targetmonth, 1)
        return periodos

    @api.multi
    def update_registros(self):
        if self.comite_gestion_id.metas:
            lines = self.env['metas.programadas']. \
                search([('comite_gestion_id', '=', self.comite_gestion_id.id),
                        ('is_close', '=', False)])
            lines.write({
                'meta_id': self.id
            })

    @api.multi
    def create(self, vals):
        metas = []
        for meta_periodo in self._obtener_periodos(
                vals['fecha_inicio'],
                vals['fecha_fin']):
            metas.append((0, 0, {
                     'meta_id': self.id,
                     'meta_presupuestal_planeada': vals['meta_presupuestal'],
                     'meta_presupuestal_ejecutada': 0,
                     'comite_gestion_id': vals['comite_gestion_id'],
                     'meta_fisica_planeada': vals['meta_fisica'],
                     'meta_fisica_ejecutada': 0,
                     'periodo': meta_periodo,
                     'meta': vals['codigo'],

                 }))
        vals['metas_programadas'] = metas
        return super(CMMetasHistoricas, self).create(vals)


class CMMetasProgramadas(models.Model):
    _name = 'metas.programadas'

    meta_id = fields.Many2one('pncm.comite.metas.historicas', u'Meta')
    comite_gestion_id = fields.Many2one(
        'pncm.comitegestion',
        u'Comité de Gestión',
        related='meta_id.comite_gestion_id',
    )
    meta_presupuestal_planeada = fields.Float(u'Meta Presupuestal Planeada',
                                              required=True)
    meta_presupuestal_ejecutada = fields.Float(u'Meta Presupuestal Ejecutada')
    meta_fisica_planeada = fields.Integer(u'Meta Fisica Planeada',
                                          required=True)
    meta_fisica_ejecutada = fields.Integer(u'Meta Fisica Ejecutada')
    periodo = fields.Char(u'Periodo', size=7, required=True)
    meta = fields.Char(u'Meta')
    is_close = fields.Boolean(u'Cerrado')
