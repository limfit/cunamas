# -*- coding: utf-8 -*-
from openerp import models
from datetime import datetime
from openerp.tools import DEFAULT_SERVER_DATE_FORMAT
from openerp.exceptions import ValidationError


class DateHelper(object):

    def _validar_fecha_inicio(self, fecha_inicio, fecha_cierre,
                              message='La fecha de inicio debe ser menor que '
                                      'la fecha de cierre'):
        if fecha_inicio:
            fecha = datetime.strptime(fecha_inicio,
                                      DEFAULT_SERVER_DATE_FORMAT).date()
            if fecha_cierre:
                cierre = datetime.strptime(fecha_cierre,
                                           DEFAULT_SERVER_DATE_FORMAT).date()
                if fecha >= cierre:
                    if message:
                        raise ValidationError(message)
                        return False
        return True

    def _validar_fecha_cierre(self, fecha_inicio, fecha_cierre,
                              message='La fecha de cierre debe ser mayor que '
                                      'la fecha de inicio'):
        if fecha_cierre:
            fecha = datetime.strptime(fecha_cierre,
                                      DEFAULT_SERVER_DATE_FORMAT).date()
            if fecha_inicio:
                inicio = datetime.strptime(fecha_inicio,
                                           DEFAULT_SERVER_DATE_FORMAT).date()
                if fecha <= inicio:
                    if message:
                        raise ValidationError(message)
                        return False
        return True

    def _validar_fecha_cierre_sin_mensaje(self, fecha_inicio, fecha_cierre):
        if fecha_cierre:
            fecha = datetime.strptime(fecha_cierre,
                                      DEFAULT_SERVER_DATE_FORMAT).date()
            if fecha_inicio:
                inicio = datetime.strptime(fecha_inicio,
                                           DEFAULT_SERVER_DATE_FORMAT).date()
                if fecha <= inicio:
                        return False
        return True

    def _validar_fecha_mayor(self, fecha_inicio, fecha_cierre,
                             message='La fecha de cierre debe ser mayor '
                                     'que la fecha de inicio'):
        if fecha_cierre:
            fecha = datetime.strptime(fecha_cierre,
                                      DEFAULT_SERVER_DATE_FORMAT).date()
            if fecha_inicio:
                inicio = datetime.strptime(fecha_inicio,
                                           DEFAULT_SERVER_DATE_FORMAT).date()
                if fecha < inicio:
                    if message:
                        raise ValidationError(message)
                        return False
        return True

    def _validar_rango_fechas(self, fecha_inicio, fecha_cierre):
        if not self._validar_fecha_inicio(fecha_inicio, fecha_cierre):
            raise ValidationError('La fecha de inicio debe ser '
                                  'menor que la fecha de cierre')
            return False

        if not self._validar_fecha_cierre(fecha_inicio, fecha_cierre):
            raise ValidationError('La fecha de cierre debe ser '
                                  'mayor que la fecha de inicio')
            return False
        return True

    def _validar_fechas_futuras(self, fiel_date,
                                message='No debe ingresar '
                                        'fechas mayores a la actual'):
        try:
            if fiel_date:
                fecha = datetime.strptime(fiel_date,
                                          DEFAULT_SERVER_DATE_FORMAT).date()
                today = datetime.now().date()
                if fecha > today:
                    raise ValidationError(
                        'No debe ingresar fechas mayores a la actual')
                    return False
        except ValueError:
            raise ValidationError('Debe Ingresar una fecha valida')
            return False
        return True


class StringHelper(object):

    def _valida_is_only_number(self, dni):
        if dni:
            try:
                int(dni.strip())
                return True
            except ValueError:
                return False

    def _validate_lenght(self, dni, lenght):
        if dni:
            largo = len(dni.strip())
            if largo > lenght or largo < lenght:
                return False
            return True

    def _is_white_space(self, str):
        if str:
            if not str.strip():
                return True
        return False


class DNIHelper(object):
    _peruvianDNILength = 8

    def _validar_peruvian_dni(self, dni):
        sh = StringHelper()
        if not sh._valida_is_only_number(dni):
            raise ValidationError('El DNI solo debe tener números')
            return False

        if not sh._validate_lenght(dni, self._peruvianDNILength):
            raise ValidationError('El DNI debe tener 8 dígitos')
            return False
        return True


class ModelHelper(object):

    def set_default_field(self, record, context, field_name, prefix=''):
        value = record[field_name]
        if type(type(value)) is models.MetaModel:
            context[prefix + field_name] = value.id
        else:
            context[prefix + field_name] = value

    def set_defaults_values_for_view(self, record, context, list_fields):
        for fn in list_fields:
            self.set_default_field(record, context, fn, 'default_')

    def set_values_for_update(self, record, context, list_fields):
        for fn in list_fields:
            self.set_default_field(record, context, fn)
