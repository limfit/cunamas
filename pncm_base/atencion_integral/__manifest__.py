# -*- coding: utf-8 -*-

{
    "name": u"Atención Integral",
    "version": "1.0",
    "author": "Ganemo",
    "website": "http://www.ganemocorp.com/",
    "description": """
        Agrega asistencia, visitas y Fichas
    """,
    "depends": [
        'base_cuna',
    ],
    "data": [
        'data/vacuna.inmunizacion.csv',
        'security/ir.model.access.csv',
        'views/actions.xml',
        'views/forms.xml',
        'views/graphs.xml',
        'views/menus.xml',
        'views/pivots.xml',
        'views/trees.xml',
    ],
    'installable': True,
    'active': False
}
