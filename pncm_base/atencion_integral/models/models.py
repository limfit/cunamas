# -*- coding: utf-8 -*-

from datetime import datetime
from dateutil.relativedelta import relativedelta
from odoo import api, fields, models
from odoo.exceptions import ValidationError
from odoo.tools import DEFAULT_SERVER_DATE_FORMAT
import pytz


def _validar_periodo_cerrado(self, ut, anio, mes, servicio, tipo_cierre):
    # Valida que si posible crear registros en el mes seleccionado.
    hoy = fields.Date.today()
    tbl = self.env['tabla.cierre.periodo'].search([
        ('unidad_territorial_id', '=', ut.id),
        ('mes', '=', mes),
        ('anio', '=', anio.id),
        ('servicio', '=', servicio),
        ('tipo_cierre', '=', tipo_cierre)
    ])
    if tbl:
        if tbl.fecha_inicio <= hoy <= tbl.fecha_fin:
            return False
        else:
            return 'El mes seleccionado ya ha sido cerrado para la Unidad Territorial seleccionada.'
    else:
        return 'No se encuentra definido el periodo de apertura para este mes.'


def _obtener_tiempo(self, time):
    # Obtiene el mes y anio por defecto al crear un registro
    date_now = datetime.now(tz=pytz.timezone('America/Lima')).replace(tzinfo=None)
    if time == 'mes':
        return months[date_now.month - 1]
    else:
        rec = self.env['tabla.anios'].search([
            ('name', '=', date_now.year)
        ])
        return rec.id


def validar_fechas(fecha_inicio, fecha_fin):
    if fecha_fin and fecha_inicio:
        if fecha_inicio >= fecha_fin:
            raise ValidationError('La fecha de fin no puede ser menor o igual a la fecha de inicio')


def get_month_day_range(date):
    # obtiene el primer dia del mes del campo de date
    date = datetime.strptime(date, '%Y-%m-%d')
    first_day = date.replace(day=1)
    return first_day


def obtener_diagnostico_tamizaje(edad_examen, resultado):
    diagnostico = ''
    if edad_examen >= 0 and edad_examen < 6:
        diagnostico = "A" if resultado < 9.5 else "N"
    elif edad_examen >= 6:
        if resultado >= 0 and resultado < 7:
            diagnostico = "AS"
        elif resultado >= 7 and resultado < 10:
            diagnostico = "AM"
        elif resultado >= 10 and resultado < 11:
            diagnostico = "AL"
        elif resultado >= 11 and resultado <= 20:
            diagnostico = "N"
    return diagnostico


def filtrar_en_vistas(self, name, model, modo):
    # Filtra los registros dependiendo la configuracion del usuario
    """
        modo:
        # filtrar
        1 = unidad_territorial
        2 = servicios
        0 = ambos

    """
    action = {
        'name': name,
        'type': "ir.actions.act_window",
        'res_model': model,
        'view_type': "form",
        'view_mode': "tree,form,pivot,graph",
    }
    user = self.env['res.users'].browse(self._uid)
    if modo == 0:
        if user.unidad_territorial_id and user.servicio and user.servicio != 'ambas':
            action['domain'] = "[('unidad_territorial_id', 'in', %s), ('servicio', '=', '%s')]" % \
                               (user.unidad_territorial.ids, user.servicio)
        elif user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', 'in', %s)]" % user.unidad_territorial_id.ids
        elif user.servicio and user.servicio != 'ambas':
            action['domain'] = "[('servicio', '=', '%s')]" % user.servicio

    elif modo == 1:
        if user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', 'in', %s)]" % user.unidad_territorial_id.ids

    elif modo == 2:
        if user.servicio and user.servicio != 'ambas':
            action['domain'] = "[('servicio', '=', '%s')]" % user.servicio
    return action


def filtrar_en_vistas_visitas(self, name, model):
    # Filtra los registros dependiendo la configuracion del usuario pero se le agrego la restriccion que el acompañante
    # tecnico solo pueda ver sus comites asignados.
    action = {
        'name': name,
        'type': "ir.actions.act_window",
        'res_model': model,
        'view_type': "form",
        'view_mode': "tree,form,pivot,graph",
    }
    user = self.env['res.users'].browse(self._uid)
    groups = self.env.user.groups_id

    cat_cunamas = self.env['ir.module.category'].sudo().search([('name', '=', 'Usuario Cunamas')])
    jefe_ut = self.env['res.groups'].sudo().search(
        [('name', '=', 'Jefe de Unidad Territorial'), ('category_id', '=', cat_cunamas.id)]
    )
    coordinador = self.env['res.groups'].sudo().search(
        [('name', '=', 'Coordinador'), ('category_id', '=', cat_cunamas.id)]
    )
    asistente_adm = self.env['res.groups'].sudo().search(
        [('name', '=', 'Administrador/ Asistente Administrativo'), ('category_id', '=', cat_cunamas.id)]
    )
    tec_informatico = self.env['res.groups'].sudo().search(
        [('name', '=', u'Técnico Informático'), ('category_id', '=', cat_cunamas.id)]
    )
    especialista_integrales = self.env['res.groups'].sudo().search(
        [('name', '=', 'Especialista Integrales'), ('category_id', '=', cat_cunamas.id)]
    )
    acompaniante_tecnico = self.env['res.groups'].sudo().search(
        [('name', '=', u'Acompañante Técnico'), ('category_id', '=', cat_cunamas.id)]
    )
    servicios_terceros = self.env['res.groups'].sudo().search(
        [('name', '=', u'Servicios por Terceros'), ('category_id', '=', cat_cunamas.id)]
    )
    especialista_nutricion = self.env['res.groups'].sudo().search(
        [('name', '=', u'Especialista en Nutrición'), ('category_id', '=', cat_cunamas.id)]
    )
    gestion_comunal = self.env['res.groups'].sudo().search(
        [('name', '=', u'Gestión Comunal'), ('category_id', '=', cat_cunamas.id)]
    )
    cunamas_groups = [
        jefe_ut.id, coordinador.id, asistente_adm.id, tec_informatico.id, especialista_integrales.id,
        acompaniante_tecnico.id, servicios_terceros.id, especialista_nutricion.id, gestion_comunal.id
    ]

    new = [c for c in groups.ids if c in cunamas_groups]
    if not len(new) == 9:
        employee = self.env['hr.employee'].search([('res_users_id', '=', user.id)])
        if not employee:
            if user.unidad_territorial_id:
                action['domain'] = "[('unidad_territorial_id', 'in', %s)]" % user.unidad_territorial_id.ids
        else:
            cgs = []
            for line in employee.comite_equipo_ids:
                if not line.fecha_fin and line.comite_gestion_id.servicio == 'saf' and not line.comite_gestion_id.id in cgs:
                    cgs.append(line.comite_gestion_id.id)
            if user.unidad_territorial_id:
                action['domain'] = "[('comite_gestion_id', 'in', %s)]" %  cgs
            else:
                action['domain'] = "[('comite_gestion_id', 'in', %s)]" % cgs
    else:
        if user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', 'in', %s)]" % user.unidad_territorial_id.ids
    return action


months = [
    ('01', 'Enero'),
    ('02', 'Febrero'),
    ('03', 'Marzo'),
    ('04', 'Abril'),
    ('05', 'Mayo'),
    ('06', 'Junio'),
    ('07', 'Julio'),
    ('08', 'Agosto'),
    ('09', 'Setiembre'),
    ('10', 'Octubre'),
    ('11', 'Noviembre'),
    ('12', 'Diciembre')
]


class SurveyLabel(models.Model):

    _inherit = 'survey.label'

    survey_id = fields.Many2one(
        comodel_name='survey.survey',
        string='Survey',
        related='question_id.page_id.survey_id',
        store=True
    )
    page_id = fields.Many2one(
        comodel_name='survey.page',
        related='question_id.page_id',
        string='Pagina',
        store=True
    )
    question_conditional_id = fields.Many2one(
        comodel_name='survey.question',
        string='Preguntas',
    )
    answer_id = fields.Many2one(
        comodel_name='survey.label',
        string='Respuesta'
    )
    answer2_id = fields.Many2one(
        comodel_name='survey.label',
        string='Respuesta Matriz'
    )
    fecha_creacion = fields.Date(
        string='Fecha Creacion',
        default=fields.Date.today()
    )
    fecha_mod = fields.Date(
        string=u'Fecha Modificación'
    )
    usuario_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Creador',
        default=lambda self: self.env.user
    )
    usuario_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Modifico'
    )
    usuario_bd_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Creador'
    )
    usuario_bd_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Modifico'
    )
    cunanet_id = fields.Integer()
    sisaf_id = fields.Integer()


class Survey(models.Model):

    _inherit = 'survey.survey'

    model_ids = fields.Many2many(
        comodel_name='ir.model',
        string="Modelos donde se filtrara la encuesta"
    )
    es_visita = fields.Boolean(
        string='¿Es ficha de visita?'
    )
    fecha_creacion = fields.Date(
        string='Fecha Creacion',
        default=fields.Date.today()
    )
    fecha_mod = fields.Date(
        string=u'Fecha Modificación'
    )
    usuario_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Creador',
        default=lambda self: self.env.user
    )
    usuario_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Modifico'
    )
    usuario_bd_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Creador'
    )
    usuario_bd_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Modifico'
    )
    cunanet_id = fields.Integer()
    sisaf_id = fields.Integer()

    @api.multi
    def action_start_survey2(self):
        return self.action_formulario('fill')

    @api.multi
    def action_mostrar_respuestas(self):
        return self.action_formulario('print')

    """
        El paremetro "word" indica:
         - fill : Se inicia automaticamente la encuesta.
         - print: Se muestra los resultados.
    """
    def action_formulario(self, word):
        """ Open the website page with the survey form """
        self.ensure_one()
        token = self.env.context.get('survey_token')
        trail = "/%s" % token if token else ""
        url = self.with_context(relative_url=True).public_url
        url = url.replace('/start/', '/%s/') % word

        return {
            'type': 'ir.actions.act_url',
            'name': "Respuestas",
            'target': 'self',
            'url': url + trail
        }


class SurveyInput(models.Model):

    _inherit = 'survey.user_input'

    fecha_visita = fields.Date(
        string='Fecha Visita',
        compute='_compute_fechas',
        store=True
    )
    fecha_prox_visita = fields.Date(
        string=u'Fecha Próxima Visita',
        compute='_compute_fechas',
        store=True
    )
    ficha_reconocimiento_id = fields.Many2one(
        comodel_name='ficha.reconocimiento',
        inverse_name='visita_ids',
        string='Ficha Reconocimiento',
        ondelete='restrict'
    )
    ficha_fortalecimiento_id = fields.Many2one(
        comodel_name='ficha.fortalecimiento',
        inverse_name='visita_ids',
        string='Ficha Fortalecimiento',
        ondelete='restrict'
    )
    ficha_gestante_id = fields.Many2one(
        comodel_name='ficha.gestante',
        inverse_name='visita_ids',
        string='Ficha Gestante',
        ondelete='restrict'
    )
    persona_ficha_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        string=u'Gestante/Niño',
        compute='_compute_anio_mes_persona_id',
        store=True,
        ondelete='restrict'
    )
    mes = fields.Selection(
        selection=months,
        string='Mes',
        compute='_compute_anio_mes_persona_id',
        store=True
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        string=u'Año',
        compute='_compute_anio_mes_persona_id',
        store=True
    )
    nro_visita = fields.Integer()
    app_origen = fields.Selection(
        selection=[
            ('T', 'Tablet'),
            ('W', 'Web')
        ],
        string='App origen',
        default='W'
    )
    usuario_tablet = fields.Integer()
    fecha_creacion = fields.Date(
        string='Fecha Creacion',
        default=fields.Date.today()
    )
    fecha_mod = fields.Date(
        string=u'Fecha Modificación'
    )
    usuario_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Creador',
        default=lambda self: self.env.user
    )
    usuario_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Modifico'
    )
    usuario_bd_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Creador'
    )
    usuario_bd_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Modifico'
    )
    cunanet_id = fields.Integer()
    sisaf_id = fields.Integer()

    @api.model
    def create(self, values):
        if values.get('fecha_visita'):
            if values.get('ficha_reconocimiento_id'):
                recs = self.env['ficha.fortalecimiento'].search([
                    ('fecha_visita', '=', values['fecha_visita'])
                ])
                if recs:
                    raise ValidationError(
                        'Ya existe una visita con la fecha % en la Ficha de Fortalecimiento.' % values['fecha_visita']
                    )
            if values.get('ficha_fortalecimiento_id'):
                recs = self.env['ficha.reconocimiento'].search([
                    ('fecha_visita', '=', values['fecha_visita'])
                ])
                if recs:
                    raise ValidationError(
                        'Ya existe una visita con la fecha % en la Ficha de Reconocimiento.' % values['fecha_visita']
                    )
        obj = super(SurveyInput, self).create(values)
        return obj

    @api.multi
    def write(self, values):
        if values.get('fecha_visita'):
            id = self.read(['id'])
            rec = self.search([('id', '=', id[0]['id'])])

            if rec.ficha_reconocimiento_id:
                recs = self.env['ficha.fortalecimiento'].search([
                    ('fecha_visita', '=', values['fecha_visita'])
                ])
                if recs:
                    raise ValidationError(
                        'Ya existe una visita con la fecha % en la Ficha de Fortalecimiento.' % values['fecha_visita']
                    )
            if rec.ficha_fortalecimiento_id:
                recs = self.env['ficha.reconocimiento'].search([
                    ('fecha_visita', '=', values['fecha_visita'])
                ])
                if recs:
                    raise ValidationError(
                        'Ya existe una visita con la fecha % en la Ficha de Reconocimiento.' % values['fecha_visita']
                    )
        r = super(SurveyInput, self).write(values)
        return r

    @api.multi
    @api.depends('ficha_reconocimiento_id', 'ficha_fortalecimiento_id', 'ficha_gestante_id')
    def _compute_anio_mes_persona_id(self):
        for rec in self:
            if rec.ficha_reconocimiento_id:
                rec.persona_ficha_id = rec.ficha_reconocimiento_id.gestante_id
                rec.mes = rec.ficha_reconocimiento_id.mes
                rec.anio = rec.ficha_reconocimiento_id.anio
            if rec.ficha_fortalecimiento_id:
                rec.persona_ficha_id = rec.ficha_fortalecimiento_id.gestante_id
                rec.mes = rec.ficha_fortalecimiento_id.mes
                rec.anio = rec.ficha_fortalecimiento_id.anio
            if rec.ficha_gestante_id:
                rec.persona_ficha_id = rec.ficha_gestante_id.gestante_id
                rec.mes = rec.ficha_gestante_id.mes
                rec.anio = rec.ficha_gestante_id.anio

    @api.multi
    @api.depends('user_input_line_ids')
    def _compute_fechas(self):
        for rec in self:
            for line in rec.user_input_line_ids:
                if line.question_id.type == 'datetime' and line.question_id.fecha_visita:
                    rec.fecha_visita = line.value_date
                if line.question_id.type == 'datetime' and line.question_id.fecha_prox_visita:
                    rec.fecha_prox_visita = line.value_date

    @api.multi
    def accion_revisar_visita(self):
        self.ensure_one()
        response = self
        if self.state == 'done':
            return self.survey_id.with_context(survey_token=response.token).action_mostrar_respuestas()
        else:
            return self.survey_id.with_context(survey_token=response.token).action_start_survey2()


class SurveyQuestion(models.Model):

    _inherit = 'survey.question'

    fecha_visita = fields.Boolean(
        string='Fecha Visita'
    )
    fecha_prox_visita = fields.Boolean(
        string=u'Fecha Próxima Visita'
    )
    fecha_creacion = fields.Date(
        string='Fecha Creacion',
        default=fields.Date.today()
    )
    fecha_mod = fields.Date(
        string=u'Fecha Modificación'
    )
    usuario_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Creador',
        default=lambda self: self.env.user
    )
    usuario_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Modifico'
    )
    usuario_bd_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Creador'
    )
    usuario_bd_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Modifico'
    )
    cunanet_id = fields.Integer()
    sisaf_id = fields.Integer()
    pregunta_esp = fields.Boolean()

    @api.multi
    @api.onchange('type')
    def _onchange_type(self):
        for rec in self:
            if rec.type != 'datetime':
                rec.fecha_visita = rec.fecha_prox_visita = False

    @api.multi
    def accion_agregar_horas(self):
        if self.labels_ids:
            raise ValidationError(u'Ya existen registros en las líneas de respuestas.')
        hora = 0.0
        while hora < 24.00:
            result = '{0:02.0f}:{1:02.0f}'.format(*divmod(hora * 60, 60))
            hora += 0.25
            self.write({
                'labels_ids': [(0, 0, {
                    'value': result,
                    'question_id': self.id
                })]
            })


class IntegranteUnidadFamiliar(models.Model):
    _inherit = 'integrante.unidad.familiar'

    fichas_rec_ids = fields.One2many(
        comodel_name='ficha.reconocimiento',
        inverse_name='gestante_id',
        string='Reconocimiento'
    )
    fichas_fort_ids = fields.One2many(
        comodel_name='ficha.fortalecimiento',
        inverse_name='gestante_id',
        string='Fortalecimiento'
    )
    ficha_gestante_ids = fields.One2many(
        comodel_name='ficha.reconocimiento',
        inverse_name='gestante_id',
        string='Ficha gestante'
    )
    ficha_salud_ids = fields.One2many(
        comodel_name='ficha.salud',
        inverse_name='ninio_id',
        string='Ficha Salud'
    )


class FichaBase(models.Model):

    _name = 'ficha.base'
    _inherit = 'tabla.base'

    app_origen = fields.Selection(
        selection=[
            ('T', 'Tablet'),
            ('W', 'Web')
        ],
        string='App origen',
        default='W'
    )
    id_exportacion = fields.Integer()
    fecha_exportacion = fields.Date()
    fecha_cierre = fields.Date()
    estado = fields.Selection(
        selection=[
            ('abierto', 'Abierto'),
            ('cerrado', 'Cerrado'),
        ],
        string='Estado',
        default='abierto',
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        required=True,
        default=lambda self: self.env.user.unidad_territorial_id[0] if self.env.user.unidad_territorial_id else False,
        ondelete='restrict'
    )
    unidad_territorial_id_log = fields.Many2many(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True), ('estado','=', 'activo')]",
        default=lambda self: self.env.user.unidad_territorial_id if self.env.user.unidad_territorial_id else False
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=', unidad_territorial_id),"
               "('servicio', '!=', 'scd')]",
        string=u'Comite Gestión',
        required=True,
        ondelete='restrict'
    )
    familia_id = fields.Many2one(
        comodel_name='unidad.familiar',
        string='Familia',
        domain="[('comite_gestion_id','=', comite_gestion_id),"
               "('services', '!=', 'scd')]",
        ondelete='restrict'
    )
    pais_id = fields.Many2one(
        comodel_name='res.country',
        string='País',
        default=lambda self: self.env.user.company_id.country_id
    )
    departamento_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Departamento',
        compute='_compute_facilitador',
        store=True
    )
    provincia_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Provincia',
        compute='_compute_facilitador',
        store=True
    )
    distrito_id = fields.Many2one(
        comodel_name='res.country.state',
        string='Distrito',
        compute='_compute_facilitador',
        store=True
    )
    centro_poblado_id = fields.Many2one(
        comodel_name='res.country.state',
        compute='_compute_facilitador',
        store=True,
        string='Centro Poblado'
    )
    cod_familia = fields.Char(
        string=u'Codigo Familia',
        compute='_compute_facilitador',
        store=True
    )
    facilitador_id = fields.Many2one(
        comodel_name='afiliacion.organizacion.lines',
        string='Facilitador/a',
        compute='_compute_facilitador',
        store=True,
        ondelete='restrict'
    )
    cod_facilitador = fields.Char(
        string=u'Código Facilitador',
        compute='_compute_facilitador',
        store=True
    )
    acomp_tecnico_id = fields.Many2one(
        comodel_name='pncm.comite.equipo.tecnico',
        string=u'Acompañante Técnico',
        ondelete='restrict'
    )
    cod_acomp_tecnico = fields.Char(
        string=u'Codigo Acompañante Técnico',
        compute='_compute_code_acomp_tecnico_id',
        store=True
    )
    gestante_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        string=u'Gestante/Niño',
        domain="[('familia_id', '=', familia_id),"
               "('es_ninio','=', True),"
               "('fecha_fin', '=', False)]",
        ondelete='restrict'
    )
    cod_gestante = fields.Char(
        string=u'Código Gestante',
        compute='_compute_code_gestante_id',
        store=True
    )
    fecha_nac_ges = fields.Date(
        string='Fecha Nacimiento',
        compute='_compute_code_gestante_id',
        store=True
    )
    edad_ges = fields.Integer(
        string='Edad',
        compute='_compute_code_gestante_id',
        store=True
    )
    genero = fields.Selection(
        selection=[
            ('m', 'Masculino'),
            ('f', 'Femenino')],
        string='Sexo',
        compute='_compute_code_gestante_id',
        store=True
    )
    cuid_principal_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        string='Cuidador Principal',
        compute='_compute_facilitador',
        store=True,
        ondelete='restrict'
    )
    cod_cuid_principal = fields.Char(
        string=u'Código Cuidador Principal',
        compute='_compute_facilitador',
        store=True
    )
    parentesco_id = fields.Many2one(
        comodel_name='tipo.parentesco',
        string=u'Parentesco con el niño',
        compute='_compute_code_gestante_id',
        store=True,
        ondelete='restrict'
    )
    survey_id = fields.Many2one(
        comodel_name='survey.survey',
        default=lambda self: self._get_survey(True, 0),
        string="Survey"
    )
    response_id = fields.Many2one(
        comodel_name='survey.user_input',
        string="Response",
        ondelete="set null",
        oldname="response"
    )
    visita_ids = fields.Many2many(
        comodel_name='survey.user_input',
        string="Visitas"
    )
    mes = fields.Selection(
        selection=months,
        required=True,
        string='Mes',
        default=lambda self: _obtener_tiempo(self, 'mes')
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        required=True,
        string=u'Año',
        default=lambda self: _obtener_tiempo(self, 'anio'),
    )
    nro_visitas = fields.Integer(
        string='N° max de Visitas',
        default=5
    )
    fecha_visita = fields.Date(
        string='Fecha de Visita',
        default=fields.Date.today()
    )
    visitas_html = fields.Html(
        string='Visitas'
    )
    model_name = fields.Char(
        default=lambda self: self._default_model_name(),
        string="Survey"
    )
    validador_meses = fields.Char(readonly=True)
    usuario_tablet = fields.Integer()

    total_visitas = fields.Integer(
        string='N° Visitas',
        compute='_compute_cantidad_visitas',
        store=True
    )
    nro_visitas_efectivas = fields.Integer(
        string='N° Visitas Efectivas',
        compute = '_compute_cantidad_visitas',
        store = True
    )

    @api.multi
    @api.depends('visita_ids', 'visita_ids.user_input_line_ids')
    def _compute_cantidad_visitas(self):
        # Calcula la cantidad de visita y visitas efectivas
        for rec in self:
            rec.total_visitas = len(rec.visita_ids)
            nro_visitas_efectivas = 0
            for visita in rec.visita_ids:
                pregunta = visita.user_input_line_ids.filtered(
                    lambda x: 'SE REALIZO LA VISITA' in x.question_id.question.upper() and
                              x.value_suggested.value.upper() == 'SI')
                if pregunta:
                    nro_visitas_efectivas += 1
            rec.nro_visitas_efectivas = nro_visitas_efectivas

    @api.onchange('unidad_territorial_id_log')
    def _onchange_domain(self):
        if self.unidad_territorial_id_log:
            return {'domain': {'unidad_territorial_id': [('id', 'in', self.unidad_territorial_id_log.ids)]}}
        else:
            return {'domain': {'unidad_territorial_id': [('is_office', '=', True), ('estado', '=', 'activo')]}}

    @api.onchange('comite_gestion_id', 'unidad_territorial_id')
    def _onchange_domain_acomp_tecnico_id(self):
        cargo = self.env['hr.positions'].search([('name', '=', u'ACOMPAÑANTE TECNICO')])
        employees_ids = []
        employees = self.env['hr.employee'].search([
            ('department_id', '=', self.unidad_territorial_id.id),
            ('position_id', '=', cargo.id)
        ])
        for employee in employees:
            if employee.comite_equipo_ids.filtered(
                    lambda x: x.comite_gestion_id == self.comite_gestion_id and not x.fecha_fin and
                              x.comite_gestion_id.servicio == 'saf'):
                employees_ids.append(employee.id)
        if employees_ids:
            return {'domain': {'acomp_tecnico_id': [('id', 'in', employees.ids)]}}
        else:
            return {'domain': {'acomp_tecnico_id': [('id', '=', False)]}}

    def _default_model_name(self):
        return self._name

    @api.onchange('mes', 'anio', 'unidad_territorial_id')
    def _onchange_mes(self):
        # Valida si el perido esta habilitado
        if self.mes and self.anio and self.unidad_territorial_id:
            if self._name == 'ficha.reconocimiento':
                tbl = _validar_periodo_cerrado(self, self.unidad_territorial_id, self.anio, self.mes, 'saf',
                                               'visitas_r')
                if tbl:
                    self.mes = False
                    self.validador_meses = tbl
                else:
                    self.validador_meses = False

            elif self._name == 'ficha.fortalecimiento':
                tbl = _validar_periodo_cerrado(self, self.unidad_territorial_id, self.anio, self.mes, 'saf',
                                               'visitas_f')
                if tbl:
                    self.mes = False
                    self.validador_meses = tbl
                else:
                    self.validador_meses = False

            elif self._name == 'ficha.gestante':
                tbl = _validar_periodo_cerrado(self, self.unidad_territorial_id, self.anio, self.mes, 'saf',
                                               'visitas_g')
                if tbl:
                    self.mes = False
                    self.validador_meses = tbl
                else:
                    self.validador_meses = False

    @api.constrains('validador_meses')
    def _constrains_validador_meses(self):
        if self.validador_meses:
            raise ValidationError('El mes seleccionado ya ha sido cerrado para la Unidad Territorial seleccionada.')

    @api.onchange('unidad_territorial_id')
    def _onchange_cg(self):
        self.comite_gestion_id = self.familia_id = self.gestante_id = False

    @api.onchange('comite_gestion_id')
    def _onchange_familia_id_ninio(self):
        self.familia_id = self.gestante_id = False

    @api.onchange('familia_id')
    def _onchange_familia_id_ninio(self):
        self.gestante_id = False

    @api.multi
    def eliminar_visita(self):
        if self.visita_ids:
            valor = len(self.visita_ids)
            self.visita_ids[valor - 1].unlink()

    def validar_no_repeticion_fechas(self, date, registro):
        # Valida que no existe una visita con misma fecha, en las fichas de reconocimiento y fortalecimiento
        survey_input = self.env['survey.user_input']
        domain = [('fecha_visita', '=', date)]
        if registro._name == 'ficha.reconocimiento':
            domain.append(('ficha_fortalecimiento_id', '!=', False))
            rec = survey_input.search(domain)
            rec.filtered(lambda x: date == x.fecha_visita and registro.gestante_id == x.ficha_fortalecimiento_id.gestante_id)
            if rec:
                raise ValidationError('Ya existe registrada una visita con la misma fecha %s' % date )

        elif registro._name == 'ficha.fortalecimiento':
            domain.append(('ficha_reconocimiento_id', '!=', False))
            rec = survey_input.search(domain)
            rec.filtered(lambda x: date == x.fecha_visita and registro.gestante_id == x.ficha_reconocimiento_id.gestante_id)
            if rec:
                raise ValidationError('Ya existe registrada una visita con la misma fecha %s' % date)
        return True

    @api.model
    def validar_fecha_visitas(self, id, fechas_v):
        # Valida que en una ficha las fechas sean de orden cronologico.
        rec = self.search([('id', '=', id)])

        itera = 0
        for f in fechas_v:
           self.validar_no_repeticion_fechas(f, rec)
        while len(fechas_v) > 1:
            fecha_base = fechas_v[0]
            del fechas_v[itera]

            for fecha in fechas_v:
                if fecha < fecha_base:
                    return {'value': False, 'res': 'Las fechas de visita deben de ir en orden cronológico.'}
            itera += 1

        return {'value': True}

    @api.model
    def guardar_datos_preguntas(self, id, data):
        # Funcion que guardar las respuestas en registros
        rec = self.search([('id', '=', id)])

        if rec.visita_ids:
            i = 0
            for visita in rec.visita_ids:
                data_f = []
                for x in data:
                    if x['nro_visita'] == i:
                        data_f.append(x)
                if visita.user_input_line_ids:
                    visita.user_input_line_ids.unlink()
                for pregunta in data_f:
                    q_id = self.env['survey.question'].search([('id', '=', str(pregunta['id_pregunta']))])
                    visita.state = 'skip'
                    if pregunta['tipo'] == 'date':
                        self.env['survey.user_input_line'].create({
                            'user_input_id': visita.id,
                            'question_id': q_id.id,
                            'answer_type': pregunta['tipo'],
                            'value_date': pregunta['value']
                        })
                    elif pregunta['tipo'] == 'numerical_box':
                        self.env['survey.user_input_line'].create({
                            'user_input_id': visita.id,
                            'question_id': q_id.id,
                            'answer_type': 'number',
                            'value_number': pregunta['value']
                        })
                    elif pregunta['tipo'] == 'simple_choice' or pregunta['tipo'] == 'multiple_choice':
                        opcion_id = self.env['survey.label'].search([('id', '=', str(pregunta['id_seleccion']))])
                        self.env['survey.user_input_line'].create({
                            'user_input_id': visita.id,
                            'question_id': q_id.id,
                            'answer_type': 'suggestion',
                            'value_suggested': opcion_id.id
                        })
                    elif pregunta['tipo'] == 'matrix':
                        opcion_id = self.env['survey.label'].search([('id', '=', str(pregunta['id_seleccion']))])
                        opcion_id2 = self.env['survey.label'].search([('id', '=', str(pregunta['value_row']))])
                        self.env['survey.user_input_line'].create({
                            'user_input_id': visita.id,
                            'question_id': q_id.id,
                            'answer_type': 'suggestion',
                            'value_suggested_row': opcion_id2.id,
                            'value_suggested': opcion_id.id
                        })
                    elif pregunta['tipo'] == 'selection':
                        opcion_id = self.env['survey.label'].search([('id', '=', str(pregunta['id_seleccion']))])
                        self.env['survey.user_input_line'].create({
                            'user_input_id': visita.id,
                            'question_id': q_id.id,
                            'answer_type': 'suggestion',
                            'value_suggested': opcion_id.id
                        })
                    else:
                        self.env['survey.user_input_line'].create({
                            'user_input_id': visita.id,
                            'question_id': q_id.id,
                            'answer_type': 'text',
                            'value_text': pregunta['value']
                        })
                visita.nro_visita = i + 1
                i += 1

    @api.model
    def get_record(self, id):
        # Manda al widget las plantillas de las encuestas
        if not id:
            survey_id = self._get_survey(True, 0)
            rec = [{
                'visitas': True,
                'plantilla': self.dibujar_visitas(survey_id),
                'preguntas': self.dibujar_respuestas_visitas(survey_id),
                'nro_visitas': 0
            }]
            return rec
        else:
            rec = self.search([('id', '=', id)])
            if rec.visita_ids:
                return rec.mapped(lambda x: {
                    'visitas': True,
                    'plantilla': self.dibujar_visitas(rec.survey_id),
                    'preguntas': self.dibujar_respuestas_visitas(rec.survey_id),
                    'nro_visitas': len(rec.visita_ids),
                    'respuestas': self.obtener_repuestas_creadas(rec.visita_ids)
                })
            else:
                return rec.mapped(lambda x: {
                    'visitas': False,
                    'plantilla': self.dibujar_visitas(rec.survey_id),
                    'preguntas': self.dibujar_respuestas_visitas(rec.survey_id),
                    'nro_visitas': 0
                })

    def dibujar_visitas(self, survey_id):
        # Dibuja la plantilla con las preguntas
        tabla = '''
                    <table cellspacing="0" border="1" id="tbl_visitas">
                        <tbody>
                            <tr indice="cabecera" class="cab-ficha">
                                <td align="center">%s</td>
                            </tr>
                            %s
                        </tbody>
                    </table>
        '''
        body = ''
        for pagina in survey_id.page_ids:
            for preguntas in pagina.question_ids:
                temp = '<td>' + preguntas.question + '</td>'
                if preguntas.type != 'simple_choice':
                    if preguntas.type == 'multiple_choice':
                        body += '<tr>%s</tr>' % (temp)
                        for label in preguntas.labels_ids:
                            temp = '<td class="tdoption">' + label.value + '</td>'
                            body += '<tr>%s</tr>' % (temp)
                    elif preguntas.type == 'matrix':
                        body += '<tr>%s</tr>' % (temp)
                        # FILAS
                        for label in preguntas.labels_ids_2:
                            temp = '<td class="tdoption">' + label.value + '</td>'
                            body += '<tr>%s</tr>' % (temp)
                    else:
                        body += '<tr>%s</tr>' % (temp)
                else:
                    if len(preguntas.labels_ids) == 2:
                        body += '<tr>%s</tr>' % (temp )
                    elif preguntas.display_mode == 'dropdown':
                        body += '<tr>%s</tr>' % (temp)
                    else:
                        body += '<tr>%s</tr>' % (temp)
                        for label in preguntas.labels_ids:
                            temp = '<td class="tdoption">' + label.value + '</td>'
                            body += '<tr>%s</tr>' % (temp)
        tabla = tabla % (survey_id.title, body)
        return tabla

    def dibujar_respuestas_visitas(self, survey_id):
        # Dibuja la plantilla del tipo de respuesta que puede tener la pregunta
        tabla = '''
                    <table cellspacing="0" border="1" id="tbl_visitas_respuestas">
                        <tbody>
                            <tr id="cabecera" class="cab-ficha" cabecera="S">
                                <td align="center">VISITA N° 2&nbsp</td>
                            </tr>
                            %s
                        </tbody>
                    </table>
        '''
        body = ''
        for pagina in survey_id.page_ids:
            for preguntas in pagina.question_ids:
                if preguntas.type != 'simple_choice':

                    if preguntas.type == 'multiple_choice':
                        body += '<tr>%s</tr>' % ('<td></td>')
                        for label in preguntas.labels_ids:
                            if label.question_conditional_id:
                                temp2 = '<td><input tipo="multiple_choice" idp="%d" idh="%d"  name="%d_%s_" type="checkbox" padre="%d_radio_%s_" value="%s"/></td>' % \
                                        (preguntas.id, label.id,
                                         preguntas.id, preguntas.question,
                                         label.question_conditional_id.id,label.question_conditional_id.question,
                                         label.value.upper())
                            else:
                                temp2 = '<td><input tipo="multiple_choice" idp="%d" idh="%d" name="%d_radio_%s_" type="checkbox" value="%s"/></td>' % \
                                        (preguntas.id, label.id, preguntas.id, preguntas.question, label.value.upper())
                            body += '<tr>%s</tr>' % temp2
                    elif preguntas.type == 'matrix':
                        if len(preguntas.labels_ids) == 2:
                            body += '<tr>%s</tr>' % ('<td></td>')
                            for rec in preguntas.labels_ids_2:
                                if preguntas.labels_ids[0].question_conditional_id:
                                    if rec.answer_id:
                                        temp2 = '''
                                            <td align="center">
                                                <input tipo="matrix" idr="%d" subp="%d_radio_%s_" subv="%s" idp="%d" idh="%d" name="%d_radio_%s_" type="radio" padre="%d_radio_%s_" value_r="%s" value="%s">Sí&nbsp;&nbsp;
                                                <input tipo="matrix" idr="%d" subp="%d_radio_%s_" subv="%s" idp="%d" idh="%d" name="%d_radio_%s_" type="radio" padre="%d_radio_%s_" value_r="%s" value="%s">No
                                            </td>
                                        ''' % (rec.id, rec.answer_id.id, preguntas.question,
                                               preguntas.labels_ids[0].value.upper(),
                                               preguntas.id, preguntas.labels_ids[0].id, rec.id, preguntas.question,
                                               preguntas.labels_ids[0].question_conditional_id.id,
                                               preguntas.labels_ids[0].question_conditional_id.question,
                                               preguntas.labels_ids[0].answer_id.value.upper(),
                                               preguntas.labels_ids[0].value.upper(),
                                               rec.id, rec.answer_id.id, preguntas.question,
                                               preguntas.labels_ids[0].value.upper(),
                                               preguntas.id, preguntas.labels_ids[1].id, rec.id, preguntas.question,
                                               preguntas.labels_ids[1].question_conditional_id.id,
                                               preguntas.labels_ids[1].question_conditional_id.question,
                                               preguntas.labels_ids[1].answer_id.value.upper(),
                                               preguntas.labels_ids[1].value.upper()
                                               )
                                    else:
                                        temp2 = '''
                                                <td align="center">
                                                    <input tipo="matrix" idr="%d" idp="%d" idh="%d" name="%d_radio_%s_" type="radio" padre="%d_radio_%s_" value_r="%s" value="%s">Sí&nbsp;&nbsp;
                                                    <input tipo="matrix" idr="%d" idp="%d" idh="%d" name="%d_radio_%s_" type="radio" padre="%d_radio_%s_" value_r="%s" value="%s">No
                                                </td>
                                            ''' % (
                                            rec.id, preguntas.id, preguntas.labels_ids[0].id, rec.id, preguntas.question,
                                            preguntas.labels_ids[0].question_conditional_id.id,
                                            preguntas.labels_ids[0].question_conditional_id.question,
                                            preguntas.labels_ids[0].answer_id.value.upper(),
                                            preguntas.labels_ids[0].value.upper(),
                                            rec.id,preguntas.id, preguntas.labels_ids[1].id, rec.id, preguntas.question,
                                            preguntas.labels_ids[1].question_conditional_id.id,
                                            preguntas.labels_ids[1].question_conditional_id.question,
                                            preguntas.labels_ids[1].answer_id.value.upper(),
                                            preguntas.labels_ids[1].value.upper())
                                    if not preguntas.pregunta_esp:
                                        body += '<tr>%s</tr>' % temp2
                                    else:
                                        body += '<tr especial="3">%s</tr>' % temp2
                                else:
                                    temp2 = '''
                                        <td align="center">
                                            <input tipo="matrix" idr="%d" idp="%d" idh="%d" name="%d_radio_%s_" type="radio" value="%s">Sí&nbsp;&nbsp;
                                            <input tipo="matrix" idr="%d" idp="%d" idh="%d" name="%d_radio_%s_" type="radio" value="%s">No
                                        </td>
                                    ''' % (rec.id, preguntas.id, preguntas.labels_ids[0].id, rec.id, preguntas.question,
                                           preguntas.labels_ids[0].value.upper(),
                                           rec.id, preguntas.id, preguntas.labels_ids[1].id, rec.id, preguntas.question,
                                           preguntas.labels_ids[1].value.upper())
                                    if not preguntas.pregunta_esp:
                                        body += '<tr>%s</tr>' % temp2
                                    else:
                                        body += '<tr especial="3">%s</tr>' % temp2
                    else:
                        temp2 = '''
                                <td>
                                    <div style="width: 100px;">
                                        %s
                                    </div>
                                </td>
                                '''
                        nombre = "{}_{}_".format(preguntas.id, preguntas.question)
                        if preguntas.type == 'datetime':
                            if preguntas.fecha_visita:
                                temp2 = temp2 % ('<input tipo="date" idp="%d" type="date" name="%s" fechav="s" style="width: 120px"/>' %
                                                 (preguntas.id, nombre))
                            else:
                                temp2 = temp2 % ('<input tipo="date" idp="%d" type="date" name="%s" style="width: 120px"/>' %
                                                 (preguntas.id, nombre))
                        elif preguntas.type == 'numerical_box':
                            temp2 = temp2 % ('<input tipo="numerical_box" idp="%d" type="text" name="%s" pattern="[0-9]+"/>' %
                                             (preguntas.id, nombre))
                        else:
                            temp2 = temp2 % ('<input tipo="textbox" idp="%d" type="text" name="%s"/>' % (preguntas.id, nombre))
                        body += '<tr>%s</tr>' % (temp2)

                else:
                    if len(preguntas.labels_ids) == 2:
                        temp2 = '''
                            <td align="center">
                                <input tipo="simple_choice" idp="%d" idh="%d" name="%d_radio_%s_" type="radio" value="%s">Sí&nbsp;&nbsp;
                                <input tipo="simple_choice" idp="%d" idh="%d" name="%d_radio_%s_" type="radio" value="%s">No
                            </td>
                        ''' % (preguntas.id, preguntas.labels_ids[0].id, preguntas.id, preguntas.question, preguntas.labels_ids[0].value.upper(),
                               preguntas.id, preguntas.labels_ids[1].id, preguntas.id, preguntas.question, preguntas.labels_ids[1].value.upper())
                        body += '<tr>%s</tr>' % (temp2)
                    elif preguntas.display_mode != 'columns':
                        options = '<option></option>'
                        for label in preguntas.labels_ids:
                            options += '<option idp="%d" idh="%d" value="%s">%s</option>' % (
                                preguntas.id, label.id, label.value.upper(), label.value)
                        if preguntas.labels_ids[0].question_conditional_id:
                            label = preguntas.labels_ids[0]
                            select = '<td><select tipo="selection" idp="%d" name="%d_radio_%s_" padre="%d_radio_%s_" value_r="%s">%s</select></td>' % \
                                     (preguntas.id,
                                      preguntas.id, preguntas.question,
                                      label.question_conditional_id.id, label.question_conditional_id.question,
                                      label.answer_id.value.upper(), options)
                        else:
                            select = '<td><select>%s</select></td>' % options
                        body += '<tr>%s</tr>' % select
                    else:
                        body += '<tr>%s</tr>' % ('<td></td>')
                        for label in preguntas.labels_ids:
                            if label.question_conditional_id:
                                temp2 = '<td><input tipo="simple_choice" idp="%d" idh="%d" name="%d_radio_%s_" type="radio" padre="%d_radio_%s_" value="%s" value_r="%s"/></td>' % \
                                        (preguntas.id, label.id,
                                         preguntas.id, preguntas.question,
                                         label.question_conditional_id.id, label.question_conditional_id.question,
                                         label.value.upper(), label.answer_id.value.upper())
                                body += '<tr>%s</tr>' % temp2
                            else:
                                temp2 = '<td><input tipo="simple_choice" idp="%d" idh="%d" name="%d_radio_%s_" type="radio" value="%s"/></td>' % \
                                        (preguntas.id, label.id, preguntas.id ,preguntas.question, label.value.upper())
                                body += '<tr>%s</tr>' % temp2
        tabla = tabla % body
        return tabla

    def obtener_repuestas_creadas(self, visita_ids):
        # Pinta las respuestas que ya han sido guardadas
        i = 0
        respuestas = []
        for visita in visita_ids:
            for input in visita.user_input_line_ids:
                nombre = '{}_{}_{}'.format(input.question_id.id, input.question_id.question, i)
                if input.answer_type == 'date':
                    date = datetime.strptime(input.value_date, '%Y-%m-%d %H:%M:%S').date()
                    respuestas.append({
                        'nombre': nombre,
                        'nro_visita': i,
                        'idp': input.question_id.id,
                        'value': str(date),
                        'tipo': 'dato'
                    })
                elif input.answer_type == 'number':
                    respuestas.append({
                        'nombre': nombre,
                        'nro_visita': i,
                        'idp': input.question_id.id,
                        'value': input.value_number,
                        'tipo': 'dato'
                    })
                elif input.answer_type == 'suggestion':
                    nombre = '{}_radio_{}_{}'.format(input.question_id.id, input.question_id.question, i)
                    if input.question_id.type == 'matrix':
                        nombre = '{}_radio_{}_{}'.format(input.value_suggested_row.id, input.question_id.question, i)
                        respuestas.append({
                             'nombre': nombre,
                             'nro_visita': i,
                             'value': input.value_suggested.value.upper(),
                             'tipo': 'matrix'
                        })
                    else:
                        if input.question_id.display_mode != 'dropdown':
                            respuestas.append({
                                'nombre': nombre,
                                'nro_visita': i,
                                'value': input.value_suggested.value.upper(),
                                'tipo': 'check'
                            })
                        else:
                            respuestas.append({
                                'nombre': nombre,
                                'nro_visita': i,
                                'value': input.value_suggested.value.upper(),
                                'tipo': 'select'
                            })
                else:
                    respuestas.append({
                        'nombre': nombre,
                        'nro_visita': i,
                        'idp': input.question_id.id,
                        'value': input.value_text,
                        'tipo': 'dato'
                    })
            i += 1
        return respuestas

    def _get_survey(self, visita, tipo):
        """
            tipo:
            0 = retorna ficha visita
            1 = dibujar visitas
            2 = dibjua respuestas visitas
        """
        self_model = self.env['ir.model'].search([('name', '=', self._name)]).id
        survey_state = self.env['survey.stage'].search([('sequence', '=', 2)]).id
        domain = [
            ('model_ids', 'in', self_model),
            ('stage_id', '=', survey_state),
            ('es_visita', '=', visita)
        ]
        rec = self.env['survey.survey'].search(domain)

        if len(rec) > 1:
            raise ValidationError('Existen activas más de una encuesta para este modelo.')
        if not rec:
            return False
            #raise ValidationError('No existen visitas activas relacionadas al modelo.')
        if tipo == 1:
            return self.dibujar_visitas(rec)
        elif tipo == 2:
            return self.dibujar_respuestas_visitas(rec)
        elif tipo == 0:
            return rec

    @api.multi
    def name_get(self):
        return [(obj.id, u'{}'.format(obj.id)) for obj in self]

    @api.depends('cuid_principal_id')
    def _compute_code_cuid_principal(self):
        if self.gestante_id:
            self.cod_cuid_principal = self.cuid_principal_id.id

    @api.depends('gestante_id')
    def _compute_code_gestante_id(self):
        if self.gestante_id:
            self.cod_gestante = self.gestante_id.id
            self.fecha_nac_ges = self.gestante_id.integrante_id.birthdate
            self.edad_ges = self.gestante_id.integrante_id.anios
            self.genero = self.gestante_id.integrante_id.gender
            self.parentesco_id = self.gestante_id.parentesco_id

    @api.depends('familia_id')
    def _compute_facilitador(self):
        if self.familia_id:
            self.facilitador_id = self.familia_id.facilitador_id
            self.cod_facilitador = self.familia_id.facilitador_id.id
            self.cod_familia = self.familia_id.id
            cuid = self.familia_id.integrantes_ids.filtered(lambda x: x.es_cuidador_principal)
            self.cuid_principal_id = cuid
            self.cod_cuid_principal = cuid.id
            self.departamento_id = self.familia_id.departamento_id
            self.provincia_id = self.familia_id.provincia_id
            self.distrito_id = self.familia_id.distrito_id
            self.centro_poblado_id = self.familia_id.centro_poblado_id

    @api.depends('acomp_tecnico_id')
    def _compute_code_acomp_tecnico_id(self):
        if self.acomp_tecnico_id:
            self.cod_acomp_tecnico = self.acomp_tecnico_id.id

    @api.multi
    def action_start_survey(self):
        self.ensure_one()
        # create a response and link it to this applicant
        if not self.survey_id:
            self.survey_id = self._get_survey(True, 0)
        if not self.response_id:
            response = self.env['survey.user_input'].create({'survey_id': self.survey_id.id})
            self.response_id = response.id
            return self.survey_id.with_context(survey_token=response.token).action_start_survey2()
        else:
            response = self.response_id
            return self.survey_id.with_context(survey_token=response.token).action_mostrar_respuestas()


class SurvetInputeLine(models.Model):
    _inherit = 'survey.user_input_line'

    fecha_creacion = fields.Date(
        string='Fecha Creacion',
        default=fields.Date.today()
    )
    fecha_mod = fields.Date(
        string=u'Fecha Modificación'
    )
    usuario_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Creador',
        default=lambda self: self.env.user
    )
    usuario_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario Modifico'
    )
    usuario_bd_creador_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Creador'
    )
    usuario_bd_mod_id = fields.Many2one(
        comodel_name='res.users',
        string='Usuario BD Modifico'
    )
    cunanet_id = fields.Integer()
    sisaf_id = fields.Integer()
    valor = fields.Char(
        string='Valor',
        compute='_compute_value',
        store=True
    )

    @api.multi
    @api.depends('value_date', 'value_number', 'value_suggested', 'value_text')
    def _compute_value(self):
        for rec in self:
            if rec.value_date:
                rec.valor = datetime.strptime(rec.value_date, "%Y-%m-%d %H:%M:%S")
            elif rec.value_number:
                rec.valor = str(rec.value_number)
            elif rec.value_suggested:
                rec.valor = rec.value_suggested.value
            else:
                rec.valor = rec.value_text

    @api.constrains('answer_type')
    def _check_answer_type(self):
        for uil in self:
            fields_type = {
                'text': bool(uil.value_text),
                'number': (bool(uil.value_number) or uil.value_number == 0),
                'date': bool(uil.value_date),
                'free_text': bool(uil.value_free_text),
                'suggestion': bool(uil.value_suggested)
            }
            if 1>10:
                raise ValidationError('The answer must be in the right type')


class FichaGestante(models.Model):

    _name = 'ficha.gestante'
    _inherit = 'ficha.base'

    survey_id = fields.Many2one(
        comodel_name='survey.survey',
        default=lambda self: self._get_survey(True, 0),
        string="Survey"
    )
    etapa = fields.Selection(
        selection=[
            ('fortalecimiento', 'Fortalecimiento'),
            ('reconocimiento', 'Reconocimiento')],
        string='Etapa',
        required=True
    )
    visita_ids = fields.One2many(
        comodel_name='survey.user_input',
        inverse_name='ficha_gestante_id',
        string="Visitas",
        ondelete="set null",
    )
    familia_id = fields.Many2one(
        comodel_name='unidad.familiar',
        string='Familia',
        compute='_compute_familia_id',
        store=True
    )
    cod_familia = fields.Char(
        string=u'Codigo Familia',
        compute='_compute_familia_id',
        store=True
    )
    mes_gestacion = fields.Selection(
        selection=[
            ('1_mes', '1 mes'),
            ('2_meses', '2 meses'),
            ('3_meses', '3 meses'),
            ('4_meses', '4 meses'),
            ('5_meses', '5 meses'),
            ('6_meses', '6 meses'),
            ('7_meses', '7 meses'),
            ('8_meses', '8 meses'),
            ('9_meses', '9 meses')
        ],
        string='Mes Gestación',
        required=True
    )
    gestante_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        inverse_name='ficha_gestante_ids',
        string=u'Gestante/Niño',
        domain="[('familia_id', '=', familia_id),"
               "('es_ninio','=', True),"
               "('fecha_fin', '=', False)]"
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=', unidad_territorial_id),"
               "('servicio', '!=', 'scd')]",
        string=u'Comite Gestión',
        required=True,
        ondelete='restrict',
        inverse_name='ficha_gestante_ids'
    )

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.visita_ids:
                raise ValidationError('Solo se puede eliminar si no tiene visitas creadas.')
        res = super(FichaGestante, self).unlink()
        return res

    @api.multi
    @api.depends('gestante_id')
    def _compute_familia_id(self):
        for rec in self:
            if rec.gestante_id:
                rec.familia_id = rec.gestante_id.familia_id
                rec.cod_familia = rec.gestante_id.familia_id.id

    @api.multi
    def action_agregar_visita(self):
        self.ensure_one()
        # create a response and link it to this applicant
        if self.visita_ids:
            record = self.visita_ids.filtered(lambda x: x.state != 'done')
            if record:
                raise ValidationError('Debe terminar las visitas ya registradas.')
        survey = self._get_survey(True, 0)
        if not survey:
            raise ValidationError('Primero se debe configurar Formulario de Visitas para este modelo.')
        response = self.env['survey.user_input'].create({
            'survey_id': survey.id,
            'ficha_gestante_id': self.id
        })
        return survey.with_context(survey_token=response.token).action_start_survey2()

    @api.multi
    def action_agregar_visita_widget(self):
        if self.visita_ids and len(self.visita_ids) == self.nro_visitas:
            raise ValidationError('No se puede agregar más visitas. El máximo permitido es %d.' % self.nro_visitas)
        survey = self._get_survey(True, 0)
        if not survey:
            raise ValidationError('Primero se debe configurar Formulario de Visitas para este modelo.')
        self.env['survey.user_input'].create({
            'survey_id': survey.id,
            'ficha_gestante_id': self.id,
            'type': 'link'
        })

    @api.multi
    def filtrar_vistas(self):
        return filtrar_en_vistas_visitas(self, 'Ficha de Visista a la Gestante', 'ficha.gestante')


class FichaReconocimiento(models.Model):

    _name = 'ficha.reconocimiento'
    _inherit = 'ficha.base'

    visita_ids = fields.One2many(
        comodel_name='survey.user_input',
        inverse_name='ficha_reconocimiento_id',
        string="Visitas",
        ondelete="set null",
    )
    gestante_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        inverse_name='fichas_rec_ids',
        string=u'Niño',
        domain="[('familia_id', '=', familia_id),"
               "('es_ninio','=', True),"
               "('fecha_fin', '=', False)]"
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=', unidad_territorial_id),"
               "('servicio', '!=', 'scd')]",
        string=u'Comite Gestión',
        required=True,
        ondelete='restrict',
        inverse_name='fichas_rec_ids'
    )

    @api.onchange('familia_id', 'anio', 'mes')
    def _onchange_filter_ninio_id(self):
        ninios = []
        self.gestante_id = False
        if self.familia_id:
            for integrante in self.familia_id.integrantes_ids.filtered(lambda x: x.es_ninio and not x.fecha_fin):
                if integrante.fichas_rec_ids:
                    if not integrante.fichas_rec_ids.filtered(lambda y: y.anio == self.anio and y.mes == self.mes):
                        ninios.append(integrante.id)
                else:
                    ninios.append(integrante.id)
        return {'domain': {'gestante_id': [('id', 'in', ninios)]}}

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.visita_ids:
                raise ValidationError('Solo se puede eliminar si no tiene visitas creadas.')
        res = super(FichaReconocimiento, self).unlink()
        return res

    @api.multi
    def action_agregar_visita(self):
        self.ensure_one()
        # create a response and link it to this applicant
        if self.visita_ids:
            record = self.visita_ids.filtered(lambda x: x.state != 'done')
            if record:
                raise ValidationError('Debe terminar las visitas ya registradas.')
        survey = self._get_survey(True, 0)
        if not survey:
            raise ValidationError('Primero se debe configurar Formulario de Visitas para este modelo.')
        response = self.env['survey.user_input'].create({
            'survey_id': survey.id,
            'ficha_reconocimiento_id': self.id
        })
        return survey.with_context(survey_token=response.token).action_start_survey2()

    @api.multi
    def action_agregar_visita_widget(self):
        if self.visita_ids and len(self.visita_ids) == self.nro_visitas:
            raise ValidationError('No se puede agregar más visitas. El máximo permitido es %d.' % self.nro_visitas)
        survey = self._get_survey(True, 0)
        if not survey:
            raise ValidationError('Primero se debe configurar Formulario de Visitas para este modelo.')
        self.env['survey.user_input'].create({
            'survey_id': survey.id,
            'ficha_reconocimiento_id': self.id,
            'type': 'link'
        })

    @api.multi
    def filtrar_vistas(self):
        return filtrar_en_vistas_visitas(self, 'Ficha Reconocimiento', 'ficha.reconocimiento')


class FichaFortalecimiento(models.Model):

    _name = 'ficha.fortalecimiento'
    _inherit = 'ficha.base'

    visita_ids = fields.One2many(
        comodel_name='survey.user_input',
        inverse_name='ficha_fortalecimiento_id',
        string="Visitas",
        ondelete="set null",
    )
    gestante_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        inverse_name='fichas_fort_ids',
        string=u'Niño',
        domain="[('familia_id', '=', familia_id),"
               "('es_ninio','=', True),"
               "('fecha_fin', '=', False)]"
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=', unidad_territorial_id),"
               "('servicio', '!=', 'scd')]",
        string=u'Comite Gestión',
        required=True,
        ondelete='restrict',
        inverse_name='fichas_fort_ids'
    )

    @api.onchange('familia_id', 'anio', 'mes')
    def _onchange_filter_ninio_id(self):
        ninios = []
        self.gestante_id = False
        if self.familia_id:
            for integrante in self.familia_id.integrantes_ids.filtered(lambda x: x.es_ninio and not x.fecha_fin):
                if integrante.fichas_fort_ids:
                    if not integrante.fichas_fort_ids.filtered(lambda y: y.anio == self.anio and y.mes == self.mes):
                        ninios.append(integrante.id)
                else:
                    ninios.append(integrante.id)
        return {'domain': {'gestante_id': [('id', 'in', ninios)]}}

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.visita_ids:
                raise ValidationError('Solo se puede eliminar si no tiene visitas creadas.')
        res = super(FichaFortalecimiento, self).unlink()
        return res

    @api.multi
    def action_agregar_visita(self):
        self.ensure_one()
        # create a response and link it to this applicant
        if self.visita_ids:
            record = self.visita_ids.filtered(lambda x: x.state != 'done')
            if record:
                raise ValidationError('Debe terminar las visitas ya registradas.')
        survey = self._get_survey(True, 0)
        if not survey:
            raise ValidationError('Primero se debe configurar Formulario de Visitas para este modelo.')
        
        response = self.env['survey.user_input'].create({
            'survey_id': survey.id,
            'ficha_fortalecimiento_id': self.id
        })
        return survey.with_context(survey_token=response.token).action_start_survey2()

    @api.multi
    def action_agregar_visita_widget(self):
        if self.visita_ids and len(self.visita_ids) == self.nro_visitas:
            raise ValidationError('No se puede agregar más visitas. El máximo permitido es %d.' % self.nro_visitas)
        survey = self._get_survey(True, 0)
        if not survey:
            raise ValidationError('Primero se debe configurar Formulario de Visitas para este modelo.')
        self.env['survey.user_input'].create({
            'survey_id': survey.id,
            'ficha_fortalecimiento_id': self.id,
            'type': 'link'
        })

    @api.multi
    def filtrar_vistas(self):
        return filtrar_en_vistas_visitas(self, 'Ficha Fortalecimiento', 'ficha.fortalecimiento')


class FichaAsistenciaSocializacion(models.Model):

    _name = 'ficha.socializacion'
    _inherit = 'tabla.base'

    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        default=lambda self: self.env.user.unidad_territorial_id[0].id if self.env.user.unidad_territorial_id else False,
    )
    unidad_territorial_id_log = fields.Many2many(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True), ('estado','=', 'activo')]",
        default=lambda self: self.env.user.unidad_territorial_id if self.env.user.unidad_territorial_id else False
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=',unidad_territorial_id)]",
        string=u'Comite Gestión',
        required=True
    )
    fecha_sesion = fields.Date(
        string=u'Fecha de Sesión',
        required=True
    )
    mes = fields.Selection(
        selection=months,
        required=True,
        string='Mes',
        default=lambda self: _obtener_tiempo(self, 'mes')
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        required=True,
        string=u'Año',
        default=lambda self: _obtener_tiempo(self, 'anio')
    )
    familias_ids = fields.One2many(
        comodel_name='socializacion.lineas',
        inverse_name='ficha_id',
        string='Familias'
    )
    nro_familias = fields.Integer(
        string='N° Familias Asistentes',
        compute='_compute_nro_familias',
        store=True
    )
    filtrado = fields.Boolean(
        string='Ya valido datos'
    )
    estado = fields.Selection(
        selection=[
            ('borrador', 'Borrador'),
            ('validado', 'Validado'),
            ('cerrado', 'Cerrado'),
        ],
        string='Estado',
        default='borrador',
    )

    @api.onchange('unidad_territorial_id_log')
    def _onchange_domain(self):
        if self.unidad_territorial_id_log:
            return {'domain': {'unidad_territorial_id': [('id', 'in', self.unidad_territorial_id_log.ids)]}}
        else:
            return {'domain': {'unidad_territorial_id': [('is_office', '=', True), ('estado', '=', 'activo')]}}

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.estado != 'borrador':
                raise ValidationError('Solo se puede eliminar si el registro se encuentra en estado borrador.')
        res = super(FichaAsistenciaSocializacion, self).unlink()
        return res

    @api.onchange('unidad_territorial_id')
    def _onchange_unidad_territorial_id(self):
        self.comite_gestion_id = False

    @api.onchange('mes')
    def _onchange_fecha_sesion(self):
        self.fecha_sesion = False

    def validar_datos(self):
        mes = datetime.strptime(self.fecha_sesion, '%Y-%m-%d').month
        anio = datetime.strptime(self.fecha_sesion, '%Y-%m-%d').year
        if int(self.anio.name) != int(anio) or int(mes) != int(self.mes):
            raise ValidationError('El mes y año seleccionado deben ser igual a los de la fecha de sesion.')

    @api.multi
    @api.depends('familias_ids')
    def _compute_nro_familias(self):
        for rec in self:
            if rec.familias_ids:
                rec.nro_familias = len(rec.familias_ids.filtered(lambda x: x.asistio))

    @api.multi
    def cerrar_ficha(self):
        self.estado = 'cerrado'

    @api.multi
    def buscar_visitas_familias(self):
        # Crea las lineas de asistencia
        self.validar_datos()
        dia = get_month_day_range(self.fecha_sesion)
        visitas = self.env['survey.user_input'].search([
            ('fecha_visita', '>=', dia),
            ('fecha_visita', '<=', self.fecha_sesion)
        ])
        lista_familia = []
        for line in visitas:
            if line.ficha_reconocimiento_id:
                ficha = self.crear_linea(line.ficha_reconocimiento_id)
                ficha.ficha_id = self.id
                lista_familia.append(ficha.id)
            if line.ficha_fortalecimiento_id:
                ficha = self.crear_linea(line.ficha_fortalecimiento_id)
                ficha.ficha_id = self.id
                lista_familia.append(ficha.id)
        if lista_familia:
            self.filtrado = True
            self.estado = 'validado'
        else:
            raise ValidationError('No se encontraron datos.')
    
    def crear_linea(self, line):
        rec = self.env['socializacion.lineas'].search([
            ('familia_id', '=', line.familia_id.id),
            ('cod_familia', '=', line.familia_id.id),
            ('cuid_principal_id', '=', line.cuid_principal_id.id),
            ('cod_cuid_principal', '=', line.cuid_principal_id.id),
            ('dni_cuid_principal', '=', line.cuid_principal_id.integrante_id.document_number),
            ('facilitador_id', '=', line.facilitador_id.id),
            ('cod_facilitador', '=', line.facilitador_id.id),
            ('dni_facilitador', '=', line.facilitador_id.person_id.document_number),
            ('unidad_territorial_id', '=', line.unidad_territorial_id.id),
            ('mes', '=', line.mes),
            ('anio', '=', line.anio.id),
        ])
        if not rec:
            rec = self.env['socializacion.lineas'].create({
                'familia_id': line.familia_id.id,
                'cod_familia': line.familia_id.id,
                'cuid_principal_id': line.cuid_principal_id.id,
                'cod_cuid_principal': line.cuid_principal_id.id,
                'dni_cuid_principal': line.cuid_principal_id.integrante_id.document_number,
                'facilitador_id': line.facilitador_id.id,
                'cod_facilitador': line.facilitador_id.id,
                'dni_facilitador': line.facilitador_id.person_id.document_number,
                'unidad_territorial_id': line.unidad_territorial_id.id,
                'mes': line.mes,
                'anio': line.anio.id
            })
        return rec

    @api.multi
    def filtrar_vistas(self):
        return filtrar_en_vistas(self, u'Ficha de Asistencia de Sesiones de Socialización', 'ficha.socializacion', 1)


class SocializacionLineas(models.Model):

    _name = 'socializacion.lineas'
    _inherit = 'tabla.base'

    ficha_id = fields.Many2one(
        comodel_name='ficha.socializacion',
        string='Ficha Socializacion',
        inverse_name='familias_ids'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial'
    )
    mes = fields.Selection(
        selection=months,
        string='Mes',
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        string=u'Año',
    )
    familia_id = fields.Many2one(
        comodel_name='unidad.familiar',
        readonly=True
    )
    cod_familia = fields.Integer(
        string=u'Código Familia',
        readonly=True
    )
    cuid_principal_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        string='Cuidador Principal',
        readonly=True
    )
    cod_cuid_principal = fields.Integer(
        string=u'Cód. Cuidador Principal',
        readonly=True
    )
    dni_cuid_principal = fields.Char(
        string='DNI Cuidador Principal',
        readonly=True
    )
    facilitador_id = fields.Many2one(
        comodel_name='afiliacion.organizacion.lines',
        string='Facilitador/a',
        readonly=True
    )
    cod_facilitador = fields.Char(
        string=u'Código Facilitador',
        readonly=True
    )
    dni_facilitador = fields.Char(
        string='DNI Facilitador',
        readonly=True
    )
    asistio = fields.Boolean(
        string=u'Asistió'
    )

    @api.multi
    def validar_asistir(self):
        if self.asistio:
            self.asistio = False
        else:
            self.asistio = True


class FichaSalud(models.Model):

    _name = 'ficha.salud'
    _inherit = 'tabla.base'

    fecha_visita = fields.Date(
        string='Fecha de Visita',
        required=True,
        default=fields.Date.today()
    )
    mes = fields.Selection(
        selection=months,
        string='Mes',
        compute='_compute_anio_mes',
        store=True
    )
    anio = fields.Many2one(
        comodel_name='tabla.anios',
        string=u'Año',
        compute='_compute_anio_mes',
        store=True,
        ondelete='restrict'
    )
    unidad_territorial_id = fields.Many2one(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        ondelete='restrict',
        default=lambda self: self.env.user.unidad_territorial_id[0].id if self.env.user.unidad_territorial_id else False
    )
    unidad_territorial_id_log = fields.Many2many(
        comodel_name='hr.department',
        string=u'Unidad Territorial',
        domain="[('is_office','=',True), ('estado','=', 'activo')]",
        default=lambda self: self.env.user.unidad_territorial_id if self.env.user.unidad_territorial_id else False
    )
    comite_gestion_id = fields.Many2one(
        comodel_name='pncm.comitegestion',
        domain="[('unidad_territorial_id','=',unidad_territorial_id)]",
        string=u'Comite Gestión',
        required=True,
        ondelete='restrict'
    )
    centro_poblado_id = fields.Many2one(
        comodel_name='res.country.state',
        string=u'Centro Poblado',
        related='comite_gestion_id.centro_poblado_id',
        store=True,
        readonly=True
    )
    cod_familia = fields.Integer(
        string=u'Código Familia',
        compute='_compute_familia',
        store=True
    )
    familia_id = fields.Many2one(
        comodel_name='unidad.familiar',
        string='Familia',
        compute='_compute_familia',
        store=True,
        ondelete='restrict'
    )
    servicio = fields.Selection(selection=[
        ('scd', u'Cuidado Diurno'),
        ('saf', u'Acompañamiento a Familias')
    ],
        string='Servicio'
    )
    ninio_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        inverse_name='ficha_salud_ids',
        string=u'Niño',
        domain="[('es_ninio','=', True),"
               "('comite_gestion_id', '=', comite_gestion_id),"
               "('services', '=', servicio),"
               "('fecha_fin', '=', False)]",
        required=True,
        ondelete='restrict'
    )
    facilitador_id = fields.Many2one(
        comodel_name='afiliacion.organizacion.lines',
        string='Facilitador/a',
        compute='_compute_familia',
        store=True,
        ondelete='restrict'
    )
    tipo_establecimiento = fields.Selection(
        selection=[
            ('puesto_salud', 'PUESTO DE SALUD'),
            ('hospital_essalud', 'HOSPITAL ESSALUD'),
            ('ubap_essalud', 'UBAP ESSALUD')
        ],
        string='Tipo de establecimiento de salud',
        required=True
    )
    nombre_establecimiento = fields.Char(
        string='Nombre de establecimiento según juridiscción',
        required=True
    )
    fecha_nac = fields.Date(
        string='Fecha de Nacimiento',
        compute='_compute_familia',
        store=True
    )
    fecha_ingreso_pncm = fields.Date(
        string='Fecha de Ingreso al PNCM',
        compute='_compute_familia',
        store=True
    )
    seguro_id = fields.Many2one(
        comodel_name='tipo.seguro',
        string='Tipo de Seguro de salud',
        ondelete='restrict',
        compute='_compute_familia',
        store=True
    )
    discapacidad_id = fields.Many2many(
        comodel_name='tipo.discapacidad',
        string='Tipo de Discapacidad',
        ondelete='restrict',
        compute='_compute_familia',
        store=True
    )
    peso_nacer = fields.Float(
        string='Peso al nacer(en kilogramos)'
    )
    talla_nacer = fields.Float(
        string=u'Talla al nacer(en centímetros)'
    )
    fecha_ini_micro = fields.Date(
        string='Fecha de Inicio Consumo de Micronutrientes(Previo al ingreso)'
    )
    nro_meses_mcn = fields.Integer(
        string='Nro. Meses Consumo Micronutrientes(Previo al ingreso)'
    )
    alergia = fields.Boolean(
        string=u'Sufre alguna alergía(Medicamentos/Alimentos)'
    )
    clase_alergia = fields.Char(
        string='Que alergias tiene?'
    )
    enfermedad_nac = fields.Boolean(
        string='Sufre alguna enfermedad de Nacimiento'
    )
    clase_enfermedad = fields.Char(
        string='Que enfermedades tiene?'
    )
    control_cred_ids = fields.One2many(
        comodel_name='control.cred',
        inverse_name='ficha_salud_id',
        string='Controles CRED'
    )
    inmunizacion_ids = fields.One2many(
        comodel_name='inmunizacion.control',
        inverse_name='ficha_salud_id',
        string='Inmunizaciones'
    )
    tamizaje_ids = fields.One2many(
        comodel_name='tamizaje.anemia',
        inverse_name='ficha_salud_id',
        string='Tamizajes Anemia'
    )
    estado = fields.Selection(
        selection=[
            ('abierto', 'Abierto'),
            ('cerrado', 'Cerrado'),
        ],
        string='Estado',
        default='abierto',
    )
    visita_riesgo_ids = fields.One2many(
        comodel_name='visita.riesgo',
        inverse_name='ficha_salud_id',
        string='Visitas Riesgo',
    )
    valor_ant_cred = fields.Float(
        compute='_compute_valor_ant_cred',
        store=True,
        string='Valor anterior para control cred'
    )
    nro_visitas_riesgo = fields.Integer(
        compute='_compute_nro_visitas_riesgo',
        store=True,
    )

    @api.onchange('unidad_territorial_id_log')
    def _onchange_domain(self):
        if self.unidad_territorial_id_log:
            return {'domain': {'unidad_territorial_id': [('id', 'in', self.unidad_territorial_id_log.ids)]}}
        else:
            return {'domain': {'unidad_territorial_id': [('is_office', '=', True), ('estado', '=', 'activo')]}}

    @api.multi
    @api.depends('control_cred_ids', 'control_cred_ids.habilitar_visita_riesgo')
    def _compute_nro_visitas_riesgo(self):
        for rec in self:
            x = 0
            if rec.visita_riesgo_ids:
                x = len(rec.control_cred_ids.filtered(lambda x: x.habilitar_visita_riesgo))
            rec.nro_visitas_riesgo = x

    @api.multi
    @api.depends
    def _compute_valor_ant_cred(self):
        for rec in self:
            if rec.control_cred_ids:
                x = len(rec.control_cred_ids)
                rec.valor_ant_cred = x
            else:
                rec.valor_ant_cred = 0

    @api.onchange('alergia')
    def onchange_clase_alergia(self):
        self.clase_alergia = False

    @api.onchange('enfermedad_nac')
    def onchange_clase_enfermedad(self):
        self.clase_enfermedad = False

    @api.multi
    def unlink(self):
        for rec in self:
            if rec.estado != 'abierto':
                raise ValidationError('Solo se puede eliminar si el registro se encuentra en estado abierto y '
                                      'no tenga controles, inmunizaciones, tamizajes relacionadas.')
        res = super(FichaSalud, self).unlink()
        return res

    @api.multi
    @api.depends('fecha_visita')
    def _compute_anio_mes(self):
        for rec in self:
            if rec.fecha_visita:
                fecha_visita = datetime.strptime(rec.fecha_visita, '%Y-%m-%d').date()
                mes = str(fecha_visita.month)
                if len(mes) == 1:
                    mes = '0' + mes
                rec.mes = mes
                rec.anio = self.env['tabla.anios'].search([
                    ('name', '=', fecha_visita.year)
                ])

    @api.multi
    @api.depends('ninio_id')
    def _compute_familia(self):
        for rec in self:
            if rec.ninio_id:
                rec.fecha_nac = rec.ninio_id.integrante_id.birthdate
                rec.fecha_ingreso_pncm = rec.ninio_id.integrante_id.fecha_ingreso_pncm
                rec.familia_id = rec.ninio_id.familia_id
                rec.cod_familia = rec.ninio_id.familia_id.id
                rec.facilitador_id = rec.ninio_id.familia_id.facilitador_id
                rec.seguro_id = rec.ninio_id.integrante_id.seguro_id
                rec.discapacidad_id = rec.ninio_id.integrante_id.discapacidad_id

    @api.multi
    def accion_agregar_tamizaje_anemia(self):
        if len(self.tamizaje_ids) == 5:
            raise ValidationError('El máximo de tamizajes es de 5 registros.')
        form_id = self.env.ref('atencion_integral.form_view_tamizaje_anemia').id
        context = {
            'default_ninio_id': self.ninio_id.id,
            'default_fecha_nac': self.fecha_nac,
            'default_ficha_salud_id': self.id
        }
        return {
            'name': 'Tamizaje',
            'type': 'ir.actions.act_window',
            'res_model': 'tamizaje.anemia',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            'views': [(form_id, 'form')],
            'view_id': form_id,
            'context': context
        }

    @api.multi
    def accion_agregar_control_cred(self):
        form_id = self.env.ref('atencion_integral.form_view_control_cred_create').id
        riesgo = False if self.control_cred_ids else True
        do_ant = peso_ant = talla_do_ant = talla_ant = 0
        if self.control_cred_ids:
            x = len(self.control_cred_ids)
            do_ant = self.control_cred_ids[x - 1].do
            peso_ant = self.control_cred_ids[x - 1].peso_kg
            talla_do_ant = self.control_cred_ids[x - 1].talla_do
            talla_ant = self.control_cred_ids[x - 1].talla_cm
        context = {
            'default_ninio_id': self.ninio_id.id,
            'default_ficha_salud_id': self.id,
            'default_calcular_riesgo': riesgo,
            'default_do_ant': do_ant,
            'default_peso_ant': peso_ant,
            'default_talla_do_ant': talla_do_ant,
            'default_talla_ant': talla_ant,
        }
        return {
            'name': 'Control CRED',
            'type': 'ir.actions.act_window',
            'res_model': 'control.cred',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            'views': [(form_id, 'form')],
            'view_id': form_id,
            'context': context
        }

    @api.multi
    def accion_agregar_visita_riesgo(self):
        form_id = self.env.ref('atencion_integral.form_view_visita_riesgo_create').id
        if self.nro_visitas_riesgo != len(self.visita_riesgo_ids):
            nro_visita = 1
            if self.visita_riesgo_ids:
                nro_visita = len(self.visita_riesgo_ids) + 1
            context = {
                'default_ninio_id': self.ninio_id.id,
                'default_ficha_salud_id': self.id,
                'default_nro_visita': nro_visita
            }
            return {
                'name': 'Visita de Riesgo',
                'type': 'ir.actions.act_window',
                'res_model': 'visita.riesgo',
                'view_type': 'form',
                'view_mode': 'form',
                'target': 'new',
                'views': [(form_id, 'form')],
                'view_id': form_id,
                'context': context
            }
        else:
            raise ValidationError('Debe verificar que necesita una visita de riesgo mediante el control CRED.')

    @api.multi
    def accion_agregar_vacuna(self):
        form_id = self.env.ref('atencion_integral.form_view_inmunizacion_control_create').id
        context = {
            'default_ninio_id': self.ninio_id.id,
            'default_ficha_salud_id': self.id
        }
        return {
            'name': 'Vacunas',
            'type': 'ir.actions.act_window',
            'res_model': 'inmunizacion.control',
            'view_type': 'form',
            'view_mode': 'form',
            'target': 'new',
            'views': [(form_id, 'form')],
            'view_id': form_id,
            'context': context
        }

    @api.multi
    def filtra_saf_vista(self):
        context = {'default_servicio': 'saf'}
        action = {
            'name': 'Ficha Salud SAF',
            'type': "ir.actions.act_window",
            'res_model': 'ficha.salud',
            'view_type': "form",
            'view_mode': "tree,form,pivot,graph",
            'context': context
        }
        user = self.env['res.users'].browse(self._uid)
        if user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', 'in', %s), ('servicio', '=', 'saf')]" % \
                                   user.unidad_territorial_id.ids
        else:
            action['domain'] = "[('servicio', '=', 'saf')]"
        return action

    @api.multi
    def filtra_scd_vista(self):
        context = {'default_servicio': 'scd'}
        action = {
            'name': 'Ficha Salud SCD',
            'type': "ir.actions.act_window",
            'res_model': 'ficha.salud',
            'view_type': "form",
            'view_mode': "tree,form,pivot,graph",
            'context': context
        }
        user = self.env['res.users'].browse(self._uid)
        if user.unidad_territorial_id:
            action['domain'] = "[('unidad_territorial_id', 'in', %s), ('servicio', '=', 'scd')]" % \
                               user.unidad_territorial_id.ids
        else:
            action['domain'] = "[('servicio', '=', 'scd')]"
        return action


class ControlCRED(models.Model):

    _name = 'control.cred'
    _inherit = 'tabla.base'

    ficha_salud_id = fields.Many2one(
        comodel_name='ficha.salud',
        inverse_name='control_cred_ids',
        string='Ficha Salud'
    )
    ninio_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        string=u'Niño',
        readonly=True
    )
    fecha_nac = fields.Date(
        string='Fecha Nacimiento',
        compute='_compute_fecha_nac',
        store=True
    )
    edad_control = fields.Selection(
        selection=[
            ('1', '1 mes'),
            ('2', '2 meses'),
            ('3', '3 meses'),
            ('4', '4 meses'),
            ('5', '5 meses'),
            ('6', '6 meses'),
            ('7', '7 meses'),
            ('8', '8 meses'),
            ('9', '9 meses'),
            ('10', '10 meses'),
            ('11', '11 meses'),
            ('12', u'1 año'),
            ('14', u'1 año 2 meses'),
            ('16', u'1 año 4 meses'),
            ('18', u'1 año 6 meses'),
            ('20', u'1 año 8 meses'),
            ('22', u'1 año 10 meses'),
            ('24', u'2 años'),
            ('27', u'2 años 3 meses'),
            ('30', u'2 años 6 meses'),
            ('33', u'2 años 9 meses'),
            ('36', u'3 años'),
        ],
        string='Edad de control',
        required=True
    )
    fecha_limite_inicial = fields.Date(
        string=u'Fecha Límite Inicial',
        compute='_compute_fecha_limite_inicio',
        store=True
    )
    fecha_limite_final = fields.Date(
        string=u'Fecha Límite Final',
        compute='_compute_fecha_limite_final',
        store=True
    )
    fecha_control = fields.Date(
        string='Fecha Control',
        required=True
    )
    peso_kg = fields.Float(
        string='Peso(kg)',
        required=True
    )
    talla_cm = fields.Float(
        string='Talla(cm)',
        required=True
    )
    talla_edad = fields.Char(
        string='Talla/Edad',
        compute='_compute_calculo_oms',
        store=True
    )
    peso_talla = fields.Char(
        string='Peso/Talla',
        compute='_compute_calculo_oms',
        store=True
    )
    peso_edad = fields.Char(
        string='Peso/Edad',
        compute='_compute_calculo_oms',
        store=True
    )
    riesgo_talla = fields.Boolean(
        string='Riesgo Talla',
        compute='_compute_calculo_oms',
        store=True
    )
    riesgo_peso = fields.Boolean(
        string='Riesgo Peso',
        compute='_compute_calculo_oms',
        store=True
    )
    riesgo_diarrea = fields.Boolean(
        string='Riego Diarrea'
    )
    examen_descarte = fields.Boolean(
        string=u'¿Realizo Exámen de descarte?'
    )
    tiene_parasitosis = fields.Boolean(
        string='¿Tiene parasitosis?'
    )
    recibe_tratamiento = fields.Boolean(
        string='¿Recibe tratamiento?'
    )
    calcular_riesgo = fields.Boolean(
        string='Calcular riesgo'
    )
    habilitar_visita_riesgo = fields.Boolean(
        string='Visita riesgo'
    )
    do = fields.Float(
        compute='_compute_calculo_oms',
        store=True
    )
    talla_do = fields.Float(
        compute='_compute_calculo_oms',
        store=True
    )
    do_ant = fields.Float()
    peso_ant = fields.Float()
    talla_do_ant = fields.Float()
    talla_ant = fields.Float()

    @api.multi
    @api.depends('peso_kg', 'talla_cm', 'edad_control', 'do_ant', 'peso_ant', 'ninio_id', 'calcular_riesgo')
    def _compute_calculo_oms(self):
        for rec in self:
            genero = 'h' if rec.ninio_id.integrante_id.gender == 'm' else 'm'
            peso_oms = self.env['tipo.oms'].search([
                ('tipo', '=', 'omspe'),
                ('sexo', '=', genero),
                ('valor', '=', rec.ninio_id.integrante_id.dias)
            ], limit=1)
            if peso_oms:
                if peso_oms.dpos2 >= rec.peso_kg >= peso_oms.dneg2:
                    rec.peso_edad = 'NORMAL'
                elif rec.peso_kg < peso_oms.dneg2:
                    rec.peso_edad = 'DESNUTRICION'
                elif rec.peso_kg > peso_oms.dpos2:
                    rec.peso_edad = 'SOBREPESO'
                rec.do = peso_oms.do
                if rec.calcular_riesgo and rec.do_ant > 0:
                    a = (peso_oms.do - rec.do_ant) / 2
                    b = rec.peso_kg - rec.peso_ant
                    if b < a:
                        rec.habilitar_visita_riesgo = True
                        rec.riesgo_peso = True

            talla_oms = self.env['tipo.oms'].search([
                ('tipo', '=', 'omste'),
                ('sexo', '=', genero),
                ('valor', '=', rec.ninio_id.integrante_id.dias)
            ])
            if talla_oms:
                ds = talla_oms.do - talla_oms.dneg1
                zte = (rec.talla_cm - talla_oms.do) / ds
                if zte >= -2 and zte <= 2:
                    rec.talla_edad = 'NORMAL'
                elif zte >= -5 and zte <= -2:
                    rec.talla_edad = 'TALLA BAJA'
                elif zte >= 2 and zte <= 5:
                    rec.talla_edad = 'ALTO'
                rec.talla_do = talla_oms.do
                if rec.calcular_riesgo and rec.talla_do_ant > 0:
                    a = (talla_oms.do - rec.talla_do_ant) / 2
                    b = rec.talla_cm - rec.talla_ant
                    if b < a:
                        rec.habilitar_visita_riesgo = True
                        rec.riesgo_talla = True
            if rec.peso_edad != 'NORMAL' and rec.talla_edad == 'TALLA BAJA':
                rec.peso_talla = 'DESNUTRICIÓN GLOBAL'
            elif rec.peso_edad == 'SOBREPESO':
                rec.peso_talla = 'SOBREPESO'
            elif rec.peso_edad and rec.talla_edad:
                rec.peso_talla = 'NORMAL'

    @api.depends('examen_descarte')
    def _onchange_examen_descarte(self):
        self.tiene_parasitosis = self.recibe_tratamiento = False

    @api.depends('tiene_parasitosis')
    def _onchange_tiene_parasitosis(self):
        self.recibe_tratamiento = False

    @api.onchange('fecha_control', 'fecha_limite_final', 'fecha_limite_inicial')
    def _onchange_fecha_control(self):
        if self.fecha_control:
            if self.fecha_limite_final < self.fecha_control:
                self.fecha_control = self.fecha_limite_final
            if self.fecha_limite_inicial > self.fecha_control:
                self.fecha_control = self.fecha_limite_inicial

    @api.depends('fecha_nac', 'edad_control')
    def _compute_fecha_limite_inicio(self):
        if self.edad_control and self.fecha_nac:
            fecha_aux = datetime.strptime(self.fecha_nac, DEFAULT_SERVER_DATE_FORMAT)
            self.fecha_limite_inicial = fecha_aux + relativedelta(months=int(self.edad_control)) - \
                relativedelta(days=1)

    @api.depends('fecha_limite_inicial')
    def _compute_fecha_limite_final(self):
        if self.fecha_limite_inicial:
            fecha_aux = datetime.strptime(self.fecha_limite_inicial, DEFAULT_SERVER_DATE_FORMAT)
            self.fecha_limite_final = fecha_aux + relativedelta(months=1) - relativedelta(days=1)

    @api.onchange('examen_descarte', 'tiene_parasitosis')
    def _onchange_parasitosis(self):
        if not self.examen_descarte:
            self.tiene_parasitosis = self.recibe_tratamiento = False
        if not self.tiene_parasitosis:
            self.recibe_tratamiento = False

    @api.depends('ninio_id')
    def _compute_fecha_nac(self):
        if self.ninio_id:
            self.fecha_nac = self.ninio_id.integrante_id.birthdate

    @api.constrains('talla_edad', 'peso_talla', 'peso_edad')
    def _constraints_calculos_oms(self):
        if not self.talla_edad or not self.peso_talla or not self.peso_edad:
            raise ValidationError('No se puede guardar porque no se ha podido validar con la Tabla OMS el peso '
                                  'y la talla.')

    @api.multi
    def create_record(self):
        self.ensure_one()


class TipoOMS(models.Model):
    _name = 'tipo.oms'
    _inherit = 'tabla.base'

    tipo = fields.Selection(
        selection=[
            ('omspe', 'OMSPE'),
            ('omste', 'OMSTE'),
        ],
        string='Tipo'
    )
    sexo = fields.Selection(
        selection=[
            ('h', 'H'),
            ('m', 'M'),
        ],
        string='Sexo'
    )
    valor = fields.Integer(
        string='Valor'
    )
    dneg4 = fields.Float(
        string='DNEG4',
        digits=(16, 3)
    )
    dneg3 = fields.Float(
        string='DNEG3',
        digits=(16, 3)
    )
    dneg2 = fields.Float(
        string='DNEG2',
        digits=(16, 3)
    )
    dneg1 = fields.Float(
        string='DNEG1',
        digits=(16, 3)
    )
    do = fields.Float(
        string='DO',
        digits=(16, 3)
    )
    dpos1 = fields.Float(
        string='DPOS1',
        digits=(16, 3)
    )
    dpos2 = fields.Float(
        string='DPOS2',
        digits=(16, 3)
    )
    dpos3 = fields.Float(
        string='DPOS3',
        digits=(16, 3)
    )
    dpos4 = fields.Float(
        string='DPOS4',
        digits=(16, 3)
    )
    valor_tipo_l = fields.Float(
        string='VALOR_TIPO_L',
        digits=(16, 3)
    )
    valor_tipo_m = fields.Float(
        string='VALOR_TIPO_M',
        digits=(16, 3)
    )
    valor_tipo_s = fields.Float(
        string='VALOR_TIPO_S',
        digits=(16, 3)
    )


class VacunaInmunizacion(models.Model):

    _name = 'vacuna.inmunizacion'
    _inherit = 'tabla.base'

    name = fields.Selection(
        selection=[
            ('bcg', 'BCG'),
            ('hvb', 'HVB'),
            ('apo', 'REFUERZO APO'),
            ('pentavalente', 'PENTAVALENTE'),
            ('neumococo', 'NEUMOCOCO'),
            ('influenza', 'INFLUENZA'),
            ('rotavirus', 'ROTAVIRUS'),
            ('spr', 'SPR'),
            ('antiamarilica', u'ANTIAMARÍLICA'),
            ('dpt', u'REFUERZO DPT'),
            ('ipv', u'IPV/APO'),
        ],
        string='Vacunas',
        required=True
    )
    periodo = fields.Selection(
        selection=[
            ('1_mes', '1 mes'),
            ('2_mes', '2 mes'),
            ('4_mes', '4 mes'),
            ('6_mes', '6 mes'),
            ('7_mes', '7 mes'),
            ('8_mes', '8 mes'),
            ('12_mes', '12 mes'),
            ('15_mes', '15 mes'),
            ('18_mes', '18 mes'),
            ('24_36_mes', '24-36 mes')
        ],
        string='Meses',
        required=True
    )
    nombre_periodo = fields.Char(
        compute='_compute_nombre_periodo',
        store=True
    )

    @api.multi
    @api.depends('periodo')
    def _compute_nombre_periodo(self):
        for rec in self:
            rec.nombre_periodo = str(dict(rec._fields['periodo'].selection).get(rec.periodo))

    @api.multi
    def name_get(self):
        return [(obj.id, u'{}'.format(obj.nombre_periodo)) for obj in self]


class Inmunizaciones(models.Model):

    _name = 'inmunizacion.control'
    _inherit = 'tabla.base'

    ficha_salud_id = fields.Many2one(
        comodel_name='ficha.salud',
        inverse_name='inmunizacion_ids',
        string='Ficha Salud'
    )
    ninio_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        string=u'Niño',
        domain="[('es_ninio','=', True),"
               "('comite_gestion_id', '=', comite_gestion_id),"
               "('services', '=', 'saf'),"
               "('fecha_fin', '=', False)]",
        readonly=True
    )
    vacunas = fields.Selection(
        selection=[
            ('bcg', 'BCG'),
            ('hvb', 'HVB'),
            ('apo', 'REFUERZO APO'),
            ('pentavalente', 'PENTAVALENTE'),
            ('neumococo', 'NEUMOCOCO'),
            ('influenza', 'INFLUENZA'),
            ('rotavirus', 'ROTAVIRUS'),
            ('spr', 'SPR'),
            ('antiamarilica', u'ANTIAMARÍLICA'),
            ('dpt', u'REFUERZO DPT'),
            ('ipv', u'IPV/APO'),
        ],
        string='Vacunas',
        required=True
    )
    periodo = fields.Many2one(
        comodel_name='vacuna.inmunizacion',
        string='Meses',
        required=True,
        domain="[('name', '=', vacunas)]",
    )
    fecha_vacuna = fields.Date(
        string='Fecha',
        required=True
    )

    @api.onchange('vacunas')
    def _onchange_vacunas(self):
        self.periodo = False
        filtro = []
        if self.vacunas:
            for rec in self.ficha_salud_id.inmunizacion_ids:
                if rec.vacunas == self.vacunas and rec.periodo:
                    filtro.append(rec.periodo.id)
            if filtro:
                return {'domain': {'periodo': [('name', '=', self.vacunas), ('id', 'not in', filtro)]}}
            else:
                return {'domain': {'periodo': [('name', '=', self.vacunas)]}}
        else:
            return {'domain': {'periodo': [('name', '=', self.vacunas)]}}

    @api.multi
    def create_record(self):
        self.ensure_one()


class TamizajeAnemia(models.Model):

    _name = 'tamizaje.anemia'
    _inherit = 'tabla.base'

    ficha_salud_id = fields.Many2one(
        comodel_name='ficha.salud',
        inverse_name='tamizaje_ids',
        string='Ficha Salud'
    )
    ninio_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        string=u'Niño',
        readonly=True
    )
    fecha_nac = fields.Date(
        string='Fecha Nacimiento',
        readonly=True
    )
    fecha_examen = fields.Date(
        string=u'Fecha Exámen',
        required=True,
        default=fields.Date.today()
    )
    resultado = fields.Float(
        string='Resultado q/dL',
        required=True
    )
    edad_examen = fields.Integer(
        string=u'Edad Exámen',
        compute='_compute_edad_examen',
        store=True
    )
    diagnostico = fields.Selection(
        selection=[
            ('A', 'Anemia'),
            ('N', 'Normal'),
            ('AS', 'Anemia Severa'),
            ('AM', 'Anemia Moderada'),
            ('AL', 'Anemia Leve'),
        ],
        string='Diagnostico',
        readonly=True
    )
    es_control = fields.Boolean(
        string='Es tamizaje de control?'
    )
    tamizaje_control_ids = fields.Many2many(
        comodel_name='tamizaje.anemia.control',
        string='Tamizaje Anemia Control'
    )
    fue_validado = fields.Boolean(
        string='Fue validado?'
    )

    @api.onchange('fecha_nac', 'fecha_examen')
    def _onchange_fechas(self):
        if self.fecha_nac and self.fecha_examen:
            if self.fecha_nac > self.fecha_examen:
                self.fecha_examen = False

    @api.onchange('resultado')
    def _onchange_resultado(self):
        if self.resultado and self.resultado > 20:
            self.resultado = 20

    @api.multi
    @api.depends('fecha_nac', 'fecha_examen')
    def _compute_edad_examen(self):
        for rec in self:
            if rec.fecha_nac and rec.fecha_examen:
                fecha_examen = fields.Datetime.from_string(str(rec.fecha_examen))
                fecha_nac = fields.Datetime.from_string(str(rec.fecha_nac))
                delta = relativedelta(fecha_examen, fecha_nac)
                years = int(delta.years)
                month = int(delta.months)
                months = years * 12 + month
                rec.edad_examen =  months

    @api.multi
    def create_record(self):
        self.ensure_one()

    @api.multi
    def delete_record(self):
        self.unlink()

    @api.onchange('edad_examen')
    def _onchange_edad_examen(self):
        if self.edad_examen > 36:
            self.edad_examen = 36
        elif self.edad_examen < 0:
            self.edad_examen = 0

    @api.onchange('resultado')
    def _onchange_resultado(self):
        if self.resultado > 20:
            self.resultado = 20
        elif self.resultado < 0:
            self.resultado = 0

    @api.multi
    def validar_diagnostico(self):
        self.diagnostico = obtener_diagnostico_tamizaje(self.edad_examen, self.resultado)
        self.fue_validado = True
        return {
            "type": "ir.actions.do_nothing",
        }


class TamizajeAnemiaControl(models.Model):

    _name = 'tamizaje.anemia.control'
    _inherit = 'tamizaje.anemia'

    diagnostico = fields.Selection(
        selection=[
            ('A', 'Anemia'),
            ('N', 'Normal'),
            ('AS', 'Anemia Severa'),
            ('AM', 'Anemia Moderada'),
            ('AL', 'Anemia Leve'),
        ],
        string='Diagnostico',
        compute='_compute_diagnostico',
        store=True
    )
    tamizaje_control_ids = fields.Char(
        string='Tamizaje'
    )
    ficha_salud_id = fields.Char(
        string='Ficha Salud'
    )

    @api.multi
    @api.depends('edad_examen', 'resultado')
    def _compute_diagnostico(self):
        for rec in self:
            rec.diagnostico = obtener_diagnostico_tamizaje(rec.edad_examen, rec.resultado)

    @api.model
    def create(self, values):
        diagnostico = obtener_diagnostico_tamizaje(values['edad_examen'], values['resultado'])
        values['diagnostico'] = diagnostico
        return super(TamizajeAnemiaControl, self).create(values)


class VisitaRiesgo(models.Model):

    _name = 'visita.riesgo'
    _inherit = 'tabla.base'

    ficha_salud_id = fields.Many2one(
        comodel_name='ficha.salud',
        inverse_name='visita_riesgo_ids',
        string='Ficha Salud'
    )
    ninio_id = fields.Many2one(
        comodel_name='integrante.unidad.familiar',
        string=u'Niño',
        readonly=True
    )
    nro_visita = fields.Integer(
        string='N° Visita',
        readonly=True
    )
    fecha_visita = fields.Date(
        string='Fecha Visita',
        required=True,
        default=fields.Date.today()
    )

    @api.multi
    def create_record(self):
        self.ensure_one()


class ComiteGestion(models.Model):

    _inherit = 'pncm.comitegestion'

    fichas_rec_ids = fields.One2many(
        comodel_name='ficha.reconocimiento',
        inverse_name='comite_gestion_id',
        string='Reconocimiento'
    )
    fichas_fort_ids = fields.One2many(
        comodel_name='ficha.fortalecimiento',
        inverse_name='comite_gestion_id',
        string='Fortalecimiento'
    )
    ficha_gestante_ids = fields.One2many(
        comodel_name='ficha.reconocimiento',
        inverse_name='comite_gestion_id',
        string='Ficha gestante'
    )
