odoo.define('pncm_infra.file_size', function (require) {
   "use strict";
    var pncm = {
        validityAccepExtension: function(input, arrayExtensions){
            input.acceptExtension = arrayExtensions;
            input.supper_file_change = input.on_file_change;
            input.on_file_change = function(obj, evt){
                var filePath = $(obj.target).val();
                var indexLast = filePath.lastIndexOf('.');
                var extension = filePath.substring(indexLast + 1, filePath.length)
                .trim();
                if(_.contains(input.acceptExtension, extension)){
                    input.supper_file_change(obj, evt);
                }
                else{
                    alert('Sole puede elegir archivos con las extensiones: '
                    + input.acceptExtension.toString());
                }
            }
        }
    }

    var core = require('web.core');
    var FieldBinaryFile = core.form_widget_registry.get ('binary');

    FieldBinaryFile.include ({
        init: function (field_manager, node) {
            var me = this;
            me._super (field_manager, node);
            me.max_upload_size =  2 * 1024 * 1024; // 2Mb
            if(me.name == 'foto'){
               me.max_upload_size =  4 * 1024 * 1024; // 4Mb
               pncm.validityAccepExtension(me, ['jpg','jpeg','png']);
            }
        }
    });
});