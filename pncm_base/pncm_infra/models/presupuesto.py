# -*- coding: utf-8 -*-
from openerp import fields, models, api
from openerp.exceptions import ValidationError
from odoo.addons.pncm_comite_gestion.models import utils


class CMPresupuesto(models.Model):
    _name = 'pncm.infra.local.presupuesto'

    _varopts_tiptrans = [
        ('acondicionamiento', u'Acondicionamiento'),
        ('prefabricado', u'Pre-fabricado'),
        ('mantenimiento', u'Mantenimiento'),
        ('atencionporemergencia', u'Atención por emergencia'),
        ('mantenimientomenor', u'Mantenimiento Menor')
    ]

    _varopts_fuente_finan = [
        ('externa', u'Fuente externa'),
        ('interna', u'Fuente interna')
    ]

    _varopts_exp = [
        ('con', u'Con ET'),
        ('sin', u'Sin ET')
    ]

    tipo_inversion = fields.Selection(
        selection=[
            ('con_inversion', u'Con Inversión'),
            ('sin_inversion', u'Sin Inversión')
        ],
        required=True,
        default='con_inversion',
        string=u'Tipo de Inversión'
    )
    state = fields.Selection(
        selection=[
            ('presupuesto', u'Presupuesto'),
            ('ejecucion', u'Ejecución'),
            ('justificacion', u'Justificación'),
            ('cerrado', u'Cerrado')
        ],
        required=True,
        default='presupuesto',
        string=u'Estado'
    )
    state_local = fields.Char(
        store=False,
        default=lambda self: self._get_state_local()
    )
    count_presupuesto = fields.Integer(
        store=False,
        default=lambda self: self._get_count_presupuesto()
    )
    tipo_transferencia = fields.Selection(
        selection=_varopts_tiptrans,
        string=u'Tipo de Transferencia',
        required=True,
        default=lambda self: self._tipo_de_transferencia_default(),
        store=True
    )
    fuente_financiamiento = fields.Selection(
        selection=_varopts_fuente_finan,
        string=u'Fuente de financiamiento',
        required=True
    )
    expediente = fields.Selection(
        selection=_varopts_exp,
        string=u'Expediente',
        store=True
    )
    monto = fields.Float(string=u'Monto Total', required=True)
    local_id = fields.Many2one(
        'pncm.infra.local',
        string=u'Local',
        required=True
    )

    # Ejecución: campos de resolución locador
    nro_rde_locador = fields.Char(
        string=u'N° RDE',
        size=250
    )
    fecha_rde_locador = fields.Date(string=u'Fecha de RDE')
    fecha_transferencia_locador = fields.Date(
        string=u'Fecha de transferencia a CG'
    )
    monto_locador = fields.Float(
        string=u'Monto RDE'
    )
    inicio_contrato_locador = fields.Date(
        string=u'Fecha de inicio de contrato'
    )
    fin_contrato_locador = fields.Date(
        string=u'Fecha de fin de contrato'
    )
    tiene_bosquejo_proyecto = fields.Boolean(
        string=u'Bosquejo del proyecto'
    )
    fecha_recepcion_proyecto = fields.Date(
        string=u'Fecha de recepción del proyecto aprobado'
    )

    # Ejecucion del proyecto
    nro_rde_ejecucion = fields.Char(
        string=u'N° RDE',
        size=250
    )
    fecha_rde_ejecucion = fields.Date(string=u'Fecha de RDE')
    fecha_transferencia_ejecucion = fields.Date(
        string=u'Fecha de transferencia a CG'
    )
    monto_ejecucion = fields.Float(
        string=u'Monto RDE'
    )
    modalidad_contratacion = fields.Selection(
        selection=[
            ('directa', u'Directa'),
            ('contrata', u'Contrata')
        ],
        string=u'Modalidad de contratación'
    )
    fecha_acta_obra = fields.Date(
        string=u'Fecha de acta de inicio de obra'
    )
    fecha_culmina_obra = fields.Date(
        string=u'Fecha de columinación de obra'
    )
    estado_ejecucion = fields.Selection(
        selection=[
            ('sin_ejecutar', u'Sin ejecutar'),
            ('ejecucion', u'En ejecución'),
            ('ejecutado', u'Ejecutado')
        ],
        string=u'Modalidad de contratación'
    )
    fecha_recepcion_obra = fields.Date(
        string=u'Fecha de recepción de obra'
    )

    # Justificacion
    esta_justificado = fields.Boolean(
        string=u'¿Está justificado?'
    )
    nro_hoja_ruta = fields.Char(
        string=u'N° Hoja de ruta',
        size=250
    )
    fecha_hoja_ruta = fields.Date(
        string=u'Fecha de hoja de ruta'
    )
    fecha_recepcion_justificacion = fields.Date(
        string=u'Fecha de recepción del expediente '
               u'remitido por el especialista'
    )
    esta_observado = fields.Boolean(
        string=u'¿Está observado?'
    )
    monto_rendido = fields.Float(
        string=u'Monto rendido ejecutado'
    )
    monto_no_ejecutado = fields.Float(
        string=u'Monto no ejecutado',
        store=True,
        compute='_calcular_saldo',
        readonly=True
    )
    esta_conforme = fields.Boolean(
        string=u'¿Está conforme?'
    )

    _sql_constraints = [
        ('code_unique',
         'unique(codepresupuesto)',
         u'El código generado ya existe')
    ]

    @api.model
    def create(self, values):
        tipo_transferencia = values.get('tipo_transferencia')
        if tipo_transferencia == 'prefabricado':
            values['expediente'] = 'con'
        elif tipo_transferencia == 'mantenimientomenor':
            values['expediente'] = 'sin'

        state_local = values.get('state_local')
        if state_local == 'saneamiento':
            values['tipo_transferencia'] = 'acondicionamiento'
        return super(CMPresupuesto, self).create(values)

    @api.model
    def write(self, values):
        ctx = dict(self._context or {})
        state_local = values.get('state_local')
        if state_local == 'saneamiento' and \
                ('tipo_transferencia' in values):
            values['tipo_transferencia'] = 'acondicionamiento'

        if state_local == 'ejecucion' \
            and ('nro_rde_locador' in values) \
                and values['nro_rde_locador']:
            values['nro_rde_locador'] = values['nro_rde_locador'].upper()

        if state_local == 'ejecucion' \
            and ('nro_rde_ejecucion' in values) \
                and values['nro_rde_ejecucion']:
            values['nro_rde_ejecucion'] = values['nro_rde_ejecucion'].upper()
        return super(CMPresupuesto, self.with_context(ctx)).write(values)

    @api.onchange('tipo_transferencia')
    def _fn_requiere_expediente(self):
        self.expediente = False
        if self.tipo_transferencia == 'prefabricado':
            self.expediente = 'con'
            domain = "[('con', u'Con ET')]"
        elif self.tipo_transferencia == 'mantenimientomenor':
            self.expediente = 'sin'
            domain = "[('sin', u'Sin ET')]"
        else:
            domain = "[('con', u'Con ET'),('sin', u'Sin ET')]"
        return {'domain': {'expediente': domain}}

    @api.constrains('monto')
    def _contraint_monto(self):
        return self._validar_monto()

    @api.constrains(
        'monto_locador',
        'monto_ejecucion'
    )
    def _contraint_montos_ejecucion(self):
        return self._validar_suma_de_montos_ejecucion()

    def _validar_monto(self):
        if self.monto <= 0:
            raise ValidationError('El monto del presupuesto debe ser mayor a '
                                  'cero')
            return False
        return True

    def _validar_suma_de_montos_ejecucion(self):
        result = True
        if self.state == 'ejecucion' and self.expediente == 'con':
            if (self.monto_locador + self.monto_ejecucion) == 0:
                raise ValidationError('La suma de montos de locador '
                                      'y ejecución debe ser mayor que cero')
                result = False
            elif (self.monto_locador + self.monto_ejecucion) > self.monto:
                raise ValidationError('La suma de montos de locador '
                                      'y ejecución debe ser menor o igual al '
                                      'monto del presupuesto')
                result = False
        elif self.state == 'ejecucion' and self.expediente == 'sin':
            if self.monto_ejecucion == 0:
                raise ValidationError('El monto de ejecución '
                                      'debe ser mayor que cero')
                result = False
            elif self.monto_ejecucion > self.monto:
                raise ValidationError('El monto de ejecución '
                                      'debe ser menor o igual al monto del '
                                      'presupuesto')
                result = False
        return result

    @api.constrains(
        'fecha_rde_locador',
        'fecha_transferencia_locador',
        'inicio_contrato_locador',
        'fin_contrato_locador',
        'fecha_recepcion_proyecto'
    )
    def _constraint_fechas_locador(self):
        return self._validar_fechas_locador()

    def _validar_fechas_locador(self):
        dhelper = utils.DateHelper()
        result = dhelper. \
            _validar_fecha_mayor(self.fecha_rde_locador,
                                 self.inicio_contrato_locador,
                                 'En los presupuestos la fecha de inicio '
                                 'de contrato debe ser mayor o '
                                 'igual a la fecha '
                                 'de la RDE del locador')

        if result:
            result = dhelper. \
                _validar_fecha_mayor(self.fecha_rde_locador,
                                     self.fecha_transferencia_locador,
                                     'En los presupuestos la fecha de '
                                     'transferencia al CG '
                                     'debe ser mayor o '
                                     'igual a la fecha de la RDE del locador')

        if result:
            result = dhelper. \
                _validar_fecha_mayor(self.inicio_contrato_locador,
                                     self.fin_contrato_locador,
                                     'En los presupuestos la fecha de fin '
                                     'de contrato debe ser mayor a la fecha '
                                     'de inicio de contrato del locador')
        if result:
            result = dhelper. \
                _validar_fecha_mayor(self.inicio_contrato_locador,
                                     self.fecha_recepcion_proyecto,
                                     'En los presupuestos la fecha de '
                                     'recepción del proyecto debe ser mayor '
                                     'a la fecha de inicio de contrato del '
                                     'locador')
        return result

    @api.constrains(
        'fecha_rde_ejecucion',
        'fecha_transferencia_ejecucion',
        'fecha_acta_obra',
        'fecha_culmina_obra',
        'fecha_recepcion_obra'
    )
    def _constrain_fechas_ejecucion(self):
        return self._validar_fechas_ejecucion()

    def _validar_fechas_ejecucion(self):
        dhelper = utils.DateHelper()
        result = dhelper.\
            _validar_fecha_mayor(self.fecha_rde_ejecucion,
                                 self.fecha_acta_obra,
                                 'En los presupuestos la fecha de inicio '
                                 'de acta de obra debe ser mayor o '
                                 'igual a la fecha '
                                 'de la RDE de ejecución')

        if result:
            result = dhelper. \
                _validar_fecha_mayor(self.fecha_rde_ejecucion,
                                     self.fecha_transferencia_ejecucion,
                                     'En los presupuestos la fecha de '
                                     'transferencia al CG '
                                     'debe ser mayor o '
                                     'igual a la fecha '
                                     'de la RDE de ejecución')

        if result:
            result = dhelper. \
                _validar_fecha_mayor(self.fecha_acta_obra,
                                     self.fecha_culmina_obra,
                                     'En los presupuestos la fecha '
                                     'de culminación de obra ser mayor '
                                     'a la fecha '
                                     'de inicio de acta de obra')

        if result:
            result = dhelper. \
                _validar_fecha_mayor(self.fecha_culmina_obra,
                                     self.fecha_recepcion_obra,
                                     'En los presupuestos la fecha de '
                                     'recepción de obra debe ser mayor '
                                     'o igual '
                                     'a la fecha de culminación de obra')
        return result

    @api.constrains(
        'fecha_culmina_obra',
        'fecha_hoja_ruta',
        'fecha_recepcion_justificacion'
    )
    def _constrain_fechas_justificacion(self):
        return self._valida_fechas_justificacion()

    def _valida_fechas_justificacion(self):
        dhelper = utils.DateHelper()
        result = dhelper. \
            _validar_fecha_cierre(self.fecha_culmina_obra,
                                  self.fecha_hoja_ruta,
                                  'En la justificación la fecha '
                                  'de hoja de ruta '
                                  'debe ser mayor a la fecha '
                                  'de culminación de '
                                  'obra')

        if result:
            result = dhelper. \
                _validar_fecha_mayor(self.fecha_hoja_ruta,
                                     self.fecha_recepcion_justificacion,
                                     'En la justificación la fecha '
                                     'de recepción del expediente debe ser '
                                     'mayor o igual a la fecha de hoja '
                                     'de ruta')

        return result

    @api.constrains(
        'monto_rendido',
        'monto_locador',
        'monto_ejecucion'
    )
    def _constrain_monto_rendido(self):
        return self._validar_monto_rendido()

    def _validar_monto_rendido(self):
        result = True
        monto_ejecutado = 0
        if self.state == 'justificacion':
            if self.expediente == 'con':
                monto_ejecutado = self.monto_locador + self.monto_ejecucion
            elif self.expediente == 'sin':
                monto_ejecutado = self.monto_ejecucion

            if self.monto_rendido > monto_ejecutado:
                raise ValidationError('el monto rendido no debe ser mayor'
                                      'al monto ejecutado')
                result = False
        return result

    def _calcular_saldo(self):
        if self.state and self.state == 'justificacion':
            saldo = 0
            if self.monto_rendido >= 0:
                if self.expediente == 'con':
                    saldo = self.monto_ejecucion + self.monto_locador
                else:
                    saldo = self.monto_ejecucion

                saldo = saldo - self.monto_rendido
            return saldo
        return False

    @api.onchange('monto_rendido')
    def _al_cambiar_monto_rendido(self):
        self.monto_no_ejecutado = self._calcular_saldo()

    @api.multi
    def ejecutar(self):
        if self._validar_monto():
            self.update(dict(state='ejecucion'))

    @api.multi
    def justificar(self):
        if self._validar_suma_de_montos_ejecucion() \
                and self._validar_fechas_ejecucion():
            self.update(dict(state='justificacion'))

    @api.multi
    def cerrar(self):
        if self._valida_fechas_justificacion() and \
                self._validar_monto_rendido():
            self.monto_no_ejecutado = self._calcular_saldo()
            self.update(dict(state='cerrado'))

    def _tipo_de_transferencia_default(self):
        current_state = self._context.get('state')
        count_presupuesto = self._context.get('count_presupuestos')
        if current_state == 'saneamiento' and count_presupuesto == 0:
            return 'acondicionamiento'
        return False

    def _get_state_local(self):
        return self._context.get('state')

    def _get_count_presupuesto(self):
        return self._context.get('count_presupuestos')
