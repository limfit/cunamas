# -*- coding: utf-8 -*-
from openerp import fields, models, api
from datetime import datetime
from odoo.addons.pncm_comite_gestion.models import utils


class CMConveniosSustentatorios(models.Model):
    _name = 'pncm.infra.local.convenios_sustentatorio'
    _options_state = [
        ('inactivo', 'Inactivo'),
        ('activo', 'Activo')
    ]

    fecha_inicio = fields.Date(
        string=u'Fecha Inicio',
        required=True
    )
    fecha_fin = fields.Date(
        string=u'Fecha Termino',
        required=True
    )
    state = fields.Selection(
        selection=_options_state,
        required=True,
        string=u'Estado'
    )
    archivo = fields.Binary(string=u'Adjunto', required=True)
    name = fields.Char(string=u'Convenio adjunto')
    local_id = fields.Many2one('pncm.infra.local', 'Local', required=True)

    @api.onchange('fecha_fin')
    @api.depends('fecha_fin')
    def _compute_state(self):
        if self.fecha_fin:
            dhelper = utils.DateHelper()
            today = datetime.now().date()
            fecha_fin = dhelper.parse_to_date(self.fecha_fin)
            if fecha_fin > today:
                self.state = u'activo'
            else:
                self.state = u'inactivo'


class CMActasAfectacion(models.Model):
    _name = 'pncm.infra.local.actas_afectacion'
    _options_state = [
        ('inactivo', 'Inactivo'),
        ('activo', 'Activo')
    ]

    fecha_inicio = fields.Date(
        string=u'Fecha Inicio',
        required=True
    )
    fecha_fin = fields.Date(
        string=u'Fecha Termino',
        required=True
    )
    fecha_caducidad_declaracion = fields.Date(string=u'Caducidad DDJJ')
    state = fields.Selection(
        selection=_options_state,
        required=True,
        string=u'Estado'
    )
    archivo_acta = fields.Binary(string=u'Adjunto acta', required=True)
    nombre_archivo_acta = fields.Char(string=u'Acta adjunta')
    archivo_declaracion = fields.Binary(
        string=u'Adjunto declaración',
        required=True
    )
    nombre_archivo_declaracion = fields.Char(string=u'Declaración adjunta')
    local_id = fields.Many2one('pncm.infra.local', 'Local', required=True)

    @api.onchange('fecha_fin')
    @api.depends('fecha_fin')
    def _compute_state(self):
        if self.fecha_fin:
            dhelper = utils.DateHelper()
            today = datetime.now().date()
            fecha_fin = dhelper.parse_to_date(self.fecha_fin)
            if fecha_fin > today:
                self.state = u'activo'
            else:
                self.state = u'inactivo'
