# Odoo - Toponimos Perú

Ubigeo Departamentos, Provincias, Distrito según INEI
Odoo 10.


# Ejemplo res.country.lugar


```python
class ResPartner(models.Model):
    _inherit = 'res.country.lugar'
```

```xml
<group>
    <field name="departamento_id" widget="selection"/>
    <field name="provincia_id" widget="selection"/>
    <field name="distrito_id" widget="selection"/>
</group>
```
