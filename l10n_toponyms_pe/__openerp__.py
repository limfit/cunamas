# -*- coding: utf-8 -*-
# vim:expandtab:smartindent:tabstop=4:softtabstop=4:shiftwidth=4:
{
    'name': 'Toponimos de Peru',
    'version': '0.0.1',
    'author': '',
    'category': 'Localisation/America',
    'summary': 'Ubigeo Departamentos, Provincias y distritos del Peru según INEI.',
    'description': """
Localizacion Peruana.
====================================

Clientes y Proveedores:
--------------------------------------------
    * Tabla de Ubigeos - Según INEI 2016
    * Departamentos, provincias y distritos de todo el Perú

    """,
    'website': '',
    'depends': [],
    'data': [
        'views/res_country_view.xml',
        'views/res_partner_view.xml',
        'data/res.country.state.csv',
        'data/patch1/res.country.state.csv',
        'data/patch2/res.country.state.csv',
        'data/patch3/res.country.state.csv',
        'security/ir.model.access.csv',
    ],
    'qweb': [],
    'demo': [],
    'test': [],
    'images': [],
    'installable': True,
    'auto_install': False,
    'application': False,
    "sequence": 1,
}


